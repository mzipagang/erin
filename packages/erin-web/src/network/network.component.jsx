import React, { Component } from 'react';
import { FooterComponent } from './footer/footer.component';
import { nodes } from './network.data';
import { edges } from './network.data';
import * as d3 from 'd3';

const width = 1000;
const height = 600;

let node = null;
let showNames = true;
let showTitles = true;
let filter = [];

const getFilteredTree = () => {
  let selNodes = nodes.filter(isSelected);
  let selLinks = [];
  if(nodes.length === selNodes.length){
    return { selNodes: nodes, selLinks: edges };
  }
  let nodeIds = selNodes.map(n => n.id);
  let attachedLinks = [...edges.filter(e => nodeIds.includes(e.target.id))];
  while(attachedLinks.length > 0){
    const attachedNodes = {};
    // eslint-disable-next-line
    attachedLinks.forEach(l => {
      if(!attachedNodes[l.source.id] && !nodeIds.includes(l.source.id)){
        selNodes.unshift(l.source);
        attachedNodes[l.source.id] = l.source;
      }
      selLinks.push(l);
    });
    nodeIds = Object.values(attachedNodes).map(n => n.id);
    // eslint-disable-next-line
    attachedLinks = edges.filter(e => nodeIds.includes(e.target.id));
  }
  return { selNodes, selLinks };
}

const getTextWidth = (text, fontSize, fontFace) => {
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  context.font = (fontSize || 14) + 'px ' + fontFace;
  return context.measureText(text).width;
}

const getCardLen = (data) => data.id && data.title ? 
  Math.max(getTextWidth(data.id, data.isHighlighted ? 20.5: 17),
  getTextWidth(data.title, data.isHighlighted ? 16.5 : 13),
  getTextWidth('Request a Referral', data.isHighlighted ? 19 : 0)) : 0;

const isSelected = d => 
  filter.length === 0 ||
  (d.title && filter.some(f => d.title.toLowerCase().includes(f))) ||
  (d.id && filter.some(f => d.id.toLowerCase().includes(f)));

const isMatched = d => d.id && d.isMatched;

const isReferred = d => d.id && d.referralStatus === 'referred';

const showContactName = d => d.id &&
  (d.referralStatus === 'accepted' ||
   d.referralStatus === 'interviewing' ||
   d.referralStatus === 'hired' ||
   d.referralStatus === 'not hired');

const buildCircle = (node) => {
  node
    .selectAll('circle')
    .remove();

  node
    .append('circle')
    .attr('r', d => d.radius)
    .attr('stroke-width', '2px')
    .attr('fill', d => d.fill)
    .attr('r', d => d.isHighlighted ? d.radius * 1.5 : d.radius)
    .attr('stroke', d => d.isDirect ? '#62a7ff' : d.isBusinessConnection ? '#19bb4b' : 'gray')
    .filter(d => isMatched(d))
      .attr('stroke', 'orange');
}

const buildImage = (node) => {
  node
    .selectAll('image')
    .remove();

  node
    .append('image')
    .attr('xlink:href', d => d.image)
    .attr('height', d => d.isHighlighted ? d.height * 1.5 : d.height)
    .attr('width', d => d.isHighlighted ? d.width * 1.5 : d.width)
    .attr('x', d => d.isHighlighted ? -30 : -20)
    .attr('y', d => d.isHighlighted ? -30 : -20); 
}

const redrawNodes = (node) => {
  node
    .selectAll('circle')
    .remove();
  buildCircle(node);

  node
    .selectAll('image')
    .remove();
  buildImage(node);

  node
    .selectAll('rect')
    .remove();

  node
    .selectAll('text')
    .remove();
  buildTitleCard(node);

  node
    .selectAll('text.actions')
    .remove();
}

const buildTitleCard = (node) => {
  node
    .selectAll('rect')
    .remove();

  if(showNames || showTitles){
    node
      .append('rect')
      .attr('fill', d => isMatched(d) ? '#bdf9bd' : '#e9e9e9')
      .attr('x', -45)
      .attr('y', d => d.isHighlighted ? 41 : 28)
      .attr('rx', 10)
      .attr('ry', 10)
      .attr('width', d => getCardLen(d))
      .attr('height', d => showNames && showContactName(d) && showTitles ? 41 : showNames && showContactName(d) ? 26 : 28);
  }

  node
    .selectAll('text')
    .remove();

  if(showNames){
    node
      .filter(d => showContactName(d))
      .append('text')
      .attr('dx', -40)
      .attr('y', d => d.isHighlighted ? 60 : 46)
      .attr('font-size', d => d.isHighlighted ? '17px' : '14px')
      .attr('fill', d => isMatched(d) ? '#1d861d' : '#333')
      .text((d) => {
        return d.id;
      });
  }

  if(showTitles){
    node
      .append('text')
      .attr('dx', -40)
      .attr('dy', d => d.isHighlighted ? (showNames && showContactName(d) ? 75 : 60) : (showNames && showContactName(d) ? 60 : 46))
      .attr('font-size', d => d.isHighlighted ? '14px' : '11px')
      .attr('fill', 'gray')
      .text(d => d.title);
  }
}

const buildActionCard = (node, data) => {
  node.raise();
  d3.selectAll('a.actions')
    .remove();

  if(data.id && data.isHighlighted) {
    node.selectAll('rect')
      .attr('height', d => showNames && showContactName(d) && showTitles ? 86 : showNames && showContactName(d) ? 70 : 72)
      .attr('width', d => getCardLen(d))
      .attr('stroke', '#c1c1c1')
      .filter(d => isMatched(d))
        .attr('stroke', '#19bb4b');

    node
      .append('a')
      .attr('xlink:href', d => isReferred(d) ? '#' : '/referrals')
      .attr('class', 'actions')
      .append('text')
      .attr('dx', -40)
      .attr('dy', d => showNames && showContactName(d) && showTitles ? 98 : !showNames && !showTitles ? 53 : 84)
      .attr('font-size', '14px')
      .attr('font-weight', 'bold')
      .attr('fill', '#242e3f')
      .text(d => isReferred(d) ? 'Referred' : 'Request a Referral');

    node
      .append('a')
      .attr('class', 'actions')
      .attr('xlink:href', '/contacts')
      .append('text')
      .attr('dx', -40)
      .attr('dy', d => showNames && showContactName(d) && showTitles ? 118 : !showNames && !showTitles ? 73 : 104)
      .attr('font-size', '14px')
      .attr('font-weight', 'bold')
      .attr('fill', '#242e3f')
      .text('Save Contact');
  }
}

const buildCirclesAndImages = (node) => {
  redrawNodes(node);

  node.on('click', function(d) {
    node.data().forEach((data) => {
      if(d.id && d.id === data.id){
        data.isHighlighted = !data.isHighlighted;
      } else {
        data.isHighlighted = false;
      }
    });

    redrawNodes(node);
    buildActionCard(d3.select(this), d);
  });
}

class NetworkComponent extends Component {
  onSearch = (value) => {
    filter = value;
    const { selNodes, selLinks } = getFilteredTree();
    this.setupGraph(selNodes, selLinks);
  }

  toggleNames = (checked) => {
    showNames = checked;
    buildTitleCard(node);
  }

  toggleTitles = (checked) => {
    showTitles = checked;
    buildTitleCard(node);
  }

  setupGraph = (nodes, edges) => {
    d3.select("#network").select("svg").remove();

    const svg = d3
      .select('#network')
      .append('svg')
      .attr('width', width)
      .attr('height', height);

    const simulation = d3
      .forceSimulation()
      .force('link', d3.forceLink().distance(100))
      .force('charge', d3.forceManyBody().strength(-50 * nodes.length))
      .force('center', d3.forceCenter(width / 2, height / 2.75));

    const links = svg
      .selectAll('graph')
      .data(edges)
      .enter()
      .append('line')
      .style('stroke', '#ccc')
      .style('stroke-width', 1);

    node = svg
      .selectAll('graph')
      .data(nodes)
      .enter()
      .append('g');

    buildCirclesAndImages(node);
    buildTitleCard(node);

    simulation.nodes(nodes);
    simulation.force('link').links(edges);
    simulation.alpha(2).restart();

    simulation.on('tick', function() {
      links
        .attr('x1', function(d) {
          return d.source.x;
        })
        .attr('y1', function(d) {
          return d.source.y;
        })
        .attr('x2', function(d) {
          return d.target.x;
        })
        .attr('y2', function(d) {
          return d.target.y;
        });

     node.attr('transform', d => 'translate(' + d.x + ',' + d.y + ')');
    });
  }

  componentDidMount() {      
    this.setupGraph(nodes, edges);
  }
  render() {
    return (
      <div>
        <div
          style={{
            letterSpacing: 2,
            fontSize: 20,
            fontWeight: 600,
            margin: '30px 57px',
          }}
        >
          BESTCO'S NETWORK
        </div>
        <div style={{ marginTop: '-20px' }}>
          <svg id='network' width={width} height={height} />
        </div>
        <div>
          <FooterComponent searchTerms={filter} onSearch={this.onSearch} toggleTitles={this.toggleTitles} toggleNames={this.toggleNames} />
        </div>
      </div>
    );
  }
}

export default NetworkComponent;