import { css } from 'emotion';

import { COLORS } from '../../_shared/styles/colors';

export const buttonActive = css({
  borderColor: COLORS.dashboardBlue,
  background: COLORS.dashboardBlue,
  width: '10px',
  height: '10px',
  borderRadius: '35%',
  marginLeft: '20px',
});

export const buttonBusiness = css({
  borderColor: COLORS.dashboardGreen,
  background: COLORS.dashboardGreen,
  width: '10px',
  height: '10px',
  borderRadius: '35%',
  marginLeft: '20px',
});

export const buttonFirst = css({
  borderColor: COLORS.dashboardLightOrange,
  background: COLORS.dashboardLightOrange,
  width: '10px',
  height: '10px',
  borderRadius: '35%',
  marginLeft: '20px',
});

export const bottomContainer = css({
  height: 60,
  marginTop: 15,
  marginBottom: 32,
  marginRight: 35,
  marginLeft: 25,
});

export const bottomSectionText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.heading,
  fontSize: '0.85em',
  marginLeft: 5,
  opacity: 0.7,
});

export const bottomSectionValue = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: COLORS.black,
  fontSize: '0.85em',
  float: 'right',
  marginRight: 21,
});

export const clearFilter = css({
  fontSize: '0.80em',
  marginLeft: 15,
  color: COLORS.blue,
});

export const inputStyles = css({
  '& .ant-input': {
    borderRadius: 20,
    marginTop: 5,
    marginLeft: 15,
  },
  '& .ant-input-suffix': {
    bottom: 10,
  },
});

export const viewOptions = css({
  fontSize: '0.85em',
  fontWeight: 600,
  fontFamily: '"Open Sans",sans-serif',
  marginBottom: 10,
  '& .ant-switch': {
    height: 8,
  },
  '& .ant-switch-checked': {
    backgroundColor: COLORS.green,
  },
  '& .ant-switch:after': {
    top: '-5px',
    backgroundColor: COLORS.green,
  },
});
