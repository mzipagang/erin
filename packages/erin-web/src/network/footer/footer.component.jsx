import React from 'react';
import { Row, Col, Switch, Select } from 'antd';
import {
  bottomContainer,
  bottomSectionText,
  bottomSectionValue,
  buttonActive,
  buttonBusiness,
  clearFilter,
  inputStyles,
  viewOptions,
} from './footer.styles';

export class FooterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerms: this.props.searchTerms || [],
    };
  }

  clearFilter = () => {
    this.props.onSearch([]);
    this.setState({ searchTerms: [] });
  }

  removeFilter = (value) => {
    if(value){
      const newTerms = [...this.state.searchTerms.filter(t => t !== value.toLowerCase())];
      this.setState({ searchTerms: newTerms });
      this.props.onSearch(newTerms);
    }
  }

  onSearch = (value) => {
    const newTerms = [...this.state.searchTerms.concat([value.toLowerCase()])];
    this.props.onSearch(newTerms);
    this.setState({ searchTerms: newTerms });
  }

  render() {
    const Search = Select;
    const { searchTerms } = this.state;
    return (
      <div style={{ width: 1200 }}>
        <Row>
          <Col span={7}>
            <div style={{ marginLeft: 45, fontWeight: 700 }}>LEGEND</div>
            <Row className={bottomContainer}>
              <Row>
                <button className={buttonActive} />
                <span className={bottomSectionText}> Current Employees: </span>
                <span className={bottomSectionValue}> 105 </span>
              </Row>
              <Row>
                <button className={buttonBusiness} />
                <span className={bottomSectionText}>
                  {' '}
                  Business Connections:{' '}
                </span>
                <span className={bottomSectionValue}> 415 </span>
              </Row>
            </Row>
          </Col>
          <Col span={9}>
            <div style={{ fontWeight: 700 }}>
              PEOPLE FILTER
              <span className={clearFilter} onClick={this.clearFilter}>Clear Filter</span>
            </div>
            <div className={inputStyles}>
              <span>Add Tag:</span>
              <Search showSearch mode='tags' style={{ width: 200 }} value={searchTerms} onSelect={this.onSearch} onDeselect={this.removeFilter} />
            </div>
          </Col>
          <Col span={7}>
            <div style={{ marginBottom: 20, fontWeight: 700 }}>
              VIEW OPTIONS
            </div>
            <div className={viewOptions}>
              Display Names:{' '}
              <span style={{ marginLeft: 10 }}>
                <Switch defaultChecked={true} onChange={this.props.toggleNames} />
              </span>
            </div>
            <div className={viewOptions}>
              Display Titles:
              <span style={{ marginLeft: 23 }}>
                <Switch defaultChecked={true} onChange={this.props.toggleTitles} />
              </span>
            </div>
          </Col>
          <Col span={1} />
        </Row>
      </div>
    );
  }
}
