import React from 'react';
import { storiesOf } from '@storybook/react';
import NetworkComponent from './network.component';

storiesOf('Development/Network ', module).add('Main', () => (
  <NetworkComponent />
));

storiesOf('QA/Network', module).add('Main', () => <NetworkComponent />);
