export const nodes = [
  {
    image:
      'http://icons.iconarchive.com/icons/google/noto-emoji-travel-places/48/42488-office-building-icon.png',
    height: 40,
    width: 40,
    radius: 33,
    fill: 'navy',
  },
  {
    id: 'Courtney Smalls',
    title: 'Human Resources',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Female-Face-FB-2-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isBusinessConnection: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'Karen James',
    title: 'Director of Sales',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Female-Face-FD-5-dark-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isDirect: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'Meghan Phipps',
    title: 'Sales Representative',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Female-Face-FA-4-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isBusinessConnection: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'James Pilsner',
    title: 'Technical Support',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Male-Face-B3-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isBusinessConnection: true,
    isMatched: false,
  },
  {
    id: 'Steve Smith',
    title: 'Sales Representative',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Male-Face-G3-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isBusinessConnection: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'Adam Westernak',
    title: 'Software Developer',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Male-Face-D5-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isDirect: true,
    isMatched: false,
    referralStatus: 'referred',
  },
  {
    id: 'Josh Evaneleest',
    title: 'CEO',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Male-Face-F5-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isDirect: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'Amanda Beaker',
    title: 'Chief Marketing Officer',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Female-Face-FH-5-slim-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isBusinessConnection: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'Mark Duklett',
    title: 'VP of Sales',
    image:
      'http://icons.iconarchive.com/icons/hopstarter/face-avatars/48/Male-Face-H2-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isDirect: true,
    isMatched: false,
    referralStatus: 'hired',
  },
  {
    id: 'MATCH FOUND',
    title: 'Sales Manager',
    image:
      'http://icons.iconarchive.com/icons/graphicloads/flat-finance/48/person-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isMatched: true,
    referralStatus: 'hired',
  },
  {
    id: 'MATCH FOUND 2',
    title: 'VP of Sales',
    image:
      'http://icons.iconarchive.com/icons/graphicloads/flat-finance/48/person-icon.png',
    height: 40,
    width: 40,
    radius: 25,
    fill: 'white',
    isMatched: true,
    referralStatus: 'hired',
  },
];

export const edges = [
  {
    source: 0,
    target: 1,
  },
  {
    source: 0,
    target: 2,
  },
  {
    source: 0,
    target: 3,
  },
  {
    source: 0,
    target: 4,
  },
  {
    source: 0,
    target: 5,
  },
  {
    source: 0,
    target: 6,
  },
  {
    source: 0,
    target: 7,
  },
  {
    source: 0,
    target: 8,
  },
  {
    source: 0,
    target: 9,
  },
  {
    source: 2,
    target: 5,
  },
  {
    source: 2,
    target: 8,
  },
  {
    source: 5,
    target: 8,
  },
  {
    source: 3,
    target: 8,
  },
  {
    source: 4,
    target: 7,
  },
  {
    source: 4,
    target: 9,
  },
  {
    source: 7,
    target: 9,
  },
  {
    source: 1,
    target: 9,
  },
  {
    source: 4,
    target: 10,
  },
  {
    source: 3,
    target: 11,
  },
  {
    source: 6,
    target: 11,
  },
];
