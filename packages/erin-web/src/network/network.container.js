import { connect } from 'react-redux';
import NetworkComponent from './network.component';

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    contacts: currentUser.contacts,
  };
};

export const Network = connect(mapStateToProps)(NetworkComponent);
