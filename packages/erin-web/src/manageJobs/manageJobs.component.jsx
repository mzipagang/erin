// #region imports
import React from 'react';
import { List, Modal, Button, Row, Col, Icon, Select, Input, Spin } from 'antd';
import _ from 'lodash';
import {
  mainContainer,
  addJobsButton,
  modalTitleText,
  loadingImage,
  departmentFilterContainer,
  addJobsButtonText,
  addJobsButtonIcon,
  searchbarStyle,
  topContainer,
  gridTitle,
  cardListStyle,
  DropDown,
  SelectContainer,
  SelectStyles,
  spinContainer,
  middleContainer,
  noJobs,
} from './manageJobs.styles';
import { AddJob } from './addJobModal/addJob.component';
import loadingGif from '../_shared/assets/addingjob.gif';

import WebIcon from '../_shared/components/web-icon.component';
import { COLORS } from '../_shared/styles/colors';

import { requiredFields } from './errorHandlers';
import { JobCard } from './jobCard/jobCard.component';

import { ToggleOpenClosed } from './toggleOpenClosed/toggleOpenClosed.component';
import { JOB_STATUSES } from '../_shared/constants/job-statuses.enum';
// #endregion

const filterOpenKey = JOB_STATUSES.OPEN;
const filterClosedKey = JOB_STATUSES.CLOSED;

const Option = Select.Option;

export class ManageJobs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      visible: false,
      filteredDepartments: [],
      filteredJobs:
        props.currentUser.role === 'manager'
          ? props.jobs.filter(job => {
              const managerDepartments = props.currentUser.managedDepartments.map(
                department => department.departmentId
              );
              return managerDepartments.includes(job.departmentId);
            })
          : props.jobs,
      allJobs:
        props.currentUser.role === 'manager'
          ? props.jobs.filter(job => {
              const managerDepartments = this.props.currentUser.managedDepartments.map(
                department => department.departmentId
              );
              return managerDepartments.includes(job.departmentId);
            })
          : props.jobs,
      searchQuery: '',
      page: 0,
      errors: {},
      filterOpenStatus: filterOpenKey,
    };
    this.filterJobs = this.filterJobs.bind(this);
  }
  componentDidMount = () => {
    if (this.props.location.pathname === '/jobs/add') this.showModal();
  };

  // TODO: replace with other api
  UNSAFE_componentWillReceiveProps = nextProps => {
    this.setState({
      filteredJobs: nextProps.jobs,
      allJobs: nextProps.jobs,
    });
  };

  filterJobs = (property, match, jobSet) => {
    return jobSet.filter(job => job[property] === match);
  };

  searchJobs = (properties, query, jobSet) =>
    jobSet.filter(
      job =>
        !query ||
        properties.some(
          property =>
            job[property] &&
            job[property].toLowerCase().includes(query.toLowerCase())
        )
    );

  pageIsValid = (currentPage, currentJob) => {
    let isValid = true;
    const errors = {};
    requiredFields.forEach(({ key, message, page }) => {
      if (!currentJob[key] && page === currentPage) {
        errors[key] = message;
        isValid = false;
      }
    });

    this.setState({
      errors,
    });

    return isValid;
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleNext = (page, values) => {
    if (this.pageIsValid(this.state.page, values)) {
      this.setState(
        { page: this.state.page < 3 ? this.state.page + 1 : 3 },
        () => {
          this.props.updateAddJobForm(values);
        }
      );
    }
  };

  updateFilteredJobs = selectedDepartments => {
    let filtered = [];
    if (selectedDepartments.length < 1) {
      filtered = this.state.allJobs;
    } else {
      this.state.allJobs.forEach(job => {
        selectedDepartments.forEach(department => {
          if (job.department && job.department.name === department)
            filtered.push(job);
          return;
        });
      });
    }
    this.setState({
      filteredJobs: filtered,
    });
  };

  handleFilterDepartment = department => {
    const { filteredDepartments } = this.state;
    if (filteredDepartments.indexOf(department) > -1) return;
    const updated = [...filteredDepartments, department];
    this.updateFilteredJobs(updated);
    this.setState({
      filteredDepartments: updated,
    });
  };

  handleRemoveFilter = department => {
    const { filteredDepartments } = this.state;
    const index = filteredDepartments.indexOf(department);
    const filterDepartments = item => item !== filteredDepartments[index];
    const departments =
      filteredDepartments.length === 1
        ? []
        : filteredDepartments.filter(filterDepartments);

    this.updateFilteredJobs(departments);
    this.setState({
      filteredDepartments: departments,
    });
  };

  handleSubmit = () => {
    const { currentUser, currentJob, onAddJob } = this.props;
    const job = {
      companyId: currentUser.companyId,
      departmentId: currentJob.department.id,
      jobType: currentJob.jobType,
      title: currentJob.title,
      description: currentJob.description,
      publicLink: currentJob.publicLink,
      salary: JSON.stringify({
        from: currentJob.salary.from,
        to: currentJob.salary.to,
        interval: currentJob.salary.interval,
      }),
      location: JSON.stringify({
        city: currentJob.location.isRemote ? null : currentJob.location.city,
        state: currentJob.location.isRemote ? null : currentJob.location.state,
        isRemote: currentJob.location.isRemote,
      }),
      userId: currentJob.hiringManager ? currentJob.hiringManager.id : null,
      status: JOB_STATUSES.OPEN,
      referralBonus: JSON.stringify({
        hasBonus: !currentJob.referralBonus.hasBonus,
        amount: !currentJob.referralBonus.hasBonus
          ? currentJob.referralBonus.amount
          : null,
      }),
      notificationType: currentJob.notificationType
        ? currentJob.notificationType
        : 'ALL',
      shares: 0,
      views: 0,
    };

    onAddJob(job);

    this.setState(
      {
        isLoading: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            visible: false,
            isLoading: false,
          });
        }, 5600);
      }
    );
  };

  handleBack = () => {
    this.setState({ page: this.state.page > 0 ? this.state.page - 1 : 0 });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  showCancelScreen = () => {
    this.setState({ page: 4 });
  };

  handleOk = () => {
    this.setState({ page: 0 });
  };

  handleSelectJob = job => {
    this.props.setCurrentJob(job);
    this.props.history.push(`/jobs/${job.id}`);
  };

  resetForm = () => {
    this.props.resetFormFields();
    this.setState({
      page: 0,
    });
  };

  onChangePage = () => {
    // console.log('New Page Number: ', current, 'Page Size:', pageSize);
    // TODO: call real api/action
  };

  renderFilters = () => {
    const { filterOpenStatus } = this.state;
    return (
      <Row className={departmentFilterContainer}>
        <Col style={{ display: 'flex', maxWidth: 820, minWidth: 560 }}>
          <ToggleOpenClosed
            onClick={filterOpenStatus => {
              this.setState({
                filterOpenStatus,
              });
            }}
            filterOpenStatus={filterOpenStatus}
            openJobsKey={filterOpenKey}
            closedJobsKey={filterClosedKey}
          />
          <div className={SelectContainer}>
            <Select
              className={SelectStyles}
              mode="multiple"
              placeholder="Filter by Department"
              dropdownClassName={DropDown}
              onSelect={department => this.handleFilterDepartment(department)}
              onDeselect={department => this.handleRemoveFilter(department)}
            >
              {this.props.departments.map(department => {
                return (
                  <Option key={department.id} value={department.name}>
                    {department.name}
                  </Option>
                );
              })}
            </Select>
            <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
          </div>
        </Col>
      </Row>
    );
  };

  renderSearchBar = () => {
    return (
      <Input
        prefix={<Icon type="search" style={{ fontSize: '18px' }} />}
        className={searchbarStyle}
        placeholder="Search Jobs"
        value={this.state.searchQuery}
        onChange={({ target }) => {
          this.setState({
            searchQuery: target.value,
          });
        }}
      />
    );
  };

  render() {
    const {
      visible,
      page,
      errors,
      isLoading,
      filteredJobs,
      filterOpenStatus,
      searchQuery,
    } = this.state;
    const { currentJob, users, departments } = this.props;
    if (!filteredJobs) {
      return (
        <div className={spinContainer}>
          <Spin size="large" tip="...Loading" />
        </div>
      );
    }

    const searchedJobs = this.searchJobs(
      ['title', 'description'],
      searchQuery,
      filteredJobs
    );

    const openJobs = this.filterJobs('status', JOB_STATUSES.OPEN, searchedJobs);
    const closedJobs = this.filterJobs(
      'status',
      JOB_STATUSES.CLOSED,
      searchedJobs
    );

    const jobTypeTitle =
      filterOpenStatus === filterOpenKey ? 'Open Jobs' : 'Closed Jobs';
    const jobData = filterOpenStatus === filterOpenKey ? openJobs : closedJobs;
    const sortedJobData = _.sortBy(jobData, function(job) {
      return job.dateCreated;
    }).reverse();

    return (
      <main className={mainContainer}>
        <div className={topContainer}>
          <div style={{ minWidth: 130 }}>
            <Button
              shape="circle"
              onClick={this.showModal}
              className={addJobsButton}
            >
              <Icon className={addJobsButtonIcon} type="plus" />
            </Button>
            <span className={addJobsButtonText}> Add Job </span>
          </div>
          <div>{this.renderFilters()}</div>
          <div span={6} style={{ float: 'right' }}>
            {this.renderSearchBar()}
          </div>
        </div>

        <div className={middleContainer}>
          <span className={gridTitle}> {jobTypeTitle} </span>
          {jobData && jobData.length > 0 ? (
            <List
              grid={{ gutter: 10, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 }}
              pagination={{ pageSize: 15 }}
              dataSource={sortedJobData}
              className={cardListStyle}
              renderItem={job => (
                <List.Item>
                  <JobCard
                    key={job.id}
                    job={job}
                    onClick={() => {
                      this.handleSelectJob(job);
                    }}
                  />
                </List.Item>
              )}
            />
          ) : !jobData && jobTypeTitle === 'Open Jobs' ? (
            <p className={noJobs}>There are no open jobs.</p>
          ) : (
            <p className={noJobs}>There are no closed jobs.</p>
          )}
        </div>

        <Modal
          visible={visible}
          width={620}
          height={735}
          title={<span className={modalTitleText}> Create a New Job </span>}
          destroyOnClose={true}
          afterClose={this.resetForm.bind(this)}
          maskClosable={false}
          onOk={null}
          onCancel={this.showCancelScreen}
          footer={null}
        >
          {isLoading && (
            <div>
              <img className={loadingImage} src={loadingGif} alt="loading..." />
            </div>
          )}
          {!isLoading && (
            <AddJob
              isNew={true}
              handleNext={this.handleNext}
              handleBack={this.handleBack}
              page={page}
              managers={users.filter(user => user.role === 'manager')}
              departments={departments}
              currentJob={currentJob}
              handleSubmit={this.handleSubmit}
              handleOk={this.handleOk}
              handleCancel={this.handleCancel}
              errors={errors}
              resetFormFields={this.props.resetFormFields}
            />
          )}
        </Modal>
      </main>
    );
  }
}
