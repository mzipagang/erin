import { connect } from 'react-redux';
import { ManageJobs as ManageJobsComponent } from './manageJobs.component';
import { actions as AllActions } from 'erin-app-state-mgmt';
import { withListJobs } from 'erin-app-state-mgmt/src/_shared/api/components/jobs/with-list-jobs.provider';
import { withListUsersAndRole } from 'erin-app-state-mgmt/src/_shared/api/components/users/with-list-users-and-role.provider';
import { compose } from '../_shared/services/utils';

const {
  manageJobsActions: { actions },
} = AllActions;

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
    currentJob: state.manageJobs.currentJob,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateAddJobForm(values) {
      dispatch(actions.updateAddJobForm(values));
    },
    fetchJobs() {
      dispatch(actions.fetchJobs());
    },
    resetFormFields() {
      dispatch(actions.resetAddJobForm());
    },
    setCurrentJob(job) {
      dispatch(actions.setCurrentJob(job));
    },
  };
};

const ManageJobsWithApi = compose(
  withListJobs,
  withListUsersAndRole
)(ManageJobsComponent);

export const ManageJobs = connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageJobsWithApi);
