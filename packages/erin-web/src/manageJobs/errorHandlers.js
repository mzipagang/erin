export const requiredFields = [
  { key: 'jobType', message: 'Please select a Job Type', page: 0 },
  { key: 'title', message: 'Please enter a Title', page: 1 },
  { key: 'department', message: 'Please select a Department', page: 1 },
  {
    key: 'description',
    message: 'Please enter a Description',
    page: 1,
  },
];

export const miscFieldErrors = {
  key: 'salary',
  message: 'Max salary must be equal to or higher than minimum salary',
  page: 0,
};
