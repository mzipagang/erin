import React from 'react';
import { Row, Col, Icon } from 'antd';
import { pageSubtitles } from '../pages/copy';
import {
  progressIndicatorContainer,
  progressIndicatorLine,
  progressIndicatorDots,
  progressIndicatorDotCenter,
  checkMark,
  subTitle,
} from './progressIndicator.styles';

export const ProgressIndicator = props => {
  const { page } = props;
  return (
    <div className={progressIndicatorContainer}>
      <Row>
        <div className={progressIndicatorLine} />
        {[0, 1, 2, 3].map(i => {
          return (
            <Col key={i} span={6}>
              <div className={progressIndicatorDots} />
              <div className={progressIndicatorDotCenter}>
                {page === i && <Icon className={checkMark} type="check" />}
              </div>
            </Col>
          );
        })}
      </Row>

      <Row>
        <Col span={6}>
          <span className={subTitle(-5, page === 0)}> {pageSubtitles[0]} </span>
        </Col>
        <Col span={6}>
          <span className={subTitle(-16, page === 1)}>{pageSubtitles[1]}</span>
        </Col>
        <Col span={6}>
          <span className={subTitle(-18, page === 2)}>{pageSubtitles[2]}</span>
        </Col>
        <Col span={6}>
          <span className={subTitle(-7, page === 3)}> {pageSubtitles[3]} </span>
        </Col>
      </Row>
    </div>
  );
};
