import React from 'react';

import { storiesOf } from '@storybook/react';
import { ProgressIndicator } from './progressIndicator.component';

storiesOf('Development/Manage Jobs', module).add('Progress Indicator', () => (
  <ProgressIndicator key={0} page={0} />
));
