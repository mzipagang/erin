import { css } from 'emotion';
import { COLORS } from '../../../_shared/styles/colors';

export const progressIndicatorContainer = css({
  width: '75%',
  height: 40,
  marginLeft: '20%',
  paddingBottom: 45,
});

export const progressIndicatorLine = css({
  width: '75%',
  backgroundColor: COLORS.red,
  height: '5px',
  zIndex: 0,
  position: 'absolute',
  marginTop: 8,
  marginLeft: 2,
});

export const progressIndicatorDots = css({
  width: '22px',
  height: '22px',
  borderRadius: 11,
  backgroundColor: COLORS.red,
  position: 'absolute',
  zIndex: 1,
});

export const progressIndicatorDotCenter = css({
  width: '18px',
  height: '18px',
  borderRadius: 9,
  backgroundColor: COLORS.white,
  position: 'absolute',
  zIndex: 10,
  marginLeft: 2,
  marginTop: 2,
});

export const checkMark = css({
  width: 20,
  height: 20,
  color: COLORS.red,
  zIndex: 20,
  position: 'absolute',
  marginTop: 3,
  marginleft: -3,
});

export const subTitle = (left, isActive) =>
  css({
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 500,
    color: isActive ? COLORS.heading : COLORS.subHeading,
    fontSize: '0.75em',
    top: 25,
    position: 'absolute',
    left,
  });
