import { Button, Icon, Row } from 'antd';
import React from 'react';
import { nextButton, backButton, createButton } from './addJobFooter.styles';

export class AddJobsFooter extends React.Component {
  render() {
    const { handleNext, handleBack, handleSubmit, page, isNew } = this.props;
    return (
      <Row style={{ justifyContent: 'center', marginTop: 15 }}>
        {page !== 0 && (
          <Button key="back" className={backButton} onClick={handleBack}>
            <Icon type="left" />
            Back
          </Button>
        )}
        {page !== 3 && (
          <Button key="next" className={nextButton} onClick={handleNext}>
            Next
            <Icon type="right" />
          </Button>
        )}
        {page === 3 && (
          <Button key="submit" className={createButton} onClick={handleSubmit}>
            {isNew ? 'Create this Job!' : 'Edit this Job!'}
            <Icon type="check-circle" />
          </Button>
        )}
      </Row>
    );
  }
}
