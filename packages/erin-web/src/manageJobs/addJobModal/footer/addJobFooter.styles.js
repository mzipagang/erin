import { css } from 'emotion';
import { COLORS } from '../../../_shared/styles/colors';

export const nextButton = css({
  backgroundColor: COLORS.red,
  color: COLORS.white,
  '&:hover': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
  '&:focus': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: COLORS.red,
  },
  height: 40,
  fontSize: 18,
  fontWeight: 300,
  float: 'right',
});

export const backButton = css({
  borderColor: COLORS.white,
  color: COLORS.blue,
  '&:hover': {
    borderColor: COLORS.white,
    color: COLORS.blue,
  },
  '&:focus': {
    borderColor: COLORS.white,
    color: COLORS.blue,
  },
  float: 'left',
  height: 40,
  fontSize: 18,
  fontWeight: 300,
});

export const createButton = css({
  backgroundColor: COLORS.green,
  color: COLORS.white,
  '&:hover': {
    backgroundColor: COLORS.green,
    color: COLORS.white,
    borderColor: COLORS.green,
  },
  '&:focus': {
    backgroundColor: COLORS.green,
    color: COLORS.white,
    borderColor: COLORS.green,
  },
  width: 220,
  float: 'right',
  right: '31%',
  height: 45,
  fontSize: 20,
  fontWeight: 200,
});
