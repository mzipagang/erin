import React from 'react';

import { storiesOf } from '@storybook/react';
import { AddJobsFooter } from './addJobFooter.component';

storiesOf('Development/Manage Jobs', module)
  .add('Footer 1', () => <AddJobsFooter page={0} />)
  .add('Footer 2', () => <AddJobsFooter page={1} />)
  .add('Footer 3', () => <AddJobsFooter page={3} />);
