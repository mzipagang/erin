import React from 'react';
import { addJobMainContainer } from './addJob.styles';
import {
  AddJobPageOne,
  AddJobPageTwo,
  AddJobPageThree,
  AddJobPageFour,
  CancelAddJob,
  CancelEditJob,
} from './pages';
import { ProgressIndicator } from './progressIndicator/progressIndicator.component';

export class AddJob extends React.Component {
  render() {
    const {
      page,
      handleSubmit,
      currentJob,
      handleNext,
      handleBack,
      handleOk,
      handleCancel,
      errors,
      isNew,
      departments,
      managers,
    } = this.props;
    return (
      <div className={addJobMainContainer}>
        {page !== 4 && <ProgressIndicator page={page} />}
        <div>
          {/* "Page" numbers start at zero to utilize indecies in other areas */}
          {page === 0 && (
            <AddJobPageOne
              currentJob={currentJob}
              handleNext={handleNext}
              page={page}
              errors={errors}
            />
          )}

          {page === 1 && (
            <AddJobPageTwo
              currentJob={currentJob}
              handleNext={handleNext}
              handleBack={handleBack}
              departments={departments}
              page={page}
              errors={errors}
            />
          )}

          {page === 2 && (
            <AddJobPageThree
              currentJob={currentJob}
              handleNext={handleNext}
              handleBack={handleBack}
              managers={managers}
              page={page}
              errors={errors}
            />
          )}

          {page === 3 && (
            <AddJobPageFour
              isNew={isNew}
              currentJob={currentJob}
              handleSubmit={handleSubmit}
              handleBack={handleBack}
              page={page}
              errors={errors}
            />
          )}

          {page === 4 &&
            isNew && (
              <CancelAddJob handleOk={handleOk} handleCancel={handleCancel} />
            )}

          {page === 4 &&
            !isNew && (
              <CancelEditJob handleOk={handleOk} handleCancel={handleCancel} />
            )}
        </div>
      </div>
    );
  }
}
