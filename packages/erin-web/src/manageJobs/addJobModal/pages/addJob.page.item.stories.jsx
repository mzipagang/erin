import React from 'react';

import { storiesOf } from '@storybook/react';
import { PageItem } from './addJob.page.item.component';

storiesOf('Development/Manage Jobs', module).add('Page Item', () => (
  <PageItem title={'Title'} subtitle={'subtitle'} optional={true}>
    <div>
      <span> Child Content </span>
    </div>
  </PageItem>
));
