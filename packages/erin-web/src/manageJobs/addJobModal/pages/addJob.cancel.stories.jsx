import React from 'react';

import { storiesOf } from '@storybook/react';
import { CancelAddJob } from './addJob.cancel.component';

storiesOf('Development/Manage Jobs', module).add('Cancel Modal', () => (
  <CancelAddJob
    handleOk={() => {
      console.log('OK Pressed');
    }}
    handleCancel={() => {
      console.log('Cancel Pressed');
    }}
    optional={true}
  />
));
