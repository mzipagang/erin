import React from 'react';
import {
  titleStyle,
  subTitleStyle,
  itemContainer,
  itemChildren,
} from './pages.styles';

export class PageItem extends React.Component {
  render() {
    const { title, subtitle, children, optional } = this.props;
    return (
      <div className={itemContainer}>
        <span className={titleStyle}> {title} </span>
        {optional && <span className={subTitleStyle}> (optional) </span>}
        <br />
        {subtitle && <span className={subTitleStyle}> {subtitle} </span>}
        <div className={itemChildren}> {children} </div>
      </div>
    );
  }
}
