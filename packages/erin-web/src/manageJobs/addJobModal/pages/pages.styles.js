import { css } from 'emotion';

import { COLORS } from '../../../_shared/styles/colors';

export const jobTypeButton = selected =>
  css({
    backgroundColor: selected ? COLORS.red : COLORS.lightGray2,
    color: selected ? COLORS.white : COLORS.heading,
    height: 50,
    width: 175,
    borderRadius: 10,
    borderWidth: 1,
    fontSize: '1.3em',
    fontWeight: 300,
    margin: 3,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
  });

export const autocompleteDropdownContainer = css({
  position: 'absolute',
  width: 500,
  zIndex: 1000,
});

export const jobTypeButtonsContainer = css({
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
});

export const inputContainer = width =>
  css({
    '&:selected': {
      borderColor: COLORS.red,
    },
    '&:focus': {
      borderColor: COLORS.red,
    },
    '&:hover': {
      borderColor: COLORS.red,
    },
    width,
  });

export const dropdownButton = width =>
  css({
    marginLeft: 8,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.white,
      color: COLORS.red,
      borderColor: COLORS.red,
    },
    width,
  });

export const typeaheadRow = active => ({
  backgroundColor: active ? COLORS.red : COLORS.lightGray2,
});

export const regularButton = selected =>
  css({
    marginLeft: 8,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: selected ? 'transparent' : COLORS.subHeading,
    },
    '&:focus': {
      backgroundColor: selected ? COLORS.red : COLORS.white,
      color: selected ? COLORS.white : COLORS.subHeading,
      borderColor: selected ? 'transparent' : COLORS.subheading,
    },
  });

export const cancelModalButton = css({
  marginLeft: 8,
  backgroundColor: COLORS.white,
  color: COLORS.blue,
  borderColor: COLORS.blue,
  '&:hover': {
    backgroundColor: COLORS.white,
    color: COLORS.blue,
    border: `2px solid ${COLORS.blue}`,
  },
  '&:focus': {
    backgroundColor: COLORS.white,
    color: COLORS.blue,
    borderColor: COLORS.blue,
  },
});

export const stateAndRemoteContainer = css({
  marginLeft: 195,
  marginBottom: 20,
});

export const keepModalOpenButton = css({
  marginLeft: 8,
  backgroundColor: COLORS.blue,
  color: COLORS.white,
  borderColor: COLORS.blue,
  '&:hover': {
    backgroundColor: COLORS.blue,
    color: COLORS.white,
    border: `2px solid ${COLORS.darkBlue}`,
  },
  '&:focus': {
    backgroundColor: COLORS.blue,
    color: COLORS.white,
    borderColor: COLORS.blue,
  },
});

export const dropdownMenuItem = css({
  '&:hover': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
});

export const titleStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: '1.3em',
});

export const subTitleStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.subHeading,
  fontSize: '0.85em',
});

export const itemContainer = css({
  padding: 10,
});

export const cancelContainer = css({
  padding: 10,
  textAlign: 'center',
});

export const itemChildren = css({
  marginTop: 10,
});

export const RteContainer = css({
  height: 205,
  overflow: 'scroll',
});

export const sectionContainer = css({
  marginLeft: 30,
  marginBottom: 15,
});

export const summaryHeaderContainer = css({
  textAlign: 'center',
  marginBottom: 20,
  marginTop: 10,
});

export const summaryHeader = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: '1.6em',
});

export const summaryTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: '1.25em',
});

export const summaryValue = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.darkGray,
  fontSize: '1.25em',
});

export const bonusAmmount = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.green,
  fontSize: '1.25em',
});

export const fieldErrors = css({
  width: 'auto',
  marginLeft: '5%',
});

export const errorText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.red,
  fontSize: '0.85em',
  left: -30,
});
