import { Button, Row, Input, InputNumber, Select } from 'antd';
import React from 'react';
import { notificationTypes } from './copy';
import { inputContainer, regularButton } from './pages.styles';
import { PageItem } from './addJob.page.item.component';
import { AddJobsFooter } from '../footer/addJobFooter.component';

export class AddJobPageThree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      publicLink: '',
      hiringManager: '',
      notificationType: 'ALL',
      referralBonus: {
        hasBonus: true,
        amount: 1000,
      },
    };
  }

  componentDidMount() {
    const {
      publicLink,
      hiringManager,
      referralBonus = { active: true, amount: undefined },
      notificationType,
    } = this.props.currentJob;

    this.setState({
      publicLink,
      hiringManager,
      referralBonus,
      notificationType,
    });
  }

  footerButtons = page => {
    return (
      <AddJobsFooter
        page={page}
        handleNext={() => {
          this.props.handleNext(page, {
            ...this.props.currentJob,
            ...this.state,
          });
        }}
        handleBack={() => {
          this.props.handleBack(page);
        }}
      />
    );
  };

  onChangeJobPostingLink = e => {
    const { value } = e.target;
    this.setState({
      publicLink: value,
    });
  };

  onChangeHiringManager = value => {
    this.setState({
      hiringManager: {
        id: value.key,
        name: value.label.join(''),
      },
    });
  };

  onSearchManagers = value => {
    let autoCompleteResult = [];
    if (!value) {
      autoCompleteResult = [];
    } else {
      this.state.managers.forEach(record => {
        if (record.toLowerCase().includes(value.toLowerCase())) {
          autoCompleteResult.push(record);
        }
      });
    }
    this.setState({ autoCompleteResult });
  };

  onChangeReferralBonusAmount = value => {
    const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.setState({
        referralBonus: { ...this.state.referralBonus, amount: value },
      });
    }
  };

  onChangeNotificationType = notificationType => {
    this.setState({
      notificationType,
    });
  };

  renderJobPostingLink = () => {
    const { publicLink } = this.state;
    return (
      <Row>
        <PageItem title="Public Job Posting Link" optional={true}>
          <Input
            className={inputContainer('60%')}
            value={publicLink}
            onChange={this.onChangeJobPostingLink.bind(this)}
            placeholder="http://linkToYourJob.org/post?id=12345"
          />
        </PageItem>
      </Row>
    );
  };

  renderHiringManager = () => {
    const { managers } = this.props;
    const Option = Select.Option;
    let options = managers.map(manager => (
      <Option key={manager.id}>
        {manager.firstName} {manager.lastName}
      </Option>
    ));
    return (
      <PageItem
        title="Hiring Manager"
        subtitle="Add a hiring manager to be listed on the job description. Note they must have an account"
        optional={true}
      >
        <Select
          className={inputContainer('60%')}
          labelInValue={true}
          onSelect={value => this.onChangeHiringManager(value)}
          onSearch={this.onSearchManager}
          placeholder="Ex. James Smith"
        >
          {options}
        </Select>
      </PageItem>
    );
  };

  renderReferralBonus = () => {
    const { amount, hasBonus } = this.state.referralBonus;
    return (
      <PageItem title="Referral Bonus">
        <Row>
          <span> $ </span>
          <InputNumber
            className={inputContainer(150)}
            max={999999999}
            min={0}
            value={amount}
            formatter={value =>
              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
            }
            parser={value => value.replace(/$\s?|(,*)/g, '')}
            onChange={this.onChangeReferralBonusAmount.bind(this)}
            placeholder="Example $3,000"
            disabled={!hasBonus}
          />
          <span style={{ margin: 10, paddingTop: 5 }}> Or </span>
          <Button
            className={regularButton(!hasBonus)}
            onClick={() => {
              this.setState({
                referralBonus: {
                  ...this.state.referralBonus,
                  hasBonus: !this.state.referralBonus.hasBonus,
                },
              });
            }}
          >
            Do Not Pay a Bonus
          </Button>
        </Row>
      </PageItem>
    );
  };

  renderNotification = () => {
    const { notificationType } = this.state;
    return (
      <PageItem
        title="Notification"
        subtitle="Who should we notify that this job has been added?"
      >
        {Object.keys(notificationTypes).map(key => {
          return (
            <Button
              key={key}
              className={regularButton(key === notificationType)}
              onClick={this.onChangeNotificationType.bind(this, key)}
            >
              {notificationTypes[key]}
            </Button>
          );
        })}
      </PageItem>
    );
  };

  render() {
    const footer = this.footerButtons(this.props.page);
    return (
      <main>
        <div>
          {this.renderJobPostingLink()}
          {this.renderHiringManager()}
          {this.renderReferralBonus()}
          {this.renderNotification()}
          {footer}
        </div>
      </main>
    );
  }
}
