import {
  Button,
  Col,
  Row,
  Input,
  InputNumber,
  Menu,
  Dropdown,
  Icon,
  Select,
} from 'antd';
import PlacesAutocomplete from 'react-places-autocomplete';

import React from 'react';
import {
  jobTypeButton,
  jobTypeButtonsContainer,
  dropdownButton,
  regularButton,
  dropdownMenuItem,
  inputContainer,
  fieldErrors,
  errorText,
  typeaheadRow,
  stateAndRemoteContainer,
  autocompleteDropdownContainer,
} from './pages.styles';
import { JobTypes, SalaryTypes, USStates } from './copy';
import { PageItem } from './addJob.page.item.component';
import { AddJobsFooter } from '../footer/addJobFooter.component';

export class AddJobPageOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salary: {
        from: '',
        to: '',
        interval: 'yearly',
      },
      location: {
        city: '',
        state: '',
        isRemote: false,
      },
      jobType: '',
    };
  }

  componentDidMount() {
    const { salary, jobType, location } = this.props.currentJob;

    this.setState({
      salary,
      jobType,
      location,
    });
  }

  footerButtons = page => {
    return (
      <AddJobsFooter
        page={page}
        handleNext={() => {
          this.props.handleNext(page, {
            ...this.props.currentJob,
            ...this.state,
          });
        }}
        handleBack={() => {
          this.props.handleBack(page);
        }}
      />
    );
  };

  handleSelectState = value => {
    this.setState({
      location: {
        ...this.state.location,
        state: value,
      },
    });
  };

  onChangeSalaryMinimum = value => {
    const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.setState({
        salary: {
          to: this.state.salary.to,
          from: value,
          interval: this.state.salary.interval,
        },
      });
    }
  };

  onChangeSalaryMaximum = value => {
    const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.setState({
        salary: {
          from: this.state.salary.from,
          to: value,
          interval: this.state.salary.interval,
        },
      });
    }
  };

  onChangeJobCity = e => {
    const { value } = e.target;
    this.setState({
      location: { state: this.state.location.state, city: value },
    });
  };

  renderJobtype = () => {
    const { jobType } = this.state;
    const { errors } = this.props;
    return (
      <div className={jobTypeButtonsContainer}>
        <PageItem title="Job Type">
          <Row type="flex" justify="center">
            {Object.keys(JobTypes).map(key => {
              return (
                <div key={key} span={8}>
                  <Button
                    className={jobTypeButton(key === jobType)}
                    onClick={() => {
                      this.setState({
                        jobType: key,
                      });
                    }}
                  >
                    {JobTypes[key]}
                  </Button>
                </div>
              );
            })}
          </Row>
          {errors && errors.jobType ? (
            <div className={fieldErrors}>
              <span className={errorText}> {errors.jobType} </span>
            </div>
          ) : null}
        </PageItem>
      </div>
    );
  };

  renderSalaryRange = () => {
    const { from, to } = this.state.salary;
    return (
      <div>
        <PageItem title="Salary Range" optional={true}>
          <Row>
            <span> $ </span>
            <InputNumber
              className={inputContainer(150)}
              max={999999999}
              min={0}
              formatter={value =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
              }
              parser={value => value.replace(/$\s?|(,*)/g, '')}
              value={from}
              onChange={this.onChangeSalaryMinimum.bind(this)}
              placeholder="Example $65,000"
            />
            <span style={{ margin: 10, paddingTop: 5 }}> To </span>
            <span> $ </span>
            <InputNumber
              className={inputContainer(150)}
              max={999999999}
              min={0}
              placeholder="Example $65,000"
              value={to}
              formatter={value =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
              }
              parser={value => value.replace(/$\s?|(,*)/g, '')}
              onChange={this.onChangeSalaryMaximum.bind(this)}
            />
            <Dropdown
              trigger={['click']}
              overlay={
                <Menu>
                  {Object.keys(SalaryTypes).map(key => {
                    return (
                      <Menu.Item
                        key={key}
                        className={dropdownMenuItem}
                        onClick={() => {
                          this.setState({
                            salary: {
                              to: this.state.salary.to,
                              from: this.state.salary.from,
                              interval: key,
                            },
                          });
                        }}
                      >
                        {SalaryTypes[key]}
                      </Menu.Item>
                    );
                  })}
                </Menu>
              }
            >
              <Button className={dropdownButton()}>
                {SalaryTypes[this.state.salary.interval] ||
                  SalaryTypes['yearly']}{' '}
                <Icon type="down" />
              </Button>
            </Dropdown>
          </Row>
          {from && to && from > to ? (
            <div className={fieldErrors}>
              <span className={errorText}>
                Max salary must be greater than Min Salary
              </span>
            </div>
          ) : null}
        </PageItem>
      </div>
    );
  };

  handleChange = address => {
    this.setState({ location: { ...this.state.location, city: address } });
  };

  /**
   * Incoming address is formatted exactly as it
   * appears in the drop down.  eg: 'New York, NY, USA'
   * The last item, when split at ',', will be the country (USA)
   * The 2nd-to-last item will be the State
   * The first item is likely to be the entirety of the location,
   * but may include additional information.
   */
  handleSelect = address => {
    const _addressArray = address.split(',');
    const _city = _addressArray[0];
    const _state = _addressArray[_addressArray.length - 2];
    this.setState({
      location: { ...this.state.location, state: _state, city: _city },
    });
  };

  renderLocation = () => {
    const { city, isRemote } = this.state.location;
    const Option = Select.Option;
    let options = [];
    Object.keys(USStates).map(key =>
      options.push(<Option key={key}>{USStates[key]}</Option>)
    );
    return (
      <div>
        <PageItem title="Location" optional={true}>
          <Col>
            <div style={{ marginLeft: 0, marginBottom: -27 }}>
              <span> City </span>
            </div>
            <PlacesAutocomplete
              value={city}
              onChange={this.handleChange}
              onSelect={this.handleSelect}
            >
              {({
                getInputProps,
                suggestions,
                getSuggestionItemProps,
                loading,
              }) => (
                <div style={{ marginLeft: 28, marginBottom: -33 }}>
                  <Input
                    {...getInputProps({
                      placeholder: 'Example: New York ...',
                      className: inputContainer(150),
                    })}
                    disabled={!!isRemote}
                  />
                  <div className={autocompleteDropdownContainer}>
                    {loading && <div>Loading...</div>}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? 'suggestion-item--active'
                        : 'suggestion-item';
                      const style = typeaheadRow(suggestion.active);
                      return (
                        <div
                          key={suggestion}
                          {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                          })}
                        >
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </PlacesAutocomplete>
          </Col>
          <Col>
            <div className={stateAndRemoteContainer}>
              <span> State </span>
              <Select
                style={{ width: 100 }}
                disabled={!!isRemote}
                placeholder="Select"
                value={this.state.location.state || 'Select'}
                onSelect={value => this.handleSelectState(value)}
              >
                {options}
              </Select>
              <Button
                className={regularButton(isRemote)}
                onClick={() => {
                  // focus remains until you click off of it
                  this.setState({
                    location: {
                      ...this.state.location,
                      isRemote: !this.state.location.isRemote,
                    },
                  });
                }}
              >
                This Position is Remote
              </Button>
            </div>
          </Col>
        </PageItem>
      </div>
    );
  };

  render() {
    const footer = this.footerButtons(this.props.page);
    return (
      <div>
        {this.renderJobtype()}
        {this.renderSalaryRange()}
        {this.renderLocation()}
        {footer}
      </div>
    );
  }
}
