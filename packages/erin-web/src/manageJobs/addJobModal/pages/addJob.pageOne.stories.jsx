import React from 'react';

import { storiesOf } from '@storybook/react';
import { AddJobPageOne } from './addJob.pageOne.component';
import { jobData } from '../../../../../../test/data';

storiesOf('Development/Manage Jobs', module).add('Page One', () => (
  <main style={{ width: 620 }}>
    <AddJobPageOne
      currentJob={jobData[0]}
      errors={{
        salary: 'Sample error message: Salary max bust be larger than minimum',
      }}
    />
  </main>
));
