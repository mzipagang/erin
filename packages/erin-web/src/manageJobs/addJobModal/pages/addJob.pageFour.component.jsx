import { Row } from 'antd';
import React from 'react';
import renderHTML from 'react-render-html';
import { AddJobsFooter } from '../footer/addJobFooter.component';
import { JobTypes, SalaryTypes, notificationTypes } from './copy';

import {
  summaryTitle,
  summaryValue,
  summaryHeader,
  sectionContainer,
  summaryHeaderContainer,
  bonusAmmount,
} from './pages.styles';

const normalizeDollar = value => {
  return value ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : null;
};

export class AddJobPageFour extends React.Component {
  footerButtons = page => {
    return (
      <AddJobsFooter
        isNew={this.props.isNew}
        page={page}
        handleNext={() => {}}
        handleBack={() => {
          this.props.handleBack(page);
        }}
        handleSubmit={() => {
          this.props.handleSubmit();
        }}
      />
    );
  };

  safeSalary = (min, max) => {
    if (!min && !max) return null;
    if (!min || min === max) return `$${normalizeDollar(max)}`;
    if (!max) return `$${normalizeDollar(min)}`;
    return `$${normalizeDollar(min)} - $${normalizeDollar(max)}`;
  };

  render() {
    const {
      jobType,
      title,
      description,
      user: hiringManager,
      location,
      salary,
      notificationType,
      department,
      referralBonus,
      publicLink,
    } = this.props.currentJob;
    const _salary = this.safeSalary(salary.from, salary.to);
    const footer = this.footerButtons(this.props.page);
    return (
      <main>
        <div className={summaryHeaderContainer}>
          <span className={summaryHeader}>
            Almost There! How does everything look?
          </span>
        </div>

        <div className={sectionContainer}>
          <Row>
            <span className={summaryTitle}> Job Type: </span>
            <span className={summaryValue}> {JobTypes[jobType]} </span>
          </Row>

          <Row>
            <span className={summaryTitle}> Salary: </span>
            <span className={summaryValue}>
              {_salary ? _salary : ''} {SalaryTypes[salary.duration]}
            </span>
          </Row>

          {!location.isRemote && (
            <Row>
              <span className={summaryTitle}> Location: </span>
              {location.city && (
                <span className={summaryValue}>
                  {location.city}, {location.state}
                </span>
              )}
            </Row>
          )}
          {location.isRemote && (
            <Row>
              <span className={summaryTitle}> Location: </span>
              <span className={summaryValue}> Remote </span>
            </Row>
          )}
        </div>

        <div className={sectionContainer}>
          <Row>
            <span className={summaryTitle}> Job Title: </span>
            <span className={summaryValue}> {title} </span>
          </Row>
          <Row>
            <span className={summaryTitle}> Department: </span>
            <span className={summaryValue}> {department.name} </span>
          </Row>
          <Row>
            <span className={summaryTitle}> Job Description: </span>
            <span className={summaryValue}>
              {renderHTML(description.toString('html'))}
            </span>
          </Row>
        </div>

        <div className={sectionContainer}>
          <Row>
            <span className={summaryTitle}> Public Job Posting Link: </span>
            <span className={summaryValue}>{publicLink || ''}</span>
          </Row>

          <Row>
            <span className={summaryTitle}> Hiring Manager: </span>
            <span className={summaryValue}>
              {hiringManager
                ? `${hiringManager.firstName} ${hiringManager.lastName}`
                : null}
            </span>
          </Row>

          <Row>
            <span className={summaryTitle}> Referral Bonus: </span>
            <span className={bonusAmmount}>
              {referralBonus.hasBonus
                ? `$${referralBonus.amount}`.replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    ','
                  )
                : '$0'}
            </span>
          </Row>

          <Row>
            <span className={summaryTitle}> Notifications: </span>
            <span className={summaryValue}>
              {notificationTypes[notificationType]}
            </span>
          </Row>
        </div>
        {footer}
      </main>
    );
  }
}
