import React from 'react';

import { storiesOf } from '@storybook/react';
import { AddJobPageTwo } from './addJob.pageTwo.component';

storiesOf('Development/Manage Jobs', module).add('Page Two', () => (
  <main style={{ width: 620 }}>
    <AddJobPageTwo currentJob={{}} errors={{}} />
  </main>
));
