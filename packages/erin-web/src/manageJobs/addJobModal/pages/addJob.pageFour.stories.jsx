import React from 'react';

import { storiesOf } from '@storybook/react';
import { AddJobPageFour } from './addJob.pageFour.component';
import { jobData } from '../../../../../../test/data';

storiesOf('Development/Manage Jobs', module).add('Page Four', () => (
  <main style={{ width: 620 }}>
    <AddJobPageFour currentJob={jobData[0]} />
  </main>
));
