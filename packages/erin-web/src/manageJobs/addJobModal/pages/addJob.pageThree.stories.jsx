import React from 'react';

import { storiesOf } from '@storybook/react';
import { AddJobPageThree } from './addJob.pageThree.component';
import { jobData } from '../../../../../../test/data';

storiesOf('Development/Manage Jobs', module).add('Page Three', () => (
  <main style={{ width: 620 }}>
    <AddJobPageThree currentJob={jobData[0]} />
  </main>
));
