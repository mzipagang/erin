import React from 'react';
import { Button, Row } from 'antd';
import {
  titleStyle,
  cancelContainer,
  cancelModalButton,
  keepModalOpenButton,
} from './pages.styles';

export class CancelEditJob extends React.Component {
  render() {
    const { handleOk, handleCancel } = this.props;
    return (
      <div className={cancelContainer}>
        <span className={titleStyle}>
          Are you sure you want to discard these changes?
        </span>

        <Row style={{ justifyContent: 'center', marginTop: 15 }}>
          <Button className={cancelModalButton} onClick={handleCancel}>
            Discard Changes
          </Button>
          <Button className={keepModalOpenButton} onClick={handleOk}>
            Continue Working
          </Button>
        </Row>
      </div>
    );
  }
}
