import React from 'react';
import { Button, Row } from 'antd';
import {
  titleStyle,
  cancelContainer,
  cancelModalButton,
  keepModalOpenButton,
} from './pages.styles';

export class CancelAddJob extends React.Component {
  render() {
    const { handleOk, handleCancel } = this.props;
    return (
      <div className={cancelContainer}>
        <span className={titleStyle}>
          Are you sure you want to discard this Job?
        </span>

        <Row style={{ justifyContent: 'center', marginTop: 15 }}>
          <Button className={cancelModalButton} onClick={handleCancel}>
            Discard Job
          </Button>
          <Button className={keepModalOpenButton} onClick={handleOk}>
            Continue Job
          </Button>
        </Row>
      </div>
    );
  }
}
