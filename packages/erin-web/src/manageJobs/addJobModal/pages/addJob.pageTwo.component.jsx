import { Button, Row, Col, Input, Menu, Dropdown, Icon } from 'antd';
import React from 'react';
import {
  RteContainer,
  inputContainer,
  dropdownMenuItem,
  dropdownButton,
  fieldErrors,
  errorText,
} from './pages.styles';
import RichTextEditor from 'react-rte';
import { PageItem } from './addJob.page.item.component';
import { AddJobsFooter } from '../footer/addJobFooter.component';

const toolbarConfig = {
  // Optionally specify the groups to display (displayed in the order listed).
  display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS'],
  INLINE_STYLE_BUTTONS: [
    { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
    { label: 'Italic', style: 'ITALIC' },
  ],
  BLOCK_TYPE_BUTTONS: [{ label: 'UL', style: 'unordered-list-item' }],
};

export class AddJobPageTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      department: '',
      jobDescriptionRaw: RichTextEditor.createEmptyValue(),
      description: null,
    };
  }

  footerButtons = page => {
    return (
      <AddJobsFooter
        page={page}
        handleNext={() => {
          this.props.handleNext(page, { ...this.props.current, ...this.state });
        }}
        handleBack={() => {
          this.props.handleBack(page);
        }}
      />
    );
  };

  onChangeRTE = value => {
    this.setState({
      jobDescriptionRaw: value,
      description: value.toString('html'),
    });
    if (this.state.description === '<p><br></p>') {
      this.setState({ description: null });
    }
  };

  onChangeJobTitle = e => {
    const { value } = e.target;
    this.setState({
      title: value,
    });
  };

  componentDidMount() {
    const { description, title, department } = this.props.currentJob;
    this.setState({
      jobDescriptionRaw: description
        ? RichTextEditor.createValueFromString(description, 'html')
        : RichTextEditor.createEmptyValue(),
      title,
      department,
      description,
    });
  }

  renderJobTitleAndDepartment = () => {
    const { title, department } = this.state;
    const { errors, departments } = this.props;
    return (
      <Row>
        <Col span={12}>
          <PageItem title="Job Title">
            <Input
              style={{ width: '100%' }}
              className={inputContainer(150)}
              value={title}
              onChange={this.onChangeJobTitle.bind(this)}
              placeholder="Ex. Marketing Director"
            />
            {errors.title && (
              <div className={fieldErrors}>
                <span className={errorText}> {errors.title} </span>
              </div>
            )}
          </PageItem>
        </Col>
        <Col span={12}>
          <PageItem title="Department">
            <Dropdown
              trigger={['click']}
              overlay={
                <Menu>
                  {departments.map(item => {
                    return (
                      <Menu.Item
                        key={item.id}
                        className={dropdownMenuItem}
                        onClick={() => {
                          this.setState({
                            department: {
                              id: item.id,
                              name: item.name,
                            },
                          });
                        }}
                      >
                        {item.name}
                      </Menu.Item>
                    );
                  })}
                </Menu>
              }
            >
              <Button className={dropdownButton('full')}>
                {department && department.name
                  ? department.name
                  : 'Select Department'}
                <Icon type="down" />
              </Button>
            </Dropdown>
            {errors.department && (
              <div className={fieldErrors}>
                <span className={errorText}> {errors.department} </span>
              </div>
            )}
          </PageItem>
        </Col>
      </Row>
    );
  };

  renderRTE = () => {
    const { errors } = this.props;
    return (
      <PageItem
        title="Job Description"
        subtitle="Describe the responsibility of the job, required experience,
              education, etc."
      >
        <RichTextEditor
          toolbarConfig={toolbarConfig}
          value={this.state.jobDescriptionRaw}
          onChange={this.onChangeRTE}
          className={RteContainer}
        />
        {errors.description && (
          <div className={fieldErrors}>
            <span className={errorText}> {errors.description} </span>
          </div>
        )}
      </PageItem>
    );
  };

  render() {
    const footer = this.footerButtons(this.props.page);
    return (
      <main>
        <div>
          {this.renderJobTitleAndDepartment()}
          {this.renderRTE()}
          {footer}
        </div>
      </main>
    );
  }
}
