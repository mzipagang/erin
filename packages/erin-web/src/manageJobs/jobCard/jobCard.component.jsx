import React from 'react';
import { Row, Col, Card } from 'antd';
import { Link } from 'react-router-dom';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import {
  jobCardMain,
  topContainer,
  jobInfoContainer,
  bonusContainer,
  middleContainer,
  divider,
  bottomContainer,
  bigNumbers,
  numberSubtitles,
  bonusAmount,
  jobTitleText,
  matchBox,
  middleColumn,
  Department,
  bottomSectionText,
  bottomSectionValue,
  jobDetailRow,
  locationStyles,
  positionIcon,
} from './jobCard.styles';

export class JobCard extends React.Component {
  render() {
    const {
      job: {
        id,
        title,
        department,
        referralBonus,
        location,
        shares,
        views,
        _accepted,
        _referrals,
        _matches,
      },
    } = this.props;

    return (
      <Card
        hoverable
        headStyle={{
          borderWidth: 0,
          padding: 10,
          paddingTop: 5,
          marginTop: -15,
        }}
        bodyStyle={{ width: 290, padding: 10 }}
        className={jobCardMain}
        title={
          <Row className={topContainer}>
            <Col className={jobInfoContainer}>
              <Link className={jobTitleText} to={`/jobs/${id}`}>
                {title}
              </Link>

              <Row type="flex" className={jobDetailRow}>
                <WebIcon color={COLORS.darkGray} size={18} name="folder" />
                <span className={Department}>
                  {department ? department.name : ''}
                </span>
                <WebIcon color={COLORS.darkGray} size={18} name="placeholder" />
                <span className={locationStyles}>
                  {location && location.city && location.state
                    ? `${location.city}, ${location.state}`
                    : 'Remote'}
                </span>
              </Row>
            </Col>
          </Row>
        }
        extra={
          <div className={bonusContainer}>
            {referralBonus &&
              referralBonus.hasBonus && (
                <span className={bonusAmount}>
                  {`$${referralBonus.amount}`.replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    ','
                  )}
                </span>
              )}
          </div>
        }
      >
        <Row className={middleContainer}>
          <Col span={8} className={middleColumn}>
            <span className={bigNumbers}> {_accepted ? _accepted : 0} </span>
            <span className={numberSubtitles}> Accepted </span>
          </Col>
          <Col span={8} className={middleColumn}>
            <span className={bigNumbers}> {_referrals ? _referrals : 0} </span>
            <span className={numberSubtitles}> Referrals </span>
          </Col>
          <Col span={8} className={matchBox}>
            <span className={bigNumbers}> {_matches ? _matches : 0} </span>
            <span className={numberSubtitles}> Matches </span>
          </Col>
        </Row>
        <div className={divider} />
        <Row className={bottomContainer}>
          <Row className={positionIcon}>
            <WebIcon color={COLORS.lightGray} size={16} name="share" />
            <span className={bottomSectionText}> Job Shares: </span>
            <span className={bottomSectionValue}> {shares} </span>
          </Row>
          <Row className={positionIcon}>
            <WebIcon color={COLORS.lightGray} size={16} name="eye" />
            <span className={bottomSectionText}> Total Job Views: </span>
            <span className={bottomSectionValue}> {views} </span>
          </Row>
        </Row>
      </Card>
    );
  }
}
