import React from 'react';

import { storiesOf } from '@storybook/react';
import { JobCard } from './jobCard.component';

import { jobData } from '../../../../../test/data';

storiesOf('Development/Manage Jobs', module).add('Job Card', () => (
  <JobCard job={jobData[0]} />
));

storiesOf('QA/Manage Jobs', module).add('Job Card', () => (
  <JobCard job={jobData[0]} />
));
