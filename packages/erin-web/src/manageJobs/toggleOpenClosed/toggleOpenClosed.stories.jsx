import React from 'react';

import { storiesOf } from '@storybook/react';
import { ToggleOpenClosed } from './toggleOpenClosed.component';

storiesOf('Development/Manage Jobs', module).add('Toggle', () => (
  <ToggleOpenClosed
    openJobsKey={'open'}
    closedJobsKey={'closed'}
    filterOpenStatus={'open'}
    onCLick={() => {
      console.warn('Toggled! Note: does not toggle.');
    }}
  />
));
