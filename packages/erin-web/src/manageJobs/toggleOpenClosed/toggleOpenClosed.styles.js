import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const toggleButtonContainerStyles = css({
  display: 'flex',
  margin: 10,
  alignItems: 'center',
  '& .ant-btn': {
    lineHeight: '12px',
  },
});

export const openButtonStyles = selected =>
  css({
    margin: 5,
    marginTop: 8,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    height: 30,
    width: 75,
  });

export const toggleTitleStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: '18',
});
