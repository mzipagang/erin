import { Icon, Row } from 'antd';
import React from 'react';
import {
  departmentContainer,
  closeIcon,
  departmentText,
} from './departmentItem.styles';

export class DepartmentItem extends React.Component {
  render() {
    const { department, onClick } = this.props;
    return (
      <div onClick={onClick}>
        <Row className={departmentContainer}>
          <span className={departmentText}> {department} </span>
          <Icon type="close-circle-o" className={closeIcon} />
        </Row>
      </div>
    );
  }
}
