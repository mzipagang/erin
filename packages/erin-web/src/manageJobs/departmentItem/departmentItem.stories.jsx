import React from 'react';

import { storiesOf } from '@storybook/react';
import { DepartmentItem } from './departmentItem.component';

storiesOf('Development/Manage Jobs', module).add('Department Item', () => (
  <DepartmentItem
    department={'Information Technology'}
    onCLick={() => {
      console.warn('Remove Item!');
    }}
  />
));
