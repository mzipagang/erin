import { connect } from 'react-redux';
import { JobDetail as JobDetailComponent } from './jobDetail.component';
import { actions as AllActions } from 'erin-app-state-mgmt';
import { withJobById } from 'erin-app-state-mgmt/src/_shared/api/components/jobs/with-job-by-id.provider';
import { compose } from '../../_shared/services/utils';
import { withListUsersAndRole } from 'erin-app-state-mgmt/src/_shared/api/components/users/with-list-users-and-role.provider';
const { manageJobsActions, referralActions } = AllActions;

const JobActions = manageJobsActions.actions;
const ReferralActions = referralActions.actions;

const mapStateToProps = (state, props) => {
  const {
    match: {
      params: { id },
    },
  } = props;
  return {
    id: id,
    jobId: id,
    currentUser: state.user.currentUser,
    jobFormData: state.manageJobs.currentJob,
    filter: { companyId: { eq: state.user.currentUser.companyId } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateAddJobForm(values) {
      dispatch(JobActions.updateAddJobForm(values));
    },
    deselectCurrentJob() {
      dispatch(JobActions.resetAddJobForm());
    },
    closeJob(jobId) {
      dispatch(JobActions.closeJob(jobId));
    },
    updateReferral(values) {
      dispatch(ReferralActions.updateReferral(values));
    },
    fetchReferrals() {
      dispatch(ReferralActions.fetchReferrals());
    },
    updateReferralsNotHired() {
      dispatch(ReferralActions.updateReferralsNotHired());
    },
    resetFormFields() {
      dispatch(JobActions.resetAddJobForm());
    },
  };
};

const JobDetailWithApi = compose(
  withJobById,
  withListUsersAndRole
)(JobDetailComponent);

export const JobDetail = connect(
  mapStateToProps,
  mapDispatchToProps
)(JobDetailWithApi);
