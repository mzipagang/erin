import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const mainContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  paddingRight: 30,
});

export const editJobButton = css({
  padding: 0,
  backgroundColor: 'transparent',
  color: COLORS.hyperLink,
  borderColor: 'transparent',
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '400 !important',
  fontSize: 18,
  '&:hover': {
    backgroundColor: 'transparent',
    color: COLORS.blue,
    borderColor: 'transparent',
  },
  '&:focus': {
    backgroundColor: 'transparent',
    color: COLORS.blue,
    borderColor: 'transparent',
  },
});

export const backButton = css({
  marginRight: 5,
  color: COLORS.hyperLink,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '400 !important',
  fontSize: 18,
});

export const hiredCountDiv = css({
  backgroundColor: COLORS.dashboardGreen,
  borderRadius: 10,
  height: 50,
  width: 330,
  marginBottom: 20,
  textAlign: 'center',
  display: 'inline-block',
});

export const editJobButtonText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.blue,
  fontSize: 18,
  marginLeft: 3,
});

export const addJobsButtonIcon = css({
  fontSize: 18,
});

export const topContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-start',
  marginBottom: 30,
  maxWidth: 1290,
  paddingRight: 20,
});

export const bottomContainer = css({
  marginTop: 25,
  marginBottom: 15,
  maxWidth: 1290,
  paddingRight: 20,
});

export const Card1Styles = css({
  width: '100%',
  minWidth: 650,
  marginRight: 15,
  height: 485,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  padding: 30,
  backgroundColor: 'white',
});

export const modalTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: '1.5em',
});

export const loadingImage = css({
  display: 'block',
  margin: 'auto',
});

export const hiredStandardText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.heading,
  fontSize: '1.25em',
  opacity: 0.75,
});

export const hiredCountText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.white,
  fontSize: '1.25em',
});

export const spacer = css({
  margin: 5,
  height: 2,
  width: '100%',
});

export const gridTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 22,
  lineHeight: '40px',
});

export const FlexContainer = css({
  display: 'flex',
  maxWidth: 1290,
  paddingRight: 20,
});
export const middleContainer = css({
  display: 'flex',
  maxWidth: 1290,
  paddingRight: 20,
  justifyContent: 'space-between',
  marginTop: 30,
});

export const openButtonStyles = selected =>
  css({
    margin: 8,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    height: 30,
    width: 'auto',
  });

export const toggleTitleStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 18,
  marginTop: 14,
});

export const NoSmartReferrals = css({
  marginTop: 40,
  width: 955,
  textAlign: 'center',
  fontSize: 14,
  fontWeight: 400,
});

export const spinContainer = css({
  width: '100%',
  height: '50vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const openJobContainer = css({
  height: 50,
  backgroundColor: COLORS.lightGray3,
  borderRadius: 10,
  width: '100%',
  marginBottom: 20,
  display: 'flex',
  justifyContent: 'center',
  '& .ant-btn:hover': {
    background: COLORS.lightGray3,
    color: COLORS.blue,
  },
});

export const openJobText = css({
  lineHeight: '50px',
});

export const openJobBtn = css({
  border: 'none',
  padding: 0,
  backgroundColor: 'transparent',
  color: COLORS.blue,
});
