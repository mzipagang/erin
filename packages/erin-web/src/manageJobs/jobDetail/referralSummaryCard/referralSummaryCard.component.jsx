import React from 'react';
import { Row, Col, Icon, Card } from 'antd';
import {
  jobCardMain,
  middleContainer,
  divider,
  bottomContainer,
  bigNumbers,
  numberSubtitles,
  matchBox,
  middleColumn,
  smallIcons,
  bottomSectionText,
  bottomSectionValue,
  referralSummaryCardTitle,
} from './referralSummaryCard.styles';

export class ReferralSummaryCard extends React.Component {
  render() {
    const { job = {} } = this.props;
    const { shares, views, referrals, matches } = job;
    const acceptedReferrals = referrals.filter(
      referral => referral.status !== 'referred'
    );
    return (
      <Card
        headStyle={{ borderWidth: 0 }}
        bodyStyle={{ padding: 5 }}
        className={jobCardMain}
      >
        <Row>
          <span className={referralSummaryCardTitle}> REFERRAL SUMMARY </span>
        </Row>
        <Row className={middleContainer}>
          <Col span={8} className={middleColumn}>
            <span className={bigNumbers('black')}>
              {referrals ? acceptedReferrals.length : 0}
            </span>
            <span className={numberSubtitles('black')}> Accepted </span>
          </Col>
          <Col span={8} className={middleColumn}>
            <span className={bigNumbers('black')}>
              {referrals ? referrals.length : 0}
            </span>
            <span className={numberSubtitles('black')}> Referrals </span>
          </Col>
          <Col span={8} className={matchBox}>
            <span className={bigNumbers('blue')}>
              {matches ? matches.length : 0}
            </span>
            <span className={numberSubtitles('blue')}> Matches </span>
          </Col>
        </Row>
        <div className={divider} />
        <Row className={bottomContainer}>
          <Row>
            <Icon className={smallIcons} type="share-alt" />
            <span className={bottomSectionText}> Job Shares: </span>
            <span className={bottomSectionValue}> {shares} </span>
          </Row>
          <Row>
            <Icon className={smallIcons} type="eye" />
            <span className={bottomSectionText}> Total Job Views: </span>
            <span className={bottomSectionValue}> {views} </span>
          </Row>
        </Row>
      </Card>
    );
  }
}
