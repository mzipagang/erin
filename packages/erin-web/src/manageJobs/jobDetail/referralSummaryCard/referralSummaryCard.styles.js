import { css } from 'emotion';

import { COLORS } from '../../../_shared/styles/colors';

export const addJobMainContainer = css({
  justifyContent: 'center',
  alignItems: 'center',
});

export const jobCardMain = css({
  height: 290,
  width: 310,
  padding: 20,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  '& ant-card-head-title': {
    margin: 0,
  },
});

export const topContainer = css({
  height: 60,
  marginTop: 10,
});

export const jobInfoContainer = css({
  height: 'auto',
});

export const bonusContainer = css({
  height: 'auto',
  marginRight: 15,
});

export const jobDetailRow = css({
  marginTop: 7,
  height: 16,
  '& svg': {
    transform: 'translate(0, 1px)',
  },
});

export const middleContainer = css({
  display: 'flex',
  justifyContent: 'center',
  height: 90,
  width: '100%',
  marginBottom: 30,
});

export const divider = css({
  height: 1,
  backgroundColor: COLORS.subHeading,
  opacity: 0.4,
});

export const bottomContainer = css({
  height: 80,
  paddingTop: 25,
  paddingBottom: 5,
});

export const bonusAmount = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.green,
  fontSize: '1em',
});

export const jobTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '600 !important',
  color: COLORS.hyperLink,
  fontSize: 16,
  textDecoration: 'none',
});

export const Department = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.darkGray,
  fontSize: 14,
  marginLeft: 5,
  marginRight: 5,
});
export const location = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.darkGray,
  fontSize: 14,
  marginRight: 5,
});

export const middleColumn = css({
  height: 100,
  textAlign: 'center',
  width: 80,
  margin: 5,
});

export const matchBox = css({
  textAlign: 'center',
  width: 80,
  backgroundColor: '#f1f1f1',
  margin: 5,
});

export const bottomSectionText = css({
  fontFamily: '"Open Sans", sans-serif semibold',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: 16,
  opacity: 0.7,
});

export const bottomSectionValue = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: COLORS.darkGray,
  fontSize: 16,
  float: 'right',
});

export const positionIcon = css({
  '& svg': {
    transform: 'translate(0, 2px)',
  },
});

export const bigNumbers = color =>
  css({
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 800,
    color: COLORS[color],
    fontSize: 36,
  });

export const numberSubtitles = color =>
  css({
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 600,
    color: COLORS[color],
    fontSize: 14,
  });

export const smallIcons = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.subHeading,
  fontSize: '1.2em',
  opacity: 0.7,
});

export const referralSummaryCardTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 18,
  letterSpacing: 1,
});
