import { Row } from 'antd';
import React from 'react';
import renderHTML from 'react-render-html';
import {
  topContainer,
  jobInfoContainer,
  jobTitleText,
  jobDetailRow,
  jobLocation,
  Department,
  sectionContainer,
  summaryTitle,
  summaryValue,
  jobLinkStyles,
  footerTextStyles,
  hiringManagerLinkStyles,
  footerStyles,
  BtnContainer,
} from './jobDescriptionCard.styles';
import WebIcon from '../../../_shared/components/web-icon.component';
import { ShareSendButton } from '../../../_shared/components/share-send-button/share-send.component';
import { normalizeDollar } from '../../../_shared/services/utils';
import { JOB_STATUSES } from '../../../_shared/constants/job-statuses.enum';
import { ReferralModal } from '../../../_shared/referral-modal/referral-modal.container';
// import { Link } from 'react-router-dom';

export class JobDescriptionCard extends React.Component {
  safeSalary = (min, max) => {
    // TODO: Handle this case better
    if (!min && !max) return '';
    if (!min || min === max) return `$${normalizeDollar(max)}`;
    if (!max) return `$${normalizeDollar(min)}`;
    return `$${normalizeDollar(min)} - $${normalizeDollar(max)}`;
  };

  onClickShare = () => {
    // TODO: TBD
  };

  render() {
    const {
      job: {
        title,
        department,
        description,
        user: hiringManager,
        location,
        jobType,
        jobId,
        jobUrl,
        salary = {},
        status,
        publicLink,
      } = {},
      onClick,
    } = this.props;

    const _salary = this.safeSalary(salary.from, salary.to);

    const showJobPosting = status === JOB_STATUSES.OPEN && publicLink;
    return (
      <div>
        <div className={topContainer}>
          <div className={jobInfoContainer}>
            <div onClick={onClick}>
              <span className={jobTitleText}> {title} </span>
            </div>
            <Row type="flex" className={jobDetailRow}>
              <WebIcon name="folder" size={20} />
              <span className={Department}>
                {' '}
                {department ? department.name : ''}{' '}
              </span>
              <WebIcon name="placeholder" size={20} />
              <span className={jobLocation}>
                {location && location.city && location.state
                  ? `${location.city}, ${location.state}`
                  : 'Remote'}
              </span>
            </Row>
          </div>
          {status === JOB_STATUSES.OPEN && (
            <div>
              <div style={{ display: 'flex' }}>
                <Row className={BtnContainer}>
                  <ReferralModal jobData={this.props.job} />
                </Row>
                {publicLink && (
                  <Row>
                    <div style={{ height: '50px', fontSize: '1.20em' }}>
                      <ShareSendButton
                        title={title}
                        jobId={jobId}
                        jobUrl={jobUrl}
                        jobType={jobType}
                        description={description}
                        city={location && location.city ? location.city : null}
                        state={
                          location && location.state ? location.state : null
                        }
                      />
                    </div>
                  </Row>
                )}
              </div>
              {showJobPosting && (
                <Row>
                  or{' '}
                  <a href={publicLink} className={jobLinkStyles}>
                    View Public Job posting
                  </a>
                </Row>
              )}
            </div>
          )}
        </div>

        <div className={sectionContainer}>
          <Row>
            <span className={summaryTitle}> Job Type: </span>
            <span className={summaryValue}> {jobType} </span>
            Ok
          </Row>
          {_salary && (
            <Row>
              <span className={summaryTitle}> Salary: </span>
              <span className={summaryValue}> {_salary} </span>
            </Row>
          )}
        </div>

        <div className={sectionContainer}>
          <Row>
            <span className={summaryTitle}> Job Title: </span>
            <span className={summaryValue}> {title} </span>
          </Row>
          <Row>
            <span className={summaryTitle}> Job Description: </span>
            <span className={summaryValue}>
              {renderHTML(description ? description.toString('html') : '')}
            </span>
          </Row>
        </div>
        {hiringManager && (
          <div className={footerStyles}>
            <span className={footerTextStyles}>Hiring Manager: </span>
            <a
              href={`mailto:${hiringManager.emailAddress}`}
              className={hiringManagerLinkStyles}
            >
              {' '}
              {`${hiringManager.firstName} ${hiringManager.lastName}`}
            </a>
          </div>
        )}
      </div>
    );
  }
}
