import React from 'react';
import { Modal } from 'antd';

import CloseJobForm from './close-job-form/close-job-form.component';

import {
  ModalTitle,
  SmallText,
  ModalStyles,
  SubTitle,
  FormStyles,
} from './close-job-modal.styles';

export class CloseJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleError = () => {
    this.setState({ error: true });
  };

  render() {
    const { visible, handleCancel } = this.props;
    const { error } = this.state;

    return (
      <Modal
        className={ModalStyles}
        onCancel={handleCancel}
        visible={visible}
        footer={null}
      >
        <div>
          <h1 className={ModalTitle}>Close This Job</h1>
          <p className={SmallText}>
            Closing this job means you are no longer acceptin referrals.
            {'\n'} You can re-open the job at any time.
          </p>
          <h3 className={SubTitle}>Would you like to also:</h3>
          <CloseJobForm
            className={FormStyles}
            error={error}
            handleError={this.handleError}
            onUpdateJob={this.props.onUpdateJob}
            onUpdateReferral={this.props.onUpdateReferral}
            referrals={this.props.referrals}
            job={this.props.job}
            onCancel={handleCancel}
          />
        </div>
      </Modal>
    );
  }
}
