import React from 'react';
import { Form, Checkbox, Button, Icon } from 'antd';

import {
  SubmitBtnContainer,
  SubmitBtn,
  CheckIcon,
  CheckBoxStyles,
  FormItemStyles,
  NotHired,
} from './close-job-form.styles';

import { JOB_STATUSES } from '../../../../_shared/constants/job-statuses.enum';

const FormItem = Form.Item;

class CloseJobForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    const { job, referrals } = this.props;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (values.updateOtherReferrals) {
          referrals.forEach(referral => {
            this.props.onUpdateReferral({
              input: {
                id: referral.id,
                status: 'notHired',
              },
            });
          });
        }
        this.props.onUpdateJob({
          id: job.id,
          status: JOB_STATUSES.CLOSED,
          jobType: job.jobType,
        });
        this.props.onCancel();
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <FormItem className={FormItemStyles}>
          {getFieldDecorator('updateOtherReferrals', {
            valuePropName: 'checked',
          })(
            <Checkbox className={CheckBoxStyles}>
              Update other referrals as{' '}
              <span className={NotHired}>Not Hired</span>
            </Checkbox>
          )}
        </FormItem>
        <FormItem className={SubmitBtnContainer}>
          <Button
            className={SubmitBtn}
            onClick={this.handleSubmit}
            htmlType="submit"
          >
            Close Job
            <Icon className={CheckIcon} type="check" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(CloseJobForm);
