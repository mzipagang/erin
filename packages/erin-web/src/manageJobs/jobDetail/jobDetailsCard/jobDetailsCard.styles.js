import { css } from 'emotion';

import { COLORS } from '../../../_shared/styles/colors';

export const jobDetailsCardMainStyles = css({
  height: 175,
  width: 310,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
});

export const detailTitleStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.subHeading,
  fontSize: '1.1em',
  letterSpacing: 1,
  paddingTop: 5,
  marginLeft: 15,
});

export const detailValueStyles = color =>
  css({
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 800,
    color: COLORS[color],
    fontSize: '1em',
    marginLeft: 5,
  });

export const closeJobTextStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.blue,
  fontSize: '0.85em',
});

export const closeButtonStyles = css({
  borderWidth: 0,
  marginLeft: 10,
});

export const detailCardTitleStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 18,
  letterSpacing: 1,
});

export const idTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.lightGray,
  fontSize: '0.85em',
});
