import React from 'react';
import { Row, Col, Card, Button } from 'antd';
import {
  jobDetailsCardMainStyles,
  detailTitleStyle,
  detailValueStyles,
  closeJobTextStyle,
  closeButtonStyles,
  detailCardTitleStyles,
  idTextStyles,
} from './jobDetailsCard.styles';

export class JobDetailsCard extends React.Component {
  render() {
    const {
      job: { status, notificationType, id, hiringManager } = {},
      onClickCloseJob,
      handleOpenJob,
    } = this.props;
    return (
      <Card
        headStyle={{ borderWidth: 0, marginBottom: 7 }}
        bodyStyle={{ padding: 5 }}
        className={jobDetailsCardMainStyles}
        title={
          <div>
            <span className={detailCardTitleStyles}> Job Details </span>
            <span className={idTextStyles}>{`ID: #${id}`}</span>
          </div>
        }
      >
        <Col style={{ margin: 'auto' }}>
          <Row>
            <span className={detailTitleStyle}> Status: </span>
            <span className={detailValueStyles('black')}>
              {status === 'open' ? 'Open' : 'Closed'}
            </span>
            {status && status === 'open' ? (
              <Button className={closeButtonStyles} onClick={onClickCloseJob}>
                <span className={closeJobTextStyle}> Close Job </span>
              </Button>
            ) : (
              <Button className={closeButtonStyles} onClick={handleOpenJob}>
                <span className={closeJobTextStyle}> (Open Job) </span>
              </Button>
            )}
          </Row>
          <Row>
            <span className={detailTitleStyle}> Notifications: </span>
            <span className={detailValueStyles('black')}>
              {notificationType && notificationType.replace('Notify ', '')}
            </span>
          </Row>
          <Row>
            <span className={detailTitleStyle}> Created By: </span>
            <span className={detailValueStyles('blue')}> {hiringManager} </span>
          </Row>
        </Col>
      </Card>
    );
  }
}
