// #region imports
import React from 'react';
import { List, Modal, Button, Icon, Spin } from 'antd';
import { Link } from 'react-router-dom';
import {
  editJobButton,
  hiredCountDiv,
  addJobsButtonIcon,
  topContainer,
  modalTitleText,
  loadingImage,
  gridTitle,
  openButtonStyles,
  toggleTitleStyles,
  backButton,
  hiredStandardText,
  hiredCountText,
  spacer,
  NoSmartReferrals,
  spinContainer,
  mainContainer,
  FlexContainer,
  bottomContainer,
  middleContainer,
  Card1Styles,
  openJobContainer,
  openJobText,
  openJobBtn,
} from './jobDetail.styles';
import { AddJob as EditJob } from '../addJobModal/addJob.component';
import loadingGif from '../../_shared/assets/updatingjob300.gif';

import { JobDescriptionCard } from './jobDescriptionCard/jobDescriptionCard.component';
import { ReferralSummaryCard } from './referralSummaryCard/referralSummaryCard.component';
import { JobDetailsCard } from './jobDetailsCard/jobDetailsCard.component';

import { ReferralCard } from '../../_shared/components/referral-card/referral-card.container';

import { requiredFields, miscFieldErrors } from '../errorHandlers';

import { CloseJob } from './close-job-modal/close-job-modal.component';
import { JOB_STATUSES } from '../../_shared/constants/job-statuses.enum';

const filterAllKey = 'All';
const filterAcceptedKey = 'Accepted';
const filterReferredKey = 'Referred';
// #endregion

export class JobDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      errors: {},
      editing: false,
      filterReferralsBy: filterAllKey,
      closeJobModalVisible: false,
    };
  }

  filterReferrals = (property, match, referrals) => {
    if (!referrals) return [];
    if (match === filterAllKey) return referrals;
    return referrals.filter(r => r[property] === match);
  };

  salaryInputIsValid = (min, max) => {
    if (min && max) {
      return Number(max) >= Number(min);
    }
    /* checking for NaN & hard min / max values
      is handled in the input 
    */
    return true;
  };

  pageIsValid = (currentPage, currentJob) => {
    let isValid = true;
    let salaryPassed = true;
    const errors = {};
    requiredFields.forEach(({ key, message, page }) => {
      if (!currentJob[key] && page === currentPage) {
        errors[key] = message;
        isValid = false;
      }
    });

    salaryPassed = this.salaryInputIsValid(
      currentJob.salaryMinimum,
      currentJob.salaryMaximum
    );

    if (!salaryPassed) {
      errors.salary = miscFieldErrors.salary;
      isValid = false;
    }

    this.setState({
      errors,
    });

    return isValid;
  };

  handleCancelCloseJob = () => {
    this.setState({
      closeJobModalVisible: false,
    });
  };

  handleShowCloseJob = () => {
    this.setState({
      closeJobModalVisible: true,
    });
  };

  handleCancel = () => {
    this.setState({ editing: false });
  };

  showCancelScreen = () => {
    this.setState({ page: 4 });
  };

  handleOk = () => {
    this.setState({ page: 0 });
  };

  handleNext = (page, values) => {
    if (this.pageIsValid(this.state.page, values)) {
      this.setState(
        { page: this.state.page < 3 ? this.state.page + 1 : 3 },
        () => {
          this.props.updateAddJobForm(values);
        }
      );
    }
  };

  handleSubmit = () => {
    // console.warning('Submit Form to API: ', this.props.currentJob);
    // TODO: call real api/action
    const { currentJob, jobFormData, onUpdateJob } = this.props;
    const job = {
      id: currentJob.id,
      departmentId: jobFormData.department.id,
      jobType: jobFormData.jobType,
      title: jobFormData.title,
      description: jobFormData.description,
      publicLink: jobFormData.publicLink,
      salary: JSON.stringify({
        from: jobFormData.salary.from,
        to: jobFormData.salary.to,
        interval: jobFormData.salary.interval,
      }),
      location: JSON.stringify({
        city: jobFormData.location.isRemote ? null : jobFormData.location.city,
        state: jobFormData.location.isRemote
          ? null
          : jobFormData.location.state,
        isRemote: jobFormData.location.isRemote,
      }),
      userId: jobFormData.hiringManager.id,
      referralBonus: JSON.stringify({
        hasBonus: jobFormData.referralBonus.hasBonus,
        amount: jobFormData.referralBonus.hasBonus
          ? jobFormData.referralBonus.amount
          : null,
      }),
      notificationType: jobFormData.notificationType,
    };

    onUpdateJob(job);

    this.setState(
      {
        isLoading: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            editing: false,
            isLoading: false,
          });
        }, 5600);
      }
    );
  };

  handleBack = () => {
    this.setState({ page: this.state.page > 0 ? this.state.page - 1 : 0 });
  };

  resetForm = () => {
    this.props.resetFormFields();
    this.setState({
      page: 0,
    });
  };

  editJob = () => {
    this.setState({ editing: true });
  };

  handleOpenJob = () => {
    const { currentJob } = this.props;
    this.props.onUpdateJob({
      id: currentJob.id,
      jobType: currentJob.jobType,
      status: JOB_STATUSES.OPEN,
    });
  };

  render() {
    const {
      editing,
      page,
      errors,
      isLoading,
      filterReferralsBy,
      closeJobModalVisible,
    } = this.state;
    const { currentJob, jobFormData, users } = this.props;
    if (!currentJob) {
      return (
        <div className={spinContainer}>
          <Spin size="large" tip="...Loading" />
        </div>
      );
    }

    const referredOnlyReferrals = this.filterReferrals(
      'status',
      filterReferredKey,
      currentJob.referrals
    );
    const acceptedOnlyReferrals = this.filterReferrals(
      'status',
      filterAcceptedKey,
      currentJob.referrals
    );
    const allReferrals = this.filterReferrals(
      'status',
      filterAllKey,
      currentJob.referrals
    );

    const _referrals = {
      [filterReferredKey]: referredOnlyReferrals,
      [filterAcceptedKey]: acceptedOnlyReferrals,
      [filterAllKey]: allReferrals,
    };

    const referralData = _referrals[filterReferralsBy];
    const numHired = 0; // TODO: currentJob._accepted;
    const nomenclature = numHired > 1 ? 'people' : 'person';

    // eslint-disable-next-line
    const hiredText = "You've hired"; // because of conflicting prettier/eslint rules

    return (
      <main className={mainContainer}>
        <section className={topContainer}>
          <div>
            <Link className={backButton} to="/jobs">
              <Icon type="left" /> Manage Jobs
            </Link>
          </div>
          <div>
            <Button onClick={this.editJob} className={editJobButton}>
              <Icon className={addJobsButtonIcon} type="edit" />
              EDIT JOB
            </Button>
          </div>
        </section>

        <section className={FlexContainer}>
          <div className={Card1Styles}>
            <JobDescriptionCard job={currentJob} />
          </div>
          <div style={{ marginLeft: 20 }}>
            {numHired > 0 && (
              <div className={hiredCountDiv}>
                <div className={spacer} />
                <span className={hiredStandardText}> {hiredText} </span>
                <span className={hiredCountText}> {`${numHired}`}</span>
                <span className={hiredStandardText}>
                  {` ${nomenclature} for this job!`}
                </span>
              </div>
            )}
            {currentJob && currentJob.status === 'closed' ? (
              <div className={openJobContainer}>
                <p className={openJobText}>
                  This Job is closed.{' '}
                  <Button className={openJobBtn} onClick={this.handleOpenJob}>
                    Click here
                  </Button>{' '}
                  open it.
                </p>
              </div>
            ) : null}
            <ReferralSummaryCard job={currentJob} />
            <br />
            <JobDetailsCard
              job={currentJob}
              onClickCloseJob={this.handleShowCloseJob}
              handleOpenJob={this.handleOpenJob}
            />
          </div>
        </section>

        <section className={middleContainer}>
          <div>
            <span className={gridTitle}> Referrals </span>
          </div>
          <div>
            <span className={toggleTitleStyles}>Show:</span>
            <Button
              onClick={() => {
                this.setState({
                  filterReferralsBy: filterAcceptedKey,
                });
              }}
              className={openButtonStyles(
                filterReferralsBy === filterAcceptedKey
              )}
            >
              Accepted
            </Button>
            <Button
              onClick={() => {
                this.setState({
                  filterReferralsBy: filterReferredKey,
                });
              }}
              className={openButtonStyles(
                filterReferralsBy === filterReferredKey
              )}
            >
              Referred
            </Button>
            <Button
              onClick={() => {
                this.setState({
                  filterReferralsBy: filterAllKey,
                });
              }}
              className={openButtonStyles(filterReferralsBy === filterAllKey)}
            >
              All
            </Button>
          </div>
        </section>
        {referralData.length > 0 ? (
          <div className={bottomContainer}>
            <List
              grid={{ gutter: 20, column: 2 }}
              dataSource={referralData}
              style={{ marginTop: 25 }}
              itemLayout={'horizontal'}
              renderItem={referral => (
                <List.Item>
                  <ReferralCard
                    currentJob={currentJob}
                    key={referral.id}
                    referral={referral}
                    onUpdateReferral={this.props.onUpdateReferral}
                    onUpdateJob={this.props.onUpdateJob}
                    allReferrals={referralData}
                  />
                </List.Item>
              )}
            />
          </div>
        ) : (
          <p className={NoSmartReferrals}>
            There are no Referrals for this job
          </p>
        )}

        <div>
          <Modal
            visible={editing}
            width={620}
            height={735}
            title={<span className={modalTitleText}> Edit Job </span>}
            destroyOnClose={true}
            afterClose={this.resetForm.bind(this)}
            maskClosable={false}
            onOk={null}
            onCancel={this.showCancelScreen}
            footer={null}
          >
            {isLoading && (
              <div>
                <img
                  className={loadingImage}
                  src={loadingGif}
                  alt="loading..."
                />
              </div>
            )}
            {!isLoading && (
              <EditJob
                isNew={false}
                handleNext={this.handleNext}
                handleBack={this.handleBack}
                page={page}
                managers={users.filter(user => user.role === 'manager')}
                currentJob={
                  jobFormData.jobType.length ? jobFormData : currentJob
                }
                departments={this.props.departments}
                handleSubmit={this.handleSubmit}
                handleOk={this.handleOk}
                handleCancel={this.handleCancel}
                errors={errors}
              />
            )}
          </Modal>
          <CloseJob
            visible={closeJobModalVisible}
            handleCancel={this.handleCancelCloseJob}
            onUpdateJob={this.props.onUpdateJob}
            onUpdateReferral={this.props.onUpdateReferral}
            referrals={this.props.currentJob.referrals}
            job={this.props.currentJob}
          />
        </div>
      </main>
    );
  }
}
