import React from 'react';

import { storiesOf } from '@storybook/react';
import { ManageJobs } from './manageJobs.component';

import { jobData } from '../../../../test/data'; // test data can be pulled from here to act like redux

storiesOf('Development/Manage Jobs', module).add('Main', () => (
  <ManageJobs jobs={jobData} fetchJobs={() => jobData} />
));

storiesOf('QA/Manage Jobs', module).add('Main', () => (
  <ManageJobs jobs={jobData} fetchJobs={() => jobData} />
));
