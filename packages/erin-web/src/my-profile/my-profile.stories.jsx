import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import MyProfileComponent from './my-profile.component';
import { userData } from '../../../../test/data/mb-data';

storiesOf('Development/My Profile', module)
  .addDecorator(StoryRouter())
  .add('Main', () => <MyProfileComponent currentUser={userData[0]} />);

storiesOf('QA/My Profile', module)
  .addDecorator(StoryRouter())
  .add('Main', () => <MyProfileComponent currentUser={userData[0]} />);
