import MyProfileComponent from './my-profile.component';
import { connect } from 'react-redux';
import { withUpdateUser } from 'erin-app-state-mgmt/src/_shared/api/components/my-profile/with-update-user.provider';
import { compose } from '../_shared/services/utils';

const mapStateToProps = state => {
  const { users } = state.employeeDetails;

  return {
    currentUser: users[0],
  };
};

const MyProfileWithApi = compose(withUpdateUser)(MyProfileComponent);

export const MyProfile = connect(mapStateToProps)(MyProfileWithApi);
