import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const MyProfileContainer = css({
  fontFamily: '"Open Sans", sans-serif',
  width: '100%',
  height: '100%',
  margin: 30,
  backgroundColor: COLORS.lightGray2,
  marginBottom: 0,
});

export const CardStyles = css({
  display: 'flex',
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  padding: 20,
  backgroundColor: 'white',
  height: 'auto',
  maxWidth: 1270,
  marginRight: 20,
});

export const EmployeeName = css({
  marginTop: '-8px',
  marginBottom: '-8px',
  fontSize: 28,
  lineSpacing: 0,
});

export const EmployeeEmail = css({
  fontSize: 16,
  fontWeight: '600 !important',
});

export const Heading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: '#444444',
  padding: 0,
});

export const SubHeading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 16,
  fontWeight: 800,
  color: '#8d99a3',
});

export const LinkStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  color: COLORS.blue,
  fontSize: 16,
  fontWeight: '600 !important',
});

export const FlexContainer2 = css({
  justifyContent: 'space-between',
  display: 'flex',
  marginBottom: 10,
  width: '100%',
});

export const AvatarStyles = css({
  height: 70,
  width: 70,
  marginRight: 20,
});

export const BlockContainer = css({
  width: '100%',
});

export const IconStyles = css({
  color: `${COLORS.blue} !important`,
});

export const ResendInvite = css({
  fontWeight: '600 !important',
});

export const EditProfileContainer = css({
  float: 'right',
});

export const EditProfile = css({
  fontSize: 14,
});

export const NoPicture = css({
  height: 65,
  width: 65,
  backgroundColor: COLORS.lightGray3,
  '& h3': {
    fontWeight: 400,
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
    lineHeight: '65px',
    marginBottom: 0,
  },
});

export const FlexContainer = css({
  display: 'flex',
  marginBottom: 30,
  marginRight: 20,
});

export const JobItemText = css({
  marginRight: 5,
  fontSize: 16,
  fontweight: 500,
  color: COLORS.lightGray,
  marginBottom: 0,
  '& span': {
    color: COLORS.darkGray,
    fontWeight: 300,
    marginLeft: 5,
  },
});

export const TableStyles = css({
  '& th': {
    backgroundColor: '#fff !important',
  },
  '& span': {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    textAlign: 'center',
  },
  '& p': {
    marginBottom: 0,
  },
  '& td': {
    textAlign: 'center',
  },
});

export const BtnStyles = css({
  backgroundColor: COLORS.lightGray2,
});

export const StatusStyles = css({
  backgroundColor: COLORS.green,
  width: 130,
  height: 30,
  color: '#fff',
  lineHeight: '30px',
  borderRadius: 4,
  margin: 'auto',
});
