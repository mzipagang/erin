import { Row } from 'antd';
import React from 'react';
import {
  departmentContainer,
  departmentText,
  DepartmentsContainer,
} from './department-item.styles';

const DepartmentItem = props => {
  const { departments } = props;
  return (
    <div className={DepartmentsContainer}>
      {departments.map(item => {
        return (
          <div key={item.departmentId}>
            <Row className={departmentContainer}>
              <span className={departmentText}> {item.department} </span>
            </Row>
          </div>
        );
      })}
    </div>
  );
};

export default DepartmentItem;
