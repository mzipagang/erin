import React from 'react';
import { FlexContainer, AvatarStyles, NoPicture } from '../my-profile.styles';

const Avatar = props => {
  const { firstName, lastName, avatar } = props;
  return (
    <div className={FlexContainer}>
      {avatar === null || avatar === '' ? (
        <div className={NoPicture}>
          <h3>
            {firstName[0]} {lastName[0]}
          </h3>
        </div>
      ) : (
        <img
          className={AvatarStyles}
          src={avatar}
          alt={`${firstName} ${lastName}`}
        />
      )}
    </div>
  );
};
export default Avatar;
