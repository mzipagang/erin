import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';
import { cx } from 'emotion';
import {
  IconStyles,
  EditProfileContainer,
  LinkStyles,
  EditProfile,
} from '../my-profile.styles';

const EditProfileLink = props => {
  const { userId } = props;
  return (
    <div className={EditProfileContainer}>
      <Link
        to={`/editprofile/${userId}`}
        className={cx(EditProfile, LinkStyles)}
      >
        <Icon className={IconStyles} type="edit" theme="filled" />
        {'  '}
        EDIT PROFILE
      </Link>
    </div>
  );
};

export default EditProfileLink;
