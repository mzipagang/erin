import React from 'react';
import { Table, Avatar, Button } from 'antd';
import linkedIn from '../../_shared/assets/linkedin-logo.png';
import gmail from '../../_shared/assets/gmail.png';
import outlook from '../../_shared/assets/outlook.png';
import twitter from '../../_shared/assets/twitter.png';
import moment from 'moment';
import { Link } from 'react-router-dom';
import {
  TableStyles,
  BtnStyles,
  StatusStyles,
  LinkStyles,
} from '../my-profile.styles';

class ConnectedApps extends React.Component {
  //render avatar based on account type
  renderAvatar = type => {
    const avatar =
      type === 'linked-in'
        ? linkedIn
        : type === 'gmail'
          ? gmail
          : type === 'outlook'
            ? outlook
            : twitter;
    return <Avatar size={50} shape="square" src={avatar} />;
  };

  //render name based on account type
  renderName = type => {
    const name =
      type === 'linked-in'
        ? 'LinkedIn'
        : type === 'gmail'
          ? 'Google'
          : type === 'outlook'
            ? 'Outlook'
            : 'Twitter';
    return <p style={{ fontWeight: 500 }}>{name}</p>;
  };

  //render button based on account type and whether user has previously synced that account
  renderButton = (synced, type) => {
    const connect = () => {
      return type === 'linked-in'
        ? this.linkedInSync()
        : type === 'gmail'
          ? this.googleSync()
          : type === 'outlook'
            ? this.outlookSync()
            : this.twetterSync();
    };
    return !synced ? (
      <Button className={BtnStyles} onClick={connect}>
        Add Connection
      </Button>
    ) : synced && type === 'linked-in' ? (
      <div className={StatusStyles}>
        <p>Imported</p>
      </div>
    ) : (
      <div>
        <p className={StatusStyles}>Connected</p>
      </div>
    );
  };

  linkedInSync = () => {
    console.log('Linked In');
  };

  googleSync = () => {
    console.log('Google');
  };

  outlookSync = () => {
    console.log('Outlook');
  };

  twitterSync = () => {
    console.log('Twitter');
  };

  render() {
    const { connectedApps } = this.props;
    const columns = [
      {
        render: record => this.renderAvatar(record.type),
      },
      {
        title: 'Name',
        dataIndex: 'type',
        render: type => this.renderName(type),
      },
      {
        title: 'Account',
        dataIndex: 'account',
        render: account => (
          <Link className={LinkStyles} to="#">
            {account}
          </Link>
        ),
      },
      {
        title: 'Date Synced',
        dataIndex: 'dateSynced',
        render: dateSynced =>
          dateSynced !== null ? (
            <div>
              <p>{moment(`${dateSynced}`).format('MMMM Do, YYYY')}</p>
              <Link to="#">ReSync</Link>
            </div>
          ) : null,
      },
      {
        title: 'Status',
        dataIndex: 'synced',
        render: (synced, record) => this.renderButton(synced, record.type),
      },
    ];
    return (
      <div>
        <h3>Connected Apps</h3>
        <Table
          rowKey={record => record.id}
          className={TableStyles}
          pagination={false}
          dataSource={connectedApps}
          columns={columns}
        />
      </div>
    );
  }
}

export default ConnectedApps;
