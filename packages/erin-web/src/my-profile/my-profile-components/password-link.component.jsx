import React from 'react';
import { Link } from 'react-router-dom';
import { LinkStyles } from '../my-profile.styles';

const PasswordLink = props => {
  const { userId } = props;
  return (
    <div style={{ marginBottom: 30 }}>
      <h3 style={{ marginBottom: 5 }}>Password</h3>
      <Link className={LinkStyles} to={`/editprofile/${userId}`}>
        Reset Password
      </Link>
    </div>
  );
};

export default PasswordLink;
