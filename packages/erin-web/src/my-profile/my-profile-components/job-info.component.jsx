import React from 'react';
import DepartmentItem from './department-item.component';
import { JobItemText } from '../my-profile.styles';

const EmployeeJobInfo = props => {
  const { department, title, role, managedDepartments } = props;

  const employeeRole = role => {
    switch (role) {
      case 'employee':
        return 'Employee';
      case 'admin':
        return 'Administrator';
      case 'manager':
        return 'Manager';
      case 'superAdmin':
        return 'Super Adminstrator';
      default:
        return null;
    }
  };
  return (
    <div style={{ marginBottom: 20 }}>
      <h3 className={JobItemText}>
        Department: <span>{department.name}</span>
      </h3>
      <h3 className={JobItemText}>
        Job Title: <span>{title}</span>
      </h3>
      <h3 className={JobItemText}>
        Role: <span>{employeeRole(role)}</span>
      </h3>
      <div style={{ display: 'flex' }}>
        <h3 className={JobItemText}>Manager Permissions:</h3>
        <DepartmentItem departments={managedDepartments} />
      </div>
    </div>
  );
};

export default EmployeeJobInfo;
