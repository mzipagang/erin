import React from 'react';
import { Link } from 'react-router-dom';
import { cx } from 'emotion';
import moment from 'moment';
import {
  EmployeeName,
  Heading,
  LinkStyles,
  EmployeeEmail,
} from '../my-profile.styles';

const EmployeeInfo = props => {
  const { firstName, lastLogin, lastName, emailAddress } = props;
  return (
    <div>
      <h1 className={cx(EmployeeName, Heading)}>
        {firstName} {lastName}
      </h1>
      <Link className={cx(LinkStyles, EmployeeEmail)} to="#">
        {emailAddress}
      </Link>
      {lastLogin ? (
        <p>Last Login: {moment(lastLogin).format('M/D/YYYY')}</p>
      ) : (
        <p>Last Login: Never</p>
      )}
    </div>
  );
};

export default EmployeeInfo;
