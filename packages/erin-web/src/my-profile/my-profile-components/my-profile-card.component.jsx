import React from 'react';

import EmployeeJobInfo from './job-info.component';
import Avatar from './Avatar.component';
import EmployeeInfo from './employee-info.component';

import {
  CardStyles,
  BlockContainer,
  FlexContainer2,
} from '../my-profile.styles';
import EditProfileLink from './edit-profile-link.component';
import PasswordLink from './password-link.component';
import ConnectedApps from './connected-apps.component';

const MyProfileCard = props => {
  const { currentUser } = props;
  const {
    firstName,
    lastName,
    avatar,
    userId,
    emailAddress,
    lastLogin,
    department,
    role,
    title,
    managedDepartments,
    connectedApps,
  } = currentUser;

  return (
    <div className={CardStyles}>
      <Avatar avatar={avatar} firstName={firstName} lastName={lastName} />
      <div className={BlockContainer}>
        <div className={FlexContainer2}>
          <EmployeeInfo
            firstName={firstName}
            lastName={lastName}
            emailAddress={emailAddress}
            lastLogin={lastLogin}
          />
          <EditProfileLink id={userId} />
        </div>
        <EmployeeJobInfo
          role={role}
          managedDepartments={managedDepartments}
          department={department}
          title={title}
        />
        <PasswordLink userId={userId} />
        <ConnectedApps connectedApps={connectedApps} />
      </div>
    </div>
  );
};

export default MyProfileCard;
