import React, { Component } from 'react';

import MyProfileCard from './my-profile-components/my-profile-card.component';
import { MyProfileContainer } from './my-profile.styles';

class MyProfileComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: props.currentUser,
    };
  }

  render() {
    const { currentUser } = this.state;
    return (
      <main className={MyProfileContainer}>
        <MyProfileCard currentUser={currentUser} />
      </main>
    );
  }
}

export default MyProfileComponent;
