import { css } from 'emotion';
import { COLORS } from './_shared/styles/colors';

export const appWideStyles = css({
  // main app container styles

  minHeight: '100vh',
  display: 'flex',
  // alignItems: 'stretch',

  // styles that will be applied across the app
  h1: {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 600,
    color: COLORS.heading,
  },
  h2: {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 700,
    color: COLORS.subHeading,
  },
  h3: {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 600,
  },
  a: {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 300,
    color: COLORS.hyperLink,
  },
});

export const mainContainer = css({
  display: 'inline-block',
  verticalAlign: 'top',
  width: '100%',
});

export const pageContainer = css({
  backgroundColor: COLORS.lightGray2,
  height: '100%',
});
