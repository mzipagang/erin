import ReactDOM from 'react-dom';
import './index.css';
import App from './app.component';
import registerServiceWorker from './registerServiceWorker';
import Amplify from 'aws-amplify';
import { WithApiProvider } from 'erin-app-state-mgmt/src/_shared/api/api-provider.component';

Amplify.configure({
  Auth: {
    region: 'us-east-2',
    userPoolId: 'us-east-2_yaXwkAIm5',
    userPoolWebClientId: '4b7ta75vc05uvtsbqto8cb3c8p',
  },
});

ReactDOM.render(WithApiProvider(App), document.getElementById('root'));
registerServiceWorker();
