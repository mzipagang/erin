import 'antd/dist/antd.css';
import * as React from 'react';
import { addDecorator } from '@storybook/react';
import { appWideStyles } from '../app.styles';

addDecorator(story => <div className={appWideStyles}>{story()}</div>);
