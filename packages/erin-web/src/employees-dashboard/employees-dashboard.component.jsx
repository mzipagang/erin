import React from 'react';
import { Table } from 'antd';
import FilterByDepartment from './employees-dashboard-components/filter-by-department.component';
import SearchByName from './employees-dashboard-components/search-by-name.component';
import AddButton from './employees-dashboard-components/add-button.component';
import { Link } from 'react-router-dom';
import EmployeeModal from './employees-dashboard-components/employees-modal.component';

import {
  paginationStyle,
  toolbarContainer,
  tableContainer,
  employeesDashboardContainer,
  nameStyle,
  emailStyle,
  departmentStyle,
  avatarContainer,
  Avatar,
} from './employees-dashboard.styles';

export class EmployeesDashboardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredData: this.props.users,
      visible: false,
      showInvertedIcon: false,
    };

    this.handleNameSearch = this.handleNameSearch.bind(this);
    this.handleDepartmentFilter = this.handleDepartmentFilter.bind(this);
    this.showModal = this.showModal.bind(this);
  }

  componentDidMount = () => {
    if (this.props.location.pathname === '/employees/form') this.showModal();
  };

  handleDepartmentFilter(value) {
    if (value.length === 0) {
      this.setState({ filteredData: this.props.users });
      return;
    }
    let results = [];
    this.props.users.forEach(record => {
      if (record.department && value.includes(record.department.name)) {
        results.push(record);
      }
    });
    this.setState({ filteredData: results });
  }

  handleNameSearch(value) {
    let results = this.props.users.filter(
      obj =>
        obj.lastName.toLowerCase().includes(value.toLowerCase()) ||
        obj.firstName.toLowerCase().includes(value.toLowerCase()) ||
        obj.emailAddress.toLowerCase().includes(value.toLowerCase()) ||
        obj.title.toLowerCase().includes(value.toLowerCase())
    );
    this.setState({ filteredData: results });
  }

  showModal() {
    this.setState({ visible: true });
  }

  handleSubmit = (emails, fileData) => {
    this.props.inviteEmployees(emails, fileData, this.props.currentUser.id);
  }

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleError = () => {
  };

  showInvertedButton = value => {
    this.setState({
      showInvertedIcon: value,
    });
  };

  render() {
    const sort = (a, b) => {
      if (a > b) return 1;
      if (a < b) return -1;
      return 0;
    };

    const columns = [
      {
        title: '',
        dataIndex: 'avatar',
        key: 'avatar',
        render: (avatar, row) => {
          return (
            <div className={avatarContainer}>
              {avatar === null || avatar === '' ? (
                <div>
                  <h3>
                    {row.firstName[0]} {row.lastName[0]}
                  </h3>
                </div>
              ) : (
                <img
                  src={`http://${avatar.bucket}.s3.amazonaws.com/${avatar.key}`}
                  className={Avatar}
                  alt={`${row.firstName} ${row.lastName}`}
                />
              )}
            </div>
          );
        },
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (name, row) => (
          <Link to={`/employees/${row.id}`} className={nameStyle}>
            {row.firstName} {row.lastName}
          </Link>
        ),
        sorter: (a, b) => sort(a.firstName, b.lastName),
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        render: (email, row) => (
          <div className={emailStyle}>{row.emailAddress}</div>
        ),
        sorter: (a, b) => sort(a.emailAddress, b.emailAddress),
      },
      {
        title: 'Department',
        dataIndex: 'department',
        key: 'department',
        sorter: (a, b) =>
          sort(
            a.department ? a.department.name : '',
            b.department ? b.department.name : ''
          ),
        render: department => (
          <div className={departmentStyle}>{department && department.name}</div>
        ),
      },
      {
        title: 'Role',
        dataIndex: 'role',
        key: 'role',
        render: (role, row) => <div className={emailStyle}>{row.title}</div>,
        sorter: (a, b) => sort(a.title, b.title),
      },
      {
        title: 'Referral',
        dataIndex: 'referral',
        key: 'referral',
        render: (referral, row) => (
          <div className={emailStyle}>{row.totalReferrals}</div>
        ),
        sorter: (a, b) => sort(a.totalReferrals, b.totalReferrals),
      },
      {
        title: 'Last Login',
        dataIndex: 'lastlogin',
        key: 'lastlogin',
        render: (lastlogin, row) => (
          <div className={emailStyle}>{row.lastLogin}</div>
        ),
        sorter: (a, b) => sort(a.lastLogin, b.lastLogin),
      },
    ];

    const { visible } = this.state;

    return (
      <main className={employeesDashboardContainer}>
        <section className={toolbarContainer}>
          <AddButton showModal={this.showModal} />
          <FilterByDepartment
            employees={this.props.users}
            handleDepartmentFilter={this.handleDepartmentFilter}
          />
          <SearchByName handleNameSearch={this.handleNameSearch} />
        </section>
        <section className={tableContainer}>
          <Table
            dataSource={this.state.filteredData}
            columns={columns}
            className={paginationStyle}
          />
        </section>
        <EmployeeModal
          visible={visible}
          error={this.props.error}
          handleSubmit={this.handleSubmit}
          handleCancel={this.handleCancel}
          handleError={this.handleError}
          showInvertedButton={this.showInvertedButton}
          showInvertedIcon={this.showInvertedIcon}
        />
      </main>
    );
  }
}
