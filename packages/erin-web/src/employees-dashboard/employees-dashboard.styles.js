import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const employeesDashboardContainer = css({
  width: '100%',
  height: '100%',
  padding: 30,
  backgroundColor: COLORS.lightGray2,
});

export const tableContainer = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  backgroundColor: 'white',
  '& thead > tr > th': {
    backgroundColor: 'white',
  },
  '& .ant-select-selection-selected-value': {
    fontFamily: '"Open Sans", sans-serif',
  },
});

export const toolbarContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: 30,
});

export const searchStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  '& input': {
    fontFamily: '"Open Sans", sans-serif',
    borderRadius: 20,
    borderColor: COLORS.lightGray,
    paddingLeft: '40px !important',
  },
  '& span': {
    left: '12px !important',
  },
  '& i': {
    color: COLORS.darkGray,
    fontSize: 18,
  },
});

export const selectStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  width: 330,
  '& .ant-select-selection': {
    borderColor: COLORS.lightGray,
  },
  '& .ant-select-selection__choice': {
    backgroundColor: COLORS.lightGreen,
    color: COLORS.green,
  },
});

export const SelectContainer = css({
  '& svg': {
    transform: 'translate(-40px, 2px)',
  },
});

export const paginationStyle = css({
  '.ant-pagination-options': {
    float: 'left',
  },
  '& ul': {
    paddingRight: 20,
  },
});

export const addButton = css({
  marginTop: -12,
  top: 8,
  marginRight: 5,
  backgroundColor: COLORS.red,
  color: COLORS.white,
  height: 40,
  width: 40,
  '&:hover': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
  '&:focus': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
});

export const AddButtonText = css({
  fontSize: 18,
  fontWeight: 600,
});

export const AddButtonIcon = css({
  fontSize: 35,
});

export const nameStyle = css({
  color: COLORS.blue,
  fontSize: 14,
  fontWeight: 500,
});

export const emailStyle = css({
  color: COLORS.black,
  fontSize: 13,
  fontWeight: 400,
});

export const departmentStyle = css({
  color: COLORS.blue,
  fontSize: 13,
  fontWeight: 400,
});

export const filterIcon = css({
  zIndex: 0,
});

export const avatarContainer = css({
  width: '50%',
  display: 'flex',
});

export const Avatar = css({
  height: 50,
  width: 70,
  fontWeight: 'bold',
  backgroundColor: COLORS.lightGray,
  color: 'white',
  fontSize: 28,
  textAlign: 'center',
});
