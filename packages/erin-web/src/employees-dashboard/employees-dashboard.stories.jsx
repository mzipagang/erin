import React from 'react';
import { storiesOf } from '@storybook/react';
import { EmployeesDashboardComponent } from './employees-dashboard.component';
import { userData } from './mu-users.data';
storiesOf('Development/EmployeesDashboardComponent', module).add(
  'Dashboard', () => <EmployeesDashboardComponent employees={userData} />
);
storiesOf('QA/EmployeesDashboardComponent', module).add('Dashboard', () => (
  <EmployeesDashboardComponent employees={userData} />
));
