import { compose } from 'redux';
import { connect } from 'react-redux';
import { actions } from 'erin-app-state-mgmt';
import { EmployeesDashboardComponent } from './employees-dashboard.component';
import { withListUsers } from 'erin-app-state-mgmt/src/_shared/api/components/users/with-list-users.provider';

const { dashboardActions } = actions;

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
    error: state.dashboard.error,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    inviteEmployees(emails, fileData, createdById) {
      dispatch(
        dashboardActions.createInviteEmployeesAction(
          emails,
          fileData,
          createdById
        )
      );
    },
  };
};

const EmployeesDashboardContainer = compose(withListUsers)(
  EmployeesDashboardComponent
);

export const EmployeesDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeesDashboardContainer);
