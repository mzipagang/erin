import React from 'react';
import { Select } from 'antd';
import _ from 'lodash';
import { selectStyle, SelectContainer } from '../employees-dashboard.styles';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

const FilterByDepartment = props => {
  const { handleDepartmentFilter, employees } = props;
  const Option = Select.Option;
  const options = [];
  const keys = employees.map(a => a.department);
  const departments = _.uniq(keys);
  const filteredDepartments = departments.filter(d => d);
  filteredDepartments.forEach(department => {
    options.push(<Option key={department.name}>{department.name}</Option>);
  });
  return (
    <div className={SelectContainer}>
      <Select
        className={selectStyle}
        placeholder="Filter By Department"
        mode="multiple"
        onChange={handleDepartmentFilter}
      >
        {options}
      </Select>
      <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
    </div>
  );
};

export default FilterByDepartment;
