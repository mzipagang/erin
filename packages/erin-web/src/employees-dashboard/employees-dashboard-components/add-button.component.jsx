import React from 'react';
import { Icon, Button } from 'antd';
import {
  addButton,
  AddButtonText,
  AddButtonIcon,
} from '../employees-dashboard.styles';

const AddButton = props => {
  return (
    <div>
      <Button shape="circle" className={addButton} onClick={props.showModal}>
        <Icon className={AddButtonIcon} type="plus" />
      </Button>
      <span className={AddButtonText}> Add Employee </span>
    </div>
  );
};

export default AddButton;
