import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const bodyStyle = css({
  '.ant-modal-content': {
    width: 680,
    borderRadius: 12,
  },
  '& .ant-modal-footer': {
    borderTop: 'none',
  },
  '& .ant-modal-wrap': {
    paddingRight: 70,
  },
  '& .ant-modal-close-x': {
    fontSize: 30,
    color: COLORS.lightGray,
  },
});

export const cardStyle = css({
  width: 640,
  padding: 10,
  minHeight: 200,
  border: 'solid 1px #ddd',
});

export const subtitleStyle = css({
  fontWeight: 600,
  fontSize: 16,
  paddingTop: 5,
  marginBottom: 0,
});

export const modalText1 = css({
  textAlign: 'center',
  fontStyle: 'italic',
  fontSize: 12,
});

export const modalText2 = css({
  textAlign: 'center',
});

export const LinkStyle = css({
  color: COLORS.blue,
  textAlign: 'center',
});

export const buttonStyle = css({
  border: 0,
  borderRadius: 3,
  backgroundColor: COLORS.red,
  color: COLORS.white,
  fontSize: 20,
  width: 120,
  height: 45,
  fontWeight: '300',
  letterSpacing: '2.0px',
});

export const fileWrap = css`
  display: inline-block;
  position: relative;
  font-weight: bold;
  color: ${COLORS.blue};
  label {
    cursor: pointer;
  }
  input {
    opacity: 0;
    width: 0.1px;
  }
`;
