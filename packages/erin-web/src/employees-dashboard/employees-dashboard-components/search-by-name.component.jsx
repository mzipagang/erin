import React from 'react';
import { Input } from 'antd';
import { searchStyle } from '../employees-dashboard.styles';

const SearchByName = props => {
  const { handleNameSearch } = props;
  const Search = Input.Search;
  return (
    <Search
      className={searchStyle}
      placeholder="Search Employees"
      style={{ width: 300 }}
      onSearch={value => handleNameSearch(value)}
    />
  );
};

export default SearchByName;
