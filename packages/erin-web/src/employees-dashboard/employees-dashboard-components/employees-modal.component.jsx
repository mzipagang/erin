import React from 'react';
import { Modal } from 'antd';
import * as ModalStyles from './employees-modal.styles';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

export default class EmployeesModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value: null,
      filename: null,
      error: null
    };
  }

  onTextAreaBlur = e => {
    this.setState({value: e.target.value});
  };

  onFileInputChange = e => {
    if (e.target.files && e.target.files.length) {
      this.setState({filename: e.target.files[0].name});
    }
  };

  submit = e => {
    e.preventDefault();
    this.setState({error: null});
    let users = [];
    let hasFiles = false;
    const data = new FormData();
    try {
      if (this.state && this.state.value) {
        const lines = this.state.value.split(/[\n]+/);
        users = lines.map(l => {
          const tokens = l.split(/[,]/);
          let emailAddress, firstName, lastName, departmentId, title;
          if (tokens.length > 0) {
            emailAddress = tokens[0].trim();
            if (!this.isEmail(emailAddress)) {
              throw new Error(`${emailAddress} is not an email`);
            }
          }
          if (tokens.length > 1) {
            firstName = tokens[1].trim();
          }
          if (tokens.length > 2) {
            lastName = tokens[2].trim();
          }
          if (tokens.length > 3) {
            departmentId = tokens[3].trim();
          }
          if (tokens.length > 4) {
            title = tokens[4].trim();
          }
          return {emailAddress, firstName, lastName, departmentId, title};
        });
      }
      hasFiles = this.uploadInput.files && this.uploadInput.files.length;
      if (hasFiles) {
        data.append('file', this.uploadInput.files[0]);
      }
    } catch (e) {
        this.setState({error: e});
        return;
      }
    this.props.handleSubmit(users, hasFiles ? data : null);
  };

  isEmail = (emailAddress) => {
    var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    var sQuotedPair = '\\x5c[\\x00-\\x7f]';
    var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
    var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
    var sDomain_ref = sAtom;
    var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
    var sWord = '(' + sAtom + '|' + sQuotedString + ')';
    var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
    var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
    var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
    var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

    var reValidEmail = new RegExp(sValidEmail);

    return reValidEmail.test(emailAddress);
  }

  render() {
    const { visible, handleCancel, handleError } = this.props;
    return (
      <Modal
        visible={visible}
        className={ModalStyles.bodyStyle}
        footer={null}
        onCancel={handleCancel}
        handleError={handleError}
      >
        <div>
          <h1>Invite People to BestCo</h1>
          <p className={ModalStyles.subtitleStyle}>Add Employees</p>
          <p>
            Enter one email address per line to send an invite to your employees
          </p>
          <form onSubmit={this.submit}>
            <textarea
              className={ModalStyles.cardStyle}
              type="text"
              onBlur={this.onTextAreaBlur}
            />
            <p className={ModalStyles.modalText1}>
              You can also add additional details in this format:{' '}
              <b>Email, First Name, Last Name, Department, Job Title</b>
            </p>
            <p className={ModalStyles.modalText2}>
              Or{' '}
              <span className={ModalStyles.fileWrap}>
                <label htmlFor="file">click here </label>
                <input type="file" id="file" name="file" ref={(ref) => { this.uploadInput = ref; }} onChange={this.onFileInputChange} />
              </span>
              to upload a .csv file{' '}
              <a
                className={ModalStyles.LinkStyle}
                href="https://res.cloudinary.com/dpoo75fay/raw/upload/v1539894343/erin_employee_import.csv"
              >
                (download a sample .csv)
              </a>
            </p>
            { this.state.filename && <center><p>
              {this.state.filename}
            </p></center> }
            { this.props.error && this.props.error.message && <center><p style={{ color: 'red' }}>
              {this.props.error.message}
            </p></center> }
            { this.state.error && this.state.error.message && <center><p style={{ color: 'red' }}>
              {this.state.error.message}
            </p></center> }
            <div style={{ textAlign: 'right' }}>
              <button type="submit" className={ModalStyles.buttonStyle}>
                <span style={{ marginLeft: '-20px' }}>Invite</span>
                <span
                  style={{ position: 'absolute', marginLeft: 5, marginTop: 4 }}
                >
                  <WebIcon name="email-filled" size={25} color={COLORS.white} />
                </span>
              </button>
            </div>
          </form>
        </div>
      </Modal>
    );
  }
};
