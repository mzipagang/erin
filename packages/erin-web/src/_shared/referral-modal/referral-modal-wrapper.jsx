import React from 'react';
import { Button } from 'antd';

import ReferralModal from 'erin-web/src/_shared/referral-modal/referral-modal.component';
import { COLORS } from '../styles/colors';
import WebIcon from '../components/web-icon.component';
import { ReferralBtn, BtnContainer } from './referral-modal-wrapper.styles';

class ReferralModalWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      job: props.jobData,
      contacts: props.contacts,
      visible: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.contacts !== this.props.contacts) {
      this.setState({ contacts: this.props.contacts });
    }
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  render() {
    const { job, contacts, visible } = this.state;
    return (
      <div className={BtnContainer}>
        <Button className={ReferralBtn} onClick={this.showModal}>
          <div styles={{ marginTop: 3 }}>
            <WebIcon name="user" color={COLORS.white} size={18} />
          </div>
          <span style={{ marginLeft: 5 }}>Refer Someone</span>
        </Button>
        <ReferralModal
          currentUser={this.props.currentUser}
          job={job}
          contacts={contacts}
          visible={visible}
          handleCancel={this.handleCancel}
          onCreateContact={this.props.onCreateContact}
          onCreateReferral={this.props.onCreateReferral}
        />
      </div>
    );
  }
}

export default ReferralModalWrapper;
