import React from 'react';
import { Modal, Icon } from 'antd';
import { cx } from 'emotion';

import WebIcon from '../components/web-icon.component';
import ReferralForm from './referral-modal-form/referral-form.component';
import { COLORS } from '../styles/colors';
import * as ModalStyles from './referral-modal.styles';
import makingReferralGif from '../assets/makingreferral300.gif';

class ReferralModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      autoCompleteResult: [],
      isSubmitting: false,
      newContact: false,
      error: false,
    };
  }

  handleError = () => {
    this.setState({ error: true });
  };

  toggleIsSubmitting = () => {
    this.setState(prevState => ({ isSubmitting: !prevState.isSubmitting }));
  };

  handleContactChange = value => {
    let autoCompleteResult = [];
    if (!value) {
      autoCompleteResult = [];
    } else {
      this.props.contacts.forEach(record => {
        if (
          record.firstName.toLowerCase().includes(value.toLowerCase()) ||
          record.lastName.toLowerCase().includes(value.toLowerCase())
        ) {
          autoCompleteResult.push(record);
        }
      });
    }
    this.setState({ autoCompleteResult });
  };

  handleNewContact = () => {
    this.setState(prevState => ({ newContact: !prevState.newContact }));
  };

  render() {
    const {
      job,
      contacts,
      visible,
      handleCancel,
      onCreateContact,
      onCreateReferral,
      currentUser,
    } = this.props;
    const { autoCompleteResult, newContact, isSubmitting, error } = this.state;
    const referralBonus = JSON.parse(job.referralBonus);
    const formattedCurrency =
      referralBonus && referralBonus.hasBonus
        ? `${referralBonus.amount}`.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        : '0';
    const { location } = job;
    const toggleClass = isSubmitting
      ? ModalStyles.ModalStylesSubmitting
      : ModalStyles.ModalStyles;
    return (
      <Modal
        className={toggleClass}
        onCancel={handleCancel}
        visible={visible}
        footer={null}
      >
        {isSubmitting ? (
          <img
            src={`${makingReferralGif}?loaded=${new Date()}`}
            alt="makingReferral"
          />
        ) : (
          <div>
            <h1 className={ModalStyles.ModalTitle}>Refer Someone</h1>
            <p className={ModalStyles.SmallText}>
              Enter a referral and we&#39;ll send them a link to apply and/or
              contact the hiring manager.
            </p>
            <div className={ModalStyles.JobInfoContainer}>
              <div className={cx(ModalStyles.Flex, ModalStyles.JobInfo)}>
                <div>
                  <h3 className={ModalStyles.JobTitle}>{job.title}</h3>
                  <div className={ModalStyles.Flex}>
                    <div
                      className={cx(
                        ModalStyles.Flex,
                        ModalStyles.addPaddingRight
                      )}
                    >
                      <Icon
                        className={ModalStyles.JobIcon}
                        type="folder"
                        theme="outlined"
                      />
                      <p>{job.department ? job.department.name : ''}</p>
                    </div>
                    <div className={ModalStyles.Flex}>
                      <WebIcon
                        name="placeholder"
                        size={20}
                        color={COLORS.darkGray}
                      />
                      <p>
                        {location && location.city && location.state
                          ? `${location.city}, ${location.state}`
                          : 'Remote'}
                      </p>
                    </div>
                  </div>
                </div>
                <div>
                  <p className={ModalStyles.ReferralBonus}>Referral Bonus:</p>
                  <p className={ModalStyles.Currency}>${formattedCurrency}</p>
                </div>
              </div>
            </div>
            <ReferralForm
              newContact={newContact}
              handleNewContact={this.handleNewContact}
              contacts={contacts}
              handleContactChange={this.handleContactChange}
              autoCompleteResult={autoCompleteResult}
              isSubmitting={isSubmitting}
              toggleIsSubmitting={this.toggleIsSubmitting}
              handleCancel={handleCancel}
              error={error}
              handleError={this.handleError}
              onCreateContact={onCreateContact}
              onCreateReferral={onCreateReferral}
              currentUser={currentUser}
              job={job}
            />
          </div>
        )}
      </Modal>
    );
  }
}

export default ReferralModal;
