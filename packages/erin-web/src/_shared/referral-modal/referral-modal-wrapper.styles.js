import { css } from 'emotion';
import { COLORS } from '../styles/colors';

export const ReferralBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  background: COLORS.red,
  padding: 8,
  fontSize: 14,
  display: 'flex',
  width: '100%',
  height: 40,
  // lineHeight: '25px',
});

export const BtnContainer = css({
  '& .ant-btn: hover': {
    color: COLORS.hyperLink,
    borderColor: COLORS.hyperLink,
  },
  '& button:hover > div > svg > path': {
    fill: COLORS.hyperLink,
  },
  '& button:focus > div > svg > path': {
    fill: COLORS.hyperLink,
  },
});
