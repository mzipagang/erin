import { connect } from 'react-redux';
import { compose } from '../services/utils';
import ReferralModalWrapper from './referral-modal-wrapper';
import { withCreateReferral } from 'erin-app-state-mgmt/src/_shared/api/components/contacts/create-referral.provider';

const mapStateToProps = state => {
  const { currentUser } = state.user;

  return {
    currentUser,
    filter: { users: { contains: currentUser.id } },
  };
};

const ReferralModalWithApi = compose(withCreateReferral)(ReferralModalWrapper);

export const ReferralModal = connect(mapStateToProps)(ReferralModalWithApi);
