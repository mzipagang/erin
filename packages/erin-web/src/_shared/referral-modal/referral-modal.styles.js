import { css } from 'emotion';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

export const ModalStyles = css({
  '& .ant-modal-content': {
    borderRadius: 20,
  },
  fontFamily: 'Open Sans',
  '& .ant-modal-close-x': {
    fontSize: 30,
    color: COLORS.lightGray,
  },
});

export const ModalStylesSubmitting = css({
  '& .ant-modal-content': {
    borderRadius: 20,
    height: 750,
    width: 520,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fontFamily: 'Open Sans',
  '& .ant-modal-close-x': {
    color: 'white',
  },
});

export const ModalTitle = css({
  width: '100%',
  textAlign: 'center',
  color: COLORS.red,
  fontSize: 36,
  marginBottom: 0,
  fontWeight: 600,
});

export const SmallText = css({
  fontSize: 11,
  fontWeight: 600,
  width: '100%',
  textAlign: 'center',
});

export const AddContactsHere = css({
  color: COLORS.lightGray,
  fontWeight: 400,
});

export const JobInfoContainer = css({
  padding: 20,
  paddingBottom: 10,
  backgroundColor: COLORS.lightGreen,
  borderRadius: 8,
  marginBottom: 15,
});

export const JobInfo = css({
  justifyContent: 'space-between',
  marginBottom: 0,
});

export const JobTitle = css({
  fontSize: 18,
  color: COLORS.darkGray,
  marginBottom: 0,
});

export const Currency = css({
  color: COLORS.green,
  fontSize: 26,
  marginBottom: 0,
  textAlign: 'right',
  padding: 0,
});

export const JobIcon = css({
  fontSize: 20,
  color: COLORS.darkGray,
  paddingRight: 3,
});

export const Flex = css({
  display: 'flex',
  marginBottom: 0,
});

export const ReferralBonus = css({
  marginBottom: 0,
});

export const addPaddingRight = css({
  paddingRight: 15,
});

export const ToggleNewContactBtn = css({
  padding: 0,
  border: 'none',
  fontSize: 11,
  color: COLORS.blue,
  fontWeight: 600,
});

export const SubmitBtn = css({
  fontFamily: 'Open Sans',
  backgroundColor: COLORS.red,
  color: 'white',
  fontSize: 20,
  fontWeight: 300,
  padding: '10px 30px',
  height: 'auto',
  border: 'none',
  marginBottom: 0,
});

export const SubmitBtnContainer = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 0,
  '& .ant-form-item-control-wrapper': {
    '@media (max-width: 575px)': {
      width: 'auto',
    },
  },
});

export const CheckIcon = css({
  borderRadius: 100,
  border: '1px solid white',
  padding: 2,
});

export const FormTitle = css({
  width: '100%',
  textAlign: 'center',
  fontWeight: 600,
  marginBottom: 0,
  color: COLORS.darkGray,
  fontSize: 24,
});

export const LabelStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  '& span': {
    fontWeight: 300,
    fontSize: 11,
  },
});

export const TextAreaStyles = css({
  marginBottom: 10,
});

export const InputStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 11,
});

export const AutoCompleteStyles = css({
  fontFamily: '"Open Sans", sans-serif',
});

export const FormItemStyles = css({
  marginBottom: '0 !important',
});

export const RemoveBottomMargin = css({
  '& .ant-form-item': {
    marginBottom: 0,
  },
});

export const SubmitError = css({
  marginBottom: 24,
});

export const LinkStyles = css({
  color: COLORS.blue,
});
