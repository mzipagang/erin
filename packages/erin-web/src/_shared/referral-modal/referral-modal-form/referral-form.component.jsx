import React from 'react';
import { Form, Button, AutoComplete, Input, Icon, Alert } from 'antd';
import * as ModalStyles from '../referral-modal.styles';
import ContactAutoComplete from './contact-autocomplete.component';

class ReferralForm extends React.Component {
  render() {
    const {
      newContact,
      handleNewContact,
      autoCompleteResult,
      handleContactChange,
      toggleIsSubmitting,
      handleCancel,
      handleError,
      error,
      job,
      onCreateContact,
      onCreateReferral,
      currentUser,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { TextArea } = Input;
    const FormItem = Form.Item;
    const AutoCompleteOption = AutoComplete.Option;
    const ContactOptions = autoCompleteResult.map(person => (
      <AutoCompleteOption key={person.id}>
        {`${person.firstName} ${person.lastName}`}
      </AutoCompleteOption>
    ));

    const handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          if (newContact) {
            onCreateContact({
              input: {
                firstName: values.firstName,
                lastName: values.lastName,
                emailAddress: values.emailAddress,
                socialMediaAccounts: null,
                users: [currentUser.id],
              },
            });
          } else {
            onCreateReferral({
              input: {
                companyId: currentUser.companyId,
                contactId: values.userId,
                userId: currentUser.id,
                jobId: job.id,
                status: 'referred',
                note: values.note ? values.note : null,
                message: values.message ? values.message : null,
              },
            });
          }
          toggleIsSubmitting();
          setTimeout(() => {
            toggleIsSubmitting();
            handleCancel();
          }, 6000);
        } else {
          handleError();
        }
      });
    };

    return (
      <Form className={ModalStyles.ModalStyles}>
        <ContactAutoComplete
          ContactOptions={ContactOptions}
          form={this.props.form}
          newContact={newContact}
          handleContactChange={handleContactChange}
          handleNewContact={handleNewContact}
        />
        <FormItem className={ModalStyles.FormItemStyles}>
          {getFieldDecorator('noteToReferral', {
            rules: [],
          })(
            <label className={ModalStyles.LabelStyles}>
              Add a Note <span>(optional)</span>
              <TextArea
                className={ModalStyles.InputStyles}
                placeholder="Personalize the message to your referral"
                rows={5}
              />
            </label>
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('hiringManagerMessage', {
            rules: [],
          })(
            <label className={ModalStyles.LabelStyles}>
              Message the Hiring Manager <span>(optional)</span>
              <TextArea
                className={ModalStyles.InputStyles}
                placeholder="How do you know them, why are they a good fit, etc."
                rows={5}
              />
            </label>
          )}
        </FormItem>
        {error ? (
          <Alert
            className={ModalStyles.SubmitError}
            message="We are having trouble submitting the form, please try again later."
            type="error"
          />
        ) : null}
        <FormItem className={ModalStyles.SubmitBtnContainer}>
          <Button
            onClick={handleSubmit}
            className={ModalStyles.SubmitBtn}
            htmlType="submit"
          >
            Submit Referral
            <Icon className={ModalStyles.CheckIcon} type="check" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(ReferralForm);
