import React from 'react';
import { Button, AutoComplete, Input, Row, Col, Form } from 'antd';
import { Link } from 'react-router-dom';
import { cx } from 'emotion';

import * as ModalStyles from '../referral-modal.styles';

const ContactAutoComplete = props => {
  const {
    newContact,
    handleNewContact,
    handleContactChange,
    ContactOptions,
  } = props;

  const { getFieldDecorator } = props.form;
  const FormItem = Form.Item;

  if (newContact) {
    return (
      <div>
        <h2 className={ModalStyles.FormTitle}>Enter Referral Information</h2>
        <Row type="flex" justify="space-between" gutter={8}>
          <Col span={8}>
            <FormItem className={ModalStyles.FormItemStyles}>
              {getFieldDecorator('firstName', {
                rules: [
                  { required: true, message: 'Please input First Name!' },
                ],
              })(
                <Input
                  className={ModalStyles.RemoveBottomMargin}
                  placeholder="First Name"
                />
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem className={ModalStyles.FormItemStyles}>
              {getFieldDecorator('lastName', {
                rules: [{ required: true, message: 'Please input Last Name!' }],
              })(
                <Input
                  className={ModalStyles.RemoveBottomMargin}
                  placeholder="Last Name"
                />
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem className={ModalStyles.FormItemStyles}>
              {getFieldDecorator('emailAddress', {
                rules: [
                  { type: 'email', message: 'Enter a valid E-mail' },
                  {
                    required: true,
                    message: 'Please input an E-mail Address!',
                  },
                ],
              })(
                <Input
                  className={ModalStyles.RemoveBottomMargin}
                  placeholder="E-mail"
                />
              )}
            </FormItem>
          </Col>
          <p className={ModalStyles.SmallText}>
            or{' '}
            <Button
              className={ModalStyles.ToggleNewContactBtn}
              onClick={handleNewContact}
            >
              click here
            </Button>{' '}
            to add an existing contact
          </p>
        </Row>
      </div>
    );
  } else {
    return (
      <div>
        <h2 className={ModalStyles.FormTitle}>Refer a Contact:</h2>
        <p className={cx(ModalStyles.SmallText, ModalStyles.AddContactsHere)}>
          add your contacts{' '}
          <Link className={ModalStyles.LinkStyles} to="#">
            here
          </Link>
        </p>
        <Row type="flex" justify="center">
          <Col span={16}>
            <FormItem className={ModalStyles.FormItemStyles}>
              {getFieldDecorator('userId', {
                rules: [
                  { required: true, message: 'Please Input Contact Name!' },
                ],
              })(
                <AutoComplete
                  dataSource={ContactOptions}
                  className={ModalStyles.AutoCompleteStyles}
                  placeholder="Start Typing a
          Contact&#39;s Name"
                  onSearch={handleContactChange}
                />
              )}
            </FormItem>
            <p className={ModalStyles.SmallText}>
              or{' '}
              <Button
                className={ModalStyles.ToggleNewContactBtn}
                onClick={handleNewContact}
              >
                click here
              </Button>{' '}
              to enter name and email
            </p>
          </Col>
        </Row>
      </div>
    );
  }
};

export default ContactAutoComplete;
