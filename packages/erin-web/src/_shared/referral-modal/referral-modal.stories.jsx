import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import ReferralModalWrapper from './referral-modal-wrapper';
import { appWideStyles } from '../../app.styles';

import { jobData, contactData } from '../../../../../test/data';

storiesOf('Development/NewReferralModal', module)
  .addDecorator(StoryRouter())
  .add('New Referral Modal', () => (
    <ReferralModalWrapper
      className={appWideStyles}
      contactData={contactData}
      jobData={jobData[0]}
    />
  ));

storiesOf('QA/NewReferralModal', module)
  .addDecorator(StoryRouter())
  .add('New Referral Modal', () => (
    <ReferralModalWrapper
      className={appWideStyles}
      contactData={contactData}
      jobData={jobData[0]}
    />
  ));
