import React from 'react';
import { Route } from 'react-router-dom';
import { RouterContext } from '../../_shared/contexts/router.context';

export const AuthenticatedRoute = props => {
  const { component: Component } = props;
  const rest = Object.assign({}, props);
  delete rest.component;
  return (
    <RouterContext.Consumer>
      {({ auth }) => {
        return (
          <Route
            {...rest}
            render={props => {
              if (!auth) {
                props.history.push('/login');
                return null;
              }
              return <Component {...props} />;
            }}
          />
        );
      }}
    </RouterContext.Consumer>
  );
};
