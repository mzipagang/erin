import React from 'react';
import { Form, Checkbox, Button, Icon, Alert } from 'antd';
import {
  SubmitBtnContainer,
  SubmitBtn,
  CheckIcon,
  CheckBoxStyles,
  FormItemStyles,
  NotHired,
} from './referral-hired-form.styles';

const FormItem = Form.Item;

class ReferralHiredForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    const {
      referral,
      onUpdateReferral,
      job,
      onUpdateJob,
      handleError,
      allReferrals,
      handleCancel,
    } = this.props;
    const otherReferrals = allReferrals.filter(r => r.id !== referral.id);
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        handleError();
        return;
      }
      try {
        onUpdateReferral({
          input: {
            id: referral.id,
            status: 'hired',
          },
        });
        if (values.closeJob) {
          onUpdateJob({
            id: job.id,
            status: 'closed',
            jobType: job.jobType,
          });
        }
        if (values.updateOtherReferrals) {
          otherReferrals.forEach(referral => {
            onUpdateReferral({
              input: {
                id: referral.id,
                status: 'notHired',
              },
            });
          });
        }
        handleCancel();
      } catch (err) {
        console.error(err);
        handleError();
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { error } = this.props;
    return (
      <Form>
        <FormItem className={FormItemStyles}>
          {getFieldDecorator('updateOtherReferrals', {
            valuePropName: 'checked',
          })(
            <Checkbox className={CheckBoxStyles}>
              Update other referrals as{' '}
              <span className={NotHired}>Not Hired</span>
            </Checkbox>
          )}
        </FormItem>
        <FormItem className={FormItemStyles}>
          {getFieldDecorator('closeJob', {
            valuePropName: 'checked',
          })(
            <Checkbox className={CheckBoxStyles}>
              Close this job(no more referrals)
            </Checkbox>
          )}
        </FormItem>
        {error ? (
          <Alert message="There was a problem submitting your changes. Please try again or contact an Administrator" />
        ) : null}
        <FormItem className={SubmitBtnContainer}>
          <Button
            className={SubmitBtn}
            onClick={this.handleSubmit}
            htmlType="submit"
          >
            Submit As Hired
            <Icon className={CheckIcon} type="check" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(ReferralHiredForm);
