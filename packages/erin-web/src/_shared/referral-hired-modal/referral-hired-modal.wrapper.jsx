import React from 'react';
import { Button } from 'antd';

import ReferralHiredModal from './referral-hired-modal.component';

class ReferralHiredModalWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      referrals: this.props.referralData,
      visible: true,
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  render() {
    const { jobData, contactData, visible } = this.state;
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>
          Open Modal
        </Button>
        <ReferralHiredModal
          jobData={jobData}
          contactData={contactData}
          visible={visible}
          handleCancel={this.handleCancel}
        />
      </div>
    );
  }
}

export default ReferralHiredModalWrapper;
