import React from 'react';
import { Modal } from 'antd';

import ReferralHiredForm from './referral-hired-form/referral-hired-form.component';

import {
  ModalTitle,
  SmallText,
  ModalStyles,
  SubTitle,
  FormStyles,
} from './referral-hired-modal.styles';

class ReferralHiredModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleError = () => {
    this.setState({ error: true });
  };

  render() {
    const {
      visible,
      handleCancel,
      job,
      referral,
      onUpdateJob,
      onUpdateReferral,
      allReferrals,
    } = this.props;
    const { error } = this.state;
    return (
      <Modal
        className={ModalStyles}
        onCancel={handleCancel}
        visible={visible}
        footer={null}
      >
        <div>
          <h1 className={ModalTitle}>Great Work!</h1>
          <p className={SmallText}>
            You&#39;re one click away from filling this position.
          </p>
          <h3 className={SubTitle}>Would you like to also:</h3>
          <ReferralHiredForm
            className={FormStyles}
            handleCancel={handleCancel}
            error={error}
            handleError={this.handleError}
            onUpdateJob={onUpdateJob}
            onUpdateReferral={onUpdateReferral}
            job={job}
            referral={referral}
            allReferrals={allReferrals}
          />
        </div>
      </Modal>
    );
  }
}

export default ReferralHiredModal;
