import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import ReferralHiredModalWrapper from './referral-hired-modal.wrapper';

import { referralData } from '../../../../../test/data/mb-data';

storiesOf('Development/ReferralHiredModal', module)
  .addDecorator(StoryRouter())
  .add('Referral Hired Modal', () => (
    <ReferralHiredModalWrapper referrals={referralData} />
  ));

storiesOf('QA/ReferralHiredModal', module)
  .addDecorator(StoryRouter())
  .add('Referral Hired Modal', () => (
    <ReferralHiredModalWrapper referrals={referralData} />
  ));
