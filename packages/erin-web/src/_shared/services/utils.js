import Cookies from 'js-cookie';

export const authHeader = () => {
  const jwt = Cookies.get('jwt');
  return {
    Authorization: `Bearer ${jwt}`,
  };
};

export const normalizeDollar = value => {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const compose = (...funcs) => {
  if (funcs.length === 0) {
    return arg => arg;
  }

  if (funcs.length === 1) {
    return funcs[0];
  }

  return funcs.reduce((a, b) => (...args) => a(b(...args)));
};
