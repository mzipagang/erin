import { connect } from 'react-redux';
import { Aside as AsideComponent } from './aside.component';
import { actions } from 'erin-app-state-mgmt';

const { navigationActions } = actions;

const mapStateToProps = state => {
  return {
    currentUser: state.user.currentUser,
    currentLocation: state.navigation.currentLocation,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updatePathname(pathname) {
      dispatch(navigationActions.createUpdatePathname(pathname));
    },
  };
};

export const Aside = connect(
  mapStateToProps,
  mapDispatchToProps
)(AsideComponent);
