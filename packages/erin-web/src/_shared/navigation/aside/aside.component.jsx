import React from 'react';
import { withRouter } from 'react-router-dom';
import Logo from '../../assets/erinwhite.png';
import { AsideMenuItem } from './aside-menu-item/aside-menu-item.component';
import { asideContainer, logoContainer } from './aside.styles';
import { USER_ROLES } from '../../constants/user-roles.enum';

const AsideItemMeta = {
  DASHBOARD: {
    title: 'Dashboard',
    iconType: 'dashboard',
    link: '/dashboard',
  },
  JOBS: {
    title: 'Manage Jobs',
    iconType: 'id',
    link: '/jobs',
  },
  REFERRALS: {
    title: 'Referrals',
    iconType: 'group',
    link: '/referrals',
  },
  NETWORK: {
    title: 'Network',
    iconType: 'network',
    link: '/network',
  },
  EMPLOYEES: {
    title: 'Employees',
    iconType: 'users',
    link: '/employees',
  },
  SETTINGS: {
    title: 'Settings',
    iconType: 'settings',
    link: '/settings',
  },
};

class BaseAside extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: props.currentLocation,
    };

    this.props.history.listen((location ) => {
      this.setState({ currentLocation: location.pathname});
    });
  }

  componentDidMount() {
    const pathname =
      this.props.location.pathname === '/'
        ? '/dashboard'
        : this.props.location.pathname;
    this.props.updatePathname(pathname);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentLocation !== this.props.currentLocation) {
      this.setState({ currentLocation: this.props.currentLocation });
    }
    if (prevProps.currentUser !== this.props.currentUser) {
      this.setState({ currentUser: this.props.currentUser });
    }
  }

  renderAsideMenuItems = () => {
    const items = [];
    const {
      currentUser: { role },
    } = this.props;
    const { currentLocation } = this.state;
    for (const key of Object.keys(AsideItemMeta)) {
      const { title, iconType, link } = AsideItemMeta[key];
      const isCurrentLocation = currentLocation.startsWith(link);
      const item = (
        <AsideMenuItem
          onMenuItemClick={this.handleLinkClick}
          key={key}
          title={title}
          iconType={iconType}
          link={link}
          active={isCurrentLocation}
        />
      );
      items.push(item);
    }
    if (role === USER_ROLES.MANAGER) {
      return items.slice(0, 4);
    } else if (role === USER_ROLES.EMPLOYEE) {
      return items[0];
    } else {
      return items;
    }
  };

  handleLinkClick = location => {
    this.props.updatePathname(location);
  };

  render() {
    return (
      <aside className={asideContainer}>
        <div className={logoContainer}>
          <img src={Logo} alt="erin" />
        </div>
        {this.renderAsideMenuItems()}
      </aside>
    );
  }
}

export const Aside = withRouter(BaseAside);
