import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { Aside } from './aside.component';
import { userData } from '../../../../../../test/data';

storiesOf('Development/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Aside', () => <Aside currentUser={userData[0]} />);

storiesOf('QA/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Aside', () => <Aside currentUser={userData[0]} />);
