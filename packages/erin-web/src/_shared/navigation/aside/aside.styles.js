import { css } from 'emotion';
import { COLORS } from '../../styles/colors';

export const asideContainer = css({
  display: 'inline-block',
  width: 260,
  minWidth: 260,
  backgroundColor: COLORS.darkBlue,
});

export const logoContainer = css({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: COLORS.darkBlue,
  height: 71,
  img: {
    width: 200,
  },
});
