import { css } from 'emotion';
import { COLORS } from '../../../../_shared/styles/colors';

export const asideMenuItemContainer = isActive =>
  css({
    display: 'flex',
    alignItems: 'center',
    backgroundColor: isActive ? COLORS.lightGray : COLORS.darkBlue,
    height: 75,
    color: COLORS.white,
    fontSize: 18,
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 300,
    borderLeft: `solid 7px ${isActive ? COLORS.red : COLORS.darkBlue}`,
    '&:hover': {
      borderLeft: `solid 7px ${COLORS.red}`,
      backgroundColor: COLORS.lightGray,
    },
  });

export const textContainer = css({
  marginLeft: 30,
  display: 'flex',
  alignItems: 'center',
  '& i': {
    marginRight: 20,
  },
});

export const titleStyle = css({
  position: 'relative',
  left: 15,
});

export const LinkStyles = css({
  textDecoration: 'none !important',
});

export const IconContainer = css({
  paddingTop: 5,
});
