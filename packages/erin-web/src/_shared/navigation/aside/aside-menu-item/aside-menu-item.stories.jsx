import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { AsideMenuItem } from './aside-menu-item.component';

storiesOf('Development/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Aside Menu Item', () => (
    <AsideMenuItem title={'Dashboard'} iconType="dashboard" link={'/test'} />
  ))
  .add('Aside Menu Item - Active', () => (
    <AsideMenuItem
      title={'Dashboard'}
      iconType="dashboard"
      active
      link={'/test'}
    />
  ));
