import React from 'react';
import { Link } from 'react-router-dom';
import WebIcon from '../../../components/web-icon.component';
import { COLORS } from '../../../styles/colors';
import {
  asideMenuItemContainer,
  textContainer,
  titleStyle,
  LinkStyles,
  IconContainer,
} from './aside-menu-item.styles';

export class AsideMenuItem extends React.Component {
  handleMenuItemClick = () => {
    const { link, onMenuItemClick } = this.props;
    onMenuItemClick(link);
  };

  render() {
    const { title, iconType, active, link } = this.props;
    return (
      <Link
        className={LinkStyles}
        text={title}
        to={{
          pathname: link,
        }}
        onClick={this.handleMenuItemClick}
      >
        <div className={asideMenuItemContainer(active)}>
          <div className={textContainer}>
            <div className={IconContainer}>
              <WebIcon name={iconType} size={30} color={COLORS.white} />
            </div>
            <span className={titleStyle}>{title}</span>
          </div>
        </div>
      </Link>
    );
  }
}
