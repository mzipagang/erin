import React from 'react';
import StoryRouter from 'storybook-react-router';
import { userData } from '../../../../../../test/data';

import { storiesOf } from '@storybook/react';
import { Header } from './header.component';

storiesOf('Development/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Header', () => <Header currentUser={userData[0]} />);

storiesOf('QA/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Header', () => <Header currentUser={userData[0]} />);
