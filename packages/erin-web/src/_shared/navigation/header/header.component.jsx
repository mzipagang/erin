import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  headerContainer,
  menuListContainer,
  FlexContainer,
} from './header.styles';
import { UserProfile } from './user-profile/user-profile.component';
import { HeaderMenuItem } from './header-menu-item/header-menu-item.component';

const HeaderItemMeta = {
  BROWSE: {
    title: 'Browse Jobs',
    link: '/browsejobs',
  },
  REFERRALS: {
    title: 'My Referrals',
    link: '/myreferrals',
  },
  NETWORK: {
    title: 'Contacts',
    link: '/mycontacts',
  },
};

class BaseHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: props.currentLocation,
      currentUser: props.currentUser,
    };
    this.props.history.listen((location ) => {
      this.setState({ currentLocation: location.pathname});
    });
  }

  componentDidMount() {
    const pathname =
      this.props.location.pathname === '/'
        ? '/dashboard'
        : this.props.location.pathname;
    this.props.updatePathname(pathname);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentLocation !== this.props.currentLocation) {
      this.setState({ currentLocation: this.props.currentLocation });
    }
    if (prevProps.currentUser !== this.props.currentUser) {
      this.setState({ currentUser: this.props.currentUser });
    }
  }

  renderMenuItems = () => {
    const items = [];
    const { currentLocation } = this.state;
    for (const key of Object.keys(HeaderItemMeta)) {
      const { title, link } = HeaderItemMeta[key];
      const isCurrentLocation = currentLocation.startsWith(link);
      const item = (
        <HeaderMenuItem
          onMenuItemClick={this.handleLinkClick}
          key={key}
          title={title}
          link={link}
          active={isCurrentLocation}
        />
      );
      items.push(item);
    }
    const menu = <ul className={menuListContainer}>{items}</ul>;
    return menu;
  };

  handleLinkClick = location => {
    this.props.updatePathname(location);
  };

  render() {
    const { currentUser } = this.state;
    return (
      <nav className={headerContainer}>
        <div className={FlexContainer}>
          {this.renderMenuItems()}
          <UserProfile currentUser={currentUser} signOut={this.props.signOut} />
        </div>
      </nav>
    );
  }
}

export const Header = withRouter(props => <BaseHeader {...props} />);
