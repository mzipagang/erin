import { css } from 'emotion';
import { COLORS } from '../../../styles/colors';

export const avatarImage = css({
  border: '1px solid',
  borderColor: COLORS.lightGray,
  width: 50,
  height: 50,
});

export const avatarNoImage = css({
  background: COLORS.lightGray3,
  width: 50,
  height: 50,
  color: 'white',
  fontSize: 24,
  lineHeight: '50px',
  marginBottom: 0,
  '& .ant-avatar-string': {
    lineHeight: '50px',
  },
});

export const companyName = css({
  margin: '13px 20px',
  color: COLORS.darkGray,
  fontSize: 18,
  fontWeight: 600,
  minWidth: 100,
  textAlign: 'center',
});

export const menuColor = css({
  color: COLORS.blue,
  cursor: 'pointer',
  marginRight: 62,
});

export const navPosition = css({
  paddingTop: 10,
  display: 'flex',
  marginBottom: 10,
  whiteSpace: 'nowrap',
});
