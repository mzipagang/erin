import { Avatar, Popover } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
import { Auth } from 'aws-amplify';
import {
  avatarNoImage,
  avatarImage,
  companyName,
  navPosition,
  menuColor,
} from './user-profile.styles';
import { WebNotifications } from '../../../../web-notifications';

export class UserProfile extends React.Component {
  handleSignOut = () => {
    const { signOut } = this.props;
    Cookies.remove('jwt');
    localStorage.removeItem('currentUser');
    Auth.signOut();
    signOut();
  };

  render() {
    const { currentUser } = this.props;
    const menu = (
      <div className={menuColor}>
        <p>
          <Link className={menuColor} to={'/myprofile'}>
            My Profile
          </Link>
        </p>
        <p>Help & Support</p>
        <p>Privacy Policy</p>
        <p onClick={this.handleSignOut}>Sign Out</p>
      </div>
    );

    return (
      <div className={navPosition}>
        <div style={{ marginTop: 7 }}>
          <WebNotifications />
        </div>
        <div className={companyName}>{`${currentUser.company.name}`}</div>
        <Popover placement="bottomRight" content={menu} trigger="hover">
          {!currentUser.avatar && (
            <Avatar className={avatarNoImage}>
              {`${currentUser.firstName[0]}${currentUser.lastName[0]}`}
            </Avatar>
          )}
          {currentUser.avatar && (
            <Avatar className={avatarImage} src={currentUser.avatar} />
          )}
        </Popover>
      </div>
    );
  }
}
