import { css } from 'emotion';
import { COLORS } from '../../styles/colors';

export const headerContainer = css({
  borderBottom: `1px solid ${COLORS.gray}`,
  height: 71,
});

export const FlexContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
  maxWidth: 1320,
  paddingRight: 20,
});

export const menuListContainer = css({
  display: 'flex',
  listStyle: 'none',
  margin: 0,
  padding: 0,
  height: 70,
});
