import { connect } from 'react-redux';
import { Header as HeaderComponent } from './header.component';
import { actions } from 'erin-app-state-mgmt';

const { userActions, navigationActions } = actions;

const mapStateToProps = state => {
  return {
    currentUser: state.user.currentUser,
    currentLocation: state.navigation.currentLocation,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signOut(token) {
      dispatch(userActions.signOut(token));
      window.location = '/login';
    },
    updatePathname(pathname) {
      dispatch(navigationActions.createUpdatePathname(pathname));
    },
  };
};

export const Header = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderComponent);
