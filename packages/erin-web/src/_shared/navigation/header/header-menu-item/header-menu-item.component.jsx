import React from 'react';
import { Link } from 'react-router-dom';
import {
  headerMenuItemContainer,
  itemLinkContainer,
} from './header-menu-item.styles';

export class HeaderMenuItem extends React.Component {
  handleMenuItemClick = () => {
    const { link, onMenuItemClick } = this.props;
    onMenuItemClick(link);
  };

  render() {
    const { title, active, link } = this.props;
    return (
      <li className={headerMenuItemContainer(active)}>
        <Link
          text={title}
          to={link}
          onClick={this.handleMenuItemClick}
          className={itemLinkContainer}
        >
          <h2>{title}</h2>
        </Link>
      </li>
    );
  }
}
