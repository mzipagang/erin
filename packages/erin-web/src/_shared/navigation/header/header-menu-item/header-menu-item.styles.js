import { css } from 'emotion';
import { COLORS } from '../../../styles/colors';

export const headerMenuItemContainer = isActive =>
  css({
    height: 70,
    width: 225,
    textAlign: 'center',
    h2: {
      ...(isActive ? { color: COLORS.darkGray } : {}),
      margin: 0,
      fontSize: 16,
      fontFamily: '"Open Sans", sans-serif',
      fontWeight: 400,
    },
    borderBottom: `solid 7px ${isActive ? COLORS.red : COLORS.white}`,
    '&:hover': {
      borderBottom: `solid 7px ${COLORS.red}`,
    },
  });

export const itemLinkContainer = css({
  width: '100%',
  height: '100%',
  paddingTop: 7,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  textDecoration: 'none !important',
});
