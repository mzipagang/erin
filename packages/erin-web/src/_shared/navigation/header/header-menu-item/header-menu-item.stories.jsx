import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { HeaderMenuItem } from './header-menu-item.component';

storiesOf('Development/Navigation', module)
  .addDecorator(StoryRouter())
  .add('Header Menu Item', () => (
    <HeaderMenuItem title={'Browse Jobs'} link={'/test'} />
  ))
  .add('Header Menu Item - Active', () => (
    <HeaderMenuItem title={'Browse Jobs'} active link={'/test'} />
  ));
