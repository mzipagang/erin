export const NOTIFICATIONS = {
  newJob: 'New Job',
  newReferral: 'New Referral',
  acceptedReferral: 'Accepted Referral',
  hiredReferral: 'Hired Referral',
  requestedReferral: 'Requested Referral',
  contactMatches: 'Contact Matches',
};
