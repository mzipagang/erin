export const REFERRAL_STATUS = {
  referred: 'Referred',
  accepted: 'Accepted',
  interviewing: 'Interviewing',
  hired: 'Hired',
  notHired: 'Not Hired',
};
