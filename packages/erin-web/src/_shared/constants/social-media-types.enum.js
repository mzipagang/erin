export const SOCIAL_MEDIA_TYPES = {
  EMAIL: 'email',
  FACEBOOK: 'facebook',
  LINKEDIN: 'linkedin',
  TWITTER: 'twitter',
};
