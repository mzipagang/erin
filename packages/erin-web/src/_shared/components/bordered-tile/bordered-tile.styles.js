import { css } from 'emotion';

export const borderedTileContainerClass = css({
  cursor: 'pointer',
  width: 310,
  margin: 20,
  background: 'white',
  padding: 20,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
});
