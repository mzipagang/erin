import React from 'react';
import { borderedTileContainerClass } from './bordered-tile.styles';

export function borderedTile(WrappedComponent) {
  return class BorderedTile extends React.Component {
    render() {
      return (
        <div className={borderedTileContainerClass}>
          <WrappedComponent {...this.props} />
        </div>
      );
    }
  };
}
