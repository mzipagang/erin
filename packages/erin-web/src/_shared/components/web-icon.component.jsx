import React from 'react';
import { css } from 'emotion';
import { ICONS } from 'erin-shared-resources/icons';

const WebIcon = props => {
  const iconClass = css({
    fill: props.color,
  });

  return (
    <svg
      width={`${props.size}px`}
      height={`${props.size}px`}
      viewBox="0 0 1000 1000"
    >
      <path className={iconClass} d={ICONS[props.name]} />
    </svg>
  );
};

WebIcon.defaultProps = {
  size: 16,
};

export default WebIcon;
