import { css } from 'emotion';

import { COLORS } from '../../styles/colors';

export const shareBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  background: COLORS.blue,
  fontSize: 14,
  marginRight: 10,
  height: 40,
  padding: 7,
  display: 'flex',
  justifyContent: 'center',
  lineHeight: '25px',
  width: 138,
});

export const BtnContainer = css({
  '& .ant-btn: hover': {
    color: COLORS.hyperLink,
    borderColor: COLORS.hyperLink,
  },
  '& button:hover > div > svg > path': {
    fill: COLORS.hyperLink,
  },
  '& button:focus > div > svg > path': {
    fill: COLORS.hyperLink,
  },
});

export const socialList = css({
  listStyleType: 'none',
  padding: 0,
  marginTop: 20,
  marginRight: 40,
});

export const socialItem = css({
  marginBottom: 6,
});

export const shareLabel = css({
  padding: '4px 10px',
  color: 'white',
});

export const socialButtonFB = css({
  padding: 0,
  width: 215,
  background: '#3B5998',
});

export const socialButtonLin = css({
  padding: 0,
  width: 215,
  background: '#0077B5',
});

export const socialButtonTwitter = css({
  padding: 0,
  width: 215,
  background: '#1DA1F2',
});

export const emailButton = css({
  padding: 0,
  width: 215,
  background: '#7F7F7F',
});

export const btnContainer = css({
  height: '100%',
  display: 'flex',
});
