import React from 'react';
import { Button, Modal, Input } from 'antd';
import {
  shareBtn,
  shareLabel,
  socialList,
  socialItem,
  socialButtonFB,
  socialButtonLin,
  socialButtonTwitter,
  emailButton,
  BtnContainer,
} from './share-send.styles';
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  EmailIcon,
} from 'react-share';
import WebIcon from '../web-icon.component';
import { COLORS } from '../../styles/colors';

export class ShareSendButton extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    // const {
    //   job: {
    //     jobUrl,
    //     jobId,
    //     title,
    //     jobType,
    //     description,
    //     location: { city, state },
    //   },
    // } = this.props;

    const {
      title,
      jobUrl,
      jobId,
      jobType,
      description,
      city,
      state,
    } = this.props;
    const location = city && state ? `${city}, ${state}` : 'Remote';
    return (
      <div className={BtnContainer}>
        <Button className={shareBtn} onClick={this.showModal}>
          <div style={{ paddingTop: 3 }}>
            <WebIcon name="share" color={COLORS.white} size={18} />
            {/* <Icon type="anticon anticon-fork" className={orientFork} /> */}
          </div>
          <span style={{ marginLeft: 10 }}>Share</span>
        </Button>

        <Modal
          title="Share Job"
          visible={this.state.visible}
          footer={null}
          onCancel={this.handleCancel}
        >
          <Input addonBefore="Job link" defaultValue={`${jobUrl}/${jobId}`} />
          <div style={{ display: 'flex' }}>
            <ul className={socialList}>
              <li className={socialItem}>
                <button className={socialButtonFB}>
                  <FacebookShareButton
                    url={`${jobUrl}${jobId}`}
                    quote={`Now Hiring: ${jobType} ${title} in ${location}`}
                  >
                    <div style={{ display: 'flex' }}>
                      <FacebookIcon size={30} round={false} />
                      <div className={shareLabel}>Share on Facebook</div>
                    </div>
                  </FacebookShareButton>
                </button>
              </li>
              <li className={socialItem}>
                <button className={socialButtonLin}>
                  <LinkedinShareButton
                    url={`${jobUrl}${jobId}`}
                    title={`Now Hiring: ${jobType} ${title} in ${location}`}
                    windowWidth={750}
                    windowHeight={600}
                  >
                    <div style={{ display: 'flex' }}>
                      <LinkedinIcon size={30} round={false} />
                      <div className={shareLabel}>Share on Linkedin</div>
                    </div>
                  </LinkedinShareButton>
                </button>
              </li>
            </ul>
            <ul className={socialList}>
              <li className={socialItem}>
                <button className={socialButtonTwitter}>
                  <TwitterShareButton
                    url={`${jobUrl}${jobId}`}
                    title={`Now Hiring: ${jobType} ${title} in ${location}`}
                  >
                    <div style={{ display: 'flex' }}>
                      <TwitterIcon size={30} round={false} />
                      <div className={shareLabel}>Share on Twitter</div>
                    </div>
                  </TwitterShareButton>
                </button>
              </li>
              <li className={socialItem}>
                <button className={emailButton}>
                  <EmailShareButton
                    url={`${jobUrl}${jobId}`}
                    subject={`Now Hiring: ${jobType} ${title} in ${location}`}
                    body={description}
                  >
                    <div style={{ display: 'flex' }}>
                      <EmailIcon size={30} round={false} />
                      <div className={shareLabel}>Share via Email</div>
                    </div>
                  </EmailShareButton>
                </button>
              </li>
            </ul>
          </div>
        </Modal>
      </div>
    );
  }
}
