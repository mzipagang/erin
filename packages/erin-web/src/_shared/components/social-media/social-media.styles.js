import { css } from 'emotion';

export const IconStyles = css({
  width: 23,
  paddingRight: 3,
  opacity: 0.7,
});
