import React from 'react';

import WebIcon from '../web-icon.component';
import { COLORS } from '../../styles/colors';

const SocialMedia = props => {
  const { email, socialMedia } = props;
  const renderSocialMedia = () => {
    //TODO: remove JSON.parse when socialMedia data is fixed
    return JSON.parse(socialMedia).map(record => {
      if (record.network === 'linkedin' && record.link !== null) {
        return (
          <a
            key={record.profile}
            style={{ padding: '0 4px' }}
            href={`${record.profile}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <WebIcon
              name="linked-in"
              size={props.size || 23}
              color={COLORS.darkGray}
            />
          </a>
        );
      } else if (record.network === 'facebook' && record.link !== null) {
        return (
          <a
            key={record.profile}
            style={{ padding: '0 4px' }}
            href={`${record.profile}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <WebIcon
              name="facebook-logo"
              size={props.size || 23}
              color={COLORS.darkGray}
            />
          </a>
        );
      } else if (record.network === 'twitter' && record.profile !== null) {
        return (
          <a
            key={record.profile}
            style={{ padding: '0 4px' }}
            href={`${record.profile}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <WebIcon
              name="twitter"
              size={props.size || 23}
              color={COLORS.darkGray}
            />
          </a>
        );
      } else return null;
    });
  };

  return (
    <div>
      {email !== undefined ? (
        <a
          href={`mailto:${email}`}
          style={{
            padding: '0 4px',
            position: 'relative',
            top: 3.5,
          }}
        >
          <WebIcon name="email-filled" size={23} color={COLORS.darkGray} />
        </a>
      ) : null}
      {socialMedia ? renderSocialMedia() : null}
    </div>
  );
};

export default SocialMedia;
