import React from 'react';
import { storiesOf } from '@storybook/react';
import { referralData } from '../../../../../../test/data/mb-data';
import SocialMedia from './social-media.component';

storiesOf('Development/Social Media Component', module).add(
  'Social Media Component',
  () => (
    <SocialMedia
      email={referralData[0].email}
      socialMedia={referralData[0].socialMedia}
    />
  )
);

storiesOf('QA/Social Media Component', module).add(
  'Social Media Component',
  () => (
    <SocialMedia
      email={referralData[0].email}
      socialMedia={referralData[0].socialMedia}
    />
  )
);
