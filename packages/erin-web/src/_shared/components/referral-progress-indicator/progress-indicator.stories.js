import React from 'react';

import { storiesOf } from '@storybook/react';
import { ProgressIndicator } from './progress-indicator.component';

storiesOf('Development/Progress Indicator', module).add(
  'Progress Indicator',
  () => <ProgressIndicator page={0} type="job" />
);
