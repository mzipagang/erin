import React from 'react';
import { Modal, Form, Input, Alert, Button, Icon } from 'antd';
import { Auth } from 'aws-amplify';
import {
  ModalContainer,
  ModalTitle,
  SubmitBtn,
  SubmitBtnContainer,
  CheckIcon,
  AlertError,
  FormStyles,
} from './reset-password-modal.styles';

const FormItem = Form.Item;

class ResetPasswordModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      matchError: false,
      lengthError: false,
      newError: false,
      submitError: false,
      oldPasswordError: false,
      success: false,
    };
  }

  removeAlerts = () => {
    this.setState({
      matchError: false,
      lengthError: false,
      newError: false,
      submitError: false,
      oldPasswordError: false,
      success: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (values.password !== values.confirmPassword) {
        this.setState({ matchError: true });
        setTimeout(() => {
          this.props.form.resetFields();
          this.removeAlerts();
        }, 1000);
        return;
      } else if (values.password === values.oldPassword) {
        this.setState({ newError: true });
        setTimeout(() => {
          this.props.form.resetFields();
          this.removeAlerts();
        }, 1000);
        return;
      } else if (values.password.length < 8) {
        setTimeout(() => {
          this.props.form.resetFields();
          this.removeAlerts();
        }, 1000);
        this.setState({ lengthError: true });
        return;
      } else if (err) {
        this.setState({ submitError: true });
        setTimeout(() => {
          this.props.form.resetFields();
          this.removeAlerts();
        }, 1000);
        return;
      }
      Auth.currentAuthenticatedUser()
        .then(user => {
          return Auth.changePassword(user, values.oldPassword, values.password);
        })
        .then(() => {
          this.setState({ success: true });
          setTimeout(() => {
            this.props.form.resetFields();
            this.removeAlerts();
          }, 1000);
        })
        .catch(err => {
          if (err.code === 'NotAuthorizedException') {
            this.setState({ oldPasswordError: true });
          } else {
            this.setState({ submitError: true });
          }
          setTimeout(() => {
            this.props.form.resetFields();
            this.removeAlerts();
          }, 1000);
          console.error(err);
        });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, handleCancel } = this.props;
    const {
      matchError,
      lengthError,
      submitError,
      newError,
      oldPasswordError,
      success,
    } = this.state;
    return (
      <Modal
        destroyOnClose={true}
        visible={visible}
        onCancel={handleCancel}
        footer={null}
      >
        <div className={ModalContainer}>
          <h1 className={ModalTitle}>Reset Employee Password</h1>
          <Form className={FormStyles}>
            <FormItem>
              {getFieldDecorator('oldPassword', {
                rules: [
                  {
                    required: true,
                    message: 'Please input a new password!',
                  },
                ],
              })(<Input type="password" placeholder="Enter Old Password" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Please input a new password!',
                  },
                ],
              })(<Input type="password" placeholder="Enter New Password" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('confirmPassword', {
                rules: [
                  {
                    required: true,
                    message: 'Please re-enter your new password!',
                  },
                ],
              })(<Input type="password" placeholder="Re-enter New Password" />)}
            </FormItem>
            {matchError ? (
              <Alert
                className={AlertError}
                type="error"
                message="Passwords Do Not Match! Please Try Again."
              />
            ) : null}
            {newError ? (
              <Alert
                className={AlertError}
                type="error"
                message="New password can not be the same as current password."
              />
            ) : null}
            {lengthError ? (
              <Alert
                className={AlertError}
                type="error"
                message="New password must be at least 6 characters long."
              />
            ) : null}
            {submitError ? (
              <Alert
                className={AlertError}
                message="We are having trouble submitting the form, please try again later."
                type="error"
              />
            ) : null}
            {oldPasswordError ? (
              <Alert
                className={AlertError}
                type="error"
                message="Old password is incorrect. Please try again."
              />
            ) : null}
            {success ? (
              <Alert
                className={AlertError}
                message="Password successfully changed!"
                type="success"
              />
            ) : null}

            <FormItem className={SubmitBtnContainer}>
              <Button
                onClick={this.handleSubmit}
                className={SubmitBtn}
                htmlType="submit"
              >
                Reset Password
                <Icon className={CheckIcon} type="check" />
              </Button>
            </FormItem>
          </Form>
        </div>
      </Modal>
    );
  }
}

export default Form.create()(ResetPasswordModal);
