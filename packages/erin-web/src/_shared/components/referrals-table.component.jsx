import React from 'react';
import { Table } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { cx } from 'emotion';
import SocialMedia from '../components/social-media/social-media.component';

import {
  NameStyles,
  LinkStyles,
  TableStyles,
  StatusStyles,
  StatusBold,
  StatusItalic,
} from './referrals-table.styles';

class ReferralsTable extends React.Component {
  renderStatus(status) {
    if (status === 'hired') {
      return <p className={cx(StatusStyles, StatusBold)}>Hired</p>;
    } else if (status === 'notHired') {
      return <p className={cx(StatusStyles, StatusItalic)}>Not Hired</p>;
    } else if (status === 'referred') {
      return <p className={StatusStyles}>Referred</p>;
    } else if (status === 'accepted') {
      return <p className={StatusStyles}>Accepted</p>;
    } else if (status === 'interviewing') {
      return <p className={StatusStyles}>Interviewing</p>;
    }
  }

  render() {
    const { filteredData, sortByAlph } = this.props;
    const columns = [
      {
        title: 'Name',
        dataIndex: 'contact',
        render: record => (
          <Link to="#" className={NameStyles}>
            {record.firstName} {record.lastName}
          </Link>
        ),
        sorter: (a, b) =>
          sortByAlph(
            a.contact.lastName + a.contact.firstName,
            b.contact.lastName + b.contact.firstName
          ),
      },
      {
        title: 'Info',
        render: record =>
          record.contact !== null ? (
            <SocialMedia
              email={record.contact.emailAddress}
              socialMedia={record.contact.socialMediaAccounts}
            />
          ) : null,
      },
      {
        title: 'Job',
        dataIndex: 'job',
        sorter: (a, b) => sortByAlph(a.job.title, b.job.title),
        render: job =>
          job !== null ? (
            <Link className={LinkStyles} to={`/jobs/${job.id}`}>
              {job.title}
            </Link>
          ) : null,
      },
      {
        title: 'Date Referred',
        dataIndex: 'referralDate',
        sorter: (a, b) => moment(a.referralDate) - moment(b.referralDate),
        render: referralDate =>
          referralDate !== null
            ? moment(referralDate).format('M/D/YYYY')
            : null,
      },
      {
        title: 'Referred By',
        dataIndex: 'user',
        sorter: (a, b) =>
          sortByAlph(
            a.user.lastName + a.user.firstName,
            b.user.lastName + b.user.firstName
          ),
        render: referredBy =>
          referredBy !== null ? (
            <Link className={LinkStyles} to={`/employees/${referredBy.id}`}>
              {referredBy.firstName} {referredBy.lastName}
            </Link>
          ) : null,
      },
      {
        title: 'Status',
        dataIndex: 'status',
        sorter: (a, b) => sortByAlph(a.status, b.status),
        render: status => (status !== null ? this.renderStatus(status) : null),
      },
    ];
    return (
      <Table
        pagination={{ pageSize: 50 }}
        rowKey={record => record.id}
        className={TableStyles}
        dataSource={filteredData}
        columns={columns}
      />
    );
  }
}

export default ReferralsTable;
