import React from 'react';
import { Row, Col, Button, Icon, Card, Dropdown, Menu } from 'antd';
import moment from 'moment';
import {
  cardMainStyles,
  candidateNameStyles,
  referrerNameStyles,
  miscTextStyles,
  statusStyles,
  dropdownMenuItem,
  dropdownButton,
} from './referral-card.styles';
import { ProgressIndicator } from '../referral-progress-indicator/progress-indicator.component';
import ReferralHiredModal from '../../referral-hired-modal/referral-hired-modal.component';
import SocialMedia from '../social-media/social-media.component';
import { REFERRAL_STATUS } from '../../constants/referral-statuses.enum';

export class ReferralCard extends React.Component {
  state = {
    hiredModalVisible: false,
  };

  handleUpdateStatus = status => {
    const { referral } = this.props;
    if (status === 'hired') {
      this.setState({ hiredModalVisible: true });
    } else {
      this.props.onUpdateReferral({
        input: {
          id: referral.id,
          contactId: referral.contactId,
          jobId: referral.jobId,
          userId: referral.userId,
          status: status,
        },
      });
    }
  };

  hideHiredModal = () => {
    this.setState({ hiredModalVisible: false });
  };

  render() {
    const { referral, allReferrals, currentJob } = this.props;
    const { status, contact, referralDate, user } = referral;

    return (
      <Card
        hoverable
        headStyle={{ height: 100, paddingRight: 15, paddingLeft: 15 }}
        bodyStyle={{ padding: 15 }}
        className={cardMainStyles}
        title={
          <Row type="flex" justify="space-between">
            <Col>
              <span className={candidateNameStyles}>
                {contact.firstName} {contact.lastName}
              </span>
              <Row>
                <SocialMedia
                  email={contact.emailAddress}
                  socialMedia={contact.socialMediaAccounts}
                />
              </Row>
            </Col>

            <Col>
              <span className={miscTextStyles}> Referred By </span>
              <br />
              <span className={referrerNameStyles}>
                {user.firstName} {user.lastName}
              </span>
              <br />
              <span className={miscTextStyles}>
                {moment(referralDate).format('M/D/YYYY')}
              </span>
            </Col>
          </Row>
        }
      >
        <Row>
          <span className={statusStyles}> Status: </span>
          <Dropdown
            trigger={['click']}
            overlay={
              <Menu>
                {Object.keys(REFERRAL_STATUS).map(key => {
                  return (
                    <Menu.Item
                      key={key}
                      className={dropdownMenuItem}
                      onClick={() => {
                        this.handleUpdateStatus(key);
                      }}
                    >
                      {REFERRAL_STATUS[key]}
                    </Menu.Item>
                  );
                })}
              </Menu>
            }
          >
            <Button className={dropdownButton()}>
              {REFERRAL_STATUS[status]} <Icon type="down" />
            </Button>
          </Dropdown>
        </Row>
        <Row>
          <ProgressIndicator
            type="referral"
            points={5}
            status={status}
            onClick={this.handleUpdateStatus}
          />
        </Row>
        <ReferralHiredModal
          referral={referral}
          onUpdateJob={this.props.onUpdateJob}
          onUpdateReferral={this.props.onUpdateReferral}
          job={currentJob}
          visible={this.state.hiredModalVisible}
          handleCancel={this.hideHiredModal}
          allReferrals={allReferrals}
        />
      </Card>
    );
  }
}
