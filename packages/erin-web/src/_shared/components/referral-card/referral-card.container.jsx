import { connect } from 'react-redux';
import { ReferralCard as ReferralCardComponent } from './referral-card.component';
import { actions as AllActions } from 'erin-app-state-mgmt';

const {
  referralActions: { actions },
} = AllActions;

const mapStateToProps = state => {
  return {
    currentReferral: state.referral.currentReferral,
    referrals: state.referral.referrals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateReferral(values) {
      dispatch(actions.updateReferral(values));
    },
  };
};

export const ReferralCard = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReferralCardComponent);
