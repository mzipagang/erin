import { css } from 'emotion';

import { COLORS } from '../../../_shared/styles/colors';

export const cardMainStyles = css({
  height: 250,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  marginBottom: 25,
});

export const candidateNameStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.blue,
  fontSize: 18,
});

export const referrerNameStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.blue,
  fontSize: '1em',
});

export const miscTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.heading,
  fontSize: '0.9em',
});

export const miscTextColStyles = {
  float: 'right',
};

export const socialIcons = css({
  fontSize: '1.75em',
  color: COLORS.heading,
  width: 23,
  paddingRight: 3,
  opacity: 0.7,
});

export const statusTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.blue,
  fontSize: '1.2em',
});

export const statusStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: '1.2em',
});

export const dropdownMenuItem = css({
  borderColor: 'transparent',
  color: COLORS.blue,
  '&:hover': {
    backgroundColor: COLORS.white,
    color: COLORS.blue,
    borderColor: 'transparent',
  },
});

export const dropdownButton = width =>
  css({
    marginLeft: 8,
    borderColor: 'transparent',
    color: COLORS.blue,
    '&:hover': {
      backgroundColor: COLORS.white,
      color: COLORS.blue,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.white,
      color: COLORS.blue,
      borderColor: COLORS.white,
    },
    width,
  });
