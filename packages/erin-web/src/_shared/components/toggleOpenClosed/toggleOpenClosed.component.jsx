import { Row, Button } from 'antd';
import React from 'react';
import {
  openButtonStyles,
  toggleTitleStyles,
  toggleButtonContainerStyles,
} from './toggleOpenClosed.styles';

export class ToggleOpenClosed extends React.Component {
  render() {
    const {
      onClick,
      openJobsKey,
      closedJobsKey,
      filterOpenStatus,
    } = this.props;

    return (
      <Row className={toggleButtonContainerStyles}>
        <span className={toggleTitleStyles}>Show:</span>
        <Button
          onClick={() => onClick(openJobsKey)}
          className={openButtonStyles(filterOpenStatus === openJobsKey)}
        >
          Open
        </Button>
        <Button
          onClick={() => onClick(closedJobsKey)}
          className={openButtonStyles(filterOpenStatus === closedJobsKey)}
        >
          Closed
        </Button>
      </Row>
    );
  }
}
