import React from 'react';
import { List } from 'antd';

import ReferralNotification from './referral-notifications/referral-notification.component';
import ReferralCard from './referral-card/my-referral-card.component';
import FilterByDepartment from './my-referrals-toolbar/filter-by-department.component';
import SearchByName from './my-referrals-toolbar/search-by-name.component';
import FilterByStatus from './my-referrals-toolbar/status-filter.component';
import {
  ReferralsDashboardContainer,
  ToolBar,
  SectionStyles,
} from './my-referrals.styles';

const filterAcceptedKey = 'accepted';
const filterReferredKey = 'referred';

class MyReferralsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredReferrals: props.referrals,
      notifications: props.notifications,
    };
    this.handleNameSearch = this.handleNameSearch.bind(this);
    this.handleDepartmentFilter = this.handleDepartmentFilter.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.referrals !== this.props.referrals) {
      this.setState({
        filteredReferrals: this.props.referrals,
      });
    }
  }

  sortByAlph = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  handleStatusFilter = value => {
    let results = this.props.referrals.filter(
      record => record.status === value
    );
    this.setState({ filteredReferrals: results });
  };

  toggleButtonStyles = filterReferralStatus => {
    this.setState({
      filterReferralStatus,
    });
  };

  clearStatusFilter = () => {
    this.setState({
      filteredReferrals: this.props.referrals,
      filterReferralStatus: '',
    });
  };

  handleDepartmentFilter(value) {
    if (value.length === 0) {
      this.setState({ filteredReferrals: this.props.referrals });
      return;
    }
    let results = [];
    this.props.referrals.forEach(record => {
      if (value.includes(record.job.department.name)) {
        results.push(record);
      }
    });
    this.setState({ filteredReferrals: results });
  }

  handleNameSearch(value) {
    let results = [];
    this.props.referrals.forEach(record => {
      if (record.contact.lastName.includes(value)) {
        results.push(record);
      }
    });
    this.setState({ filteredReferrals: results });
  }

  render() {
    const { currentUser } = this.props;
    const {
      filterReferralStatus,
      notifications,
      filteredReferrals,
    } = this.state;
    const referralNotifications = notifications.filter(
      notification =>
        notification.type === 'Requested Referral' && notification.status === 0
    );

    let referralData = filteredReferrals;
    if (!filteredReferrals) {
      referralData = [];
    }

    const sortedReferrals = referralData.sort((a, b) => {
      return b.dateReferred - a.dateReferred;
    });

    return (
      <main className={ReferralsDashboardContainer}>
        <section className={ToolBar}>
          <FilterByStatus
            toggleButtonStyles={this.toggleButtonStyles}
            filterOpenStatus={filterReferralStatus}
            acceptedKey={filterAcceptedKey}
            referredKey={filterReferredKey}
            clearStatusFilter={this.clearStatusFilter}
            handleStatusFilter={this.handleStatusFilter}
          />
          <FilterByDepartment
            referrals={filteredReferrals}
            handleDepartmentFilter={this.handleDepartmentFilter}
          />
          <SearchByName handleNameSearch={this.handleNameSearch} />
        </section>
        <section className={SectionStyles}>
          {referralNotifications.length > 0 ? (
            <List
              dataSource={referralNotifications}
              itemLayout={'horizontal'}
              renderItem={notification => (
                <List.Item>
                  <ReferralNotification
                    key={
                      notification.id ||
                      Math.floor(Math.random() * Math.floor(1203869142))
                    }
                    notification={notification}
                    updateNotification={this.props.updateNotification}
                    createReferral={this.props.createReferral}
                    currentUser={currentUser}
                  />
                </List.Item>
              )}
            />
          ) : null}
        </section>
        <section className={SectionStyles}>
          <List
            grid={{ gutter: 35, column: 2 }}
            dataSource={sortedReferrals}
            itemLayout={'horizontal'}
            renderItem={referral => (
              <List.Item>
                <ReferralCard
                  key={
                    referral.id ||
                    Math.floor(Math.random() * Math.floor(1203869142))
                  }
                  referral={referral}
                />
              </List.Item>
            )}
          />
        </section>
      </main>
    );
  }
}

export default MyReferralsComponent;
