import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const ReferralContainer = css({
  backgroundColor: '#E4F7FE',
  // width: 1240,
  maxWidth: 1290,
  borderRadius: 8,
  padding: 20,
  display: 'flex',
  justifyContent: 'space-between',
  width: '100%',
});

export const Title = css({
  color: COLORS.heading,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  fontSize: '18',
  marginBottom: 0,
});

export const MiscText = css({
  color: COLORS.lightGray,
  fontSize: 16,
  marginBottom: 5,
});

export const TitleName = css({
  fontWeight: 600,
});

export const ContactName = css({
  color: COLORS.heading,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  fontSize: '18',
  marginBottom: 0,
});

export const JobTitle = css({
  color: COLORS.blue,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  fontSize: '18',
  marginBottom: 0,
});

export const BonusAmount = css({
  color: COLORS.green,
  textAlign: 'right',
  fontSize: 18,
});

export const ReferralBonus = css({
  fontSize: 16,
  color: COLORS.lightGray,
});

export const MiddleContainer = css({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100%',
  flex: 1,
  paddingLeft: 10,
});

export const ReferBtn = css({
  width: 170,
  height: 50,
  backgroundColor: COLORS.red,
  color: 'white',
  margin: 5,
  fontSize: 16,
  paddingTop: 3,
});

export const CancelBtn = css({
  width: 170,
  height: 50,
  backgroundColor: COLORS.blue,
  color: 'white',
  margin: 5,
  fontSize: 16,
  paddingTop: 5,
  '& .ant-btn: hover': {
    borderColor: COLORS.blue,
    color: `${COLORS.blue} !important`,
  },
});

export const BtnText = css({
  position: 'relative',
  top: -4,
});

export const BtnContainer = css({
  display: 'flex',
  justifyContent: 'center',
  flexWrap: 'wrap',
  height: '100%',
  alignItems: 'center',
  flex: 1,
});
