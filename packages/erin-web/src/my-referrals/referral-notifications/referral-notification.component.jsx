import React from 'react';
import { Button, Icon } from 'antd';

import SocialMedia from '../../_shared/components/social-media/social-media.component';
import WebIcon from '../../_shared/components/web-icon.component';
import {
  ReferralContainer,
  Title,
  TitleName,
  ContactName,
  JobTitle,
  MiscText,
  BonusAmount,
  ReferralBonus,
  ReferBtn,
  CancelBtn,
  BtnText,
  BtnContainer,
  MiddleContainer,
} from './referral-notification.styles';

const ReferralNotification = props => {
  const {
    notification,
    updateNotification,
    createReferral,
    currentUser,
  } = props;
  const { createdBy, contact, job } = notification;

  const handleDontRefer = e => {
    e.preventDefault();
    const updatedNotification = { ...notification, status: 2 };
    updateNotification(updatedNotification, notification.id);
  };

  const handleMakeReferral = e => {
    e.preventDefault();
    const updatedNotification = { ...notification, status: 1 };
    updateNotification(updatedNotification, notification.id);
    const newReferral = {
      contact: {
        firstName: `${contact.firstName}`,
        lastName: `${contact.lastName}`,
      },
      email: `${contact.email}`,
      socialMedia: contact.socialMedia,
      department: `${job.department.name}`,
      job: `${job.title}`,
      dateReferred: new Date(),
      referredBy: {
        firstName: `${currentUser.firstName}`,
        lastName: `${currentUser.lastName}`,
        userId: `${currentUser.userId}`,
      },
      status: 'Referred',
      referralBonusAmount: `${job.referralBonusAmount}`,
    };
    createReferral(newReferral);
  };

  return (
    <div className={ReferralContainer}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: 1,
          justifyContent: 'center',
        }}
      >
        <h3 className={Title}>
          <span className={TitleName}>
            {createdBy.firstName} {createdBy.lastName}
          </span>{' '}
          has requested a referral!
        </h3>
        <span />
        {/* inline style is the only way I could figure out how to change line height */}
        <span className={MiscText} style={{ lineHeight: 1 }}>
          {createdBy.firstName} doesn&#39;t know who your contact is but our
          system matched them as a candidate for an open position!
        </span>
      </div>
      <div className={MiddleContainer}>
        <div style={{ marginRight: 10, marginLeft: 10 }}>
          <h3 className={ContactName}>
            {contact.firstName} {contact.lastName}
          </h3>
          <SocialMedia
            email={contact.email}
            socialMedia={contact.socialMedia}
            size={20}
          />
        </div>
        <p
          style={{
            fontSize: 16,
            textAlign: 'center',
            marginBottom: 0,
          }}
        >
          for
        </p>
        <div style={{ marginLeft: 10 }}>
          <h3 className={JobTitle}>{job.title}</h3>
          <span className={ReferralBonus}>
            Referral Bonus:{' '}
            <span className={BonusAmount}>
              {`$${job.referralBonusAmount}`.replace(
                /\B(?=(\d{3})+(?!\d))/g,
                ','
              )}
            </span>
          </span>
        </div>
      </div>
      <div className={BtnContainer}>
        <Button onClick={handleMakeReferral} className={ReferBtn}>
          <span styles={{ marginTop: 5 }}>
            <WebIcon name="user" color="white" size={24} />
          </span>{' '}
          <span className={BtnText}>Make Referral</span>
        </Button>
        <Button onClick={handleDontRefer} className={CancelBtn}>
          <Icon style={{ fontSize: 24 }} type="close-circle-o" />{' '}
          <span className={BtnText}>Don&#39;t Refer</span>
        </Button>
      </div>
    </div>
  );
};

export default ReferralNotification;
