import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import MyReferralsComponent from './my-referrals.component';
import {
  myReferralsData,
  userData,
  notificationData,
} from '../../../../test/data/mb-data';

const notifications = notificationData.filter(
  notification =>
    notification.type === 'Requested Referral' && notification.status === 0
);

storiesOf('Development/My Referrals', module)
  .addDecorator(StoryRouter())
  .add('Main', () => (
    <MyReferralsComponent
      user={userData[0]}
      getUserNotifications={() => notifications}
      getUserReferrals={() => myReferralsData}
    />
  ));
