import React from 'react';
import { Select } from 'antd';
import _ from 'lodash';
import { SelectStyles, SelectContainer } from '../my-referrals.styles';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

const FilterByDepartment = props => {
  const { handleDepartmentFilter, referrals } = props;
  const Option = Select.Option;
  //populate select options
  const options = [];
  const keys = referrals.map(
    a => (a.job.department !== null ? a.job.department.name : null)
  );
  const departments = _.uniq(keys);
  for (const department of departments) {
    options.push(<Option key={department}>{department}</Option>);
  }

  return (
    <div className={SelectContainer}>
      <Select
        className={SelectStyles}
        mode="multiple"
        placeholder="Filter By Department"
        onChange={value => handleDepartmentFilter(value)}
      >
        {options}
      </Select>
      <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
    </div>
  );
};

export default FilterByDepartment;
