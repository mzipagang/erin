import React from 'react';
import { Button } from 'antd';
import {
  StatusFilter,
  StyledHeading,
  ReferralButtonStyles,
  AllButtonStyles,
} from '../my-referrals.styles';

const FilterByStatus = props => {
  const {
    clearStatusFilter,
    referredKey,
    acceptedKey,
    filterReferralStatus,
    handleStatusFilter,
    toggleButtonStyles,
  } = props;

  const handleClick = (style, status) => {
    handleStatusFilter(status);
    toggleButtonStyles(style);
  };
  return (
    <div className={StatusFilter}>
      <h3 className={StyledHeading}>Show:</h3>
      <Button
        onClick={() => handleClick(acceptedKey, 'accepted')}
        className={ReferralButtonStyles(filterReferralStatus === acceptedKey)}
      >
        Accepted
      </Button>
      <Button
        onClick={() => handleClick(referredKey, 'referred')}
        className={ReferralButtonStyles(filterReferralStatus === referredKey)}
      >
        Referred
      </Button>
      <span>
        <Button
          className={AllButtonStyles}
          size="small"
          onClick={clearStatusFilter}
        >
          All
        </Button>
      </span>
    </div>
  );
};
export default FilterByStatus;
