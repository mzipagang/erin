import React from 'react';
import { Input } from 'antd';

import { SearchStyles } from '../my-referrals.styles';

const SearchByName = props => {
  const { handleNameSearch } = props;
  const Search = Input.Search;
  return (
    <Search
      className={SearchStyles}
      placeholder="Search by Last Name"
      style={{ width: 300 }}
      onSearch={value => handleNameSearch(value)}
    />
  );
};

export default SearchByName;
