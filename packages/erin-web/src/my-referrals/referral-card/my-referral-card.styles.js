import { css } from 'emotion';

import { COLORS } from '../../_shared/styles/colors';

export const cardMainStyles = css({
  height: 250,
  // maxWidth: 600,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
});

export const candidateNameStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.blue,
  fontSize: 18,
  marginBottom: 0,
  lineHeight: 1,
  paddingBottom: 5,
});

export const referrerNameStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.blue,
  fontSize: '1em',
});

export const miscTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: 14,
  margin: 0,
  textAlign: 'right',
});

export const miscTextColStyles = css({
  float: 'right',
  marginTop: -55,
});

export const statusTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.heading,
  fontSize: '16px',
});

export const statusStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.lightGray,
  fontSize: 16,
});

export const dropdownMenuItem = css({
  borderColor: 'transparent',
  color: COLORS.blue,
  '&:hover': {
    backgroundColor: COLORS.white,
    color: COLORS.blue,
    borderColor: 'transparent',
  },
});

export const dropdownButton = width =>
  css({
    marginLeft: 8,
    borderColor: 'transparent',
    color: COLORS.blue,
    '&:hover': {
      backgroundColor: COLORS.white,
      color: COLORS.blue,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.white,
      color: COLORS.blue,
      borderColor: COLORS.white,
    },
    width,
  });

export const BonusAmount = css({
  color: COLORS.green,
  textAlign: 'right',
});
