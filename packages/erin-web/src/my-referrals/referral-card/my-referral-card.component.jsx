import React from 'react';
import { Row, Col, Card } from 'antd';
import moment from 'moment';
import {
  cardMainStyles,
  candidateNameStyles,
  statusTextStyles,
  miscTextStyles,
  statusStyles,
  miscTextColStyles,
  BonusAmount,
} from './my-referral-card.styles';
import { ProgressIndicator } from '../progress-indicator/progress-indicator.component';
import SocialMedia from '../../_shared/components/social-media/social-media.component';

class ReferralCard extends React.Component {
  render() {
    const { referral } = this.props;
    const {
      status,
      contact: { firstName, lastName, emailAddress, socialMediaAccounts },
      referralDate,
      job,
    } = referral;

    const referralStatus = status => {
      switch (status) {
        case 'accepted':
          return 'Accepted';
        case 'hired':
          return 'Hired';
        case 'referred':
          return 'Referred';
        case 'notHired':
          return 'Not Hired';
        case 'interviewing':
          return 'Interviewing';
        default:
          return null;
      }
    };

    return (
      <Card
        hoverable
        headStyle={{ height: 100 }}
        className={cardMainStyles}
        title={
          <Row>
            <Col>
              <span
                className={candidateNameStyles}
              >{`${firstName} ${lastName}`}</span>
              <Row style={{ paddingTop: 5 }}>
                <SocialMedia
                  email={emailAddress}
                  socialMedia={socialMediaAccounts}
                />
              </Row>
            </Col>
            <Col className={miscTextColStyles}>
              <h3 className={candidateNameStyles}>
                {job !== null ? job.title : 'Sales Admin'}
              </h3>
              {job !== null && job.referralBonus ? (
                <div>
                  <p className={miscTextStyles}>
                    {' '}
                    Referred on: {moment(referralDate).format('M/D/YYYY')}
                  </p>
                  <h3 className={BonusAmount}>
                    {`$${job.referralBonus}`.replace(
                      /\B(?=(\d{3})+(?!\d))/g,
                      ','
                    )}
                  </h3>
                </div>
              ) : (
                <div>
                  <p className={miscTextStyles}> Referred on: </p>
                  <p className={miscTextStyles}>
                    {moment(referralDate).format('M/D/YYYY')}
                  </p>
                </div>
              )}
            </Col>
          </Row>
        }
      >
        <Row>
          <span className={statusStyles}>
            {' '}
            Status:{' '}
            <span className={statusTextStyles}>{referralStatus(status)}</span>
          </span>
        </Row>
        <Row>
          <ProgressIndicator
            type="referral"
            points={5}
            status={referralStatus(status)}
            referral={referral}
          />
        </Row>
      </Card>
    );
  }
}

export default ReferralCard;
