import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const SelectStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  width: 300,
  '& .ant-select-selection': {
    borderColor: COLORS.lightGray,
  },
  '& .ant-select-selection__choice': {
    backgroundColor: COLORS.lightGreen,
    color: COLORS.green,
  },
});

export const SearchStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  '& input': {
    fontFamily: '"Open Sans", sans-serif',
    borderRadius: 20,
    borderColor: COLORS.lightGray,
    paddingLeft: '40px !important',
  },
  '& span': {
    left: '12px !important',
  },
  '& i': {
    color: COLORS.darkGray,
    fontSize: 18,
  },
});

export const StatusFilter = css({
  display: 'flex',
  padding: 0,
  '& h3': {
    marginRight: 10,
    fontSize: 18,
  },
  '& .ant-btn': {
    lineHeight: '12px',
  },
});

export const AllButtonStyles = css({
  backgroundColor: COLORS.red,
  width: 50,
  height: 30,
  color: 'white',
  border: 'none',
  '&:hover': {
    backgroundColor: 'white',
    color: COLORS.hyperLink,
    border: `1px solid ${COLORS.hyperLink}`,
  },
  '&:focus': {
    backgroundColor: COLORS.red,
    color: 'white',
    border: 'none',
  },
});

export const StyledHeading = css({
  fontFamily: '"Open Sans", sans-serif',
  color: '#444444',
  fontWeight: 600,
});

export const styleFont = css({
  fontFamily: '"Open Sans", sans-serif',
});

export const ReferralsDashboardContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  paddingRight: 30,
  backgroundColor: COLORS.lightGray2,
});

export const ToolBar = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  // width: 1240,
  maxWidth: 1290,
  paddingRight: 20,
});

export const SectionStyles = css({
  // width: 1240,
  maxWidth: 1290,
  paddingRight: 20,
  marginTop: 10,
});

export const ReferralButtonStyles = selected =>
  css({
    marginRight: 10,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.blue,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.blue,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    height: 30,
    width: 'auto',
  });

export const SelectContainer = css({
  '& svg': {
    transform: 'translate(-40px, 2px)',
  },
});
