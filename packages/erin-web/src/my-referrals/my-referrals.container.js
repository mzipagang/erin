import { connect } from 'react-redux';
import MyReferralsComponent from './my-referrals.component';
import { actions } from 'erin-app-state-mgmt';
import { withListReferrals } from 'erin-app-state-mgmt/src/_shared/api/components/referrals/with-list-referrals.provider';
import { compose } from '../_shared/services/utils';

const { myReferralsActions } = actions;

const mapStateToProps = state => {
  const { notifications } = state.myReferrals;
  const { currentUser } = state.user;
  return {
    currentUser,
    notifications,
    filter: { userId: { eq: currentUser.id } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateNotification(updatedNotification, notificationId) {
      dispatch(
        myReferralsActions.createUpdateUserNotificationAction(
          updatedNotification,
          notificationId
        )
      );
    },
    createReferral(newReferral) {
      dispatch(myReferralsActions.createCreateReferralAction(newReferral));
    },
  };
};

export const MyReferralsWithApi = compose(withListReferrals)(
  MyReferralsComponent
);

export const MyReferrals = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyReferralsWithApi);
