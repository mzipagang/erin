import React from 'react';
import { Row, Col, Icon } from 'antd';
import {
  progressIndicatorContainer,
  progressIndicatorLine,
  progressIndicatorDots,
  progressIndicatorDotCenter,
  checkMark,
  subTitle,
} from './progress-indicator.styles';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

const referralSubtitles = [
  'Referred',
  'Accepted',
  'Interviewing',
  'Hired',
  'Not Hired',
];

const progressBarOptions = [
  { title: referralSubtitles[0], color: COLORS.yellow, previousStates: [0] },
  { title: referralSubtitles[1], color: COLORS.yellow, previousStates: [0, 1] },
  {
    title: referralSubtitles[2],
    color: COLORS.yellow,
    previousStates: [0, 1, 2],
  },
  {
    title: referralSubtitles[3],
    color: COLORS.red,
    previousStates: [0, 1, 2, 3],
  },
  {
    title: referralSubtitles[4],
    color: COLORS.red,
    previousStates: [0, 1, 2, 3, 4],
  },
];

export const ProgressIndicator = props => {
  const { status } = props;

  let color = COLORS.dashboardLightOrange;

  if (status === progressBarOptions[3].title) {
    color = COLORS.green;
  }

  if (status === progressBarOptions[4].title) {
    color = COLORS.red;
  }

  let hasCheck = index => {
    const indexForCurrentStatus = referralSubtitles.indexOf(status);
    return (
      progressBarOptions[indexForCurrentStatus].previousStates.indexOf(
        index
      ) !== -1
    );
  };

  return (
    <div className={progressIndicatorContainer}>
      <Row>
        <div className={progressIndicatorLine(color)} />
        {Array.apply(null, { length: 5 })
          .map(Function.call, Number)
          .map(i => {
            return (
              <Col key={i} span={4}>
                <div className={progressIndicatorDots(color)} />
                <div className={progressIndicatorDotCenter}>
                  {hasCheck(i) && (
                    <Icon className={checkMark(color)} type="check" />
                  )}
                </div>
              </Col>
            );
          })}
      </Row>

      <Row>
        <Col span={4}>
          <span className={subTitle(-11, status)}>
            {progressBarOptions[0].title}
          </span>
        </Col>
        <Col span={4}>
          <span className={subTitle(-11, status)}>
            {progressBarOptions[1].title}
          </span>
        </Col>
        <Col span={4}>
          <span className={subTitle(-19, status)}>
            {progressBarOptions[2].title}
          </span>
        </Col>
        <Col span={4}>
          <span className={subTitle(-2, status)}>
            {progressBarOptions[3].title}
          </span>
        </Col>
        <Col span={4}>
          <span className={subTitle(-13, status)}>
            {progressBarOptions[4].title}
          </span>
        </Col>
      </Row>
    </div>
  );
};
