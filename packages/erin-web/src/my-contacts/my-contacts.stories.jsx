import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import MyContactsComponent from './my-contacts.component';
import SearchByName from './search-by-name.component';
import { userData } from '../../../../test/data/mb-data';

storiesOf('Development/My Contacts', module)
  .addDecorator(StoryRouter())
  .add('Main', () => <MyContactsComponent contacts={userData[0].contacts} />)
  .add('Search By Name', () => <SearchByName />);

storiesOf('QA/My Contacts', module)
  .addDecorator(StoryRouter())
  .add('Main', () => <MyContactsComponent contacts={userData[0].contacts} />)
  .add('Search By Name', () => <SearchByName />);
