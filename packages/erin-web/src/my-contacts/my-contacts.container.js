import { connect } from 'react-redux';
import MyContactsComponent from './my-contacts.component';
import { withListContacts } from 'erin-app-state-mgmt/src/_shared/api/components/contacts/with-list-contacts.provider';
import { compose } from '../_shared/services/utils';

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
    //TODO: add back when ID has been fixed
    //filter: { users: { contains: currentUser.id } },
  };
};

export const MyContactsWithApi = compose(withListContacts)(MyContactsComponent);

export const MyContacts = connect(mapStateToProps)(MyContactsWithApi);
