import React, { Component } from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Table, Avatar, Icon } from 'antd';
import SocialMedia from '../_shared/components/social-media/social-media.component';

import {
  NoPicture,
  TableStyles,
  NameStyles,
  LinkStyles,
} from './my-contacts.styles';

class ContactsTable extends Component {
  render() {
    const { contacts, sortByAlph } = this.props;
    const columns = [
      {
        key: 'avatar',
        render: record =>
          record.avatar !== null && record.avatar !== undefined ? (
            <Avatar
              style={{ borderRadius: 0 }}
              src={record.avatar}
              size={50}
              shape="square"
            />
          ) : (
            <div className={NoPicture} style={{ float: 'left' }}>
              <h3>
                {record.firstName[0]}
                {record.lastName[0]}
              </h3>
            </div>
          ),
      },
      {
        title: 'Name',
        key: 'name',
        render: record => (
          <h3 className={NameStyles}>
            {record.firstName} {record.lastName}
          </h3>
        ),
        sorter: (a, b) =>
          sortByAlph(a.lastName + a.firstName, b.lastName + b.firstName),
      },
      {
        title: 'Email',
        dataIndex: 'emailAddress',
        key: 'email',
        sorter: (a, b) => sortByAlph(a.emailAddress, b.emailAddress),
      },
      {
        title: 'Social',
        dataIndex: 'socialMediaAccounts',
        key: 'info',
        render: socialMediaAccounts => (
          <SocialMedia socialMedia={socialMediaAccounts} />
        ),
      },
      {
        title: 'Added From',
        dataIndex: 'addedFrom',
        key: 'addedFrom',
        sorter: (a, b) => sortByAlph(a.addedFrom, b.addedFrom),
        render: addedFrom =>
          addedFrom !== null && addedFrom !== undefined ? addedFrom : null,
      },
      {
        title: 'Date Added',
        dataIndex: 'dateAdded',
        key: 'dateAdded',
        sorter: (a, b) => a.dateAdded - b.dateAdded,
        render: dateAdded =>
          dateAdded !== null && dateAdded !== undefined
            ? moment(dateAdded).format('M/D/YYYY')
            : null,
      },
      {
        title: 'Action',
        key: 'action',
        render: () => (
          <Link className={LinkStyles} to="#">
            <Icon type="edit" /> edit
          </Link>
        ),
      },
    ];
    return (
      <Table
        pagination={{ pageSize: 50 }}
        className={TableStyles}
        dataSource={contacts}
        columns={columns}
        rowKey={record => record.id}
      />
    );
  }
}

export default ContactsTable;
