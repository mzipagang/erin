import React, { Component } from 'react';
import { Button, Icon } from 'antd';

import ContactsTable from './contacts-table.component';
import SearchByName from './search-by-name.component';

import {
  MyContactsContainer,
  TableContainer,
  addContactsButton,
  addContactsButtonIcon,
  addContactsButtonText,
  ToolBar,
} from './my-contacts.styles';

class MyContactsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredContacts: props.contacts,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.contacts !== this.props.contacts) {
      this.setState({ filteredContacts: this.props.contacts });
    }
  }

  sortByAlph = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  handleNameSearch = value => {
    let results = [];
    this.props.contacts.forEach(record => {
      if (record.lastName.includes(value)) {
        results.push(record);
      }
    });
    this.setState({ filteredContacts: results });
  };

  render() {
    const { filteredContacts } = this.state;
    let initialData = filteredContacts;
    if (!filteredContacts) {
      initialData = [];
    }
    return (
      <main className={MyContactsContainer}>
        <div className={ToolBar}>
          <div>
            <Button
              shape="circle"
              onClick={this.showModal}
              className={addContactsButton}
            >
              <Icon className={addContactsButtonIcon} type="plus" />
            </Button>
            <span className={addContactsButtonText}> Add Contact </span>
          </div>
          <SearchByName handleNameSearch={this.handleNameSearch} />
        </div>

        <div className={TableContainer}>
          <ContactsTable contacts={initialData} sortByAlph={this.sortByAlph} />
        </div>
      </main>
    );
  }
}

export default MyContactsComponent;
