import { css } from 'emotion';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

export const MyContactsContainer = css({
  width: '100%',
  paddingRight: 30,
  margin: 30,
});

export const TableContainer = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  backgroundColor: 'white',
  maxWidth: 1270,
  minWidth: 900,
  marginRight: 20,
  '& thead > tr > th': {
    backgroundColor: 'white',
    fontWeight: 700,
  },
  '& tbody > tr > td': {
    paddingTop: 0,
    paddingBottom: 0,
    height: 65,
  },
});

export const NoPicture = css({
  height: 50,
  width: 50,
  backgroundColor: COLORS.lightGray3,
  '& h3': {
    fontWeight: 400,
    color: 'white',
    fontSize: 26,
    textAlign: 'center',
    lineHeight: '50px',
    marginBottom: 0,
  },
});

export const NameStyles = css({
  fontSize: 16,
  fontWeight: '600 !important',
  marginBottom: 0,
  color: COLORS.darkGray,
});

export const LinkStyles = css({
  color: COLORS.blue,
  fontWeight: '600 !important',
  fontSize: 14,
});

export const TableStyles = css({
  backgroundColor: 'white',
  '& .ant-table': {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 400,
    fontSize: 14,
  },
  //adds padding to pagination
  '& ul': {
    paddingRight: 20,
  },
  '& .ant-pagination-options': {
    float: 'left',
  },
});

export const addContactsButton = css({
  marginTop: -12,
  top: 8,
  marginRight: 5,
  backgroundColor: COLORS.red,
  color: COLORS.white,
  height: 40,
  width: 40,
  '&:hover': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
  '&:focus': {
    backgroundColor: COLORS.red,
    color: COLORS.white,
    borderColor: 'transparent',
  },
});

export const addContactsButtonText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.darkGray,
  fontSize: 18,
});

export const addContactsButtonIcon = css({
  fontSize: 35,
});

export const SearchStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  '& input': {
    fontFamily: '"Open Sans", sans-serif',
    borderRadius: 20,
    borderColor: COLORS.lightGray,
    paddingLeft: '40px !important',
  },
  '& span': {
    left: '12px !important',
  },
  '& i': {
    color: COLORS.darkGray,
    fontSize: 18,
  },
});

export const ToolBar = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: 20,
  maxWidth: 1290,
  paddingRight: 20,
});
