import { css } from 'emotion';
import LoginBackground from '../_shared/assets/Erin_login_background.jpg';

export const LoginContainer = css({
  backgroundImage: `url(${LoginBackground})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  height: '100vh',
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
});

export const Logo = css({
  width: 350,
});

export const Card = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 4,
  backgroundColor: 'white',
  padding: 40,
  marginTop: 30,
  width: 500,
});

export const NavBar = css({
  marginTop: 15,
  fontSize: 16,
  fontWeight: '900 !important',
  color: 'white !important',
  '& a': {
    textDecoration: 'none',
    fontSize: 16,
    fontWeight: '600 !important',
    color: 'white !important',
  },
  '& :hover': {
    color: 'white',
  },
});
