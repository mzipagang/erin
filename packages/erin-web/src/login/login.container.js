import { connect } from 'react-redux';
import LoginComponent from './login.component';
import { actions } from 'erin-app-state-mgmt';

const { userActions } = actions;

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentUser(user) {
      dispatch(userActions.createSetCurrentUserAction(user));
    },
  };
};

export const Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent);
