import React from 'react';
import { Form, Input, Button, Icon, Alert } from 'antd';
import { withRouter } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import { GetUserByCognitoId } from 'erin-app-state-mgmt/src/_shared/api/graphql/custom/users/getUserByCognitoId';

import {
  SubmitBtn,
  SubmitBtnContainer,
  BtnContainer,
  CheckIcon,
  InputStyles,
  LabelStyles,
  FormStyles,
  ForgotPasswordBtn,
} from './login-card.styles';

class LoginCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      invalidCredentials: false,
      serverError: false,
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ serverError: false, invalidCredentials: false });
    const { onAuthentication, setCurrentUser, history } = this.props;
    this.props.form.validateFields((err, values) => {
      if (err) {
        this.setState({ serverError: true });
        return;
      }

      Auth.signIn(values.email, values.password)
        .then(async user => {
          const { data } = await this.props.client.query({
            query: GetUserByCognitoId,
            variables: { cognitoId: user.username },
          });
          setCurrentUser(data.getUserByCognitoId);
          await localStorage.setItem(
            'currentUser',
            JSON.stringify(data.getUserByCognitoId)
          );
          onAuthentication(user.signInUserSession.accessToken.jwtToken);
          history.push('/dashboard');
        })
        .catch(err => {
          if (err.code === 'NotAuthorizedException') {
            this.setState({ invalidCredentials: true });
          } else {
            this.setState({ serverError: true });
            console.error(err);
          }
        });
    });
  };

  render() {
    const { serverError, invalidCredentials } = this.state;
    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    const { emailPassword } = this.props;
    return (
      <div>
        <Form className={FormStyles}>
          <FormItem className={InputStyles}>
            <label className={LabelStyles}>Email Address</label>
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: 'Not a Valid E-mail Adress',
                },
                {
                  required: true,
                  message: 'E-mail Required',
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem className={InputStyles}>
            <label className={LabelStyles}>Password</label>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Password Required',
                },
              ],
            })(<Input className={InputStyles} type="password" />)}
          </FormItem>
          <div className={BtnContainer}>
            <Button className={ForgotPasswordBtn} onClick={emailPassword}>
              Forgot Your Password?
            </Button>
          </div>
          {serverError ? (
            <Alert
              type="error"
              message="There was a problem with the server. Please Try Again Later."
            />
          ) : null}
          {invalidCredentials ? (
            <Alert
              type="error"
              message="Invalid credentials. Please try again"
            />
          ) : null}
          <FormItem className={SubmitBtnContainer}>
            <Button
              onClick={this.handleSubmit}
              className={SubmitBtn}
              htmlType="submit"
            >
              Log In
              <Icon className={CheckIcon} type="arrow-right" />
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default withRouter(Form.create()(LoginCard));
