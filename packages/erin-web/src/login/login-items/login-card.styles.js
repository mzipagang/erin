import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const SubmitBtn = css({
  fontFamily: 'Open Sans',
  backgroundColor: COLORS.red,
  color: 'white',
  fontSize: 20,
  fontWeight: 300,
  padding: '10px 30px',
  height: 'auto',
  border: 'none',
  marginBottom: 0,
  width: 200,
});

export const SubmitBtnContainer = css({
  width: '100%',
  marginTop: 30,
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 0,
  '& .ant-form-item-control-wrapper': {
    '@media (max-width: 575px)': {
      width: 'auto',
    },
  },
  '& .ant-btn:hover': {
    backgroundColor: COLORS.lightGray2,
    border: `1px solid ${COLORS.blue} !important`,
    color: COLORS.blue,
  },
});

export const BtnContainer = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 0,
  '& .ant-form-item-control-wrapper': {
    '@media (max-width: 575px)': {
      width: 'auto',
    },
  },
});

export const CheckIcon = css({
  borderRadius: 100,
  border: '1px solid white',
  padding: 2,
});

export const LabelStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: '16px !important',
  fontWeight: 600,
});

export const InputStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 16,
  '& .ant-input': {
    height: 40,
    fontFamily: '"Open Sans", sans-serif',
    fontSize: 16,
  },
  '& .ant-form-explain': {
    fontSize: 12,
  },
});

export const FormStyles = css({
  '& .ant-form-item': {
    marginBottom: 10,
  },
});

export const ForgotPasswordBtn = css({
  color: COLORS.blue,
  border: 'none',
  backgroundColor: 'none',
  padding: 0,
  margin: 'auto',
  textAlign: 'center',
});
