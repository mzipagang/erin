import React from 'react';
import { Link } from 'react-router-dom';
import { RouterContext } from '../_shared/contexts/router.context';
import LoginCard from './login-items/login-card.component';
import { LoginContainer, Logo, Card, NavBar } from './login.styles';
import ErinLogo from '../_shared/assets/erinwhite.png';
import { withApollo } from 'react-apollo';

class LoginComponent extends React.Component {
  render() {
    const { users, setCurrentUser, client } = this.props;
    return (
      <main className={LoginContainer}>
        <img className={Logo} src={ErinLogo} alt="Erin Logo" />
        <div className={Card}>
          <RouterContext.Consumer>
            {({ onAuthentication }) => (
              <LoginCard
                onAuthentication={onAuthentication}
                users={users}
                setCurrentUser={setCurrentUser}
                client={client}
              />
            )}
          </RouterContext.Consumer>
        </div>
        <span className={NavBar}>
          <Link to="#">About Erin</Link> &#183; <Link to="#">Help</Link> &#183;{' '}
          <Link to="#">Blog</Link>
        </span>
      </main>
    );
  }
}

export default withApollo(LoginComponent);
