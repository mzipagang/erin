import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import Login from './login.component';

import { userData } from '../../../../test/data/mb-data';

storiesOf('Development/Login Front End', module)
  .addDecorator(StoryRouter())
  .add('Login Page', () => <Login users={userData} />);

storiesOf('QA/Login Front End', module)
  .addDecorator(StoryRouter())
  .add('Login Page', () => <Login users={userData} />);
