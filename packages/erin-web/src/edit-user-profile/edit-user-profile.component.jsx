import React from 'react';
import { Icon } from 'antd';

import EditUserProfileCard from './edit-user-profile-form/edit-user-profile-card.component';
import ResetPassword from './reset-password/reset-password.component';

import {
  EditUserContainer,
  CardStyles2,
  BackIcon,
  BackLink,
  CardsContainer,
} from './edit-user-profile.styles';

class EditUserProfileComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitting: false,
      error: false,
      selectedUser: props.user,
      id: props.id,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user !== this.props.user) {
      this.setState({ selectedUser: this.props.user });
    }
  }
  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleError = () => {
    this.setState({ error: true });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {
    const { departments, currentUser, updateUserProfile } = this.props;
    const { error, selectedUser, id } = this.state;
    let initialData = { selectedUser, departments };
    if (!departments || !selectedUser) {
      initialData = {
        selectedUser: {
          firstName: '',
          lastName: '',
          departmentId: '',
          department: { name: '' },
          title: '',
          emailAddress: '',
        },
        departments: {
          departments: [],
        },
      };
    }
    return (
      <main className={EditUserContainer}>
        <div onClick={this.goBack} className={BackLink}>
          <Icon className={BackIcon} type="left" theme="outlined" />
          Back
        </div>
        <div className={CardsContainer}>
          <EditUserProfileCard
            selectedUser={initialData.selectedUser}
            currentUser={currentUser}
            id={id}
            error={error}
            departments={initialData.departments}
            updateUserProfile={updateUserProfile}
            handleError={this.handleError}
            onUpdate={this.props.onUpdate}
          />
          <div className={CardStyles2}>
            <ResetPassword
              handleCancel={this.handleCancel}
              onUpdate={this.props.onUpdate}
              user={initialData.selectedUser}
            />
          </div>
        </div>
      </main>
    );
  }
}

export default EditUserProfileComponent;
