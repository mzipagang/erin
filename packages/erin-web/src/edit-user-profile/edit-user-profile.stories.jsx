import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import EditUserProfile from './edit-user-profile.component';

import {
  referralData,
  userData,
  companyData,
} from '../../../../test/data/mb-data';

storiesOf('Development/Edit Profile', module)
  .addDecorator(StoryRouter())
  .add('Edit Profile', () => (
    <EditUserProfile
      departments={companyData[0].departments}
      currentUser={referralData[1]}
      user={userData[0]}
    />
  ));

storiesOf('QA/Edit Profile', module)
  .addDecorator(StoryRouter())
  .add('Edit Profile', () => (
    <EditUserProfile
      departments={companyData[0].departments}
      currentUser={referralData[1]}
      user={userData[0]}
    />
  ));
