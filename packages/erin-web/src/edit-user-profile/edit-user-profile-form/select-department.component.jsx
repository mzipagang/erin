import React from 'react';
import { Select } from 'antd';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

import {
  SelectStyles,
  DropDown,
  SortDown,
  SelectContainer,
} from './select.styles';

const SelectDepartments = props => {
  const Option = Select.Option;
  const { departments } = props;
  //populate select options
  //TODO: Get options from database
  let options = [];

  for (const department of departments) {
    options.push(<Option key={department}>{department}</Option>);
  }
  return (
    <div className={SelectContainer}>
      <Select
        className={SelectStyles}
        dropdownClassName={DropDown}
        placeholder={
          <p style={{ fontSize: 16, margin: 0 }}>Add a Department</p>
        }
      >
        {options}
      </Select>
      <WebIcon
        style={{ transform: 'translate(400px, -36px)' }}
        className={SortDown}
        color={COLORS.darkGray}
        size={14}
        name="sort-down"
      />
    </div>
  );
};

export default SelectDepartments;
