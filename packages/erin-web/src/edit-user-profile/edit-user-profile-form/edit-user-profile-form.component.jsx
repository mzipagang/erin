import React from 'react';
import { Form, Select, Input, Button, Icon, Upload, message } from 'antd';
import { cx } from 'emotion';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

import {
  SelectStyles,
  DropDown,
  SortDown,
  SelectContainer,
} from './select.styles';
import {
  InputStyles,
  SubmitBtn,
  SubmitBtnContainer,
  CheckIcon,
  FormStyles,
  FormItemStyles,
  LabelStyles,
} from './edit-user-profile-form.styles';

class EditUserProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDepartment: {
        key: props.user.departmentId,
        label: props.user.department.name,
      },
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    let { user } = this.props;
    this.props.form.validateFields((err, values) => {
      if (err) {
        this.props.handleError();
        return;
      }
      const avatarUrl =
        values.Avatar === undefined
          ? user.avatar
          : 'https://source.unsplash.com/random/100x100';
      this.props.onUpdate({
        input: {
          avatar: avatarUrl,
          firstName: values.firstName,
          lastName: values.lastName,
          title: values.title,
          departmentId: this.state.selectedDepartment.key,
          emailAddress: values.email,
          companyId: user.companyId,
          role: user.role,
          active: user.active,
          id: user.id,
        },
      });
    });
  };

  handleSelect = value => {
    this.setState({ selectedDepartment: value });
  };

  render() {
    const { user, currentUser, departments } = this.props;
    const { selectedDepartment } = this.state;
    const Option = Select.Option;
    let options = [];
    for (const department of departments) {
      options.push(<Option key={department.id}>{department.name}</Option>);
    }

    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    const disabled = currentUser.role === 'admin' ? false : true;
    const props = {
      name: 'file',
      multiple: false,
      action: '//jsonplaceholder.typicode.com/posts/',
      onChange(info) {
        const status = info.file.status;
        if (status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (status === 'done') {
          message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };
    return (
      <Form className={{ FormStyles }}>
        <FormItem className={FormItemStyles}>
          <div>
            {getFieldDecorator('Avatar', {
              valuePropName: 'avatar',
              getValueFromEvent: this.normFile,
            })(
              <label className={LabelStyles}>
                New Profile Picture:
                <Upload.Dragger className={InputStyles} {...props}>
                  <p>
                    <Icon type="inbox" />
                  </p>
                  <p>
                    Click here to upload new profile picture, or drag a file to
                    this area to upload.
                  </p>
                </Upload.Dragger>
              </label>
            )}
          </div>
        </FormItem>
        <FormItem className={cx(InputStyles, FormItemStyles)}>
          <label className={LabelStyles}>First Name:</label>
          {getFieldDecorator('firstName', {
            initialValue: `${user.firstName}`,
            rules: [{ required: true, message: 'Please input First Name!' }],
          })(<Input placeholder="First Name" />)}
        </FormItem>
        <FormItem className={cx(InputStyles, FormItemStyles)}>
          <label className={LabelStyles}>Last Name:</label>
          {getFieldDecorator('lastName', {
            initialValue: `${user.lastName}`,
            rules: [{ required: true, message: 'Please input Last Name!' }],
          })(<Input placeholder="Last Name" />)}
        </FormItem>
        <FormItem className={cx(InputStyles, FormItemStyles)}>
          <label className={LabelStyles}>Department:</label>
          <div className={SelectContainer}>
            <Select
              defaultValue={{
                key: user.departmentId,
                label: user.department.name,
              }}
              labelInValue={true}
              className={SelectStyles}
              dropdownClassName={DropDown}
              value={{
                key: selectedDepartment.key,
                label: selectedDepartment.label,
              }}
              showArrow={false}
              onSelect={value => this.handleSelect(value)}
              placeholder={
                <p style={{ fontSize: 16, margin: 0 }}>Add a Department</p>
              }
            >
              {options}
            </Select>
            <WebIcon
              className={SortDown}
              color={COLORS.darkGray}
              size={14}
              name="sort-down"
            />
          </div>
        </FormItem>
        <FormItem className={cx(InputStyles, FormItemStyles)}>
          <label className={LabelStyles}>Job Title:</label>
          {getFieldDecorator('title', {
            initialValue: `${user.title}`,
            rules: [
              { required: true, message: 'Please input your job title!' },
            ],
          })(<Input placeholder="Job Title" />)}
        </FormItem>
        <FormItem className={cx(InputStyles, FormItemStyles)}>
          <label className={LabelStyles}>E-mail:</label>
          {getFieldDecorator('email', {
            initialValue: `${user.emailAddress}`,
            rules: [
              { required: true, message: 'Please input your E-mail Address!' },
            ],
          })(<Input disabled={disabled} placeholder="E-mail" />)}
        </FormItem>
        <FormItem className={SubmitBtnContainer}>
          <Button
            className={SubmitBtn}
            onClick={this.handleSubmit}
            htmlType="submit"
          >
            Submit
            <Icon className={CheckIcon} type="check" />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(EditUserProfileForm);
