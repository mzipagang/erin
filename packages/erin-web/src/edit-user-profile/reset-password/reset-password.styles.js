import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const ModalContainer = css({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

export const ModalTitle = css({
  width: '100%',
  textAlign: 'center',
  color: `${COLORS.red} !important`,
  fontSize: 28,
  marginBottom: 30,
  fontWeight: 600,
});

export const SubmitBtn = css({
  fontFamily: 'Open Sans',
  backgroundColor: COLORS.red,
  color: 'white',
  fontSize: 20,
  fontWeight: 300,
  padding: '10px 30px',
  height: 'auto',
  border: 'none',
  marginBottom: 0,
});

export const SubmitBtnContainer = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  paddingTop: 15,
  '& .ant-form-item-control-wrapper': {
    '@media (max-width: 575px)': {
      width: 'auto',
    },
  },
  '& .ant-btn: hover': {
    color: COLORS.blue,
    fontWeight: 300,
    border: `2px solid ${COLORS.blue}`,
  },
});

export const CheckIcon = css({
  borderRadius: 100,
  border: '1px solid white',
  padding: 2,
});

export const AlertError = css({
  marginBottom: 20,
});

export const FormStyles = css({
  width: '100%',
});
