import { connect } from 'react-redux';
import EditUserProfileComponent from './edit-user-profile.component';
import { withUserByIdEditProfile } from 'erin-app-state-mgmt/src/_shared/api/components/edit-profile/with-user-by-id-edit-profile.provider';
import { compose } from '../_shared/services/utils';

const mapStateToProps = (state, props) => {
  const { currentUser, users } = state.editUserProfile;
  const { id } = props.match.params;
  const selectedUser = users.find(user => user.userId === id);
  return {
    selectedUser,
    currentUser,
    id,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const EditUserProfileWithAPI = compose(withUserByIdEditProfile)(
  EditUserProfileComponent
);

export const EditUserProfile = connect(mapStateToProps)(EditUserProfileWithAPI);
