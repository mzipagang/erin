import React from 'react';
import Cookies from 'js-cookie';
import { Provider } from 'react-redux';
import store from 'erin-app-state-mgmt';
import 'antd/dist/antd.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { RouterContext } from './_shared/contexts/router.context';
import { configureApp } from './app.config';
import { AppRouter } from './app-router.component';

// run configuration scripts
configureApp();

export default class App extends React.Component {
  constructor() {
    super();
    this.state = { auth: false };
    this.onAuthentication = this.onAuthentication.bind(this);
  }

  onAuthentication = authToken => {
    Cookies.set('jwt', authToken);
    this.setState({
      auth: true,
    });
  };

  onSignout = () => {
    Cookies.remove('jwt');
    this.setState({
      auth: false,
    });
  };

  render() {
    const getCachedJwt = () => {
      const jwt = Cookies.get('jwt');
      return jwt;
    };

    const providerValue = Object.assign(this.state, {
      auth: getCachedJwt(),
      onAuthentication: this.onAuthentication,
    });

    return (
      <Provider store={store}>
        <RouterContext.Provider value={providerValue}>
          <Router>
            <AppRouter auth={this.state.auth} />
          </Router>
        </RouterContext.Provider>
      </Provider>
    );
  }
}
