import React from 'react';
import { List, Avatar } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import _ from 'lodash';
import ErinLogo from '../../_shared/assets/erin_square.png';
import { COLORS } from '../../_shared/styles/colors';
import WebIcon from '../../_shared/components/web-icon.component';
import { NOTIFICATIONS } from '../../_shared/constants/notification-types.enum';
import {
  ListStyles,
  ListPageStyles,
  RemoveMargins,
  Bold,
  ListItemStyles,
  ListItemStylesBlue,
  ListItemPageStyles,
  ListItemPageStylesBlue,
  ViewMore,
  NoPicture,
  ListTextContainer,
  TimeStyles,
  CompanyAvatar,
} from '../web-notifications.styles';

const WebNotificationList = props => {
  const {
    sortedNotifications,
    currentUser: { checkedNotifications },
    dropDown,
  } = props;

  const pagination = {
    showTotal: function(total, range) {
      return `${range[0]}-${range[1]} of ${total} notifications`;
    },
  };

  const renderAvatar = notification => {
    const {
      type,
      createdBy: { avatar, firstName, lastName },
    } = notification;
    return type === NOTIFICATIONS.contactMatches ? (
      <div>
        <img
          style={{ width: 50 }}
          className={Avatar}
          src={ErinLogo}
          alt="Parsed"
        />
      </div>
    ) : type === NOTIFICATIONS.newJob ? (
      <div className={CompanyAvatar}>
        <WebIcon name="id" size={40} color="white" />
      </div>
    ) : type === NOTIFICATIONS.hiredReferral ? (
      <div className={CompanyAvatar}>
        <WebIcon name="id" size={40} color="white" />
      </div>
    ) : avatar !== null ? (
      <div>
        <img className={Avatar} src={avatar} alt={`${firstName} ${lastName}`} />
      </div>
    ) : (
      <div className={NoPicture} style={{ float: 'left' }}>
        <h3>
          {firstName[0]}
          {lastName[0]}
        </h3>
      </div>
    );
  };

  return (
    <div>
      <List
        className={dropDown ? ListStyles : ListPageStyles}
        dataSource={_.take(sortedNotifications, 10)}
        pagination={dropDown ? null : pagination}
        renderItem={item => (
          <List.Item
            className={
              (!checkedNotifications || item.created > checkedNotifications) &&
              dropDown
                ? ListItemStylesBlue
                : item.created < checkedNotifications && dropDown
                  ? ListItemStyles
                  : (!checkedNotifications ||
                      item.created > checkedNotifications) &&
                    !dropDown
                    ? ListItemPageStylesBlue
                    : item.created < checkedNotifications && !dropDown
                      ? ListItemPageStyles
                      : null
            }
          >
            <div>{renderAvatar(item)}</div>
            {item.type === NOTIFICATIONS.newReferral ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.createdBy.firstName} {item.createdBy.lastName}
                  </Link>{' '}
                  has made a <span className={Bold}>referral</span> for{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.referral.job}
                  </Link>
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : item.type === NOTIFICATIONS.newJob ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  <span className={Bold}>{item.createdBy.name}</span> has added
                  a <span className={Bold}>new job</span>{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.job.title}
                  </Link>
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : item.type === NOTIFICATIONS.acceptedReferral ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.createdBy.firstName} {item.createdBy.lastName}
                  </Link>{' '}
                  has <span className={Bold}>accepted</span> a referral for{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.referral.job}
                  </Link>
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : item.type === NOTIFICATIONS.hiredReferral ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  <span className={Bold}>{item.createdBy.name}</span> just{' '}
                  <span className={Bold}>hired</span>{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.referral.name.first} {item.referral.name.last}
                  </Link>{' '}
                  for{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.referral.job}
                  </Link>
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : item.type === NOTIFICATIONS.requestedReferral ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.createdBy.firstName} {item.createdBy.lastName}
                  </Link>{' '}
                  has requested a <span className={Bold}>referral</span> for{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.job.title}
                  </Link>
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : item.type === NOTIFICATIONS.contactMatches ? (
              <div className={ListTextContainer}>
                <p className={RemoveMargins}>
                  You know{' '}
                  <span className={Bold}>
                    {item.matchedContacts.length} people
                  </span>{' '}
                  that match the{' '}
                  <Link style={{ fontWeight: 600, color: COLORS.blue }} to="#">
                    {item.job.title}
                  </Link>{' '}
                  job. Click here to view them.
                </p>
                <span className={TimeStyles}>
                  {moment(`${item.created}`).fromNow()}
                </span>
              </div>
            ) : null}
          </List.Item>
        )}
      />
      {!dropDown ? null : (
        <div className={ViewMore}>
          <Link to="/notifications">View More</Link>
        </div>
      )}
    </div>
  );
};

export default WebNotificationList;
