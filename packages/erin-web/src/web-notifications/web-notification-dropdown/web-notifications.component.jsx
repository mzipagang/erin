import React from 'react';
import { Popover, Badge } from 'antd';

import WebIcon from 'erin-web/src/_shared/components/web-icon.component';
import WebNotificationList from '.././web-notification-list/web-notification-list.component';
import { COLORS } from 'erin-web/src/_shared/styles/colors';
import {
  PopoverContainer,
  NotificationsContainer,
} from '../web-notifications.styles';

class WebNotificationsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userNotifications: props.notifications,
    };
  }

  render() {
    const { currentUser } = this.props;
    const { userNotifications } = this.state;
    const sortedNotifications = userNotifications.sort((a, b) => {
      return b.created - a.created;
    });

    const newNotificationCount = () => {
      let count = 0;
      sortedNotifications.forEach(notification => {
        if (!currentUser.checkedNotifications) {
          count = sortedNotifications.length;
        } else if (notification.created > currentUser.checkedNotifications) {
          count += 1;
        }
      });
      return count > 99 ? '99' : count;
    };
    return (
      <div className={NotificationsContainer}>
        <div className={PopoverContainer}>
          <Popover
            trigger="hover"
            placement="bottomRight"
            content={
              <WebNotificationList
                currentUser={currentUser}
                sortedNotifications={sortedNotifications}
                dropDown={true}
              />
            }
          >
            <Badge count={newNotificationCount()}>
              <WebIcon color={`${COLORS.lightGray}`} name="ring" size={25} />
            </Badge>
          </Popover>
        </div>
      </div>
    );
  }
}

export default WebNotificationsComponent;
