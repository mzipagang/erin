import { connect } from 'react-redux';
import WebNotificationsComponent from './web-notifications.component';
import { actions } from 'erin-app-state-mgmt';

const { notificationsActions } = actions;

const mapStateToProps = state => {
  const { notifications } = state.notifications;
  const { currentUser } = state.user;
  return {
    currentUser,
    notifications,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserNotifications(currentUser) {
      dispatch(
        notificationsActions.createFetchNotificationsAction(currentUser)
      );
    },
  };
};

export const WebNotifications = connect(
  mapStateToProps,
  mapDispatchToProps
)(WebNotificationsComponent);
