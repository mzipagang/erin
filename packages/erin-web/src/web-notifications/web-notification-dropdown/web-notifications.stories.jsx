import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import WebNotificationsComponent from './web-notifications.component';
import { notificationData, userData } from '../../../../../test/data/mb-data';

const currentUser = userData[0];

storiesOf('Development/Web Notifications', module)
  .addDecorator(StoryRouter())
  .add('Drop Down', () => (
    <WebNotificationsComponent
      currentUser={currentUser}
      notifications={notificationData}
    />
  ));

storiesOf('QA/Web Notifications', module)
  .addDecorator(StoryRouter())
  .add('Drop Down', () => (
    <WebNotificationsComponent
      currentUser={currentUser}
      notifications={notificationData}
    />
  ));
