import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import WebNotificationsPageComponent from './web-notifications-page.component';
import { notificationData, userData } from '../../../../../test/data/mb-data';

const currentUser = userData[0];

storiesOf('Development/Web Notifications', module)
  .addDecorator(StoryRouter())
  .add('List Page', () => (
    <WebNotificationsPageComponent
      currentUser={currentUser}
      notifications={notificationData}
    />
  ));

storiesOf('QA/Web Notifications', module)
  .addDecorator(StoryRouter())
  .add('List Page', () => (
    <WebNotificationsPageComponent
      currentUser={currentUser}
      notifications={notificationData}
    />
  ));
