import React from 'react';
import WebNotificationList from '.././web-notification-list/web-notification-list.component';
import { Avatar } from 'antd';
import {
  NotificationPageContainer,
  ListContainer,
  ListAvatar,
  Header,
  NameStyles,
  NotificationCount,
  InfoContainer,
  Bold,
  NoPicture,
} from '../web-notifications.styles';

class WebNotificationsPageComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userNotifications: props.notifications,
    };
  }

  render() {
    const { currentUser } = this.props;
    const { userNotifications } = this.state;
    const sortedNotifications = userNotifications.sort((a, b) => {
      return b.created - a.created;
    });

    const newNotificationCount = () => {
      let count = 0;
      sortedNotifications.forEach(notification => {
        if (notification.created > currentUser.checkedNotifications) {
          count += 1;
        }
      });
      return count > 99 ? '99' : count;
    };

    return (
      <div className={NotificationPageContainer}>
        <div className={ListContainer}>
          <div className={Header}>
            {currentUser.avatar === null || currentUser.avatar === '' ? (
              <div className={NoPicture}>
                <h3>
                  {currentUser.firstName[0]}
                  {currentUser.lastName[0]}
                </h3>
              </div>
            ) : (
              <Avatar
                src={currentUser.avatar}
                size={50}
                shape="square"
                className={ListAvatar}
              />
            )}
            <div className={InfoContainer}>
              <h1 className={NameStyles}>
                {currentUser.firstName} {currentUser.lastName}
              </h1>
              <p className={NotificationCount}>
                You have <span className={Bold}>{newNotificationCount()}</span>{' '}
                new notification(s)
              </p>
            </div>
          </div>
          <WebNotificationList
            currentUser={currentUser}
            sortedNotifications={sortedNotifications}
            dropDown={false}
          />
        </div>
      </div>
    );
  }
}

export default WebNotificationsPageComponent;
