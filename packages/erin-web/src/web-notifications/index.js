export {
  WebNotifications,
} from './web-notification-dropdown/web-notifications.container';
export {
  WebNotificationsPage,
} from './web-notification-page/web-notifications-page.container';
