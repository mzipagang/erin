import React from 'react';

import { storiesOf } from '@storybook/react';
import { SuperUser } from './superUser.component';
import { userData } from '../../../../test/data';

storiesOf('Development/Super User', module).add('Super User Screen', () => (
  <SuperUser users={userData} />
));

storiesOf('QA/Super User', module).add('Super User Screen', () => (
  <SuperUser users={userData} />
));
