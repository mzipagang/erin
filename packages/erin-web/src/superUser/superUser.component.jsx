import React from 'react';
import { SuperUserContainer, Card, Banner } from './superUser.styles';

import CompanyCard from './addCompanyCard/companyCard.component';
// import SuperUserCard from './addSuperUserCard/superUserCard.component';

export class SuperUser extends React.Component {
  render() {
    console.log(this.props);
    const { users, addCompany } = this.props;
    return (
      <main className={SuperUserContainer}>
        <div className={Card}>
          <div className={Banner}>Erin System Admin Console</div>
          <div style={{ display: 'flex' }}>
            <CompanyCard users={users} addCompany={addCompany} />
            {/* <SuperUserCard users={users} addSuperUser={addSuperUser} /> */}
          </div>
        </div>
      </main>
    );
  }
}
