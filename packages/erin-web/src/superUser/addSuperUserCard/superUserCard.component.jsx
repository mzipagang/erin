import React from 'react';
import {
  addCompanyLabel,
  InputStyles,
  titleStyle,
  inputContainer,
} from './superUserCard.styles';
import { Card, Form, Input, Button, Alert, message } from 'antd';

class SuperUserCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailExists: false,
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const { users, addSuperUser } = this.props;
    this.props.form.validateFields((err, values) => {
      if (err) {
        console.log(err);
        return;
      }
      if (users.find(user => user.email === values.email)) {
        this.setState({ emailExists: true });
      } else {
        this.setState({ emailExists: false });
        message.config({
          top: 40,
        });
        message
          .loading('Processing...', 2.5)
          .then(() => message.success('Superuser successfully created!', 2.5));
        addSuperUser(values);
      }
    });
  };

  render() {
    const { emailExists } = this.state;
    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    return (
      <Card style={{ width: '45%', border: '0' }}>
        <div className={addCompanyLabel}>Add a New SuperUser</div>
        <Form>
          <FormItem className={InputStyles}>
            <label className={titleStyle}>Admin Email Address</label>
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Email address required',
                },
              ],
            })(<Input className={inputContainer} />)}
          </FormItem>
          <FormItem className={InputStyles}>
            <label className={titleStyle}>Password</label>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Password required',
                },
              ],
            })(<Input className={inputContainer} />)}
          </FormItem>
          {emailExists ? (
            <Alert type="error" message="Email is already in use" />
          ) : null}
          <FormItem style={{ float: 'right', marginTop: '10px' }}>
            <Button
              onClick={this.handleSubmit}
              type="primary"
              htmlType="submit"
            >
              Submit
            </Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

export default Form.create()(SuperUserCard);
