import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const SuperUserContainer = css({
  background: COLORS.lightGray2,
  padding: 50,
  fontWeight: 600,
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
});

export const Card = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 4,
  backgroundColor: COLORS.white,
  padding: 20,
  marginTop: 30,
  width: 800,
  //height: 500,
});

export const Banner = css({
  height: 60,
  width: '95%',
  padding: 15,
  background: COLORS.lightGray2,
  textAlign: 'center',
  margin: 'auto',
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: '1.5em',
});
