import React from 'react';
import {
  addCompanyLabel,
  InputStyles,
  titleStyle,
  alertStyle,
  inputContainer,
} from './companyCard.styles';
import { Card, Form, Input, Button, Alert, message, Table } from 'antd';

class CompanyCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailExists: false,
      companyExists: false,
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const { users, addCompany } = this.props;
    this.props.form.validateFields((err, values) => {
      if (err) {
        console.log(err);
        return;
      }
      if (
        users.find(
          user => user.company === values.company && user.email === values.email
        )
      ) {
        this.setState({ companyExists: true });
        this.setState({ emailExists: true });
      } else if (
        users.find(
          user => user.email !== values.email && user.company === values.company
        )
      ) {
        this.setState({ emailExists: false });
        this.setState({ companyExists: true });
      } else if (
        users.find(
          user => user.email === values.email && user.company !== values.company
        )
      ) {
        this.setState({ emailExists: true });
        this.setState({ companyExists: false });
      } else {
        this.setState({ companyExists: false });
        this.setState({ emailExists: false });
        message.config({
          top: 40,
        });
        message
          .loading('Processing...', 2.5)
          .then(() =>
            message.success(`${values.company} successfully created!`, 2.5)
          );
        addCompany(values);
      }
    });
  };

  render() {
    const dataSource = [
      {
        key: '1',
        name: 'TestCo',
        email: 'test@test.com',
        password: 'testpassword',
        date: '12/13/2018',
      },
    ];

    const columns = [
      {
        title: 'Company Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Password',
        dataIndex: 'password',
        key: 'password',
      },
      {
        title: 'Date Added',
        dataIndex: 'date',
        key: 'date',
      },
    ];

    const { emailExists, companyExists } = this.state;
    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    return (
      <div style={{ width: '100%' }}>
        <div style={{ display: 'flex' }}>
          <Card style={{ width: '50%', border: '0', marginRight: '15px' }}>
            <div className={addCompanyLabel}>Add a New Company</div>
            <Form>
              <FormItem className={InputStyles}>
                <label className={titleStyle}>Company Name</label>
                {getFieldDecorator('company', {
                  rules: [
                    {
                      required: true,
                      message: 'Company name required',
                    },
                  ],
                })(<Input className={inputContainer} />)}
              </FormItem>
              <FormItem className={InputStyles}>
                <label className={titleStyle}>Admin Email Address</label>
                {getFieldDecorator('email', {
                  rules: [
                    {
                      required: true,
                      message: 'Email address required',
                    },
                  ],
                })(<Input className={inputContainer} />)}
              </FormItem>
              <FormItem className={InputStyles}>
                <label className={titleStyle}>Password</label>
                {getFieldDecorator('password', {
                  rules: [
                    {
                      required: true,
                      message: 'Password required',
                    },
                  ],
                })(<Input className={inputContainer} />)}
              </FormItem>
              {companyExists ? (
                <Alert type="error" message="Company already exists" />
              ) : null}
              {emailExists ? (
                <Alert type="error" message="Email is already in use" />
              ) : null}
              <FormItem style={{ float: 'right', marginTop: '10px' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.handleSubmit}
                >
                  Submit
                </Button>
              </FormItem>
            </Form>
          </Card>
          <div className={alertStyle}>
            These login credentials are for set-up purposes only. Please add
            company admins from within Erin and not from this screen.
          </div>
        </div>
        <Table dataSource={dataSource} columns={columns} />
      </div>
    );
  }
}

export default Form.create()(CompanyCard);
