import { connect } from 'react-redux';
import { SuperUser as SuperUserComponent } from './superUser.component';
import { actions } from 'erin-app-state-mgmt';

const { userActions } = actions;

const mapStateToProps = state => {
  const { users } = state.user;
  return {
    users,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addSuperUser(user) {
      dispatch(userActions.addSuperUserAction(user));
    },
    addCompany(company) {
      dispatch(userActions.addCompanyAction(company));
    },
  };
};

export const SuperUser = connect(
  mapStateToProps,
  mapDispatchToProps
)(SuperUserComponent);
