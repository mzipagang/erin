import React from 'react';

import ReferralsTable from '../_shared/components/referrals-table.component';
import FilterByDepartment from './referrals-admin-dashboard-components/filter-by-department.component';
import SearchByName from './referrals-admin-dashboard-components/search-by-name.component';
import FilterByStatus from './referrals-admin-dashboard-components/status-filter.component';

import {
  ReferralsDashboardContainer,
  TableContainer,
  ToolBar,
  FilterContainer,
} from './referrals-admin-dashboard.styles.js';

const filterAcceptedKey = 'accepted';
const filterReferredKey = 'referred';

class ReferralsDashboardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredData: this.props.referrals,
      filterReferralStatus: '',
    };
    this.handleNameSearch = this.handleNameSearch.bind(this);
    this.handleDepartmentFilter = this.handleDepartmentFilter.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.referrals !== this.props.referrals) {
      this.setState({ filteredData: this.props.referrals });
    }
  }

  sortByAlph = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  handleStatusFilter = value => {
    let results = this.props.referrals.filter(
      record => record.status.toLowerCase() === value.toLowerCase()
    );
    this.setState({ filteredData: results });
  };

  clearStatusFilter = () => {
    this.setState({
      filteredData: this.props.referrals,
      filterReferralStatus: '',
    });
  };

  toggleButtonStyles = filterReferralStatus => {
    this.setState({
      filterReferralStatus,
    });
  };

  handleDepartmentFilter(value) {
    if (value.length === 0) {
      this.setState({ filteredData: this.props.referrals });
      return;
    }
    let results = [];
    this.props.referrals.forEach(record => {
      if (value.includes(record.job.department.name)) {
        results.push(record);
      }
    });
    this.setState({ filteredData: results });
  }

  handleNameSearch(value) {
    let results = [];
    this.props.referrals.forEach(record => {
      if (record.contact.lastName.includes(value)) {
        results.push(record);
      }
    });
    this.setState({ filteredData: results });
  }

  render() {
    const { filterReferralStatus } = this.state;
    return (
      <main className={ReferralsDashboardContainer}>
        <section className={ToolBar}>
          <div className={FilterContainer}>
            <FilterByStatus
              toggleButtonStyles={this.toggleButtonStyles}
              filterOpenStatus={filterReferralStatus}
              acceptedKey={filterAcceptedKey}
              referredKey={filterReferredKey}
              handleStatusFilter={this.handleStatusFilter}
              clearStatusFilter={this.clearStatusFilter}
            />
            <FilterByDepartment
              departments={this.props.departments}
              handleDepartmentFilter={this.handleDepartmentFilter}
            />
          </div>
          <SearchByName handleNameSearch={this.handleNameSearch} />
        </section>
        <section className={TableContainer}>
          <ReferralsTable
            filteredData={this.state.filteredData}
            sortByAlph={this.sortByAlph}
            rowKey={record => record.id}
          />
        </section>
      </main>
    );
  }
}

export default ReferralsDashboardComponent;
