import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import ReferralsDashboardComponent from './referrals-admin-dashboard.component';
import { referralData, userData } from '../../../../test/data/mb-data';

const departments = [
  { name: 'Department 001' },
  { name: 'Department 002' },
  { name: 'Department 003' },
  { name: 'Department 004' },
];
storiesOf('Development/ReferralsDashboard', module)
  .addDecorator(story => <div style={{ width: '100%' }}>{story()}</div>)
  .addDecorator(StoryRouter())
  .add('table', () => (
    <ReferralsDashboardComponent
      departments={departments}
      currentUser={userData[0]}
      referrals={referralData}
    />
  ));

storiesOf('QA/ReferralsDashboard', module)
  .addDecorator(story => <div style={{ width: '100%' }}>{story()}</div>)
  .addDecorator(StoryRouter())
  .add('table', () => (
    <ReferralsDashboardComponent
      departments={departments}
      currentUser={userData[0]}
      referrals={referralData}
    />
  ));
