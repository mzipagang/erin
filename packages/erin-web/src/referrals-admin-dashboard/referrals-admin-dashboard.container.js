import React from 'react';
import { connect } from 'react-redux';
import ReferralsDashboardComponent from './referrals-admin-dashboard.component';
import { withListReferrals } from 'erin-app-state-mgmt/src/_shared/api/components/referralsDashboard/withListReferrals';

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
  };
};

export const ReferralsDashboard = props => {
  const Component = withListReferrals(ReferralsDashboardComponent, {
    filter: { companyId: { eq: props.currentUser.companyId } },
  });
  return <Component {...props} />;
};

export const ReferralsDashboardContainer = connect(mapStateToProps)(
  ReferralsDashboard
);
