import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const ReferralsDashboardContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  paddingRight: 30,
});

export const StyledHeading = css({
  fontFamily: '"Open Sans", sans-serif',
  color: '#444444',
  fontWeight: 600,
});

export const SortDown = css({
  zIndex: 99,
});

export const styleFont = css({
  fontFamily: '"Open Sans", sans-serif',
});

export const ToolBar = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: 20,
  maxWidth: 1290,
  paddingRight: 20,
});

export const StatusFilter = css({
  display: 'flex',
  padding: 0,
  '& h3': {
    marginRight: 10,
    fontSize: 18,
  },
  '& .ant-btn': {
    lineHeight: '12px',
  },
});

export const FilterContainer = css({
  display: 'flex',
  maxWidth: 700,
});

export const TableContainer = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  backgroundColor: 'white',
  '& thead > tr > th': {
    backgroundColor: 'white',
    fontWeight: 700,
  },
  '& tbody > tr > td': {
    paddingTop: 0,
    paddingBottom: 0,
    height: 65,
  },
  maxWidth: 1270,
  marginRight: 20,
});

export const SearchStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  '& input': {
    fontFamily: '"Open Sans", sans-serif',
    borderRadius: 20,
    borderColor: COLORS.lightGray,
    paddingLeft: '40px !important',
  },
  '& span': {
    left: '12px !important',
  },
  '& i': {
    color: COLORS.darkGray,
    fontSize: 18,
  },
  marginTop: -5,
});

export const SelectStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  marginRight: 10,
  marginLeft: 10,
  width: 300,
  '& .ant-select-selection': {
    borderColor: COLORS.lightGray,
  },
  '& .ant-select-selection__choice': {
    backgroundColor: COLORS.lightGreen,
    color: COLORS.green,
  },
});

export const FormItemStyles = css({
  marginBottom: '0 !important',
});

export const ReferralButtonStyles = selected =>
  css({
    marginRight: 10,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.blue,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.blue,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    height: 30,
    width: 'auto',
  });

export const AllButtonStyles = css({
  backgroundColor: COLORS.red,
  width: 50,
  height: 30,
  color: 'white',
  border: 'none',
  '&:hover': {
    backgroundColor: 'white',
    color: COLORS.hyperLink,
    border: `1px solid ${COLORS.hyperLink}`,
  },
  '&:focus': {
    backgroundColor: COLORS.red,
    color: 'white',
    border: 'none',
  },
});

export const SelectContainer = css({
  '& svg': {
    transform: 'translate(-40px, 2px)',
  },
});
