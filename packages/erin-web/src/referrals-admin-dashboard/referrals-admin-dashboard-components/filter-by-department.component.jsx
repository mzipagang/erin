import React from 'react';
import { Select } from 'antd';

import {
  SelectStyles,
  SelectContainer,
} from '../referrals-admin-dashboard.styles';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';

const FilterByDepartment = props => {
  const { handleDepartmentFilter, departments } = props;
  const Option = Select.Option;

  const options = departments.map(department => {
    return <Option key={department.name}>{department.name}</Option>;
  });

  return (
    <div className={SelectContainer}>
      <Select
        className={SelectStyles}
        mode="multiple"
        placeholder="Filter By Department"
        onChange={value => handleDepartmentFilter(value)}
      >
        {options}
      </Select>
      <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
    </div>
  );
};

export default FilterByDepartment;
