import React from 'react';
import moment from 'moment';
import { Spin } from 'antd';
import { dashboardMainContainer, dashList } from './dashboard.styles';
import { QuickLinks } from './tiles/quick-links/quick-links.component';
import { LeaderBoard } from './tiles/leader-board/leader-board.component';
import { JobCard } from './tiles/job-card/job-card.component';
import { ReferralCard } from './tiles/referral-card/referral-card.component';
import { NetworkCard } from './tiles/business-network/business-network.component';
import { ConnectCard } from './tiles/connect-earn/connect-earn.component';
import { RankingCard } from './tiles/employee-ranking/employee-rank.component';
import { RecommendReferral } from './tiles/recommended-referral/rec-referral.component';
import { USER_ROLES } from '../_shared/constants/user-roles.enum';

export class Dashboard extends React.Component {
  handleBrowseJobs = () => {
    this.props.history.push('/browsejobs');
  };

  handleMyNetwork = () => {
    this.props.history.push('/network');
  };

  handleReferrals = () => {
    this.props.history.push('/referrals');
  };

  handleEmployees = () => {
    this.props.history.push('/employees');
  };

  componentDidMount = () => {
    this.props.getActiveJobs();
  };

  render() {
    if (!this.props.topReferrers) {
      return <Spin />;
    }
    if (!this.props.departments) {
      return <Spin />;
    }
    if (!this.props.referrals) {
      return <Spin />;
    }
    if (!this.props.jobs) {
      return <Spin />;
    }

    return (
      <main className={dashboardMainContainer}>
        <div>
          <ul className={dashList}>
            {this.props.currentUser.role === USER_ROLES.ADMIN && (
              <li>
                <NetworkCard
                  title="Business Network"
                  {...this.props}
                  onClick={() => {
                    this.handleMyNetwork();
                  }}
                />
                <LeaderBoard
                  isNumbered={true}
                  title="Top Referrers"
                  listAllPath="/employees"
                  entities={this.props.topReferrers
                    .map(u => ({
                      name: `${u.firstName} ${u.lastName}`,
                      score: u.totalReferrals,
                    }))
                    .sort(
                      (a, b) =>
                        b.score > a.score ? 1 : a.score > b.score ? -1 : 0
                    )}
                  onClick={() => {
                    this.handleEmployees();
                  }}
                />
              </li>
            )}
            <li>
              <JobCard
                title="Jobs"
                {...this.props}
                onClick={() => {
                  this.handleBrowseJobs();
                }}
              />
              {this.props.currentUser.role === USER_ROLES.EMPLOYEE && (
                <RecommendReferral
                  tileTitle="Recommended Referral"
                  {...this.props}
                  onClick={() => {
                    this.handleBrowseJobs();
                  }}
                />
              )}
              {this.props.currentUser.role === USER_ROLES.ADMIN && (
                <LeaderBoard
                  isNumbered={true}
                  title="Top Departments"
                  entities={this.props.departments
                    .map(d => ({
                      name: d.name,
                      score: `${d.totalUsers} Users`,
                    }))
                    .sort(
                      (a, b) =>
                        b.score > a.score ? 1 : a.score > b.score ? -1 : 0
                    )}
                  onClick={() => {
                    this.handleEmployees();
                  }}
                />
              )}
              {this.props.currentUser.role === USER_ROLES.MANAGER && (
                <LeaderBoard
                  isNumbered={false}
                  title="Recent Referrals"
                  listAllPath="/referrals"
                  entities={this.props.referrals
                    .sort(
                      (a, b) =>
                        moment(a.referralDate) > moment(b.referralDate)
                          ? -1
                          : moment(b.referralDate) > moment(a.referralDate)
                            ? 1
                            : 0
                    )
                    .map(r => ({
                      name: `${r.user.firstName} ${r.user.lastName}`,
                    }))
                    .slice(0, 5)}
                  onClick={() => {
                    this.handleReferrals();
                  }}
                />
              )}
            </li>
            <li>
              {this.props.currentUser.role === USER_ROLES.ADMIN && (
                <div>
                  <ReferralCard
                    title="Accepted Referrals"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                  <ReferralCard
                    title="Referrals"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                </div>
              )}
              {this.props.currentUser.role === USER_ROLES.MANAGER && (
                <div>
                  <ReferralCard
                    title="Accepted Referrals"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                  <ReferralCard
                    title="Referrals"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                </div>
              )}
              {this.props.currentUser.role === USER_ROLES.EMPLOYEE && (
                <div>
                  <ReferralCard
                    title="Referrals Made"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                  <RankingCard
                    title="Your Ranking"
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                </div>
              )}
              {this.props.currentUser.role === USER_ROLES.ADMIN && (
                <QuickLinks />
              )}
              {this.props.currentUser.role === USER_ROLES.MANAGER && (
                <LeaderBoard
                  isNumbered={false}
                  title="Recent Jobs"
                  listAllPath="/browseJobs"
                  entities={this.props.jobs
                    .filter(j => (j.status = 'open'))
                    .slice(0, 5)
                    .map(u => ({
                      name: u.title,
                      id: u.id,
                    }))
                    .sort(
                      (a, b) =>
                        a.title > b.title ? 1 : b.title > a.title ? -1 : 0
                    )}
                />
              )}
            </li>
            <li>
              {this.props.currentUser.role === USER_ROLES.EMPLOYEE && (
                <div>
                  <ConnectCard
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                </div>
              )}
              {this.props.currentUser.role === USER_ROLES.MANAGER && (
                <div>
                  <ConnectCard
                    {...this.props}
                    onClick={() => {
                      this.handleReferrals();
                    }}
                  />
                  <LeaderBoard
                    isNumbered={false}
                    title="Your Departments"
                    entities={this.props.departments
                      .map(d => ({
                        name: d.name,
                      }))
                      .sort(
                        (a, b) =>
                          a.name > b.name ? 1 : b.name > a.name ? -1 : 0
                      )}
                    onClick={() => {
                      this.handleEmployees();
                    }}
                  />
                </div>
              )}
            </li>
          </ul>
        </div>
      </main>
    );
  }
}
