import { compose } from 'redux';
import { connect } from 'react-redux';
import { Dashboard as DashboardComponent } from './dashboard.component';
import { actions } from 'erin-app-state-mgmt';
import { withListJobs } from 'erin-app-state-mgmt/src/_shared/api/components/jobs/with-list-jobs.provider';
import { withListDepartment } from 'erin-app-state-mgmt/src/_shared/api/components/departments/with-list-departments.provider';
import { withListReferrals } from 'erin-app-state-mgmt/src/_shared/api/components/referrals/with-list-referrals.provider';
import { withListUsers } from 'erin-app-state-mgmt/src/_shared/api/components/users/with-list-users.provider';
import { withDashboardReferrals } from 'erin-app-state-mgmt/src/_shared/api/components/dashboard/with-dashboard-referrals.provider';
import { withAcceptedDashboardReferrals } from 'erin-app-state-mgmt/src/_shared/api/components/dashboard/with-accepted-dashboard-referrals.provider';
import { withDashboardJobs } from 'erin-app-state-mgmt/src/_shared/api/components/dashboard/with-dashboard-jobs.provider';
import { withBusinessNetwork } from 'erin-app-state-mgmt/src/_shared/api/components/dashboard/with-business-network.provider';
import { withTopReferrers } from 'erin-app-state-mgmt/src/_shared/api/components/dashboard/with-top-referrers.provider';

const { dashboardActions } = actions;

const mapStateToProps = state => {
  const { currentUser } = state.user;
  const { referrals } = state.referral;
  const { topReferrers } = state.user;
  const { activeJobs } = state.dashboard;
  const { selectJob } = state.dashboard;
  const { jobShares } = state.dashboard;
  const { jobViews } = state.dashboard;
  const { jobMatches } = state.dashboard;
  const { jobOpenPositions } = state.dashboard;
  const { referralTotal } = state.dashboard;
  const { prevReferralTotal } = state.dashboard;
  const { referralAcceptedTotal } = state.dashboard;
  const { prevReferralAcceptedTotal } = state.dashboard;
  const { businessNetwork } = state.dashboard;
  const { businessNetworkTotal } = state.dashboard;
  const { departments } = state.dashboard;
  return {
    currentUser,
    referrals,
    activeJobs,
    selectJob,
    jobShares,
    jobViews,
    jobMatches,
    jobOpenPositions,
    referralTotal,
    prevReferralTotal,
    referralAcceptedTotal,
    prevReferralAcceptedTotal,
    businessNetwork,
    businessNetworkTotal,
    departments,
    topReferrers,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getActiveJobs() {
      dispatch(dashboardActions.createSetActiveJobsAction());
    },
    getBusinessNetwork() {
      dispatch(dashboardActions.createGetBusinessNetworkAction());
    },
    getDepartments() {
      dispatch(dashboardActions.createGetDepartmentsAction());
    },
  };
};

const DashboardContainer = compose(
  withListJobs,
  withListDepartment,
  withListReferrals,
  withListUsers,
  withDashboardJobs,
  withDashboardReferrals,
  withAcceptedDashboardReferrals,
  withBusinessNetwork,
  withTopReferrers
)(DashboardComponent);

export const Dashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardContainer);
