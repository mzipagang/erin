import { css } from 'emotion';

export const dashboardMainContainer = css({
  display: 'flex',
  margin: 10,
  width: '100%',
  //justifyContent: 'center',
});

export const dashList = css({
  display: 'flex',
  listStyleType: 'none',
  padding: 0,
});
