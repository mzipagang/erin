import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { Dashboard } from './dashboard.component';

import { userData, jobData, referralData } from '../../../../test/data';
import { jobData as prevJobData } from '../../../../test/data/mz-data/jobs.data';
import { businessNetworkData } from '../../../../test/data/mz-data/business-network.data';
import { departmentData } from '../../../../test/data/departments.data';

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Dashboard', () => (
    <Dashboard
      getActiveJobs={() => jobData}
      getBusinessNetwork={() => businessNetworkData}
      getDepartments={() => departmentData}
      currentUser={userData[0]}
      referrals={referralData}
      activeJobs={jobData.filter(prop => prop.isOpen)}
      jobShares={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop.shares);
      }, 0)}
      jobViews={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop.views);
      }, 0)}
      jobMatches={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop['_matches']);
      }, 0)}
      jobOpenPositions={jobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop.isOpen);
        }, 0)}
      businessNetwork={businessNetworkData}
      businessNetworkTotal={businessNetworkData.reduce((sum, prop) => {
        return (sum += prop.amount);
      }, 0)}
      referralTotal={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop['_referrals']);
      }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      referralAcceptedTotal={jobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      prevReferralAcceptedTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      topReferrers={userData}
      departments={departmentData}
      selectJob={jobData[0]}
    />
  ));

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Dashboard', () => (
    <Dashboard
      getActiveJobs={() => jobData}
      getBusinessNetwork={() => businessNetworkData}
      getDepartments={() => departmentData}
      currentUser={userData[0]}
      referrals={referralData}
      activeJobs={jobData.filter(prop => prop.isOpen)}
      jobShares={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop.shares);
      }, 0)}
      jobViews={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop.views);
      }, 0)}
      jobMatches={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop['_matches']);
      }, 0)}
      jobOpenPositions={jobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop.isOpen);
        }, 0)}
      businessNetwork={businessNetworkData}
      businessNetworkTotal={businessNetworkData.reduce((sum, prop) => {
        return (sum += prop.amount);
      }, 0)}
      referralTotal={jobData.filter(prop => prop.isOpen).reduce((sum, prop) => {
        return (sum += prop['_referrals']);
      }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      referralAcceptedTotal={jobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      prevReferralAcceptedTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      topReferrers={userData}
      departments={departmentData}
      selectJob={jobData[0]}
    />
  ));
