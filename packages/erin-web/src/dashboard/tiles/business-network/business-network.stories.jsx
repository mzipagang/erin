import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { NetworkCard } from './business-network.component';
import { businessNetworkData } from '../../../../../../test/data/mz-data/business-network.data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Network Card', () => (
    <NetworkCard
      businessNetwork={businessNetworkData}
      businessNetworkTotal={businessNetworkData.reduce((sum, prop) => {
        return (sum += prop.amount);
      }, 0)}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Network Card', () => (
    <NetworkCard
      businessNetwork={businessNetworkData}
      businessNetworkTotal={businessNetworkData.reduce((sum, prop) => {
        return (sum += prop.amount);
      }, 0)}
    />
  ));
