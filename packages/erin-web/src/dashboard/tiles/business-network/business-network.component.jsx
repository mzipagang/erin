import React from 'react';
import { Row, Col } from 'antd';
import {
  headerClass,
  middleContainer,
  middleColumn,
  divider,
  bottomContainer,
  bottomSectionText,
  bottomSectionValue,
  donutChart,
  chartTotal,
  numTotal,
  opacity,
  buttonActive,
  buttonBusiness,
  buttonFirst,
} from './business-network.styles';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import { COLORS } from '../../../_shared/styles/colors';
import DonutChart from 'react-donut-chart';

class BaseNetworkCard extends React.Component {
  render() {
    const {
      onClick,
      title,
      activeEmployees,
      businessConnections,
      firstConnections,
    } = this.props;
    return (
      <div onClick={onClick}>
        <h2 className={headerClass}>{title}</h2>
        <Row className={middleContainer}>
          <Col span={24} className={middleColumn}>
            <div className={donutChart}>
              <div className={chartTotal}>
                <div className={numTotal}>
                  {activeEmployees + businessConnections + firstConnections}
                </div>
                <div>People</div>
              </div>
              <DonutChart
                height={153}
                width={140}
                colors={[
                  COLORS.dashboardLightOrange,
                  COLORS.dashboardGreen,
                  COLORS.dashboardBlue,
                ]}
                strokeColor=""
                legend={false}
                clickToggle={false}
                data={[
                  {
                    label: 'first',
                    value: parseFloat(firstConnections),
                    className: opacity,
                  },
                  {
                    label: 'business',
                    value: parseFloat(businessConnections),
                    className: opacity,
                  },
                  {
                    label: 'active',
                    value: parseFloat(activeEmployees),
                    className: opacity,
                  },
                ]}
              />
            </div>
          </Col>
        </Row>
        <div className={divider} />
        <Row className={bottomContainer}>
          <Row>
            <button className={buttonActive} />
            <span className={bottomSectionText}> Active Employees: </span>
            <span className={bottomSectionValue}> {activeEmployees} </span>
          </Row>
          <Row>
            <button className={buttonBusiness} />
            <span className={bottomSectionText}> Business Connections: </span>
            <span className={bottomSectionValue}> {businessConnections}</span>
          </Row>
          <Row>
            <button className={buttonFirst} />
            <span className={bottomSectionText}> First Connections: </span>
            <span className={bottomSectionValue}> {firstConnections}</span>
          </Row>
        </Row>
      </div>
    );
  }
}

export const NetworkCard = borderedTile(BaseNetworkCard);
