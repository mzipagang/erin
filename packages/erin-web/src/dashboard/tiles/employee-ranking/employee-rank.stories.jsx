import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { RankingCard } from './employee-rank.component';
import { jobData as currentJobData } from '../../../../../../test/data';
import { jobData as prevJobData } from '../../../../../../test/data/mz-data/jobs.data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Ranking Card', () => (
    <RankingCard
      title="Your Ranking"
      referralTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Ranking Card', () => (
    <RankingCard
      title="Your Ranking"
      referralTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
    />
  ));
