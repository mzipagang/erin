import { css } from 'emotion';

import { COLORS } from '../../../_shared/styles/colors';

export const headerClass = css({
  textTransform: 'uppercase',
  fontSize: 16,
  letterSpacing: 2,
  fontWeight: 300,
  color: `${COLORS.heading} !important`,
  marginLeft: 15,
});

export const middleContainer = css({
  height: 80,
  //width: 180,
  margin: 'auto',
  marginBottom: 20,
});

export const middleColumn = css({
  textAlign: 'center',
  width: 100,
});

export const bigNumbers = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: COLORS.black,
  fontSize: '2em',
  marginLeft: 30,
});

export const bigNumbersOpen = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.white,
  fontSize: '2.75em',
});

export const numberSubtitles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.white,
  fontSize: '0.85em',
});

export const numberSubtitlesOpen = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.lightGray,
  fontSize: '0.85em',
  width: '100%',
  marginLeft: 15,
  marginBottom: 20,
});

export const smallIcons = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.subHeading,
  fontSize: '0.85em',
  marginLeft: 21,
  opacity: 0.7,
});
