import React from 'react';
import { Row, Col, Icon, Progress } from 'antd';
import {
  headerClass,
  middleContainer,
  middleColumn,
  bigNumbers,
  bigNumbersOpen,
  numberSubtitlesOpen,
} from './employee-rank.styles';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import { COLORS } from '../../../_shared/styles/colors';

class BaseRankingCard extends React.Component {
  getReferralColor() {
    if (this.props.referralTotal > this.props.prevReferralTotal) {
      return COLORS.dashboardGreen;
    }
    if (this.props.referralTotal < this.props.prevReferralTotal) {
      return COLORS.red;
    }
    return COLORS.dashboardLightOrange;
  }

  getReferralStatus() {
    if (this.props.referralTotal > this.props.prevReferralTotal) {
      return 'arrow-up';
    }
    if (this.props.referralTotal < this.props.prevReferralTotal) {
      return 'arrow-down';
    }
    return '';
  }
  render() {
    const { onClick, title, referralTotal, prevReferralTotal } = this.props;
    const percentChange = parseFloat(
      Math.abs(
        ((referralTotal - prevReferralTotal) / prevReferralTotal) * 100
      ).toFixed(0)
    );
    return (
      <div onClick={onClick}>
        <h2 className={headerClass}>{title}</h2>
        <Row className={middleContainer}>
          <Col span={8} className={middleColumn}>
            <span className={bigNumbers}># {referralTotal}</span>
            <div className={numberSubtitlesOpen}> Of All Employees </div>
          </Col>
          <Col span={8} />
          <Col
            span={8}
            style={{
              textAlign: 'center',
              width: 80,
              backgroundColor: this.getReferralColor(),
              borderRadius: '3%',
              height: 60,
              float: 'right',
            }}
          >
            <span className={bigNumbersOpen}>
              <Icon type={this.getReferralStatus()} />
            </span>
          </Col>
          <div style={{ fontWeight: '800', fontSize: '12px' }}>
            <Progress
              percent={percentChange}
              strokeColor={this.getReferralColor()}
              format={percentChange => `${percentChange}%`}
            />
          </div>
        </Row>
      </div>
    );
  }
}

export const RankingCard = borderedTile(BaseRankingCard);
