import { css } from 'emotion';
import { COLORS } from '../../../_shared/styles/colors';

export const tileContainerClass = css({
  width: '100%',
  height: 191,
  position: 'relative',
});

export const headerClass = css({
  fontSize: 16,
  textTransform: 'uppercase',
  letterSpacing: 2,
  fontWeight: 300,
  color: `${COLORS.heading} !important`,
  marginLeft: 0,
  width: '100%',
});

export const leaderListClass = css({
  marginBottom: 5,
  margin: '15px 0px',
  padding: 0,
  width: '100%',
  listStyle: 'none',
  counterReset: 'li',
  '& li': {
    counterIncrement: 'li',
  },
  '& li::before': {
    content: 'counter(li)',
    color: COLORS.lightGray,
    fontWeight: 600,
    display: 'inline-block',
    width: '1em',
    marginRight: '-5em',
  },
});
export const leaderListClassNoBullet = css({
  marginBottom: 5,
  margin: '15px 0px',
  padding: 0,
  width: '100%',
  listStyle: 'none',
  fontWeight: 600,
});

export const leaderListItemClass = css({
  display: 'flex',
  justifyContent: 'space-between',
});

export const emptyTile = css({
  textAlign: 'center',
  marginTop: '15px',
  color: COLORS.lightGray,
});
