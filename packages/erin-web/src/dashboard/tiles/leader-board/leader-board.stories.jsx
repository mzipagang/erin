import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { LeaderBoard } from './leader-board.component';

import { userData, departmentData } from '../../../../../../test/data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Top Referrers', () => (
    <LeaderBoard
      title="Top Referrers"
      listAllPath="/employees"
      entities={userData
        .map(u => ({
          name: `${u.name['firstname']} ${u.name['lastname']}`,
          score: u.referral,
        }))
        .sort((a, b) => (b.score > a.score ? 1 : a.score > b.score ? -1 : 0))}
    />
  ))
  .add('Top Departments', () => (
    <LeaderBoard
      title="Top Departments"
      listAllPath="/employees"
      entities={departmentData
        .map(d => ({
          name: d.name,
          score: `${d.users} Users`,
        }))
        .sort((a, b) => (b.score > a.score ? 1 : a.score > b.score ? -1 : 0))}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Top Referrers', () => (
    <LeaderBoard
      title="Top Referrers"
      listAllPath="/employees"
      entities={userData
        .map(u => ({
          name: `${u.name['firstname']} ${u.name['lastname']}`,
          score: u.referral,
        }))
        .sort((a, b) => (b.score > a.score ? 1 : a.score > b.score ? -1 : 0))}
    />
  ))
  .add('Top Departments', () => (
    <LeaderBoard
      title="Top Departments"
      listAllPath="/employees"
      entities={departmentData
        .map(d => ({
          name: d.name,
          score: `${d.users} Users`,
        }))
        .sort((a, b) => (b.score > a.score ? 1 : a.score > b.score ? -1 : 0))}
    />
  ));
