import React from 'react';
import { Link } from 'react-router-dom';
import WebIcon from '../../../_shared/components/web-icon.component';
import { COLORS } from '../../../_shared/styles/colors';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import {
  leaderListClassNoBullet,
  leaderListClass,
  leaderListItemClass,
  headerClass,
  tileContainerClass,
  emptyTile,
} from './leader-board.styles';

class BaseLeaderBoard extends React.Component {
  renderEntities(title, entities) {
    const listItems = entities.map((entity, index) => (
      <li className={leaderListItemClass} key={index}>
        {title === 'Recent Jobs' && (
          <span>
            <Link to={`/browsejobs/${entity.id}`}>{entity.name}</Link>
          </span>
        )}
        {title === 'Recent Referrals' && (
          <span>
            <Link to={`/referrals/${entity.id}`}>{entity.name}</Link>
          </span>
        )}
        {title === 'Your Departments' && (
          <span>
            <Link to="#">{entity.name}</Link>
          </span>
        )}
      </li>
    ));
    return listItems;
  }

  renderNumberedEntities(entities) {
    const listItems = entities.map((entity, index) => (
      <li key={index} className={leaderListItemClass}>
        <span style={{ fontWeight: 600, color: COLORS.lightGray }}>
          {entity.name}
        </span>
        <span style={{ fontWeight: 800 }}>{entity.score}</span>
      </li>
    ));
    return listItems;
  }

  render() {
    const { onClick, title, entities, isNumbered } = this.props;
    // const { onClick, title, entities, listAllPath, isNumbered } = this.props;
    const showEmptyContainer = !entities.length;
    return (
      <div onClick={onClick} className={tileContainerClass}>
        <div>
          <h2 className={headerClass}>{title}</h2>
          <div>
            {!showEmptyContainer ? (
              <div>
                {isNumbered ? (
                  <ol className={leaderListClass}>
                    {this.renderNumberedEntities(entities)}
                  </ol>
                ) : (
                  <ul className={leaderListClassNoBullet}>
                    {this.renderEntities(title, entities)}
                  </ul>
                )}
                <div style={{ position: 'absolute', bottom: 0, left: 105 }}>
                  <Link to="#">VIEW ALL</Link>
                </div>
              </div>
            ) : (
              <div className={emptyTile}>
                {title === 'Top Referrers' && (
                  <div>
                    <WebIcon name="sad" size={60} color={COLORS.lightGray} />
                    <div style={{ marginTop: '10px' }}>
                      Uh oh. You haven't added any employees yet. Don't worry,
                      just
                      <a style={{ color: COLORS.blue }}> click here </a> to add
                      them!
                    </div>
                  </div>
                )}
                {title === 'Top Departments' && (
                  <div>
                    <WebIcon name="gas" size={60} color={COLORS.lightGray} />
                    <div style={{ marginTop: '10px' }}>
                      You don't have any departments right now, but we'll update
                      this when you do!
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export const LeaderBoard = borderedTile(BaseLeaderBoard);
