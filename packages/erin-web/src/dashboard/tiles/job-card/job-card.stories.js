import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { JobCard } from './job-card.component';
import { jobData } from '../../../../../../test/data';
import { userData } from '../../../../../../test/data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Jobs Card', () => (
    <JobCard
      currentUser={userData[0]}
      jobShares={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.shares);
      }, 0)}
      jobViews={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.views);
      }, 0)}
      jobMatches={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b['_matches']);
      }, 0)}
      jobOpenPositions={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.isOpen);
      }, 0)}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Jobs Card', () => (
    <JobCard
      currentUser={userData[0]}
      jobShares={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.shares);
      }, 0)}
      jobViews={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.views);
      }, 0)}
      jobMatches={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b['_matches']);
      }, 0)}
      jobOpenPositions={jobData.filter(a => a.isOpen).reduce((a, b) => {
        return (a += b.isOpen);
      }, 0)}
    />
  ));
