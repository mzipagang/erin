import React from 'react';
import { Row, Col, Icon } from 'antd';
import {
  headerClass,
  middleContainer,
  middleContainers,
  middleColumn,
  bigNumbers,
  bigNumbersOpen,
  numSubtitles,
  numberSubtitles,
  numberSubtitlesOpen,
  matchBox,
  divider,
  bottomContainer,
  bottomContainers,
  smallIcons,
  bottomSectionText,
  bottomSectionValue,
} from './job-card.styles';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import { USER_ROLES } from '../../../_shared/constants/user-roles.enum';

class BaseJobCard extends React.Component {
  render() {
    const {
      onClick,
      title,
      jobShares,
      jobViews,
      jobMatches,
      jobOpenPositions,
      currentUser,
    } = this.props;
    return (
      <div style={{ height: 324 }} onClick={onClick}>
        {currentUser.role === USER_ROLES.ADMIN && (
          <h2 className={headerClass}>{title}</h2>
        )}
        {currentUser.role === USER_ROLES.MANAGER && (
          <h2 className={headerClass}>Your Department's Jobs</h2>
        )}
        {currentUser.role === USER_ROLES.EMPLOYEE && (
          <h2 className={headerClass}>Open Jobs</h2>
        )}
        {currentUser.role === USER_ROLES.ADMIN && (
          <div>
            <Row className={middleContainer}>
              <Col span={12} className={middleColumn}>
                <span className={bigNumbers}>{jobOpenPositions}</span>
                <div className={numberSubtitlesOpen}> Open Positions </div>
              </Col>
              <Col span={12} className={matchBox}>
                <span className={bigNumbersOpen}>{jobMatches}</span>
                <div className={numberSubtitles}> In Network Matches </div>
              </Col>
            </Row>
            <div className={divider} />
          </div>
        )}
        {currentUser.role === USER_ROLES.MANAGER && (
          <div>
            <Row className={middleContainer}>
              <Col span={12} className={middleColumn}>
                <span className={bigNumbers}>{jobOpenPositions}</span>
                <div className={numberSubtitlesOpen}> Open Positions </div>
              </Col>
              <Col span={12} className={matchBox}>
                <span className={bigNumbersOpen}>{jobMatches}</span>
                <div className={numberSubtitles}> In Network Matches </div>
              </Col>
            </Row>
            <div className={divider} />
          </div>
        )}
        {currentUser.role === USER_ROLES.EMPLOYEE && (
          <div>
            <Row className={middleContainers}>
              <Col span={12} className={middleColumn}>
                <span className={bigNumbers}>{jobOpenPositions}</span>
                <div className={numberSubtitlesOpen}> Open Positions </div>
              </Col>
              <Col span={12} className={matchBox}>
                <span className={bigNumbersOpen}>{jobMatches}</span>
                <div className={numSubtitles}> Referrals Recommended </div>
              </Col>
            </Row>
          </div>
        )}
        {currentUser.role === USER_ROLES.ADMIN && (
          <Row className={bottomContainer}>
            <Row>
              <Icon className={smallIcons} type="share-alt" />
              <span className={bottomSectionText}> Job Shares: </span>
              <span className={bottomSectionValue}>
                {jobShares && jobShares.length > 0 ? jobShares : 0}
              </span>
            </Row>
            <Row>
              <Icon className={smallIcons} type="eye" />
              <span className={bottomSectionText}> Total Job Views: </span>
              <span className={bottomSectionValue}>{jobViews}</span>
            </Row>
          </Row>
        )}
        {currentUser.role === USER_ROLES.MANAGER && (
          <Row className={bottomContainers}>
            <Row>
              <Icon className={smallIcons} type="share-alt" />
              <span className={bottomSectionText}> Job Shares: </span>
              <span className={bottomSectionValue}>
                {' '}
                {jobShares && jobShares.length > 0 ? jobShares : 0}
              </span>
            </Row>
            <Row>
              <Icon className={smallIcons} type="eye" />
              <span className={bottomSectionText}> Total Job Views: </span>
              <span className={bottomSectionValue}>{jobViews}</span>
            </Row>
          </Row>
        )}
      </div>
    );
  }
}

export const JobCard = borderedTile(BaseJobCard);
