import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { RecommendReferral } from './rec-referral.component';

import { jobData } from '../../../../../../test/data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Recommended Referral', () => (
    <RecommendReferral
      tileTitle="Recommended Referral"
      selectJob={jobData[0]}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Recommended Referral', () => (
    <RecommendReferral
      tileTitle="Recommended Referral"
      selectJob={jobData[0]}
    />
  ));
