import React from 'react';
import { Row, Col, Icon, Spin } from 'antd';
import {
  headerClass,
  topContainer,
  jobInfoContainer,
  bottomContainer,
  bonus,
  bonusAmount,
  jobTitleText,
  locationAdnDepartment,
  jobIcons,
  jobDetailRow,
  jobMatch,
  jobMatchNumber,
} from './rec-referral.styles';
import WebIcon from '../../../_shared/components/web-icon.component';
import { COLORS } from '../../../_shared/styles/colors';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';

class BaseRecReferral extends React.Component {
  render() {
    const { tileTitle, selectJob } = this.props;
    if (!selectJob) {
      return <Spin />;
    }
    return (
      <div style={{ height: 132 }}>
        <div>
          <h2 className={headerClass}>{tileTitle}</h2>
        </div>
        <Row className={topContainer}>
          <Col className={jobInfoContainer}>
            <div>
              <span className={jobTitleText}>{selectJob.title}</span>
            </div>
            <Row className={jobDetailRow}>
              <Icon type="folder" className={jobIcons} />
              <span className={locationAdnDepartment}>
                {selectJob.department}
              </span>
              <Icon type="environment" className={jobIcons} />
              <span className={locationAdnDepartment}>{`${
                selectJob.location.city
              }, ${selectJob.location.state}`}</span>
            </Row>
          </Col>
        </Row>
        <Row>
          <div>
            {selectJob.referralBonusAvailable && (
              <div className={bonus}>
                Referral Bonus:{' '}
                <span className={bonusAmount}>
                  {' '}
                  {`$${selectJob.referralBonusAmount}`.replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    ','
                  )}
                </span>
              </div>
            )}
          </div>
        </Row>
        <Row className={bottomContainer}>
          <Row className={jobMatch}>
            <WebIcon name="tick-inside-circle" color={COLORS.green} size={14} />
            <span className={jobMatchNumber}>
              {' '}
              {selectJob['_matches']} Person/s{' '}
            </span>{' '}
            In Your Network Match This Job!
          </Row>
        </Row>
      </div>
    );
  }
}
export const RecommendReferral = borderedTile(BaseRecReferral);
