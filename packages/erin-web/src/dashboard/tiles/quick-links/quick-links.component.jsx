import React from 'react';
import { Link } from 'react-router-dom';
import {
  headerClass,
  linkListContainer,
  quickLinkClass,
  linkItemClass,
} from './quick-links.styles';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';

class BaseQuickLinks extends React.Component {
  render() {
    return (
      <div style={{ height: 191 }}>
        <div>
          <h2 className={headerClass}>QUICK LINKS</h2>
        </div>
        <div style={{ marginLeft: '15px' }}>
          <ul className={linkListContainer}>
            <li className={linkItemClass}>
              <Link
                className={quickLinkClass}
                text="Post A New Job"
                to={{
                  pathname: '/jobs/add',
                }}
              >
                Post A New Job
              </Link>
            </li>
            <li className={linkItemClass}>
              <Link
                className={quickLinkClass}
                text="Connect Social Networks"
                to={{
                  pathname: '/mycontacts',
                }}
              >
                Connect Social Networks
              </Link>
            </li>
            <li className={linkItemClass}>
              <Link
                className={quickLinkClass}
                text="Invite Employees"
                to={{
                  pathname: '/employees/form',
                }}
              >
                Invite Employees
              </Link>
            </li>
            <li className={linkItemClass}>
              <Link
                className={quickLinkClass}
                text="Company Settings"
                to={'/settings'}
              >
                Company Settings
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export const QuickLinks = borderedTile(BaseQuickLinks);
