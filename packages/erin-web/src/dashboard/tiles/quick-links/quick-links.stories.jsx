import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { QuickLinks } from './quick-links.component';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Quick Links', () => <QuickLinks />);

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Quick Links', () => <QuickLinks />);
