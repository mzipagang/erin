import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { ConnectCard } from './connect-earn.component';
import { userData } from '../../../../../../test/data/users.data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Connect and Earn', () => <ConnectCard currentUser={userData[0]} />);

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Connect and Earn', () => <ConnectCard currentUser={userData[0]} />);
