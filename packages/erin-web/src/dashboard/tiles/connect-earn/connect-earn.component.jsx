import React from 'react';
import { Row, Col } from 'antd';
import {
  headerClass,
  headerClasses,
  middleContainer,
  middleColumn,
  divider,
  bottomContainer,
  bottomSectionText,
  bottomSectionValue,
  donutChart,
  chartTotal,
  numTotal,
  opacity,
  buttonActive,
  buttonFirst,
  connectEarn,
  connectContacts,
} from './connect-earn.styles';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import { COLORS } from '../../../_shared/styles/colors';
import WebIcon from '../../../_shared/components/web-icon.component';
import DonutChart from 'react-donut-chart';
import Group from '../../../_shared/assets/group.png';
import PiggyBank from '../../../_shared/assets/piggy-bank.png';

class BaseConnectCard extends React.Component {
  render() {
    const { onClick, currentUser } = this.props;
    const direct = currentUser.contact ? currentUser.contact['direct'] : 0;
    const social = currentUser.contact ? currentUser.contact['social'] : 0;
    const total = parseFloat(direct + social);
    return (
      <div onClick={onClick} style={{ height: 324 }}>
        {direct !== 0 &&
          social !== 0 && (
          <div>
            <h2 className={headerClass}>Your Network</h2>
            <Row className={middleContainer}>
              <Col span={24} className={middleColumn}>
                <div className={donutChart}>
                  <div className={chartTotal}>
                    <div className={numTotal}>{total}</div>
                    <div>People</div>
                  </div>
                  <DonutChart
                    height={153}
                    width={140}
                    colors={[
                      COLORS.dashboardLightOrange,
                      COLORS.dashboardBlue,
                    ]}
                    strokeColor=""
                    legend={false}
                    clickToggle={false}
                    data={[
                      {
                        label: 'direct',
                        value: parseFloat(direct),
                        className: opacity,
                      },
                      {
                        label: 'social',
                        value: parseFloat(social),
                        className: opacity,
                      },
                    ]}
                  />
                </div>
              </Col>
            </Row>
            <div className={divider} />
            <Row className={bottomContainer}>
              <Row>
                <button className={buttonFirst} />
                <span className={bottomSectionText}> Direct: </span>
                <span className={bottomSectionValue}> {direct} </span>
              </Row>
              <Row>
                <button className={buttonActive} />
                <span className={bottomSectionText}> Social: </span>
                <span className={bottomSectionValue}> {social}</span>
              </Row>
            </Row>
          </div>
        )}
        {direct === 0 && social === 0 &&
          currentUser.company.contactIncentiveBonus !== 0 && (
            <div>
              <h2 className={headerClasses}>Connect & Earn More</h2>
              <ul
                style={{
                  listStyleType: 'none',
                  padding: '5px 15px',
                  fontSize: '0.68em',
                }}
              >
                <li>
                  <div style={{ textAlign: 'center', marginBottom: '5px' }}>
                    <img
                      src={PiggyBank}
                      width={85}
                      height={85}
                      alt="earn and connect"
                    />
                  </div>
                </li>
                <li>
                  Connect your contacts to automatically find people to refer
                  and earn higher referral bonuses!
                <strong>
                  {' '}
                    We never share your contact's information until you make a
                    referral.
                </strong>
              </li>
              <li>
                <button className={connectEarn}>
                  <div>Earn an additional</div>
                  <div style={{ fontSize: '14px', fontWeight: 'bold' }}>
                      $500
                  </div>
                </button>
                <button className={connectContacts}>
                  <WebIcon name="network" size={14} color={COLORS.white} />{' '}
                    Connect Contacts
                </button>
                <div
                  style={{
                    fontSize: '0.80em',
                    marginTop: '5px',
                    textAlign: 'center',
                  }}
                >
                    You can edit your <a>Privacy & Security</a> settings at any
                    time
                  </div>
                </li>
              </ul>
            </div>
          )}
        {direct === 0 && social === 0 &&
         currentUser.company.contactIncentiveBonus === 0 && (
            <div>
              <h2 className={headerClass}>Connect Contacts</h2>
              <ul
                style={{
                  listStyleType: 'none',
                  padding: '15px',
                  fontSize: '0.70em',
                }}
              >
                <li>
                  <div style={{ textAlign: 'center', marginBottom: '15px' }}>
                    <img
                      src={Group}
                      width={95}
                      height={95}
                      alt="earn and connect"
                    />
                  </div>
                </li>
                <li>
                  Connect your contacts to automatically find people to refer.
                <strong>
                  {' '}
                    We never share your contact's information until you make a
                    referral.
                </strong>
              </li>
              <li>
                <div
                  style={{
                    fontSize: '0.95em',
                    marginTop: '12px',
                    textAlign: 'center',
                  }}
                >
                    You can edit your <a>Privacy & Security</a> settings at any
                    time
                </div>
                <button className={connectContacts}>
                  <WebIcon name="network" size={14} color={COLORS.white} />{' '}
                    Connect Contacts
                </button>
              </li>
            </ul>
          </div>
        )}
      </div>
    );
  }
}

export const ConnectCard = borderedTile(BaseConnectCard);
