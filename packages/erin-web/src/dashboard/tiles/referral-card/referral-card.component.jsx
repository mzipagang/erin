import React from 'react';
import { Row, Col, Icon, Progress } from 'antd';
import {
  headerClass,
  middleContainer,
  middleColumn,
  bigNumbers,
  bigNumber,
  bigNumbersOpen,
  numberSubtitlesOpen,
  numSubtitlesOpen,
} from './referral-card.style';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile.component';
import { COLORS } from '../../../_shared/styles/colors';

class BaseRefCard extends React.Component {
  getReferralColor() {
    if (this.props.currentReferralCount > this.props.previousReferralCount) {
      return COLORS.dashboardGreen;
    }
    if (this.props.currentReferralCount < this.props.previousReferralCount) {
      return COLORS.red;
    }
    return COLORS.dashboardLightOrange;
  }

  getReferralStatus() {
    if (this.props.currentReferralCount > this.props.previousReferralCount) {
      return 'arrow-up';
    }
    if (this.props.currentReferralCount < this.props.previousReferralCount) {
      return 'arrow-down';
    }
    return '';
  }

  getReferralAcceptedColor() {
    if (
      this.props.currentAcceptedReferralCount >
      this.props.previousAcceptedReferralCount
    ) {
      return COLORS.dashboardGreen;
    }
    if (
      this.props.currentAcceptedReferralCount <
      this.props.previousAcceptedReferralCount
    ) {
      return COLORS.red;
    }
    return COLORS.dashboardLightOrange;
  }

  getReferralAcceptedStatus() {
    if (
      this.props.currentAcceptedReferralCount >
      this.props.previousAcceptedReferralCount
    ) {
      return 'arrow-up';
    }
    if (
      this.props.currentAcceptedReferralCount <
      this.props.previousAcceptedReferralCount
    ) {
      return 'arrow-down';
    }
    return '';
  }

  render() {
    const {
      onClick,
      title,
      currentReferralCount,
      previousReferralCount,
      currentAcceptedReferralCount,
      previousAcceptedReferralCount,
    } = this.props;
    const percentChange = parseFloat(
      Math.abs(
        ((currentReferralCount - previousReferralCount) /
          previousReferralCount) *
          100
      ).toFixed(0)
    );
    const percentChangeAccept = parseFloat(
      Math.abs(
        ((currentAcceptedReferralCount - previousAcceptedReferralCount) /
          previousAcceptedReferralCount) *
          100
      ).toFixed(0)
    );
    return (
      <div onClick={onClick}>
        {title === 'Referrals' && <h2 className={headerClass}>{title}</h2>}
        {title === 'Accepted Referrals' && (
          <h2 className={headerClass}>{title}</h2>
        )}
        {title === 'Referrals Made' && <h2 className={headerClass}>{title}</h2>}
        <Row className={middleContainer}>
          {title === 'Referrals Made' && (
            <div style={{ textAlign: 'center' }}>
              <span className={bigNumber}>{currentAcceptedReferralCount}</span>
              <div className={numSubtitlesOpen}> Made This Year </div>
            </div>
          )}
          <Col span={8} className={middleColumn}>
            {title === 'Referrals' && (
              <div>
                <span className={bigNumbers}>{currentReferralCount}</span>
                <div className={numberSubtitlesOpen}> Made This Month </div>
              </div>
            )}
            {title === 'Accepted Referrals' && (
              <div>
                <span className={bigNumbers}>
                  {currentAcceptedReferralCount}
                </span>
                <div className={numberSubtitlesOpen}> Made This Month </div>
              </div>
            )}
          </Col>
          <Col span={8} />
          {title === 'Referrals' && (
            <Col
              span={8}
              style={{
                textAlign: 'center',
                width: 80,
                backgroundColor: this.getReferralColor(),
                borderRadius: '3%',
                height: 60,
                float: 'right',
              }}
            >
              <span className={bigNumbersOpen}>
                <Icon type={this.getReferralStatus()} />
              </span>
            </Col>
          )}
          {title === 'Accepted Referrals' && (
            <Col
              span={8}
              style={{
                textAlign: 'center',
                width: 80,
                backgroundColor: this.getReferralAcceptedColor(),
                borderRadius: '3%',
                height: 60,
                float: 'right',
              }}
            >
              <span className={bigNumbersOpen}>
                <Icon type={this.getReferralAcceptedStatus()} />
              </span>
            </Col>
          )}
          {title === 'Referrals' && (
            <div style={{ fontWeight: '800', fontSize: '12px' }}>
              <Progress
                percent={percentChange}
                strokeColor={this.getReferralColor()}
                format={percentChange => `${percentChange}%`}
              />
            </div>
          )}
          {title === 'Accepted Referrals' && (
            <div style={{ fontWeight: '800', fontSize: '12px' }}>
              <Progress
                percent={percentChangeAccept}
                strokeColor={this.getReferralAcceptedColor()}
                format={percentChangeAccept => `${percentChangeAccept}%`}
              />
            </div>
          )}
        </Row>
      </div>
    );
  }
}

export const ReferralCard = borderedTile(BaseRefCard);
