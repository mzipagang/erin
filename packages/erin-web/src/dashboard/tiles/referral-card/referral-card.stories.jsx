import React from 'react';
import StoryRouter from 'storybook-react-router';

import { storiesOf } from '@storybook/react';
import { ReferralCard } from './referral-card.component';
import { jobData as currentJobData } from '../../../../../../test/data';
import { jobData as prevJobData } from '../../../../../../test/data/mz-data/jobs.data';

storiesOf('Development/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Referral Card', () => (
    <ReferralCard
      title="Referrals"
      referralTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      referralAcceptedTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      prevReferralAcceptedTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
    />
  ));

storiesOf('QA/Dashboard', module)
  .addDecorator(StoryRouter())
  .add('Referral Card', () => (
    <ReferralCard
      title="Referrals"
      referralTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      prevReferralTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_referrals']);
        }, 0)}
      referralAcceptedTotal={currentJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
      prevReferralAcceptedTotal={prevJobData
        .filter(prop => prop.isOpen)
        .reduce((sum, prop) => {
          return (sum += prop['_accepted']);
        }, 0)}
    />
  ));
