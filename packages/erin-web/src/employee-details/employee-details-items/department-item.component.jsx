import { Icon, Row } from 'antd';
import React from 'react';
import {
  departmentContainer,
  closeIcon,
  departmentText,
  DepartmentsContainer,
} from './department-item.styles';

const DepartmentItem = props => {
  const { departments, onDeleteDepartment } = props;

  const handleDeleteDepartment = id => {
    onDeleteDepartment({
      input: {
        id: id,
      },
    });
  };
  return (
    <div className={DepartmentsContainer}>
      {departments.map(item => {
        return (
          <div key={item.id} onClick={() => handleDeleteDepartment(item.id)}>
            <Row className={departmentContainer}>
              <span className={departmentText}> {item.department.name} </span>
              <Icon type="close-circle-o" className={closeIcon} />
            </Row>
          </div>
        );
      })}
    </div>
  );
};

export default DepartmentItem;
