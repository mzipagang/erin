import { css } from 'emotion';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

export const NameStyles = css({
  color: COLORS.blue,
  fontSize: 16,
  fontWeight: '600 !important',
});

export const LinkStyles = css({
  color: COLORS.blue,
  fontWeight: '600 !important',
  fontSize: 14,
});

export const IconStyles = css({
  width: 23,
  paddingRight: 3,
  opacity: 0.7,
});

export const TableStyles = css({
  backgroundColor: 'white',
  '& .ant-table': {
    fontFamily: '"Open Sans", sans-serif',
    fontWeight: 400,
    fontSize: 14,
  },
  //adds padding to pagination
  '& ul': {
    paddingRight: 20,
  },
  '& .ant-pagination-options': {
    float: 'left',
  },
});

export const StatusStyles = css({
  paddingTop: 16,
});

export const StatusBold = css({
  fontWeight: 700,
});

export const StatusItalic = css({
  fontStyle: 'italic',
});

export const BonusStyles = css({
  fontSize: 14,
  fontweight: '600 !important',
  color: COLORS.green,
});
