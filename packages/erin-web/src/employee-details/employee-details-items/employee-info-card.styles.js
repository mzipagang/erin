import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const EmployeeName = css({
  marginTop: '-8px',
  marginBottom: '-8px',
  fontSize: 28,
  lineSpacing: 0,
});

export const EmployeeEmail = css({
  fontSize: 16,
  fontWeight: '600 !important',
});

export const Heading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: '#444444',
  padding: 0,
});

export const SubHeading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 16,
  fontWeight: 800,
  color: '#8d99a3',
});

export const LinkStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  color: COLORS.blue,
  fontWeight: '600 !important',
});

export const FlexContainer = css({
  display: 'flex',
  marginBottom: 30,
  width: '100%',
});

export const FlexContainer2 = css({
  justifyContent: 'space-between',
  display: 'flex',
  marginBottom: 10,
  width: '100%',
});

export const Avatar = css({
  height: 70,
  width: 70,
  marginRight: 20,
});

export const ActionsContainer = css({
  display: 'flex',
  flexDirection: 'column',
});

export const PermissionsContainer = css({
  marginBottom: 30,
});

export const SectionTitle = css({
  fontSize: 18,
});

export const JobInfoContainer = css({
  marginBottom: 30,
});

export const JobInfo = css({
  fontSize: 14,
  margin: 0,
  padding: 0,
});

export const ManagerPermissions = css({
  display: 'flex',
  marginBottom: 20,
});

export const EmployeeRole = css({
  display: 'flex',
  marginBottom: 20,
});

export const ResetPasswordBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  textAlign: 'left',
  fontSize: 14,
  width: 150,
  border: 'none',
  padding: 0,
  margin: 0,
  color: COLORS.blue,
  fontWeight: '600 !important',
  '& span': {
    color: COLORS.blue,
  },
});

export const BlockContainer = css({
  width: '100%',
});

export const IconStyles = css({
  color: `${COLORS.blue} !important`,
});

export const EditProfileContainer = css({
  float: 'right',
});

export const EditProfile = css({
  fontSize: 14,
});

export const NoPicture = css({
  height: 65,
  width: 65,
  backgroundColor: COLORS.lightGray3,
  '& h3': {
    color: 'white',
    paddingTop: 7,
    fontSize: 28,
    textAlign: 'center',
    lineHeight: '50px',
  },
});

export const FlexContainer3 = css({
  display: 'flex',
  marginBottom: 30,
  marginRight: 20,
});
