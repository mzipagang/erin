import React from 'react';
import { Select } from 'antd';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import { SelectStyles, DropDown, SortDown } from './select.styles';

const SelectDepartments = props => {
  const { id, onAddDepartment, managedDepartments, departments = [] } = props;
  const Option = Select.Option;

  let options = [];
  for (const department of departments) {
    options.push(<Option key={department.id}>{department.name}</Option>);
  }
  const handleAddDepartment = value => {
    var found = managedDepartments.some(item => {
      return item.departmentId === value.key;
    });
    if (!found) {
      onAddDepartment({
        userId: id,
        departmentId: value.key,
      });
    }
    return;
  };

  return (
    <div>
      <Select
        labelInValue={true}
        showArrow={false}
        showSearch={true}
        className={SelectStyles}
        dropdownClassName={DropDown}
        placeholder={
          <p style={{ fontSize: 16, margin: 0 }}>Add a Department</p>
        }
        onChange={value => handleAddDepartment(value)}
      >
        {options}
      </Select>
      <WebIcon
        className={SortDown}
        color={COLORS.darkGray}
        size={14}
        name="sort-down"
      />
    </div>
  );
};

export default SelectDepartments;
