import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Button } from 'antd';
import { cx } from 'emotion';
import moment from 'moment';
import { upperFirst } from 'lodash';

import SelectRole from './select-role.component';
import ResetPasswordModal from '../../_shared/components/reset-password-modal/reset-password-modal.component';
import SelectDepartments from './select-departments.component';
import DepartmentItem from './department-item.component';
import DisableUserBtn from './disable-user-btn.component';
import {
  EmployeeName,
  Heading,
  LinkStyles,
  EmployeeEmail,
  FlexContainer,
  Avatar,
  BlockContainer,
  FlexContainer2,
  ActionsContainer,
  PermissionsContainer,
  SubHeading,
  SectionTitle,
  JobInfo,
  JobInfoContainer,
  ManagerPermissions,
  ResetPasswordBtn,
  EmployeeRole,
  IconStyles,
  EditProfileContainer,
  EditProfile,
  NoPicture,
  FlexContainer3,
} from './employee-info-card.styles';

const EmployeeInfoCard = props => {
  const {
    employee,
    visible,
    showModal,
    handleCancel,
    updatePassword,
    handleChangePermissions,
    handleChangeRole,
    allDepartments,
    onUpdate,
    onAddDepartment,
    onDeleteDepartment,
  } = props;
  const {
    firstName,
    lastName,
    avatar,
    email,
    title,
    managedDepartments,
    lastLogin,
    role,
    password,
    id,
    department,
  } = employee;

  return (
    <div className={FlexContainer}>
      <div className={FlexContainer3}>
        {avatar === null || avatar === '' ? (
          <div className={NoPicture}>
            <h3>
              {firstName[0]} {lastName[0]}
            </h3>
          </div>
        ) : (
          <img
            className={Avatar}
            src={avatar}
            alt={`${firstName} ${lastName}`}
          />
        )}
      </div>
      <div className={BlockContainer}>
        <div className={FlexContainer2}>
          <div>
            <h1 className={cx(EmployeeName, Heading)}>
              {firstName} {lastName}
            </h1>
            <Link className={cx(LinkStyles, EmployeeEmail)} to="#">
              {email}
            </Link>
            {lastLogin ? (
              <p>Last Login: {moment(lastLogin).format('M/D/YYYY')}</p>
            ) : (
              <p>
                Last Login: Never (
                <Link className={LinkStyles} to="#">
                  Resend Invite
                </Link>
                )
              </p>
            )}
          </div>
          <div className={EditProfileContainer}>
            <Link
              to={`/editprofile/${id}`}
              className={cx(EditProfile, LinkStyles)}
            >
              <Icon className={IconStyles} type="edit" />
              {'  '}
              EDIT PROFILE
            </Link>
          </div>
        </div>
        <div className={JobInfoContainer}>
          <p className={JobInfo}>
            <span className={SubHeading}>Department:</span> {department.name}
          </p>
          <p className={JobInfo}>
            <span className={SubHeading}>Job Title:</span> {title}
          </p>
          <p className={JobInfo}>
            <span className={SubHeading}>Role:</span> {upperFirst(role)}
          </p>
        </div>
        <div className={PermissionsContainer}>
          <h3 className={cx(Heading, SectionTitle)}>Permissions</h3>
          <p>
            Managers are able to manage jobs and referrals in their departments,
            Administrators can perform all functions.
          </p>
          <div className={EmployeeRole}>
            <h4 className={SubHeading}>Employee Role:</h4>
            <SelectRole role={role} handleChangeRole={handleChangeRole} />
          </div>
          {role === 'manager' ? (
            <div>
              <div className={ManagerPermissions}>
                <h4 className={SubHeading}>Manager Permissions:</h4>
                <SelectDepartments
                  departments={allDepartments}
                  managedDepartments={managedDepartments}
                  handleChangePermissions={handleChangePermissions}
                  onAddDepartment={onAddDepartment}
                  role={role}
                  id={id}
                />
              </div>
              <DepartmentItem
                onDeleteDepartment={onDeleteDepartment}
                departments={managedDepartments}
              />
            </div>
          ) : null}
        </div>
        <div className={ActionsContainer}>
          <h3 className={cx(Heading, SectionTitle)}>Actions</h3>
          <Button onClick={showModal} className={ResetPasswordBtn} to="#">
            Reset Password
          </Button>
          <DisableUserBtn onUpdate={onUpdate} employee={employee} />
        </div>
      </div>
      <ResetPasswordModal
        visible={visible}
        handleCancel={handleCancel}
        password={password}
        onUpdate={onUpdate}
        updatePassword={updatePassword}
        employee={employee}
      />
    </div>
  );
};

export default EmployeeInfoCard;
