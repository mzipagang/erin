import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const ReferralInfoContainer = css({
  fontFamily: '"Open Sans", sans-serif',
  width: '100%',
});

export const SubHeading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: '#8d99a3',
  margin: 'auto',
});
export const Totals = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  margin: 'auto',
});

export const Heading = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: '#444444',
  padding: 0,
  marginBottom: 0,
  fontSize: 18,
  width: '100%',
  textAlign: 'center',
});

export const FlexContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
  width: '70%',
  margin: 'auto',
});

export const BonusStyles = css({
  color: COLORS.green,
  fontSize: 32,
  fontWeight: 600,
  marginBottom: 20,
  width: '100%',
  textAlign: 'center',
});
