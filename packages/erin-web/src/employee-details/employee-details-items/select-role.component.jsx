import React from 'react';
import { Select } from 'antd';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import { SelectRoleStyles, Container, SortDown } from './select.styles';

const Option = Select.Option;

const SelectRole = props => {
  const { handleChangeRole, role } = props;
  return (
    <div className={Container}>
      <Select
        className={SelectRoleStyles}
        defaultValue={`${role}`}
        onChange={value => handleChangeRole(value)}
        showArrow={false}
      >
        <Option value="employee">Employee</Option>
        <Option value="manager">Manager</Option>
        <Option value="admin">Administrator</Option>
        <Option value="superAdmin">Super Administrator</Option>
      </Select>
      <WebIcon
        className={SortDown}
        color={COLORS.darkGray}
        size={14}
        name="sort-down"
      />
    </div>
  );
};

export default SelectRole;
