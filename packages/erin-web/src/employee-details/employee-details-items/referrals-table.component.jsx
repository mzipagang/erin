import React from 'react';
import { Table } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { cx } from 'emotion';
import { parseJsonFields } from 'erin-app-state-mgmt/src/_shared/services/parse-api.service';
import SocialMedia from '../../_shared/components/social-media/social-media.component';

import {
  NameStyles,
  LinkStyles,
  TableStyles,
  StatusStyles,
  StatusBold,
  StatusItalic,
  BonusStyles,
} from './referrals-table.styles';

class ReferralsTable extends React.Component {
  renderStatus(status) {
    if (status === 'hired') {
      return <p className={cx(StatusStyles, StatusBold)}>Hired</p>;
    } else if (status === 'notHired') {
      return <p className={cx(StatusStyles, StatusItalic)}>Not Hired</p>;
    } else if (status === 'referred') {
      return <p className={StatusStyles}>Referred</p>;
    } else if (status === 'accepted') {
      return <p className={StatusStyles}>Accepted</p>;
    } else if (status === 'interviewing') {
      return <p className={StatusStyles}>Interviewing</p>;
    }
  }

  renderReferralBonus = job => {
    if (job) {
      job = Object.assign(job, parseJsonFields(['referralBonus'], job));
    }
    return job !== null && job.referralBonus.hasBonus ? (
      <span className={BonusStyles}>
        {/* TODO: remove JSON.parse */}
        {`$${job.referralBonus.amount}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
      </span>
    ) : (
      <span className={BonusStyles}>$0</span>
    );
  };

  render() {
    const { filteredData, sortByAlph } = this.props;
    const columns = [
      {
        title: 'Name',
        dataIndex: 'contact',
        render: record => (
          <Link to="#" className={NameStyles}>
            {record.firstName} {record.lastName}
          </Link>
        ),
        sorter: (a, b) =>
          sortByAlph(
            a.contact.lastName + a.contact.firstName,
            b.contact.lastName + b.contact.firstName
          ),
      },
      {
        title: 'Info',
        render: record => (
          <SocialMedia
            email={record.contact.emailAddress}
            socialMedia={record.contact.socialMediaAccounts}
          />
        ),
      },
      {
        title: 'Job',
        dataIndex: 'job',
        key: () => Math.floor(Math.random() * 100000 + 1),
        sorter: (a, b) => sortByAlph(a.job.title, b.job.title),
        render: job => (
          <Link className={LinkStyles} to="#">
            {job !== null ? job.title : null}
          </Link>
        ),
      },
      {
        title: 'Date Referred',
        dataIndex: 'dateReferred',
        sorter: (a, b) => a.dateReferred - b.dateReferred,
        render: dateReferred => moment(dateReferred).format('M/D/YYYY'),
      },
      {
        title: 'Potential Bonus',
        dataIndex: 'job',
        sorter: (a, b) => sortByAlph(a.job.referralBonus, b.job.referralBonus),
        render: job => this.renderReferralBonus(job),
      },
      {
        title: 'Status',
        dataIndex: 'status',
        sorter: (a, b) => sortByAlph(a.status, b.status),
        render: status => this.renderStatus(status),
      },
    ];
    return (
      <Table
        rowKey={record => record.id}
        className={TableStyles}
        dataSource={filteredData}
        columns={columns}
      />
    );
  }
}

export default ReferralsTable;
