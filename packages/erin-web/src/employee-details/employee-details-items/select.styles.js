import { css } from 'emotion';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

export const SelectStyles = css({
  width: 300,
  left: 40,
  '& .ant-select-selection': {
    padding: '5px 0 5px 0',
    backgroundColor: 'transparent',
    height: 40,
  },
});
export const SelectRoleStyles = css({
  width: 300,
  left: 40,

  fontSize: 16,
  height: 40,
  '& .ant-select-selection--single': {
    height: 40,
    paddingTop: 4,
    backgroundColor: 'transparent',
  },
});
export const FlexContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
  color: COLORS.lightGray,
});

export const Container = css({
  display: 'block',
});

export const SortDown = css({
  zIndex: 99,
});

export const DropDown = css({
  fontFamily: '"Open Sans", sans-serif',
});
