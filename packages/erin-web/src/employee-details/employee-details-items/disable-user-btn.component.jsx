import React from 'react';
import { Button } from 'antd';
import { DisableBtn } from './disable-user-btn.styles';

const DisableUserBtn = props => {
  const { onUpdate, employee } = props;
  const handleToggleDisabled = () => {
    onUpdate({
      input: {
        id: employee.id,
        emailAddress: employee.emailAddress,
        active: !employee.active,
        companyId: employee.companyId,
        firstName: employee.firstName,
        lastName: employee.lastName,
        avatar: employee.avatar,
        title: employee.title,
        role: employee.role,
        password: employee.password,
      },
    });
  };

  return (
    <div>
      {!employee.active ? (
        <Button className={DisableBtn} onClick={handleToggleDisabled}>
          Enable User
        </Button>
      ) : (
        <Button className={DisableBtn} onClick={handleToggleDisabled}>
          Disable User
        </Button>
      )}
    </div>
  );
};

export default DisableUserBtn;
