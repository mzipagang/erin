import React from 'react';
import { parseJsonFields } from 'erin-app-state-mgmt/src/_shared/services/parse-api.service';
import {
  ReferralInfoContainer,
  FlexContainer,
  BonusStyles,
  Heading,
  SubHeading,
  Totals,
} from './employee-referral-info.styles';

const EmployeeReferralInfo = props => {
  const { employeeReferrals } = props;

  const calculateReferralBonuses = () => {
    // TODO: Looks like this needs to be added to the DB
    let total = 0;
    employeeReferrals.forEach(referral => {
      let job = referral.job;
      if (job) {
        job = Object.assign(job, parseJsonFields(['referralBonus'], job));
      }
      // const referralBonus = JSON.parse(referral.job.referralBonus);
      if (
        referral.status === 'hired' &&
        referral.job !== null &&
        job.referralBonus.hasBonus
      ) {
        total += job.referralBonus.amount;
      }
    });
    return total.toLocaleString('en-US');
  };

  const totalReferrals = employeeReferrals.length;
  const referralsHired = employeeReferrals.filter(
    referral => referral.status === 'Hired'
  ).length;

  return (
    <div className={ReferralInfoContainer}>
      <h1 className={Heading}>Referral Bonuses Earned:</h1>
      <p className={BonusStyles}>${calculateReferralBonuses()}</p>
      <div className={FlexContainer}>
        <p className={SubHeading}>Total Referrals:</p>
        <p className={Totals}>{totalReferrals}</p>
      </div>
      <div className={FlexContainer}>
        <p className={SubHeading}>Referrals Hired:</p>
        <p className={Totals}>{referralsHired}</p>
      </div>
    </div>
  );
};

export default EmployeeReferralInfo;
