import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const DisableBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 14,
  border: 'none',
  padding: 0,
  margin: 0,
  fontWeight: '600 !important',
  color: COLORS.blue,
  '& span': {
    color: COLORS.blue,
  },
});
