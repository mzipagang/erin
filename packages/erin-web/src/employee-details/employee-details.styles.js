import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const EmployeeEmail = css({
  fontSize: 16,
  fontWeight: '600 !important',
});

export const EmployeeDetailsContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  backgroundColor: COLORS.lightGray2,
  marginBottom: 0,
  paddingRight: 30,
});

export const FlexContainer = css({
  display: 'flex',
  marginBottom: 30,
  width: '100%',
  paddingTop: 20,
  maxWidth: 1290,
  paddingRight: 20,
});

export const TableContainerStyles = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  backgroundColor: 'white',
  maxWidth: 1290,
  height: 'auto',
  '& thead > tr > th': {
    backgroundColor: 'white',
    fontWeight: 700,
  },
  '& tbody > tr > td': {
    paddingTop: 0,
    paddingBottom: 0,
    height: 65,
  },
});

export const Card1Styles = css({
  width: '70%',
  marginRight: 15,
  height: '100%',
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  padding: 20,
  backgroundColor: 'white',
});

export const Card2Styles = css({
  width: '30%',
  marginLeft: 15,
  height: 200,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  padding: 20,
  backgroundColor: 'white',
  minWidth: 260,
});

export const link = css({
  fontFamily: '"Open Sans", sans-serif',
  color: COLORS.blue,
});

export const BackLink = css({
  ...link,
  border: 'none',
  backgroundColor: 'transparent',
  paddingLeft: 5,
  fontSize: 14,
  fontWeight: '600 !important',
});

export const BackIcon = css({
  fontSize: 14,
});

export const EmployeeName = css({
  marginTop: '-8px',
  marginBottom: '-8px',
  fontSize: 28,
  lineSpacing: 0,
});

export const GridTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 22,
  marginLeft: 5,
  marginTop: 40,
  marginBottom: 30,
});

export const NoReferrals = css({
  marginTop: 40,
  width: '100%',
  textAlign: 'center',
  fontSize: 14,
  fontWeight: 400,
});
