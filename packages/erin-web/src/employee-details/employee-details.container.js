import { connect } from 'react-redux';
import EmployeeDetailsComponent from './employee-details.component';
import { actions } from 'erin-app-state-mgmt';
import { withUserById } from 'erin-app-state-mgmt/src/_shared/api/components/employee-details/with-user-by-id.provider';
import { withListDepartment } from 'erin-app-state-mgmt/src/_shared/api/components/departments/with-list-departments.provider';
import { compose } from '../_shared/services/utils';

const { employeeDetailsActions } = actions;

const mapStateToProps = (state, props) => {
  const { currentUser } = state.user;
  const { id } = props.match.params;
  return {
    currentUser,
    id,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUser(user) {
      dispatch(employeeDetailsActions.createFetchUserAction(user));
    },
    updatePassword(userId, password) {
      dispatch(
        employeeDetailsActions.createUpdatePasswordAction(userId, password)
      );
    },
    updateRole(userId, role) {
      dispatch(employeeDetailsActions.createUpdateRoleAction(userId, role));
    },
    setPermissions(userId, permissions) {
      dispatch(
        employeeDetailsActions.createSetPermissionsAction(userId, permissions)
      );
    },
    toggleDisableUser(userId, disabled) {
      dispatch(
        employeeDetailsActions.createToggleDisabledAction(userId, disabled)
      );
    },
  };
};

const EmployeeDetailsWithAPI = compose(
  withUserById,
  withListDepartment
)(EmployeeDetailsComponent);

export const EmployeeDetails = connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeDetailsWithAPI);
