import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import EmployeeDetailsComponent from './employee-details.component';
import { referralData, userData } from '../../../../test/data/mb-data';

const user = userData[0];
user.referrals = referralData;

storiesOf('Development/Employee Details', module)
  .addDecorator(story => <div style={{ width: '100%' }}>{story()}</div>)
  .addDecorator(StoryRouter())
  .add('Employee Details View', () => (
    <EmployeeDetailsComponent user={user} referrals={user.referrals} id={1} />
  ));

storiesOf('QA/Employee Details', module)
  .addDecorator(story => <div style={{ width: '100%' }}>{story()}</div>)
  .addDecorator(StoryRouter())
  .add('Employee Details View', () => (
    <EmployeeDetailsComponent
      user={userData[0]}
      referrals={referralData}
      id={1}
    />
  ));
