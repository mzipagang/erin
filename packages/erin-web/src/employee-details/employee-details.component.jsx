import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';

import EmployeeInfoCard from './employee-details-items/employee-info-card.component';
import ReferralsTable from './employee-details-items/referrals-table.component';
import EmployeeReferralInfo from './employee-details-items/employee-referral-info.component';
import {
  Card1Styles,
  Card2Styles,
  EmployeeDetailsContainer,
  FlexContainer,
  BackLink,
  BackIcon,
  GridTitle,
  NoReferrals,
  TableContainerStyles,
} from './employee-details.styles';

export class EmployeeDetailsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUser: props.user,
      id: props.id,
      visible: false,
      departments: props.departments ? props.departments : [],
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user !== this.props.user) {
      this.setState({ selectedUser: this.props.user });
    }
  }

  sortByAlph = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  handleError = () => {
    this.setState({ error: true });
  };

  toggleDisabled = () => {
    const { id, selectedUser } = this.state;
    this.props.toggleDisableUser(id, selectedUser.disabled);
  };

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleAddPermissions = key => {
    const { id } = this.state;
    this.props.setPermissions(id, key);
  };

  handleChangeRole = value => {
    const { id } = this.state;
    const {
      companyId,
      emailAddress,
      active,
      firstName,
      lastName,
      avatar,
      title,
    } = this.state.selectedUser;
    this.props.onUpdate({
      input: {
        id,
        companyId,
        emailAddress,
        active,
        role: value,
        firstName,
        lastName,
        avatar: avatar ? avatar : null,
        title,
      },
    });
  };

  render() {
    const { selectedUser, visible, departments } = this.state;
    const {
      updatePassword,
      onUpdate,
      onAddDepartment,
      onDeleteDepartment,
    } = this.props;
    let userData = selectedUser;
    if (!selectedUser) {
      userData = {
        firstName: '',
        lastName: '',
        avatar: null,
        emailAddress: '',
        title: '',
        managedDepartments: [],
        lastLogin: '',
        role: '',
        password: '',
        id: '',
        department: '',
        referrals: [],
      };
    }
    let departmentData = departments;
    if (!departments) {
      departmentData = [];
    }
    return (
      <main className={EmployeeDetailsContainer}>
        <Link to="/employees" className={BackLink}>
          <Icon className={BackIcon} type="left" theme="outlined" /> Employees
        </Link>
        <div className={FlexContainer}>
          <div className={Card1Styles}>
            <EmployeeInfoCard
              employee={userData}
              toggleDisabled={this.toggleDisabled}
              visible={visible}
              showModal={this.showModal}
              handleCancel={this.handleCancel}
              updatePassword={updatePassword}
              handleChangePermissions={this.handleChangePermissions}
              handleChangeRole={this.handleChangeRole}
              allDepartments={departmentData}
              onUpdate={onUpdate}
              onAddDepartment={onAddDepartment}
              onDeleteDepartment={onDeleteDepartment}
            />
          </div>
          <div className={Card2Styles}>
            <EmployeeReferralInfo employeeReferrals={userData.referrals} />
          </div>
        </div>
        <div className={GridTitle}>
          {userData.firstName}
          &#39;s Referrals
        </div>
        {userData.referrals.length > 0 ? (
          <section className={TableContainerStyles}>
            <ReferralsTable
              sortByAlph={this.sortByAlph}
              filteredData={userData.referrals}
            />
          </section>
        ) : (
          <p className={NoReferrals}>You haven&#39;t made any referrals.</p>
        )}
      </main>
    );
  }
}

export default EmployeeDetailsComponent;
