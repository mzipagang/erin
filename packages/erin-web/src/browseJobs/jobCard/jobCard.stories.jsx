import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import { JobCard } from './jobCard.component';
import { jobData } from '../../../../../test/data';

storiesOf('Development/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Job Card', () => <JobCard job={jobData[0]} />);

storiesOf('QA/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Job Card', () => <JobCard job={jobData[0]} />);
