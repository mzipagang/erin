import { css } from 'emotion';

import { COLORS } from '../../_shared/styles/colors';

export const addJobMainContainer = css({
  justifyContent: 'center',
  alignItems: 'center',
});

export const jobCardMain = css({
  height: 350,
  width: 310,
  minwidth: 310,
  padding: 10,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  '& ant-card-head-title': {
    margin: 0,
  },
});

export const topContainer = css({
  height: 60,
  marginTop: 10,
});

export const hidden = css({
  display: 'none',
});

export const jobInfoContainer = css({
  height: 'auto',
});

export const bonusContainer = css({
  height: 'auto',
  marginRight: 15,
});

export const jobDetailRow = css({
  marginTop: 7,
  marginLeft: 5,
  height: 16,
  '& svg': {
    transform: 'translate(0, 1px)',
  },
});

export const middleContainer = css({
  height: 160,
});

export const divider = css({
  height: 1,
  marginTop: 20,
  backgroundColor: COLORS.subHeading,
  opacity: 0.4,
  width: '80%',
  margin: 'auto',
});

export const bottomContainer = css({
  height: 60,
  marginTop: 15,
});

export const JobIcon = css({
  fontSize: 16,
  color: COLORS.darkGray,
  paddingRight: 3,
});

export const bigNumbers = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: COLORS.black,
  fontSize: '2em',
});

export const numberSubtitles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.black,
  fontSize: '0.85em',
});

export const bonusAmount = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.green,
  fontSize: '1em',
});

export const jobTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '600 !important',
  color: COLORS.hyperLink,
  fontSize: 16,
  padding: '5px 0',
  marginLeft: 5,
});

export const IconContainer = css({
  paddingTop: 3,
});

export const jobDescText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.black,
  fontSize: 14,
});

export const referBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.white,
  background: COLORS.red,
  fontSize: 14,
  padding: 4,
});

export const jobMatch = css({
  fontSize: 12,
  width: '100%',
  marginTop: 15,
});

export const jobMatchNumber = css({
  color: COLORS.blue,
  fontWeight: 500,
  padding: '0 2px',
});

export const Department = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.darkGray,
  fontSize: 14,
  marginLeft: 5,
  marginRight: 5,
});
export const locationStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.darkGray,
  fontSize: 14,
  marginRight: 5,
});

export const middleColumn = css({
  textAlign: 'center',
  width: 60,
});

export const matchBox = css({
  textAlign: 'center',
  width: 60,
  backgroundColor: COLORS.lightGray2,
});

export const smallIcons = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.subHeading,
  fontSize: '0.85em',
  marginLeft: 21,
  opacity: 0.7,
});

export const bottomSectionText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.heading,
  fontSize: '0.85em',
  marginLeft: 5,
  opacity: 0.7,
});

export const bottomSectionValue = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 800,
  color: COLORS.black,
  fontSize: '0.85em',
  float: 'right',
  marginRight: 21,
});
