import React from 'react';
import { Row, Col, Card } from 'antd';
import renderHTML from 'react-render-html';
import { Link } from 'react-router-dom';
import {
  jobCardMain,
  topContainer,
  jobInfoContainer,
  bonusContainer,
  middleContainer,
  bottomContainer,
  bonusAmount,
  jobTitleText,
  Department,
  jobDetailRow,
  jobDescText,
  jobMatch,
  jobMatchNumber,
  hidden,
  locationStyles,
} from './jobCard.styles';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import { ShareSendButton } from '../../_shared/components/share-send-button/share-send.component';
import { ReferralModal } from '../../_shared/referral-modal/referral-modal.container';

export class JobCard extends React.Component {
  render() {
    const {
      job,
      job: {
        publicLink,
        id,
        title,
        department,
        jobType,
        description,
        _matches,
        referralBonus,
        location,
        status,
      },
    } = this.props;
    return (
      <div>
        <Card
          hoverable
          headStyle={{
            borderWidth: 0,
            padding: 0,
            marginTop: -15,
            marginBottom: 0,
          }}
          bodyStyle={{ minWidth: 290, padding: 5 }}
          className={jobCardMain}
          title={
            <Row className={topContainer}>
              <Col className={jobInfoContainer}>
                <Link to={`/browsejobs/${id}`} className={jobTitleText}>
                  {title}
                </Link>
                <Row type="flex" className={jobDetailRow}>
                  <WebIcon name="folder" size={18} />
                  <span className={Department}>
                    {' '}
                    {department ? department.name : ''}{' '}
                  </span>
                  <WebIcon name="placeholder" size={18} />
                  <span className={locationStyles}>
                    {location && location.city && location.state
                      ? `${location.city}, ${location.state}`
                      : 'Remote'}
                  </span>
                </Row>
              </Col>
            </Row>
          }
          extra={
            <div className={bonusContainer}>
              {referralBonus &&
                referralBonus.hasBonus && (
                  <span className={bonusAmount}>
                    {`$${referralBonus.amount}`.replace(
                      /\B(?=(\d{3})+(?!\d))/g,
                      ','
                    )}
                  </span>
                )}
            </div>
          }
        >
          <Row className={middleContainer}>
            <div className={jobDescText}>
              {' '}
              {renderHTML(description.toString('html'))}
            </div>
          </Row>
          <div className={status === 'open' ? '' : hidden}>
            <Row className={bottomContainer}>
              <Row gutter={5}>
                <Col span={12}>
                  <ReferralModal jobData={job} />
                </Col>
                <Col span={12}>
                  <ShareSendButton
                    title={title}
                    jobId={id}
                    jobUrl={publicLink}
                    jobType={jobType}
                    description={description}
                    city={location ? location.city : null}
                    state={location ? location.state : null}
                  />
                </Col>
              </Row>
              {_matches && _matches === 1 ? (
                <Row type="flex" justify="center" className={jobMatch}>
                  <WebIcon
                    name="tick-inside-circle"
                    color={COLORS.green}
                    size={14}
                  />
                  <span className={jobMatchNumber}>{_matches}</span> Smart
                  Referral for this job!
                </Row>
              ) : _matches && _matches > 1 ? (
                <Row type="flex" justify="center" className={jobMatch}>
                  <WebIcon
                    name="tick-inside-circle"
                    color={COLORS.green}
                    size={14}
                  />
                  <span className={jobMatchNumber}> {_matches}</span> Smart
                  Referrals for this job!
                </Row>
              ) : null}
            </Row>
          </div>
        </Card>
      </div>
    );
  }
}
