import { css } from 'emotion';
import { COLORS } from 'erin-web/src/_shared/styles/colors';

export const mainContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  paddingRight: 30,
});

export const middleContainer = css({
  maxWidth: 960,
  minWidth: 960,
});

export const DropDown = css({
  fontFamily: '"Open Sans", sans-serif',
});

export const SelectContainer = css({
  '& svg': {
    transform: 'translate(-40px, 2px)',
  },
  height: 40,
  marginTop: 17,
});

export const SelectStyles = css({
  width: 300,
  '& .ant-select-selection__choice': {
    backgroundColor: COLORS.lightGreen,
    color: COLORS.green,
  },
  '& .ant-select-selection--single': {
    height: 40,
    paddingTop: 4,
  },
});

export const paginationStyles = css({
  position: 'fixed',
  left: '50%',
  bottom: 57, // This is because the eldest-parent container has height of 100%
  transform: 'translate(-50%, -50%)',
  margin: '0, auto',
});

export const cardListStyle = css({
  '& .ant-pagination': {
    textAlign: 'center',
  },
  marginTop: 15,
  '& .ant-list-empty-text': {
    visibility: 'hidden',
  },
});

export const departmentFilterContainer = css({
  width: 'auto',
  display: 'flex',
  marginTop: -21,
});

export const topContainer = css({
  marginBottom: 0,
  maxWidth: 1290,
  paddingRight: 20,
});

export const gridTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 18,
});

export const addJobsButtonText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: '1.2em',
});

export const searchbarStyle = css({
  fontFamily: '"Open Sans", sans-serif',
  '& input': {
    fontFamily: '"Open Sans", sans-serif',
    borderRadius: 20,
    width: 248,
    marginTop: -7,
    height: 32,
  },
  '& .ant-input:not(:first-child)': {
    paddingLeft: 35,
  },
});

export const addJobsButtonIcon = css({
  fontSize: 35,
});

export const modalTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: '1.5em',
});

export const modalContainer = css({
  width: 10,
});

export const loadingImage = css({
  display: 'block',
  margin: 'auto',
});

export const dropdownMenuItem = css({
  marginTop: 16,
  width: 200,
  height: 30,
});

export const dropdownButton = width =>
  css({
    marginTop: 17,
    height: 30,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.white,
      color: COLORS.red,
      borderColor: COLORS.red,
    },
    width,
  });

export const SearchIconStyles = css({
  marginRight: 10,
  marginTop: -3,
});

export const spinContainer = css({
  width: '100%',
  height: '50vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});
