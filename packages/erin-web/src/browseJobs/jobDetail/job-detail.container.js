import { connect } from 'react-redux';
import { JobDetails as JobDetailsComponent } from './job-detail.component';
import { actions as AllActions } from 'erin-app-state-mgmt';
import { withJobById } from 'erin-app-state-mgmt/src/_shared/api/components/jobs/with-job-by-id.provider';
import { compose } from '../../_shared/services/utils';
const { browseJobsActions } = AllActions;

const JobActions = browseJobsActions.actions;

const mapStateToProps = (state, props) => {
  const { currentUser } = state.user;
  return {
    currentUser,
    currentJob: state.browseJobs.currentJob,
    id: props.match.params.id,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deselectCurrentJob() {
      dispatch(JobActions.resetAddJobForm());
    },
  };
};

const JobDetailsContainer = compose(withJobById)(JobDetailsComponent);

export const JobDetails = connect(
  mapStateToProps,
  mapDispatchToProps
)(JobDetailsContainer);
