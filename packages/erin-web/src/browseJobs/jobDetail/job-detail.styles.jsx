import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const mainContainer = css({
  width: '100%',
  height: '100%',
  margin: 30,
  paddingRight: 30,
});

export const editJobButton = css({
  marginTop: -12,
  top: 8,
  marginLeft: 30,
  marginRight: 5,
  backgroundColor: 'transparent',
  color: COLORS.blue,
  borderColor: 'transparent',
  height: 40,
  width: 40,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  fontSize: '1.25em',
  '&:hover': {
    backgroundColor: 'transparent',
    color: COLORS.blue,
    borderColor: 'transparent',
  },
  '&:focus': {
    backgroundColor: 'transparent',
    color: COLORS.blue,
    borderColor: 'transparent',
  },
});

export const spinContainer = css({
  width: '100%',
  height: '50vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const backButton = css({
  color: COLORS.blue,
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 18,
  fontWeight: '600 !important',
});

export const hiredCountDiv = css({
  backgroundColor: COLORS.dashboardGreen,
  borderRadius: 10,
  height: 50,
  width: 330,
  marginBottom: 20,
  textAlign: 'center',
  display: 'inline-block',
});

export const editJobButtonText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  letterSpacing: 0.85,
  color: COLORS.blue,
  fontSize: '1.25em',
  marginTop: 22,
  marginLeft: 3,
});

export const addJobsButtonIcon = css({
  fontSize: 20,
});

export const topContainer = css({
  marginBottom: 30,
  maxWidth: 1290,
  paddingRight: 30,
  marginRight: 30,
});

export const modalTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 500,
  color: COLORS.heading,
  fontSize: '1.5em',
});

export const loadingImage = css({
  display: 'block',
  margin: 'auto',
});

export const hiredStandardText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.heading,
  fontSize: '1.25em',
  opacity: 0.75,
});

export const hiredCountText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.white,
  fontSize: '1.25em',
});

export const spacer = css({
  margin: 5,
  height: 2,
  width: '100%',
});

export const gridTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '600 !important',
  color: COLORS.black,
  fontSize: 18,
  marginTop: 30,
  marginBottom: 30,
});

export const NoSmartReferrals = css({
  marginTop: 40,
  width: 955,
  textAlign: 'center',
  fontSize: 14,
  fontWeight: 400,
});

export const toggleButtonContainerStyles = css({
  display: 'flex',
  float: 'right',
});

export const openButtonStyles = selected =>
  css({
    margin: 8,
    backgroundColor: selected ? COLORS.red : COLORS.white,
    color: selected ? COLORS.white : COLORS.subHeading,
    '&:hover': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    '&:focus': {
      backgroundColor: COLORS.red,
      color: COLORS.white,
      borderColor: 'transparent',
    },
    height: 30,
    width: 'auto',
  });

export const toggleTitleStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.black,
  fontSize: '1.15em',
  marginTop: 14,
});

export const ListStyles = css({
  width: 1020,
  '& .ant-pagination': {
    textAlign: 'center',
  },
});
