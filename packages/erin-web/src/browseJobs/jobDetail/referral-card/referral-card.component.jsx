import React from 'react';
import { Row, Card, Button } from 'antd';
import { Link } from 'react-router-dom';
import {
  referralCardMain,
  topContainer,
  jobInfoContainer,
  middleContainer,
  bottomContainer,
  jobTitleText,
  jobDescText,
  buttonRow,
  referBtn,
  noReferBtn,
  jobMatch,
  jobDescTitle,
} from './referral-card.styles';
import WebIcon from '../../../_shared/components/web-icon.component';
import { COLORS } from '../../../_shared/styles/colors';
import SocialMedia from '../../../_shared/components/social-media/social-media.component';

export class ReferralCard extends React.Component {
  render() {
    const {
      referral: {
        contact: { firstName, lastName, socialMediaAccounts, emailAddress },
        job: { title },
        status,
        company,
      },
    } = this.props;

    return (
      <div>
        {status && (
          <Card
            hoverable
            headStyle={{ paddingRight: 15, paddingLeft: 15, borderWidth: 0 }}
            bodyStyle={{ padding: 15 }}
            className={referralCardMain}
          >
            <div>
              <Row className={topContainer}>
                <div className={jobInfoContainer}>
                  <Link
                    to="#"
                    className={jobTitleText}
                  >{`${firstName} ${lastName}`}</Link>
                  <SocialMedia
                    email={emailAddress}
                    socialMedia={socialMediaAccounts}
                  />
                </div>
              </Row>
            </div>
            <Row className={middleContainer}>
              <div className={jobDescText}>
                <span className={jobDescTitle}>Company:</span>{' '}
                {company && company.name}
              </div>
              <div className={jobDescText}>
                <span className={jobDescTitle}>Job Title:</span> {title}
              </div>
            </Row>
            <div>
              <Row className={bottomContainer}>
                <Row className={buttonRow}>
                  <Button className={referBtn}>
                    <WebIcon name="user" color={COLORS.white} size={18} />
                    <span style={{ marginLeft: 5, top: -4 }}>
                      Make Referral
                    </span>
                  </Button>
                  <Button className={noReferBtn}>
                    <WebIcon
                      name="cancel-button"
                      color={COLORS.white}
                      size={18}
                    />
                    <span style={{ marginLeft: 5 }}> Don&#39;t Refer </span>
                  </Button>
                </Row>
                <Row className={jobMatch}>
                  <div>
                    {' '}
                    Is this information correct? You can update it <a>here</a>
                  </div>
                </Row>
              </Row>
            </div>
          </Card>
        )}
      </div>
    );
  }
}
