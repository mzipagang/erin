import { css } from 'emotion';
import { COLORS } from '../../../_shared/styles/colors';

export const referralCardMain = css({
  height: 220,
  width: 320,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 2,
  '& ant-card-head-title': {
    margin: 0,
  },
});

export const topContainer = css({
  height: 40,
});

export const jobInfoContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
});

export const middleContainer = css({
  marginBottom: 15,
});

export const bottomContainer = css({
  height: 60,
  marginTop: 15,
});

export const jobTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: '600 !important',
  color: COLORS.blue,
  fontSize: 18,
});

export const jobDescText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.black,
  fontSize: 14,
});

export const jobDescTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.lightGray,
  fontSize: 14,
});

export const referBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  background: COLORS.red,
  fontSize: 14,
  padding: 10,
  height: 40,
  width: 142,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const noReferBtn = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  background: COLORS.blue,
  fontSize: 14,
  marginLeft: '5px',
  padding: 10,
  height: 40,
  width: 142,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const buttonRow = css({
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 10,
  marginTop: 15,
});

export const jobMatch = css({
  fontSize: 12,
  textAlign: 'center',
});
