import React from 'react';

import { storiesOf } from '@storybook/react';
import { ReferralCard } from './referral-card.component';
import { referralData } from '../../../../../../test/data/referrals.data';

storiesOf('Development/Browse Jobs', module).add('Referral Card', () => (
  <ReferralCard referral={referralData[0]} />
));

storiesOf('QA/Browse Jobs', module).add('Referral Card', () => (
  <ReferralCard referral={referralData[0]} />
));
