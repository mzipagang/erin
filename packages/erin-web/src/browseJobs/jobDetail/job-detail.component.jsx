import React from 'react';
import { List, Row, Col, Icon, Spin } from 'antd';
import { Link } from 'react-router-dom';
import {
  topContainer,
  gridTitle,
  backButton,
  ListStyles,
  NoSmartReferrals,
  spinContainer,
  mainContainer,
} from './job-detail.styles';

import { JobDescriptionCard } from './job-description/job-description.component';
import { ReferralCard } from './referral-card/referral-card.component';

export class JobDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentJob: props.currentJob,
      page: 0,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentJob !== this.props.currentJob) {
      this.setState({ currentJob: this.props.currentJob });
    }
  }

  handleBack = () => {
    this.setState({ page: this.state.page > 0 ? this.state.page - 1 : 0 });
  };

  resetForm = () => {
    this.setState({
      page: 0,
    });
  };

  render() {
    const { currentJob } = this.state;
    if (!currentJob) {
      return (
        <div className={spinContainer}>
          <Spin size="large" tip="...Loading" />
        </div>
      );
    }
    return (
      <main className={mainContainer}>
        <Row className={topContainer}>
          <Col>
            <Link className={backButton} to="/browsejobs">
              <Icon type="left" /> View All Jobs
            </Link>
          </Col>
        </Row>
        <Row>
          <Col>{currentJob && <JobDescriptionCard job={currentJob} />}</Col>
        </Row>
        <Row>
          <div>
            <h3 className={gridTitle}> Recommended Referrals </h3>
            {currentJob.referrals.length > 0 ? (
              <div>
                <div>
                  <List
                    className={ListStyles}
                    grid={{ gutter: 5, xs: 1, sm: 1, md: 2, lg: 3, xl: 3 }}
                    pagination={{ pageSize: 3 }}
                    dataSource={currentJob ? currentJob.referrals : []}
                    renderItem={referral => (
                      <List.Item>
                        <ReferralCard referral={referral} />
                      </List.Item>
                    )}
                  />
                </div>
              </div>
            ) : (
              <p className={NoSmartReferrals}>
                There are no Smart Referrals for this job
              </p>
            )}
          </div>
        </Row>
      </main>
    );
  }
}
