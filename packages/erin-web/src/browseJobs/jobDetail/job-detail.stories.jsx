import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import { JobDetails } from './job-detail.component';
import { jobData } from '../../../../../test/data';
import { referralData } from '../../../../../test/data/referrals.data';

storiesOf('Development/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Job Detail', () => (
    <JobDetails currentJob={jobData[0]} fetchReferrals={() => referralData} />
  ));

storiesOf('QA/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Job Detail', () => (
    <JobDetails currentJob={jobData[0]} fetchReferrals={() => referralData} />
  ));
