import { Card, Col, Row } from 'antd';
import React from 'react';
import renderHTML from 'react-render-html';
import {
  topContainer,
  jobInfoContainer,
  jobTitleText,
  jobDetailRow,
  locationAdnDepartment,
  cardContainer,
  sectionContainer,
  summaryTitle,
  summaryValue,
  footerTextStyles,
  hiringManagerLinkStyles,
  footerStyles,
  bonusStyles,
  jobMatch,
  jobMatchNumber,
  linkStyles,
  rightContainer,
  descriptionContainer,
} from './job-description.styles';
import { COLORS } from '../../../_shared/styles/colors';
import WebIcon from '../../../_shared/components/web-icon.component';
import { ReferralModal } from '../../../_shared/referral-modal/referral-modal.container';
import { ShareSendButton } from '../../../_shared/components/share-send-button/share-send.component';
import { normalizeDollar } from '../../../_shared/services/utils';

export class JobDescriptionCard extends React.Component {
  safeSalary = (min, max) => {
    // TODO: handle this better
    if (!min || !max) return '';
    if (!min || min === max) return `$${normalizeDollar(max)}`;
    if (!max) return `$${normalizeDollar(min)}`;
    return `$${normalizeDollar(min)} - $${normalizeDollar(max)}`;
  };

  onClickRefer = () => {
    // TODO: show new referral modal here
  };

  onClickShare = () => {
    // TODO: TBD
  };

  renderMatches = () => {
    const { _matches } = this.props;
    return (
      <div>
        <WebIcon name="tick-inside-circle" color={COLORS.green} size={14} />
        {_matches === 1 ? (
          <span>
            <span className={jobMatchNumber}> {_matches} Person </span> In Your
            Network Match This Job!
          </span>
        ) : (
          <span>
            <span className={jobMatchNumber}> {_matches} People </span> In Your
            Network Match This Job!
          </span>
        )}
      </div>
    );
  };

  render() {
    const {
      job,
      job: {
        title,
        department,
        description,
        hiringManager, // TODO: get hiring manager details
        location,
        jobType,
        salary,
        id,
        publicLink,
        referralBonus,
        _matches,
      },
    } = this.props;
    const _salary =
      salary && (salary.to || salary.from)
        ? this.safeSalary(salary.from, salary.to)
        : null;

    return (
      <Card
        headStyle={{ paddingRight: 30, paddingLeft: 30, borderWidth: 0 }}
        bodyStyle={{ paddingRight: 30, paddingLeft: 30, paddingTop: 0 }}
        className={cardContainer}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <Row className={topContainer}>
              <Col className={jobInfoContainer}>
                <Row type="flex">
                  <Col>
                    <span className={jobTitleText}> {title} </span>
                  </Col>
                  {_matches && _matches > 0 ? (
                    <Col className={jobMatch}>{this.renderMatches()}</Col>
                  ) : null}
                </Row>
                <Row className={jobDetailRow}>
                  <WebIcon name="folder" color={COLORS.darkGray} size={20} />
                  <span className={locationAdnDepartment}>
                    {' '}
                    {department && department.name}{' '}
                  </span>
                  <WebIcon
                    name="placeholder"
                    color={COLORS.darkGray}
                    size={20}
                  />
                  <span className={locationAdnDepartment}>
                    {location && location.city
                      ? `${location.city}, ${location.state}`
                      : 'Remote'}
                  </span>
                </Row>
              </Col>
            </Row>
            <div className={sectionContainer}>
              <Row>
                <span className={summaryTitle}> Job Type: </span>
                <span className={summaryValue}> {jobType} </span>
              </Row>
              {_salary && (
                <Row>
                  <span className={summaryTitle}> Salary: </span>
                  <span className={summaryValue}> {_salary} </span>
                </Row>
              )}
            </div>

            <div className={descriptionContainer}>
              <Row>
                <span className={summaryTitle}> Job Description: </span>
                <span className={summaryValue}>
                  {renderHTML(description.toString('html'))}
                </span>
              </Row>
            </div>
            {hiringManager && (
              <div className={footerStyles}>
                <span className={footerTextStyles}>Hiring Manager: </span>
                <a
                  href={`mailto:${'test@email.com'}`}
                  className={hiringManagerLinkStyles}
                >
                  {hiringManager}
                  <WebIcon name="email-filled" color={COLORS.blue} size={22} />
                </a>
              </div>
            )}
          </div>
          <div className={rightContainer}>
            <Row className={bonusStyles}>
              Referral Bonus:
              <span style={{ color: 'green', fontWeight: '700' }}>
                {' '}
                $
                {referralBonus && referralBonus.hasBonus
                  ? referralBonus.amount
                  : 0}
              </span>
            </Row>
            <div
              style={{
                display: 'flex',
                marginTop: 5,
                marginRight: -10,
              }}
            >
              <Row style={{ marginRight: 5 }}>
                <div>
                  <ReferralModal jobData={job} />
                </div>
              </Row>
              <Row>
                <div>
                  <ShareSendButton
                    title={title}
                    jobId={id}
                    jobUrl={publicLink}
                    jobType={jobType}
                    description={description}
                    city={location ? location.city : null}
                    state={location ? location.state : null}
                  />
                </div>
              </Row>
            </div>
            <Row style={{ marginTop: 8 }}>
              <span>
                or{' '}
                <a className={linkStyles} href={publicLink}>
                  View Public Job Posting
                </a>
              </span>
            </Row>
          </div>
        </div>
      </Card>
    );
  }
}
