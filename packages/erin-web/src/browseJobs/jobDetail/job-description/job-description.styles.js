import { css } from 'emotion';
import { COLORS } from '../../../_shared/styles/colors';

export const topContainer = css({
  marginTop: 25,
  marginBottom: 20,
});

export const jobInfoContainer = css({
  height: 'auto',
});

export const jobTitleText = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 600,
  color: COLORS.black,
  fontSize: 20,
});

export const jobIcons = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 300,
  color: COLORS.subHeading,
  fontSize: '1.45em',
  opacity: 0.7,
});

export const jobDetailRow = css({
  marginTop: 5,
  '& svg': {
    transform: 'translate(0, 3px)',
  },
});

export const bonusStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  marginTop: '5px',
  fontSize: 18,
});

export const locationAdnDepartment = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: 16,
});

export const bonusAmount = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.green,
  fontSize: 18,
});

export const cardContainer = css({
  height: 'auto',
  width: 1005,
  backgroundColor: COLORS.white,
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  fontFamily: '"Open Sans", sans-serif',
});

export const sectionContainer = css({
  marginBottom: 15,
});
export const descriptionContainer = css({
  marginBottom: 15,
  minHeight: 150,
});

export const summaryTitle = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: '1.25em',
});

export const summaryValue = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.darkGray,
  fontSize: '1.25em',
});

export const jobLinkStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.blue,
  fontSize: '1em',
});

export const referSomoneButtonStyles = css({
  height: 50,
  borderColor: COLORS.white,
  backgroundColor: COLORS.red,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  fontSize: '1.25em',
  '&:hover': {
    borderColor: COLORS.white,
    color: COLORS.white,
    backgroundColor: COLORS.red,
  },
  '&:focus': {
    borderColor: COLORS.white,
    color: COLORS.white,
    backgroundColor: COLORS.red,
  },
});

export const shareButtonStyles = css({
  height: 50,
  borderColor: COLORS.white,
  backgroundColor: COLORS.blue,
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 200,
  color: COLORS.white,
  fontSize: '1.25em',
  '&:hover': {
    backgroundColor: COLORS.blue,
    borderColor: COLORS.white,
    color: COLORS.white,
  },
  '&:focus': {
    backgroundColor: COLORS.blue,
    borderColor: COLORS.white,
    color: COLORS.white,
  },
});

export const buttonIconStyles = css({
  fontWeight: 300,
  color: COLORS.white,
  fontSize: '1.45em',
});

export const footerTextStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.heading,
  fontSize: '1.25em',
});

export const hiringManagerLinkStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontWeight: 400,
  color: COLORS.blue,
  fontSize: '1.25em',
  marginLeft: 5,
});

export const footerStyles = css({
  '& svg': {
    transform: 'translate(20px, 6px)',
  },
});

export const jobMatch = css({
  fontSize: 14,
  marginLeft: 20,
  marginTop: 6,
});

export const jobMatchNumber = css({
  color: COLORS.blue,
  fontWeight: 500,
});

export const locationIcon = css({
  width: 18,
  marginTop: -2,
});

export const cardHeadStyle = {
  borderWidth: 0,
  padding: 5,
  marginTop: -15,
};

export const cardBodyStyle = {
  width: 955,
  padding: 5,
};

export const linkStyles = css({
  fontWeight: '600 !important',
  color: COLORS.hyperLink,
});

export const rightContainer = css({
  marginTop: 20,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-end',
});
