import { connect } from 'react-redux';
import { BrowseJobs as BrowseJobsComponent } from './browseJobs.component';
import { withListJobs } from 'erin-app-state-mgmt/src/_shared/api/components/jobs/with-list-jobs.provider';
import { compose } from '../_shared/services/utils';

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser: state.user.currentUser,
    filter: { companyId: { eq: currentUser.companyId } },
  };
};

const BrowseJobsContainer = compose(withListJobs)(BrowseJobsComponent);

export const BrowseJobs = connect(mapStateToProps)(BrowseJobsContainer);
