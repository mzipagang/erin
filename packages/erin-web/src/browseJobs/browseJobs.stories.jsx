import React from 'react';
import StoryRouter from 'storybook-react-router';
import { storiesOf } from '@storybook/react';
import { BrowseJobs } from './browseJobs.component';
import { jobData } from '../../../../test/data'; // test data can be pulled from here to act like redux

storiesOf('Development/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Main', () => (
    <BrowseJobs
      jobs={jobData}
      setCurrentJob={() => jobData[0]}
      fetchJobs={() => jobData}
    />
  ));

storiesOf('QA/Browse Jobs', module)
  .addDecorator(StoryRouter())
  .add('Main', () => (
    <BrowseJobs
      jobs={jobData}
      currentJob={jobData[0]}
      fetchJobs={() => jobData}
    />
  ));
