import React from 'react';
import {
  List,
  Row,
  Col,
  Icon,
  Select,
  Input,
  Spin,
  //Pagination,
} from 'antd';
import _ from 'lodash';
import {
  DropDown,
  departmentFilterContainer,
  searchbarStyle,
  topContainer,
  gridTitle,
  cardListStyle,
  SelectContainer,
  SelectStyles,
  spinContainer,
  mainContainer,
  middleContainer,
} from './browseJobs.styles';
import WebIcon from '../_shared/components/web-icon.component';
import { COLORS } from '../_shared/styles/colors';
import { JobCard } from './jobCard/jobCard.component';
import { ToggleOpenClosed } from '../_shared/components/toggleOpenClosed/toggleOpenClosed.component';

const filterOpenKey = 'open';
const filterClosedKey = 'closed';

const Option = Select.Option;

export class BrowseJobs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      visible: false,
      filteredDepartments: [],
      filteredJobs: props.jobs,
      allJobs: props.jobs,
      searchQuery: '',
      page: 0,
      errors: {},
      filterOpenStatus: filterOpenKey,
    };
  }

  // TODO: replace with other api
  UNSAFE_componentWillReceiveProps = nextProps => {
    this.setState({
      filteredJobs: nextProps.jobs,
      allJobs: nextProps.jobs,
    });
  };

  // TODO: filter jobs using API
  filterJobs = (property, match, jobSet) => {
    return jobSet.filter(job => job[property] === match);
  };

  // TODO: query jobs using API
  searchJobs = (properties, query, jobSet) =>
    jobSet.filter(
      job =>
        !query ||
        properties.some(
          property =>
            job[property] &&
            job[property].toLowerCase().includes(query.toLowerCase())
        )
    );

  updateFilteredJobs = selectedDepartments => {
    let filtered = [];
    if (selectedDepartments.length < 1) {
      filtered = this.state.allJobs;
    } else {
      this.state.allJobs.forEach(job => {
        selectedDepartments.forEach(department => {
          if (job.department && job.department.name === department)
            filtered.push(job);
          return;
        });
      });
    }
    this.setState({
      filteredJobs: filtered,
    });
  };

  handleFilterDepartment = department => {
    const { filteredDepartments } = this.state;
    if (filteredDepartments.indexOf(department) > -1) return;
    const updated = [...filteredDepartments, department];
    this.updateFilteredJobs(updated);
    this.setState({
      filteredDepartments: updated,
    });
  };

  handleRemoveFilter = department => {
    const { filteredDepartments } = this.state;
    const index = filteredDepartments.indexOf(department);
    const filterDepartments = item => item !== filteredDepartments[index];
    const departments =
      filteredDepartments.length === 1
        ? []
        : filteredDepartments.filter(filterDepartments);

    this.updateFilteredJobs(departments);
    this.setState({
      filteredDepartments: departments,
    });
  };

  onChangePage = () => {
    // current, pageSize are arguments
    // console.log('New Page Number: ', current, 'Page Size:', pageSize);
    // TODO: call real api/action
  };

  renderFilters = () => {
    const { filterOpenStatus } = this.state;
    const { departments } = this.props;
    return (
      <Row className={departmentFilterContainer}>
        <Col style={{ display: 'flex', maxWidth: 820 }}>
          <ToggleOpenClosed
            onClick={filterOpenStatus => {
              this.setState({
                filterOpenStatus,
              });
            }}
            filterOpenStatus={filterOpenStatus}
            openJobsKey={filterOpenKey}
            closedJobsKey={filterClosedKey}
          />
          <div className={SelectContainer}>
            <Select
              className={SelectStyles}
              mode="multiple"
              placeholder="Filter by Department"
              dropdownClassName={DropDown}
              onSelect={department => this.handleFilterDepartment(department)}
              onDeselect={department => this.handleRemoveFilter(department)}
            >
              {departments.map(department => {
                return (
                  <Option key={department.id} value={department.name}>
                    {department.name}
                  </Option>
                );
              })}
            </Select>
            <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
          </div>
        </Col>
      </Row>
    );
  };

  renderSearchBar = () => {
    return (
      <Input
        prefix={<Icon type="search" style={{ fontSize: '18px' }} />}
        className={searchbarStyle}
        placeholder="Search Jobs"
        value={this.state.searchQuery}
        onChange={({ target }) => {
          this.setState({
            searchQuery: target.value,
          });
        }}
      />
    );
  };

  sortJobs = (a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  render() {
    const { filteredJobs, filterOpenStatus, searchQuery } = this.state;
    if (!filteredJobs) {
      return (
        <div className={spinContainer}>
          <Spin size="large" tip="...Loading" />
        </div>
      );
    }
    const searchedJobs = this.searchJobs(
      ['title', 'description'],
      searchQuery,
      filteredJobs
    );

    const openJobs = this.filterJobs('status', 'open', searchedJobs);
    const closedJobs = this.filterJobs('status', 'closed', searchedJobs);

    const jobTypeTitle =
      filterOpenStatus === filterOpenKey ? 'Open Jobs' : 'Closed Jobs';
    const jobData = filterOpenStatus === filterOpenKey ? openJobs : closedJobs;
    const sortedJobData = _.sortBy(jobData, function(job) {
      return job.dateCreated;
    });
    return (
      <main className={mainContainer}>
        <Row type="flex" justify="space-between" className={topContainer}>
          <div>{this.renderFilters()}</div>
          <div>{this.renderSearchBar()}</div>
        </Row>

        <div className={middleContainer}>
          <span className={gridTitle}> {jobTypeTitle} </span>
          <List
            grid={{ gutter: 10, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 }}
            pagination={{ pageSize: 15 }}
            dataSource={sortedJobData}
            className={cardListStyle}
            renderItem={job => (
              <List.Item>
                <JobCard
                  key={
                    job.id || Math.floor(Math.random() * Math.floor(1203869142))
                  }
                  job={job}
                />
              </List.Item>
            )}
          />
        </div>
      </main>
    );
  }
}
