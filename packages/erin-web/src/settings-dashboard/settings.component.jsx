import React from 'react';
import SettingsForm from './settings-form.component';
import { SettingsContainer, Card } from './settings.styles';

class SettingsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      company: props.company,
      success: false,
      error: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.company !== this.props.company) {
      this.setState({ company: this.props.company });
    }
  }

  handleError = () => {
    this.setState({ error: true });
  };

  handleSuccess = () => {
    this.setState({ success: true, error: false });
  };

  render() {
    const { company, error, success } = this.state;
    const { onCreateDepartment, onDeleteDepartment, onUpdate } = this.props;

    return (
      <main className={SettingsContainer}>
        <div className={Card}>
          <SettingsForm
            company={company}
            error={error}
            success={success}
            handleError={this.handleError}
            handleSuccess={this.handleSuccess}
            onCreateDepartment={onCreateDepartment}
            onDeleteDepartment={onDeleteDepartment}
            onUpdate={onUpdate}
            updateCurrentUserCompany={this.props.updateCurrentUserCompany}
          />
        </div>
      </main>
    );
  }
}

export default SettingsComponent;
