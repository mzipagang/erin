import React from 'react';
import { storiesOf } from '@storybook/react';
import SettingsComponent from './settings.component';
import { companyData } from '../../../../test/data/mb-data';

storiesOf('Development/Settings Dashboard', module).add('Main', () => (
  <SettingsComponent
    company={companyData[0]}
    getCompany={() => companyData[0]}
  />
));

storiesOf('QA/Settings Dashboard', module).add('Main', () => (
  <SettingsComponent
    company={companyData[0]}
    getCompany={() => companyData[0]}
  />
));
