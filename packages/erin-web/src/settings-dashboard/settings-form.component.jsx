import React, { Component } from 'react';

import { Form, Input, Collapse, Button, Icon, Alert } from 'antd';
import { cx } from 'emotion';
import { DepartmentItem } from './department-items/dapartment-item.component';
import { COLORS } from '../_shared/styles/colors';

import {
  FormItemContainer,
  FormStyles,
  InputStyles,
  WebsiteStyles,
  BonusStyles,
  SubmitBtnContainer,
  SubmitBtn,
  CheckIcon,
  miscText,
} from './settings.styles';

class SettingsForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    let { company } = this.props;
    this.props.form.validateFields((err, values) => {
      if (err) {
        this.props.handleError();
        return;
      }
      if (values.departmentName !== undefined) {
        this.props.onCreateDepartment({
          input: {
            companyId: company.id,
            name: values.departmentName,
            active: true,
          },
        });
      }
      this.props.onUpdate({
        input: {
          id: company.id,
          name: values.name,
          defaultBonusAmount:
            values.defaultBonusAmount !== undefined
              ? values.defaultBonusAmount.replace(/[^0-9]+/g, '')
              : 0,
          contactIncentiveBonus:
            values.contactIncentiveBonus !== undefined
              ? values.contactIncentiveBonus.replace(/[^0-9]+/g, '')
              : 0,
          websiteUrl: values.url !== undefined ? values.url : '',
        },
      });
      const updatedCompany = {
        ...company,
        name: values.name,
        id: company.id,
        defaultBonusAmount:
          values.defaultBonusAmount !== undefined
            ? values.defaultBonusAmount.replace(/[^0-9]+/g, '')
            : 0,
        contactIncentiveBonus:
          values.contactIncentiveBonus !== undefined
            ? values.contactIncentiveBonus.replace(/[^0-9]+/g, '')
            : 0,
        websiteUrl: values.url !== undefined ? values.url : '',
      };
      this.props.updateCurrentUserCompany(updatedCompany);
      this.props.handleSuccess();
      this.props.form.resetFields();
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    const Panel = Collapse.Panel;
    const { company, success, error } = this.props;
    let initialData = company;
    if (!company) {
      initialData = {
        name: '',
        websiteUrl: '',
        defaultBonusAmount: '',
        contactIncentiveAmount: '',
        departments: [],
      };
    }

    return (
      <div>
        <Form className={FormStyles}>
          <h1>Company Information</h1>

          <div className={FormItemContainer}>
            <label>Company Name:</label>
            <FormItem>
              {getFieldDecorator('name', {
                initialValue: `${initialData.name}`,
                rules: [
                  { required: true, message: 'Please input company name.' },
                ],
              })(<Input className={InputStyles} />)}
            </FormItem>
          </div>

          <div className={FormItemContainer}>
            <label htmlFor="companyWebsite">Company Website:</label>
            <FormItem>
              {getFieldDecorator('url', {
                initialValue: `${initialData.websiteUrl}`,
                rules: [
                  {
                    required: true,
                    message: 'Please input company website url.',
                  },
                ],
              })(<Input className={cx(InputStyles, WebsiteStyles)} />)}
            </FormItem>
          </div>

          <h1>Referral Bonuses</h1>

          <div className={FormItemContainer}>
            <label htmlFor="defaultBonusAmount">Default Bonus Amount:</label>
            <FormItem>
              {getFieldDecorator('defaultBonusAmount', {
                initialValue: `$${initialData.defaultBonusAmount}`.replace(
                  /\B(?=(\d{3})+(?!\d))/g,
                  ','
                ),
                rules: [
                  {
                    required: true,
                    message: 'Please input default bonus amount.',
                  },
                ],
              })(<Input className={cx(InputStyles, BonusStyles)} />)}
            </FormItem>
          </div>

          <div className={FormItemContainer}>
            <label htmlFor="contactIncentiveBonus">
              Contact Incentive Amount:
            </label>
            <FormItem>
              {getFieldDecorator('contactIncentiveBonus', {
                initialValue: `$${initialData.contactIncentiveBonus}`.replace(
                  /\B(?=(\d{3})+(?!\d))/g,
                  ','
                ),
                rules: [
                  {
                    required: true,
                    message: 'Please input contact incentive amount.',
                  },
                ],
              })(<Input className={InputStyles} />)}
            </FormItem>
          </div>
          <div className={miscText}>
            <span>
              Incentivize your employees to connect their contacts. All job
              referrals will be reduced by this amount for employees who have
              not connected their contacts.
            </span>
          </div>

          <h1 style={{ marginTop: 20 }}>Manage Departments</h1>

          <div className={FormItemContainer}>
            <label htmlFor="departments">Departments:</label>
            <FormItem style={{ width: 300 }}>
              {getFieldDecorator('departmentName')(
                <Input
                  className={InputStyles}
                  placeholder="Enter a new department"
                />
              )}
            </FormItem>
          </div>

          <Collapse
            style={{
              position: 'relative',
              left: -16,
              fontFamily: '"Open Sans", sans-serif',
              color: COLORS.lighGray,
            }}
            bordered={false}
          >
            <Panel
              style={{ border: 'none', fontSize: 18 }}
              header="View or Delete Departments"
              key="1"
            >
              <DepartmentItem
                onDeleteDepartment={this.props.onDeleteDepartment}
                departments={initialData.departments}
              />
            </Panel>
          </Collapse>
          {error ? (
            <Alert
              message="There was an error submitting your updates, please try again."
              type="error"
            />
          ) : null}
          {success ? (
            <Alert
              message="Your updates were successfully submitted."
              type="success"
            />
          ) : null}
          <FormItem className={SubmitBtnContainer}>
            <Button
              className={SubmitBtn}
              onClick={this.handleSubmit}
              htmlType="submit"
            >
              Update Settings
              <Icon className={CheckIcon} type="check" />
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default Form.create()(SettingsForm);
