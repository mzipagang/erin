import { css } from 'emotion';
import { COLORS } from '../_shared/styles/colors';

export const SettingsContainer = css({
  fontFamily: '"Open Sans", sans-serif',
  width: '100%',
  height: '100%',
  padding: 30,
  backgroundColor: COLORS.lightGray2,
  margin: 0,
});

export const Card = css({
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 4,
  padding: 40,
  backgroundColor: 'white',
  height: 'auto',
  '& button:focus': {
    outline: 'none',
  },
  maxWidth: 1270,
});

export const FormItemContainer = css({
  display: 'flex',
  '& label': {
    fontSize: 18,
    color: COLORS.lightGray,
    lineHeight: '40px',
    marginRight: 20,
  },
});

export const FormStyles = css({
  '& .ant-form-item': {
    marginBottom: 0,
  },
});

export const InputStyles = css({
  fontSize: 18,
  height: 40,
  marginBottom: 10,
});

export const WebsiteStyles = css({
  color: COLORS.blue,
});

export const BonusStyles = css({
  color: COLORS.green,
});

export const MiscTextStyles = css({
  fontSize: 'italic',
});

export const SubmitBtn = css({
  fontFamily: 'Open Sans',
  backgroundColor: COLORS.red,
  color: 'white',
  fontSize: 20,
  fontWeight: 300,
  padding: '10px 30px',
  height: 'auto',
  border: 'none',
  marginBottom: 0,
});

export const SubmitBtnContainer = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  padding: 10,
  marginTop: 20,
  // '& .ant-form-item-control-wrapper': {
  //   '@media (max-width: 575px)': {
  //     width: 'auto',
  //   },
  // },
  '& .ant-btn: hover': {
    color: COLORS.blue,
    fontWeight: 300,
    border: `2px solid ${COLORS.blue}`,
  },
});

export const CheckIcon = css({
  borderRadius: 100,
  border: '1px solid white',
  padding: 2,
});

export const miscText = css({
  maxWidth: 600,
  fontStyle: 'italic',
});
