import { Icon, Row } from 'antd';
import React from 'react';
import {
  departmentContainer,
  closeIcon,
  departmentText,
  DepartmentsContainer,
} from './department-item.styles';

export const DepartmentItem = props => {
  const { departments, onDeleteDepartment } = props;
  return (
    <div className={DepartmentsContainer}>
      {departments.map(department => {
        return (
          <div
            key={department.id}
            onClick={() => onDeleteDepartment({ input: { id: department.id } })}
          >
            <Row className={departmentContainer}>
              <span className={departmentText}> {department.name} </span>
              <Icon type="close-circle-o" className={closeIcon} />
            </Row>
          </div>
        );
      })}
    </div>
  );
};
