import { connect } from 'react-redux';
import SettingsComponent from 'erin-web/src/settings-dashboard/settings.component';
import { withGetCompany } from 'erin-app-state-mgmt/src/_shared/api/components/settings/with-get-company.provider';
import { actions } from 'erin-app-state-mgmt';
import { compose } from '../_shared/services/utils';

const { userActions } = actions;

const mapStateToProps = state => {
  const { currentUser } = state.user;
  return {
    currentUser,
    id: currentUser.companyId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCurrentUserCompany(company) {
      dispatch(userActions.updateUserCompany(company));
    },
  };
};

export const SettingsWithApi = compose(withGetCompany)(SettingsComponent);

export const Settings = connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsWithApi);
