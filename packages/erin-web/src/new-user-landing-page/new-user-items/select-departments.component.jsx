import React from 'react';
import { Select } from 'antd';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import { SelectStyles, DropDown, SortDown } from './select.styles';

const SelectDepartments = props => {
  const { departments = [] } = props;
  const Option = Select.Option;

  let options = [];
  for (const department of departments) {
    options.push(
      <Option value={department.name} key={department.id}>
        {department.name}
      </Option>
    );
  }

  return (
    <div style={{ height: 50 }}>
      <Select
        labelInValue={true}
        showArrow={false}
        showSearch={true}
        className={SelectStyles}
        dropdownClassName={DropDown}
      >
        {options}
      </Select>
      <div className={SortDown}>
        <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
      </div>
    </div>
  );
};

export default SelectDepartments;
