import { css } from 'emotion';

export const SelectStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  width: 275,
  height: 50,
  zIndex: 1,
  '& .ant-select-selection': {
    backgroundColor: 'transparent',
    height: 50,
    fontSize: 16,
  },
  '& .ant-select-selection__rendered': {
    lineHeight: '50px !important',
  },
});

export const SortDown = css({
  position: 'relative',
  zIndex: 0,
  top: -40,
  left: 245,
});

export const DropDown = css({
  fontFamily: '"Open Sans", sans-serif',
});

export const SelectContainer = css({
  display: 'block',
});
