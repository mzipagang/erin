import React, { Component } from 'react';
import { Form, Input, Button, Icon, Select, Alert } from 'antd';
import WebIcon from '../../_shared/components/web-icon.component';
import { COLORS } from '../../_shared/styles/colors';
import { SelectStyles, DropDown, SortDown } from './select.styles';
import { Auth } from 'aws-amplify';
import { withRouter } from 'react-router-dom';

import {
  InputStyles,
  InputStyles2,
  LabelStyles,
  FlexContainer,
  TitleStyles,
  SubTitleStyles,
  FormStyles,
  SubmitBtn,
  SubmitBtnContainer,
  CheckIcon,
  MiscText,
  AlertError,
} from './new-user-form.styles';

class NewUserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serverError: false,
      lengthError: false,
      department: '',
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const { onCreate, company } = this.props;
    this.setState({ serverError: false });
    this.props.form.validateFields((err, values) => {
      if (err) {
        this.setState({ serverError: true });
      } else if (values.password.length < 8) {
        this.setState({ lengthError: true });
        return;
      }
      Auth.signUp({
        username: values.emailAddress,
        password: values.password,
      })
        .then(user => {
          onCreate({
            input: {
              cognitoId: user.userSub,
              companyId: company.id,
              emailAddress: values.emailAddress,
              role: 'employee',
              firstName: values.firstName,
              lastName: values.lastName,
              title: values.jobTitle,
              departmentId: values.department,
              avatar: null,
              lastLogin: null,
              active: true,
            },
          });
          this.props.history.push('/login');
        })
        .catch(err => {
          this.setState({ serverError: true });
          console.error(err);
        });
    });
  };

  handleSelectDepartment = value => {
    this.setState({ department: value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { serverError, lengthError } = this.state;
    const FormItem = Form.Item;
    const { departments = [] } = this.props.company;
    const Option = Select.Option;
    let options = [];
    for (const department of departments) {
      options.push(
        <Option value={department.id} key={department.id}>
          {department.name}
        </Option>
      );
    }
    return (
      <div>
        <h1 className={TitleStyles}>Let&#39;s Get Started</h1>
        <h2 className={SubTitleStyles}>First complete your profile</h2>
        <Form className={FormStyles}>
          <div className={FlexContainer}>
            <FormItem className={InputStyles}>
              <label className={LabelStyles}>First Name</label>
              {getFieldDecorator('firstName', {
                rules: [
                  {
                    required: true,
                    message: 'First Name Required',
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem className={InputStyles}>
              <label className={LabelStyles}>Last Name</label>
              {getFieldDecorator('lastName', {
                rules: [
                  {
                    required: true,
                    message: 'Last Name Required',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </div>
          <div className={FlexContainer}>
            <FormItem className={InputStyles}>
              <label className={LabelStyles}>Department</label>
              {getFieldDecorator('department', {
                rules: [
                  {
                    required: true,
                    message: 'Department Required',
                  },
                ],
              })(
                <Select
                  showArrow={false}
                  className={SelectStyles}
                  dropdownClassName={DropDown}
                >
                  {options}
                </Select>
              )}
              <div className={SortDown}>
                <WebIcon color={COLORS.darkGray} size={14} name="sort-down" />
              </div>
            </FormItem>
            <FormItem className={InputStyles}>
              <label className={LabelStyles}>Job Title</label>
              {getFieldDecorator('jobTitle', {
                rules: [
                  {
                    required: true,
                    message: 'Job Title Required',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </div>
          <FormItem className={InputStyles2}>
            <label className={LabelStyles}>Email Address</label>
            {getFieldDecorator('emailAddress', {
              rules: [
                {
                  type: 'email',
                  message: 'Not a Valid E-mail Adress',
                },
                {
                  required: true,
                  message: 'Email Address Required',
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem className={InputStyles2}>
            <label className={LabelStyles}>Create Password</label>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Password Required',
                },
              ],
            })(<Input className={InputStyles2} type="password" />)}
          </FormItem>
          {lengthError ? (
            <Alert
              className={AlertError}
              type="error"
              message="Password must be at least 8 characters long."
            />
          ) : null}
          <p className={MiscText}>
            By sigining up, I agree to the Erin <strong>Privacy Policy</strong>{' '}
            and <strong>Terms of Service.</strong>
          </p>
          {serverError ? (
            <Alert
              type="error"
              message="There was a problem creating your account. Please try again."
            />
          ) : null}
          <FormItem className={SubmitBtnContainer}>
            <Button
              onClick={this.handleSubmit}
              className={SubmitBtn}
              htmlType="submit"
            >
              Get Started
              <Icon className={CheckIcon} type="arrow-right" />
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default withRouter(Form.create()(NewUserForm));
