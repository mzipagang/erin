import { css } from 'emotion';
import { COLORS } from '../../_shared/styles/colors';

export const InputStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: 16,
  marginBottom: 15,
  height: 90,
  width: 275,
  '& .ant-input': {
    height: 50,
    fontFamily: '"Open Sans", sans-serif',
    fontSize: 16,
  },
  '& .ant-form-explain': {
    fontSize: 12,
  },
});

export const InputStyles2 = css({
  fontFamily: '"Open Sans", sans-serif',
  marginBottom: 15,
  fontSize: 16,
  '& .ant-input': {
    height: 50,
    fontFamily: '"Open Sans", sans-serif',
    fontSize: 16,
  },
  '& .ant-form-explain': {
    fontSize: 12,
  },
});

export const LabelStyles = css({
  fontFamily: '"Open Sans", sans-serif',
  fontSize: '18px !important',
  fontWeight: 600,
});

export const FlexContainer = css({
  display: 'flex',
  justifyContent: 'space-between',
});

export const TitleStyles = css({
  marginBottom: 0,
  textAlign: 'center',
  fontSize: 32,
});

export const SubTitleStyles = css({
  marginBottom: 0,
  textAlign: 'center',
});

export const FormStyles = css({
  marginTop: 20,
});

export const SubmitBtnContainer = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'flex-end',
  marginBottom: 0,
  '& .ant-form-item-control-wrapper': {
    '@media (max-width: 575px)': {
      width: 'auto',
    },
  },
  '& .ant-btn:hover': {
    backgroundColor: COLORS.lightGray2,
    border: `1px solid ${COLORS.blue} !important`,
    color: COLORS.blue,
  },
});

export const SubmitBtn = css({
  fontFamily: 'Open Sans',
  backgroundColor: COLORS.red,
  color: 'white',
  fontSize: 20,
  fontWeight: 300,
  padding: '10px 30px',
  height: 'auto',
  border: 'none',
  marginBottom: 0,
  width: 200,
});

export const CheckIcon = css({
  borderRadius: 100,
  border: '1px solid white',
  padding: 2,
});

export const MiscText = css({
  textAlign: 'center',
});

export const SelectContainer = css({
  display: 'block',
  position: 'relative',
});

export const AlertError = css({
  marginBottom: 20,
});
