import React, { Component } from 'react';

import { LandingPageContainer, Logo, Card } from './new-user-landing.styles';
import ErinLogo from '../_shared/assets/erinwhite.png';
import NewUserForm from './new-user-items/new-user-form.component';

class NewUserLandingPageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company: this.props.company,
    };
  }
  render() {
    const { company } = this.state;
    return (
      <div className={LandingPageContainer}>
        <img className={Logo} src={ErinLogo} alt="Erin Logo" />
        <div className={Card}>
          <NewUserForm onCreate={this.props.onCreate} company={company} />
        </div>
      </div>
    );
  }
}

export default NewUserLandingPageComponent;
