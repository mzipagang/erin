import { connect } from 'react-redux';
import NewUserLandingPageComponent from './new-user-landing.component';
import { withGetCompany } from 'erin-app-state-mgmt/src/_shared/api/components/newUserLandingPage/newUserLandingPage.provider';
import { compose } from '../_shared/services/utils';

const mapStateToProps = (state, props) => {
  const { id } = props.match.params;
  //'5cb0bc6b-397c-41ca-bb8e-ad9d2e9c39f5';
  return {
    id: id,
  };
};

const NewUserLandingPageWithApi = compose(withGetCompany)(
  NewUserLandingPageComponent
);

export const NewUserLandingPage = connect(mapStateToProps)(
  NewUserLandingPageWithApi
);
