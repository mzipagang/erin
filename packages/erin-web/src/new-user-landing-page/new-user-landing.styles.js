import { css } from 'emotion';
import LoginBackground from '../_shared/assets/Erin_login_background.jpg';

export const LandingPageContainer = css({
  backgroundImage: `url(${LoginBackground})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  minHeight: 910,
  height: '94vh',
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
});

export const Logo = css({
  width: 350,
});

export const Card = css({
  fontFamily: '"Open Sans", sans-serif',
  boxShadow: '1px 1px 15px rgba(0,0,0,0.25)',
  borderRadius: 4,
  backgroundColor: 'white',
  padding: 30,
  marginTop: 30,
  width: 625,
});
