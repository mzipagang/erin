import React from 'react';

import { Route } from 'react-router-dom';
import { Login } from './login';
import { RouterContext } from './_shared/contexts/router.context';
import { Dashboard } from './dashboard';
import { ReferralsDashboardContainer } from './referrals-admin-dashboard';
import { EmployeeDetails } from './employee-details';
import { EditUserProfile } from './edit-user-profile';
import { ManageJobs } from './manageJobs';
import { BrowseJobs } from './browseJobs';
import { MyReferrals } from './my-referrals';
import { JobDetails } from 'erin-web/src/browseJobs/jobDetail/job-detail.container';
import { AuthenticatedRoute } from './_shared/router/authenticated-route.component';
import { WebNotificationsPage } from './web-notifications';
import { JobDetail } from 'erin-web/src/manageJobs/jobDetail/jobDetail.container';
import { MyContacts } from './my-contacts';
import { Settings } from './settings-dashboard';
import { Network } from './network';
import { EmployeesDashboard } from './employees-dashboard/employees-dashboard.container';
import { NewUserLandingPage } from './new-user-landing-page';
import { SuperUser } from './superUser';
import { MyProfile } from './my-profile';

import { Col, Row } from 'antd';
import { Aside } from './_shared/navigation/aside/aside.container';
import { Header } from './_shared/navigation/header/header.container';
import { appWideStyles, mainContainer, pageContainer } from './app.styles';

export class AppRouter extends React.Component {
  render() {
    return (
      <RouterContext.Consumer>
        {({ auth }) => (
          <div>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={Login} />
            <Route
              path="/newuser/:userInvitedId"
              component={NewUserLandingPage}
            />
            <div className={auth && appWideStyles}>
              {auth && <Aside />}
              <Row className={auth && mainContainer}>
                {auth && (
                  <Col style={{ zIndex: 100 }} span={24}>
                    <Header />
                  </Col>
                )}
                <Col span={24} className={auth && pageContainer}>
                  <AuthenticatedRoute
                    exact
                    path="/browsejobs"
                    component={BrowseJobs}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/browsejobs/:id"
                    component={JobDetails}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/dashboard"
                    component={Dashboard}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/employees"
                    component={EmployeesDashboard}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/employees/form"
                    component={EmployeesDashboard}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/referrals"
                    component={ReferralsDashboardContainer}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/employees/:id"
                    component={EmployeeDetails}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/jobs"
                    component={ManageJobs}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/jobs/:id"
                    component={JobDetail}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/jobs/add"
                    component={ManageJobs}
                  />
                  <AuthenticatedRoute
                    exact
                    path="/editprofile/:id"
                    component={EditUserProfile}
                  />
                  <AuthenticatedRoute
                    path="/myreferrals"
                    component={MyReferrals}
                  />
                  <AuthenticatedRoute
                    path="/notifications"
                    component={WebNotificationsPage}
                  />
                  <AuthenticatedRoute
                    path="/mycontacts"
                    component={MyContacts}
                  />
                  <AuthenticatedRoute path="/superuser" component={SuperUser} />
                  <AuthenticatedRoute path="/settings" component={Settings} />
                  <AuthenticatedRoute path="/myprofile" component={MyProfile} />
                  <AuthenticatedRoute path="/network" component={Network} />
                </Col>
              </Row>
            </div>
          </div>
        )}
      </RouterContext.Consumer>
    );
  }
}

const Home = props => {
  return (
    <RouterContext.Consumer>
      {({ auth }) => {
        if (auth) props.history.push('/dashboard');
        if (!auth) props.history.push('/login');
      }}
    </RouterContext.Consumer>
  );
};
