import { configure } from '@storybook/react';

function loadStories() {
  require('../src/stories/storybook');
  require('../src/stories');
  const req = require.context('../src', true, /stories\.jsx?$/);
  req.keys().forEach(req);
}

configure(loadStories, module);
