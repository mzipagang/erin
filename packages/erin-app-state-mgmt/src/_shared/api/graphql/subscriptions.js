// this is an auto generated file. This will be overwritten
import gql from "graphql-tag";

export const OnCreateDepartment = gql`
  subscription OnCreateDepartment(
    $id: ID
    $companyId: ID
    $name: String
    $active: Boolean
  ) {
    onCreateDepartment(
      id: $id
      companyId: $companyId
      name: $name
      active: $active
    ) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const OnUpdateDepartment = gql`
  subscription OnUpdateDepartment(
    $id: ID
    $companyId: ID
    $name: String
    $active: Boolean
  ) {
    onUpdateDepartment(
      id: $id
      companyId: $companyId
      name: $name
      active: $active
    ) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const OnDeleteDepartment = gql`
  subscription OnDeleteDepartment(
    $id: ID
    $companyId: ID
    $name: String
    $active: Boolean
  ) {
    onDeleteDepartment(
      id: $id
      companyId: $companyId
      name: $name
      active: $active
    ) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const OnCreateCompany = gql`
  subscription OnCreateCompany($id: ID, $name: String) {
    onCreateCompany(id: $id, name: $name) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const OnUpdateCompany = gql`
  subscription OnUpdateCompany($id: ID, $name: String) {
    onUpdateCompany(id: $id, name: $name) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const OnDeleteCompany = gql`
  subscription OnDeleteCompany($id: ID, $name: String) {
    onDeleteCompany(id: $id, name: $name) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const OnCreateUser = gql`
  subscription OnCreateUser(
    $id: ID
    $companyId: ID
    $emailAddress: String
    $password: String
    $firstName: String
  ) {
    onCreateUser(
      id: $id
      companyId: $companyId
      emailAddress: $emailAddress
      password: $password
      firstName: $firstName
    ) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const OnUpdateUser = gql`
  subscription OnUpdateUser(
    $id: ID
    $companyId: ID
    $emailAddress: String
    $password: String
    $firstName: String
  ) {
    onUpdateUser(
      id: $id
      companyId: $companyId
      emailAddress: $emailAddress
      password: $password
      firstName: $firstName
    ) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const OnDeleteUser = gql`
  subscription OnDeleteUser(
    $id: ID
    $companyId: ID
    $emailAddress: String
    $password: String
    $firstName: String
  ) {
    onDeleteUser(
      id: $id
      companyId: $companyId
      emailAddress: $emailAddress
      password: $password
      firstName: $firstName
    ) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const OnCreateJob = gql`
  subscription OnCreateJob(
    $id: ID
    $companyId: ID
    $departmentId: ID
    $title: String
    $description: String
  ) {
    onCreateJob(
      id: $id
      companyId: $companyId
      departmentId: $departmentId
      title: $title
      description: $description
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const OnUpdateJob = gql`
  subscription OnUpdateJob(
    $id: ID
    $companyId: ID
    $departmentId: ID
    $title: String
    $description: String
  ) {
    onUpdateJob(
      id: $id
      companyId: $companyId
      departmentId: $departmentId
      title: $title
      description: $description
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const OnDeleteJob = gql`
  subscription OnDeleteJob(
    $id: ID
    $companyId: ID
    $departmentId: ID
    $title: String
    $description: String
  ) {
    onDeleteJob(
      id: $id
      companyId: $companyId
      departmentId: $departmentId
      title: $title
      description: $description
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const OnCreateContact = gql`
  subscription OnCreateContact(
    $id: ID
    $firstName: String
    $lastName: String
    $emailAddress: AWSEmail
    $socialMediaAccounts: AWSJSON
  ) {
    onCreateContact(
      id: $id
      firstName: $firstName
      lastName: $lastName
      emailAddress: $emailAddress
      socialMediaAccounts: $socialMediaAccounts
    ) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const OnUpdateContact = gql`
  subscription OnUpdateContact(
    $id: ID
    $firstName: String
    $lastName: String
    $emailAddress: AWSEmail
    $socialMediaAccounts: AWSJSON
  ) {
    onUpdateContact(
      id: $id
      firstName: $firstName
      lastName: $lastName
      emailAddress: $emailAddress
      socialMediaAccounts: $socialMediaAccounts
    ) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const OnDeleteContact = gql`
  subscription OnDeleteContact(
    $id: ID
    $firstName: String
    $lastName: String
    $emailAddress: AWSEmail
    $socialMediaAccounts: AWSJSON
  ) {
    onDeleteContact(
      id: $id
      firstName: $firstName
      lastName: $lastName
      emailAddress: $emailAddress
      socialMediaAccounts: $socialMediaAccounts
    ) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const OnCreateReferral = gql`
  subscription OnCreateReferral(
    $id: ID
    $contactId: ID
    $userId: ID
    $jobId: ID
  ) {
    onCreateReferral(
      id: $id
      contactId: $contactId
      userId: $userId
      jobId: $jobId
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const OnUpdateReferral = gql`
  subscription OnUpdateReferral(
    $id: ID
    $contactId: ID
    $userId: ID
    $jobId: ID
  ) {
    onUpdateReferral(
      id: $id
      contactId: $contactId
      userId: $userId
      jobId: $jobId
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const OnDeleteReferral = gql`
  subscription OnDeleteReferral(
    $id: ID
    $contactId: ID
    $userId: ID
    $jobId: ID
  ) {
    onDeleteReferral(
      id: $id
      contactId: $contactId
      userId: $userId
      jobId: $jobId
    ) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const OnCreateUserDepartment = gql`
  subscription OnCreateUserDepartment($userId: ID, $departmentId: ID) {
    onCreateUserDepartment(userId: $userId, departmentId: $departmentId) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
    }
  }
`;
export const OnDeleteUserDepartment = gql`
  subscription OnDeleteUserDepartment($userId: ID, $departmentId: ID) {
    onDeleteUserDepartment(userId: $userId, departmentId: $departmentId) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
    }
  }
`;
export const OnCreateWebNotification = gql`
  subscription OnCreateWebNotification(
    $id: ID
    $userId: ID
    $referralId: ID
    $jobId: ID
    $matches: Int
  ) {
    onCreateWebNotification(
      id: $id
      userId: $userId
      referralId: $referralId
      jobId: $jobId
      matches: $matches
    ) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const OnUpdateWebNotification = gql`
  subscription OnUpdateWebNotification(
    $id: ID
    $userId: ID
    $referralId: ID
    $jobId: ID
    $matches: Int
  ) {
    onUpdateWebNotification(
      id: $id
      userId: $userId
      referralId: $referralId
      jobId: $jobId
      matches: $matches
    ) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const OnDeleteWebNotification = gql`
  subscription OnDeleteWebNotification(
    $id: ID
    $userId: ID
    $referralId: ID
    $jobId: ID
    $matches: Int
  ) {
    onDeleteWebNotification(
      id: $id
      userId: $userId
      referralId: $referralId
      jobId: $jobId
      matches: $matches
    ) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const OnCreateUserInvite = gql`
  subscription OnCreateUserInvite(
    $id: ID
    $userId: ID
    $emailAdddress: String
    $firstName: String
    $lastName: String
  ) {
    onCreateUserInvite(
      id: $id
      userId: $userId
      emailAdddress: $emailAdddress
      firstName: $firstName
      lastName: $lastName
    ) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
export const OnUpdateUserInvite = gql`
  subscription OnUpdateUserInvite(
    $id: ID
    $userId: ID
    $emailAdddress: String
    $firstName: String
    $lastName: String
  ) {
    onUpdateUserInvite(
      id: $id
      userId: $userId
      emailAdddress: $emailAdddress
      firstName: $firstName
      lastName: $lastName
    ) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
export const OnDeleteUserInvite = gql`
  subscription OnDeleteUserInvite(
    $id: ID
    $userId: ID
    $emailAdddress: String
    $firstName: String
    $lastName: String
  ) {
    onDeleteUserInvite(
      id: $id
      userId: $userId
      emailAdddress: $emailAdddress
      firstName: $firstName
      lastName: $lastName
    ) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
