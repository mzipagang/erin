// this is an auto generated file. This will be overwritten
import gql from "graphql-tag";

export const GetDepartment = gql`
  query GetDepartment($id: ID!) {
    getDepartment(id: $id) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const ListDepartments = gql`
  query ListDepartments(
    $filter: TableDepartmentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDepartments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        companyId
        name
        active
        totalUsers
      }
      nextToken
    }
  }
`;
export const QueryDepartmentsByCompanyIdIndex = gql`
  query QueryDepartmentsByCompanyIdIndex(
    $companyId: ID!
    $first: Int
    $after: String
  ) {
    queryDepartmentsByCompanyIdIndex(
      companyId: $companyId
      first: $first
      after: $after
    ) {
      items {
        id
        companyId
        name
        active
        totalUsers
      }
      nextToken
    }
  }
`;
export const GetCompany = gql`
  query GetCompany($id: ID!) {
    getCompany(id: $id) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const ListCompanies = gql`
  query ListCompanies(
    $filter: TableCompanyFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCompanies(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      nextToken
    }
  }
`;
export const GetUser = gql`
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const GetUserByCognitoId = gql`
  query GetUserByCognitoId($cognitoId: ID!) {
    getUserByCognitoId(cognitoId: $cognitoId) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const ListUsers = gql`
  query ListUsers(
    $filter: TableUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      nextToken
    }
  }
`;
export const QueryUsersByEmailAddressPasswordIndex = gql`
  query QueryUsersByEmailAddressPasswordIndex(
    $emailAddress: String!
    $first: Int
    $after: String
  ) {
    queryUsersByEmailAddressPasswordIndex(
      emailAddress: $emailAddress
      first: $first
      after: $after
    ) {
      items {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      nextToken
    }
  }
`;
export const GetJob = gql`
  query GetJob($id: ID!) {
    getJob(id: $id) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const ListJobs = gql`
  query ListJobs(
    $filter: TableJobFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listJobs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      nextToken
    }
  }
`;
export const QueryJobsByCompanyIdTitleIndex = gql`
  query QueryJobsByCompanyIdTitleIndex(
    $companyId: ID!
    $title: String!
    $first: Int
    $after: String
  ) {
    queryJobsByCompanyIdTitleIndex(
      companyId: $companyId
      title: $title
      first: $first
      after: $after
    ) {
      items {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      nextToken
    }
  }
`;
export const QueryJobsByDescriptionIndex = gql`
  query QueryJobsByDescriptionIndex(
    $description: String!
    $first: Int
    $after: String
  ) {
    queryJobsByDescriptionIndex(
      description: $description
      first: $first
      after: $after
    ) {
      items {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      nextToken
    }
  }
`;
export const GetContact = gql`
  query GetContact($id: ID!) {
    getContact(id: $id) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const ListContacts = gql`
  query ListContacts(
    $filter: TableContactFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listContacts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      nextToken
    }
  }
`;
export const QueryContactsByEmailAddressIndex = gql`
  query QueryContactsByEmailAddressIndex(
    $emailAddress: AWSEmail!
    $first: Int
    $after: String
  ) {
    queryContactsByEmailAddressIndex(
      emailAddress: $emailAddress
      first: $first
      after: $after
    ) {
      items {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      nextToken
    }
  }
`;
export const GetReferral = gql`
  query GetReferral($id: ID!) {
    getReferral(id: $id) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const ListReferrals = gql`
  query ListReferrals(
    $filter: TableReferralFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listReferrals(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      nextToken
    }
  }
`;
export const QueryReferralsByJobIdIndex = gql`
  query QueryReferralsByJobIdIndex($jobId: ID!, $first: Int, $after: String) {
    queryReferralsByJobIdIndex(jobId: $jobId, first: $first, after: $after) {
      items {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      nextToken
    }
  }
`;
export const GetTopReferrers = gql`
  query GetTopReferrers($companyId: ID!) {
    getTopReferrers(companyId: $companyId) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const GetBusinessNetwork = gql`
  query GetBusinessNetwork($companyId: ID!) {
    getBusinessNetwork(companyId: $companyId) {
      activeEmployees
      businessConnections
      firstConnections
    }
  }
`;
export const GetDashboardJobs = gql`
  query GetDashboardJobs($companyId: ID!) {
    getDashboardJobs(companyId: $companyId) {
      openPositions
      inNetworkMatches
      jobShares
      totalJobViews
    }
  }
`;
export const GetDashboardReferrals = gql`
  query GetDashboardReferrals($companyId: ID!, $status: ReferralStatus) {
    getDashboardReferrals(companyId: $companyId, status: $status) {
      currentReferralCount
      previousReferralCount
    }
  }
`;
export const GetUserDepartment = gql`
  query GetUserDepartment($userId: ID!, $departmentId: ID!) {
    getUserDepartment(userId: $userId, departmentId: $departmentId) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
    }
  }
`;
export const ListUserDepartments = gql`
  query ListUserDepartments(
    $filter: TableUserDepartmentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserDepartments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        departmentId
      }
      nextToken
    }
  }
`;
export const GetWebNotification = gql`
  query GetWebNotification($id: ID!) {
    getWebNotification(id: $id) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const ListWebNotifications = gql`
  query ListWebNotifications(
    $filter: TableWebNotificationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listWebNotifications(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      nextToken
    }
  }
`;
export const GetUserInvite = gql`
  query GetUserInvite($id: ID!) {
    getUserInvite(id: $id) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
export const ListUserInvites = gql`
  query ListUserInvites(
    $filter: TableUserInviteFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserInvites(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        emailAddress
        firstName
        lastName
        departmentId
        title
        dateCreated
      }
      nextToken
    }
  }
`;
