import gql from 'graphql-tag';

export const ListReferralsDashboard = gql`
  query ListReferrals(
    $filter: TableReferralFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listReferrals(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        companyId
        contact {
          id
          emailAddress
          lastName
          firstName
          socialMediaAccounts
        }
        user {
          id
          firstName
          lastName
        }
        job {
          id
          title
          department {
            id
            name
          }
        }
        note
        message
        referralDate
        status
      }
      nextToken
    }
  }
`;
