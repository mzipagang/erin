import gql from 'graphql-tag';

export const GetJob = gql`
  query GetJob($id: ID!) {
    getJob(id: $id) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        active
        avatar {
          bucket
          region
          key
        }
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        contact {
          id
          firstName
          lastName
          emailAddress
          socialMediaAccounts
          users
        }
        companyId
        company {
          id
          name
          defaultBonusAmount
          contactIncentiveBonus
          websiteUrl
        }
        contactId
        userId
        user {
          id
          firstName
          lastName
        }
        jobId
        job {
          id
          companyId
          departmentId
          title
          description
          publicLink
          salary
          location
          userId
          referralBonus
          shares
          views
        }
        status
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        users
      }
    }
  }
`;
