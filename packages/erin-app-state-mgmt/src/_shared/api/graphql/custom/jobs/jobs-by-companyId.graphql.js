import gql from 'graphql-tag';

export const ListJobs = gql`
  query ListJobs(
    $filter: TableJobFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listJobs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        companyId
        company {
          id
          name
          departments {
            id
            companyId
            name
            active
            totalUsers
          }
          defaultBonusAmount
          contactIncentiveBonus
          websiteUrl
        }
        departmentId
        department {
          id
          companyId
          name
          active
          totalUsers
          userDepartments {
            id
            userId
            departmentId
          }
        }
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        status
        dateCreated
      }
      nextToken
    }
  }
`;
