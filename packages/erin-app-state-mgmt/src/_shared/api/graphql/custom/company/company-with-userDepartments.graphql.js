import gql from 'graphql-tag';

export const GetCompanyWithUserDepartments = gql`
  query GetCompany($id: ID!) {
    getCompany(id: $id) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
        userDepartments {
          id
          userId
          departmentId
        }
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
