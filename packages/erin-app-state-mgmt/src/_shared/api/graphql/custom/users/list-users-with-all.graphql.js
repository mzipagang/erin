import gql from 'graphql-tag';

export const ListUsers = gql`
  query ListUsers(
    $filter: TableUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        avatar {
          bucket
          region
          key
        }
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        department {
          id
          companyId
          name
          active
          totalUsers
        }
        lastLogin
        lastNotificationCheck
        totalReferrals
        active
      }
      nextToken
    }
  }
`;
