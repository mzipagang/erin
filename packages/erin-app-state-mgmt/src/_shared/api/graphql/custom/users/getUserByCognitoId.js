import gql from 'graphql-tag';

export const GetUserByCognitoId = gql`
  query GetUserByCognitoId($cognitoId: ID!) {
    getUserByCognitoId(cognitoId: $cognitoId) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        name
      }
      lastLogin
      lastNotificationCheck
      totalReferrals
      active
      managedDepartments {
        departmentId
        department {
          id
          name
        }
      }
    }
  }
`;
