import gql from 'graphql-tag';

export const UpdateUser = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      totalReferrals
      active
    }
  }
`;
