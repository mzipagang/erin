// this is an auto generated file. This will be overwritten
import gql from "graphql-tag";

export const CreateDepartment = gql`
  mutation CreateDepartment($input: CreateDepartmentInput!) {
    createDepartment(input: $input) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const UpdateDepartment = gql`
  mutation UpdateDepartment($input: UpdateDepartmentInput!) {
    updateDepartment(input: $input) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const DeleteDepartment = gql`
  mutation DeleteDepartment($input: DeleteDepartmentInput!) {
    deleteDepartment(input: $input) {
      id
      companyId
      name
      active
      totalUsers
      userDepartments {
        id
        userId
        departmentId
      }
    }
  }
`;
export const CreateCompany = gql`
  mutation CreateCompany($input: CreateCompanyInput!) {
    createCompany(input: $input) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const UpdateCompany = gql`
  mutation UpdateCompany($input: UpdateCompanyInput!) {
    updateCompany(input: $input) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const DeleteCompany = gql`
  mutation DeleteCompany($input: DeleteCompanyInput!) {
    deleteCompany(input: $input) {
      id
      name
      departments {
        id
        companyId
        name
        active
        totalUsers
      }
      defaultBonusAmount
      contactIncentiveBonus
      websiteUrl
    }
  }
`;
export const CreateUser = gql`
  mutation CreateUser($input: CreateUserInput!) {
    createUser(input: $input) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const UpdateUser = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const DeleteUser = gql`
  mutation DeleteUser($input: DeleteUserInput!) {
    deleteUser(input: $input) {
      id
      cognitoId
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      emailAddress
      role
      firstName
      lastName
      title
      avatar {
        bucket
        region
        key
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      managedDepartments {
        id
        userId
        departmentId
      }
      lastLogin
      lastNotificationCheck
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      contacts {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      totalReferrals
      webNotifications {
        id
        userId
        referralId
        jobId
        matches
        dateCreated
      }
      connectedApps
      active
      createdById
      createdBy {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
    }
  }
`;
export const CreateJob = gql`
  mutation CreateJob($input: CreateJobInput!) {
    createJob(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const UpdateJob = gql`
  mutation UpdateJob($input: UpdateJobInput!) {
    updateJob(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const DeleteJob = gql`
  mutation DeleteJob($input: DeleteJobInput!) {
    deleteJob(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      jobType
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      description
      publicLink
      salary
      location
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralBonus
      notificationType
      status
      shares
      views
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      matches {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      dateCreated
    }
  }
`;
export const CreateContact = gql`
  mutation CreateContact($input: CreateContactInput!) {
    createContact(input: $input) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const UpdateContact = gql`
  mutation UpdateContact($input: UpdateContactInput!) {
    updateContact(input: $input) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const DeleteContact = gql`
  mutation DeleteContact($input: DeleteContactInput!) {
    deleteContact(input: $input) {
      id
      firstName
      lastName
      emailAddress
      socialMediaAccounts
      referrals {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      importMethod
      fullContactData
      dateCreated
    }
  }
`;
export const CreateReferral = gql`
  mutation CreateReferral($input: CreateReferralInput!) {
    createReferral(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const UpdateReferral = gql`
  mutation UpdateReferral($input: UpdateReferralInput!) {
    updateReferral(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const DeleteReferral = gql`
  mutation DeleteReferral($input: DeleteReferralInput!) {
    deleteReferral(input: $input) {
      id
      companyId
      company {
        id
        name
        defaultBonusAmount
        contactIncentiveBonus
        websiteUrl
      }
      contactId
      contact {
        id
        firstName
        lastName
        emailAddress
        socialMediaAccounts
        userId
        fullContactData
        dateCreated
      }
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      status
      note
      message
      referralDate
    }
  }
`;
export const CreateUserDepartment = gql`
  mutation CreateUserDepartment($input: CreateUserDepartmentInput!) {
    createUserDepartment(input: $input) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
    }
  }
`;
export const DeleteUserDepartment = gql`
  mutation DeleteUserDepartment($input: DeleteUserDepartmentInput!) {
    deleteUserDepartment(input: $input) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
    }
  }
`;
export const CreateWebNotification = gql`
  mutation CreateWebNotification($input: CreateWebNotificationInput!) {
    createWebNotification(input: $input) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const UpdateWebNotification = gql`
  mutation UpdateWebNotification($input: UpdateWebNotificationInput!) {
    updateWebNotification(input: $input) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const DeleteWebNotification = gql`
  mutation DeleteWebNotification($input: DeleteWebNotificationInput!) {
    deleteWebNotification(input: $input) {
      id
      type
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      referralId
      referral {
        id
        companyId
        contactId
        userId
        jobId
        note
        message
        referralDate
      }
      jobId
      job {
        id
        companyId
        departmentId
        title
        description
        publicLink
        salary
        location
        userId
        referralBonus
        shares
        views
        dateCreated
      }
      matches
      dateCreated
    }
  }
`;
export const CreateUserInvite = gql`
  mutation CreateUserInvite($input: CreateUserInviteInput!) {
    createUserInvite(input: $input) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
export const UpdateUserInvite = gql`
  mutation UpdateUserInvite($input: UpdateUserInviteInput!) {
    updateUserInvite(input: $input) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
export const DeleteUserInvite = gql`
  mutation DeleteUserInvite($input: DeleteUserInviteInput!) {
    deleteUserInvite(input: $input) {
      id
      userId
      user {
        id
        cognitoId
        companyId
        emailAddress
        firstName
        lastName
        title
        departmentId
        lastLogin
        lastNotificationCheck
        totalReferrals
        connectedApps
        active
        createdById
      }
      emailAddress
      firstName
      lastName
      departmentId
      department {
        id
        companyId
        name
        active
        totalUsers
      }
      title
      dateCreated
    }
  }
`;
