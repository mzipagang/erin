import { graphql } from 'react-apollo';
import { GetCompanyWithUserDepartments } from '../../graphql/custom/company/company-with-userDepartments.graphql';
import { compose } from 'react-apollo';
import uuid from 'uuid/v4';
import {
  CreateDepartment,
  DeleteDepartment,
  UpdateCompany,
} from '../../graphql/mutations';

export const withGetCompany = (Component, variables) => {
  return compose(
    graphql(GetCompanyWithUserDepartments, {
      options: {
        variables,
        fetchPolicy: 'cache-and-network',
      },
      props: props => ({
        company: props.data.getCompany ? props.data.getCompany : undefined,
      }),
    }),
    graphql(UpdateCompany, {
      props: props => ({
        onUpdate: input => {
          const optimisticResponseData = {
            ...props.ownProps.company,
            ...input.input,
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typeName: 'Mutation',
              updateCompany: {
                ...optimisticResponseData,
                __typeName: 'updateCompany',
              },
            },
            update: proxy => {
              const data = proxy.readQuery({
                query: GetCompanyWithUserDepartments,
                variables: { id: props.ownProps.company.id },
              });
              proxy.writeQuery({
                query: GetCompanyWithUserDepartments,
                data,
                variables: { id: props.ownProps.company.id },
              });
            },
          });
        },
      }),
    }),
    graphql(CreateDepartment, {
      props: props => ({
        onCreateDepartment: input => {
          const optimisticResponseData = {
            ...input.input,
            id: uuid(),
            totalUsers: 0,
            userDepartments: [],
            __typename: 'Department',
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typeName: 'Mutation',
              createDepartment: {
                __typeName: 'createDepartment',
                ...optimisticResponseData,
              },
            },
            update: (proxy, { data: { createDepartment } }) => {
              const data = proxy.readQuery({
                query: GetCompanyWithUserDepartments,
                variables: { id: props.ownProps.userCompany.id },
              });
              if (
                !data.getCompany.departments.find(
                  dept => dept.id === createDepartment.id
                )
              ) {
                data.getCompany.departments.push(createDepartment);
              }
              proxy.writeQuery({
                query: GetCompanyWithUserDepartments,
                data,
                variables: { id: props.ownProps.userCompany.id },
              });
            },
          });
        },
      }),
    }),
    graphql(DeleteDepartment, {
      props: props => ({
        onDeleteDepartment: input => {
          const deletedDepartment = props.ownProps.userCompany.departments.find(
            dept => dept.id === input.input.id
          );
          const optimisticResponseData = {
            ...input,
            ...deletedDepartment,
            company: props.ownProps.company,
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typeName: 'Mutation',
              deleteDepartment: {
                ...optimisticResponseData,
                __typeName: 'deleteDepartment',
              },
            },
            update: (proxy, { data: { deleteDepartment } }) => {
              const data = proxy.readQuery({
                query: GetCompanyWithUserDepartments,
                variables: { id: props.ownProps.company.id },
              });
              data.getCompany.departments = data.getCompany.departments.filter(
                dept => dept.id !== deleteDepartment.id
              );
              proxy.writeQuery({
                query: GetCompanyWithUserDepartments,
                data,
                variables: { id: props.ownProps.company.id },
              });
            },
          });
        },
      }),
    })
  )(Component);
};
