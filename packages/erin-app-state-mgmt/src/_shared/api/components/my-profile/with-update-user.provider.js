import { graphql } from 'react-apollo';
import { UpdateUser } from '../../graphql/mutations';
import { GetUser } from '../../graphql/queries';

export const withUpdateUser = Component => {
  return graphql(UpdateUser, {
    props: props => ({
      onUpdate: input => {
        const optimisticResponseData = {
          ...props.currentUser,
          ...input.input,
        };
        props.mutate({
          variables: input,
          optimisticResponse: {
            __typename: 'Mutation',
            updateUser: {
              ...optimisticResponseData,
              __typeName: 'updateUser',
            },
          },
          update: proxy => {
            const data = proxy.readQuery({
              query: GetUser,
              variables: { id: props.currentUser.id },
            });
            proxy.writeQuery({
              query: GetUser,
              data,
              variables: { id: props.currentUser.id },
            });
          },
        });
      },
    }),
  })(Component);
};
