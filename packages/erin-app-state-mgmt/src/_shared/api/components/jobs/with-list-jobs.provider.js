import { graphql, compose } from 'react-apollo';
import { CreateJob } from '../../graphql/mutations';
import { ListDepartments } from '../../graphql/queries';
import { ListJobs } from '../../graphql/custom/jobs/jobs-by-companyId.graphql.js';
import { get } from 'lodash';
import { parseJsonFields } from '../../../services/parse-api.service';

export const withListJobs = (Component, variables) => {
  return compose(
    graphql(ListJobs, {
      options: { variables, fetchPolicy: 'cache-and-network' },
      props: props => {
        let jobs = get(props, 'data.listJobs.items', []);
        for (let job of jobs) {
          job = Object.assign(
            job,
            parseJsonFields(['location', 'salary'], job)
          );
        }
        return {
          jobs,
        };
      },
    }),
    graphql(ListDepartments, {
      options: { variables, fetchPolicy: 'cache-and-network' },
      props: props => ({
        departments: props.data.listDepartments
          ? props.data.listDepartments.items
          : [],
      }),
    }),
    graphql(CreateJob, {
      options: {
        fetchPolicy: 'cache-and-network',
      },
      props: props => ({
        onAddJob: input => {
          const optimisticResponseData = {
            ...input,
          };
          console.log(input)
          props.mutate({
            variables: { input },
            optimisticResponse: {
              __typename: 'Mutation',
              createJob: {
                ...optimisticResponseData,
                __typename: 'createJob',
              },
            },
            update: (proxy, { data: { createJob } }) => {
              const data = proxy.readQuery({
                query: ListJobs,
                variables: {
                  filter: {
                    companyId: { eq: props.ownProps.currentUser.companyId },
                  },
                  nextToken: null,
                  limit: null,
                },
              });
              if (!data.listJobs.items.find(job => job.id === createJob.id)) {
                data.listJobs.items.push(createJob);
              }
              proxy.writeQuery({
                query: ListJobs,
                data,
                variables: {
                  filter: {
                    companyId: { eq: props.ownProps.currentUser.companyId },
                  },
                  nextToken: null,
                  limit: null,
                },
              });
            },
          });
        },
      }),
    })
  )(Component);
};
