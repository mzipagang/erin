import { graphql, compose } from 'react-apollo';
import { UpdateJob, UpdateReferral } from '../../graphql/mutations';
import { GetJob } from '../../graphql/custom/jobs/job-by-id.graphql.js';
import { ListDepartments } from '../../graphql/queries';
import { get } from 'lodash';
import { parseJsonFields } from '../../../services/parse-api.service';

export const withJobById = (Component, variables) => {
  return compose(
    graphql(GetJob, {
      options: { variables, fetchPolicy: 'cache-and-network' },
      props: props => {
        let job = get(props, 'data.getJob', undefined);
        if (job) {
          job = Object.assign(
            job,
            parseJsonFields(['location', 'salary'], job)
          );
        }
        return {
          currentJob: job,
        };
      },
    }),
    graphql(ListDepartments, {
      options: { variables, fetchPolicy: 'cache-and-network' },
      props: props => ({
        departments: props.data.listDepartments
          ? props.data.listDepartments.items
          : [],
      }),
    }),

    graphql(UpdateJob, {
      options: {
        fetchPolicy: 'cache-and-network',
      },
      props: props => ({
        onUpdateJob: input => {
          const optimisticResponseData = {
            ...input,
          };
          props.mutate({
            variables: { input },
            optimisticResponse: {
              __typename: 'Mutation',
              updateJob: {
                ...optimisticResponseData,
                __typename: 'updateJob',
              },
            },
          });
        },
      }),
    }),
    graphql(UpdateReferral, {
      props: props => ({
        onUpdateReferral: input => {
          const optimisticResponseData = {
            ...input.input,
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typename: 'Mutation',
              updateReferral: {
                ...optimisticResponseData,
                __typename: 'updateReferral',
              },
            },
          });
        },
      }),
    })
  )(Component);
};
