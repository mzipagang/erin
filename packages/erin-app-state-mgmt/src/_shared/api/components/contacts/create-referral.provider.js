import { graphql, compose } from 'react-apollo';
import { ListContacts } from '../../graphql/queries';
import { CreateContact, CreateReferral } from '../../graphql/mutations';
import uuid from 'uuid/v4';
import moment from 'moment';

export const withCreateReferral = (Component, variables) => {
  return compose(
    graphql(ListContacts, {
      options: {
        variables,
        fetchPolicy: 'cache-and-network',
      },
      props: props => ({
        contacts: props.data.listContacts ? props.data.listContacts.items : [],
      }),
    }),
    graphql(CreateContact, {
      props: props => ({
        onCreateContact: input => {
          const optimisticResponseData = {
            id: uuid(),
            ...input.input,
            referrals: null,
            __typename: 'Contact',
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typename: 'Mutation',
              createContact: {
                __typename: 'createContact',
                ...optimisticResponseData,
              },
            },
            update: (proxy, { data: { createContact } }) => {
              const data = proxy.readQuery({
                query: ListContacts,
                variables: {
                  filter: {
                    users: { contains: props.ownProps.currentUser.id },
                  },
                  limit: null,
                  nextToken: null,
                },
              });

              if (
                !data.listContacts.items.find(
                  contact => contact.id === createContact.id
                )
              ) {
                data.listContacts.items.push(createContact);
              }
              proxy.writeQuery({
                query: ListContacts,
                variables: {
                  filter: {
                    users: { contains: props.ownProps.currentUser.id },
                  },
                  limit: null,
                  nextToken: null,
                },
                data,
              });
            },
          });
        },
      }),
    }),
    graphql(CreateReferral, {
      props: props => ({
        onCreateReferral: input => {
          const optimisticResponseData = {
            ...input.input,
            id: uuid(),
            referralDate: moment(),
          };
          props.mutate({
            variables: input,
            optimisticResponse: {
              __typeName: 'Mutation',
              createDepartment: {
                __typeName: 'createReferral',
                ...optimisticResponseData,
              },
            },
          });
        },
      }),
    })
  )(Component);
};
