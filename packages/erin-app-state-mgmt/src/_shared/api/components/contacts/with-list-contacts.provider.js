import { graphql } from 'react-apollo';
import { ListContacts } from '../../graphql/queries';

export const withListContacts = (Component, variables) => {
  return graphql(ListContacts, {
    options: {
      variables,
      fetchPolicy: 'cache-and-network',
    },
    props: props => ({
      options: {
        variables,
        fetchPolicy: 'cache-and-network',
      },
      contacts: props.data.listContacts ? props.data.listContacts.items : [],
    }),
  })(Component);
};
