import { graphql } from 'react-apollo';
import { ListUsers } from '../../graphql/custom/users/list-users-with-all.graphql';

export const withListUsers = (Component, variables) => {
  return graphql(ListUsers, {
    options: { variables, fetchPolicy: 'cache-and-network' },
    props: props => ({
      users: props.data.listUsers ? props.data.listUsers.items : [],
    }),
  })(Component);
};
