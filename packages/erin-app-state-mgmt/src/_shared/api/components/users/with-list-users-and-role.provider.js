import { graphql } from 'react-apollo';
import { ListUsersWithRole } from '../../graphql/custom/users/list-users-with-role.graphql';

export const withListUsersAndRole = Component => {
  return graphql(ListUsersWithRole, {
    props: props => ({
      users: props.data.listUsers ? props.data.listUsers.items : [],
    }),
  })(Component);
};
