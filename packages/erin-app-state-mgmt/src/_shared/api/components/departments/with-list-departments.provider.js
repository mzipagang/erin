import { graphql } from 'react-apollo';
import { ListDepartments } from '../../graphql/queries';

export const withListDepartment = (Component, variables) => {
  return graphql(ListDepartments, {
    options: { variables, fetchPolicy: 'cache-and-network' },
    props: props => ({
      departments: props.data.listDepartments
        ? props.data.listDepartments.items
        : [],
    }),
  })(Component);
};
