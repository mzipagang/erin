import { graphql, compose } from 'react-apollo';
import { ListReferralsDashboard } from '../../graphql/custom/referrals/referralsWithContactAndJob.graphql';
import { ListDepartments } from '../../graphql/queries';
export const withListReferrals = (Component, variables) => {
  return compose(
    graphql(ListReferralsDashboard, {
      options: { variables, fetchPolicy: 'cache-and-network' },
      props: props => ({
        referrals: props.data.listReferrals
          ? props.data.listReferrals.items
          : undefined,
      }),
    }),
    graphql(ListDepartments, {
      props: props => ({
        departments: props.data.listDepartments
          ? props.data.listDepartments.items
          : [],
      }),
    })
  )(Component);
};
