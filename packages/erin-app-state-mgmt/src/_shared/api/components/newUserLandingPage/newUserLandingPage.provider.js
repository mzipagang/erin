import { graphql, compose } from 'react-apollo';
import { GetCompany } from '../../graphql/queries';
import { CreateUser } from '../../graphql/mutations';

export const withGetCompany = (Component, variables) => {
  return compose(
    graphql(GetCompany, {
      options: { variables },
      props: props => ({
        company: props.data.getCompany ? props.data.getCompany : [],
      }),
    }),
    graphql(CreateUser, {
      props: props => ({
        onCreate: input => {
          props.mutate({
            variables: input,
          });
        },
      }),
    })
  )(Component);
};
