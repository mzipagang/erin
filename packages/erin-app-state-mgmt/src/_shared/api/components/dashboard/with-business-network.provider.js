import { graphql } from 'react-apollo';
import { GetBusinessNetwork } from '../../graphql/queries';

export const withBusinessNetwork = (Component) => {
  return graphql(GetBusinessNetwork, {
  	options: props => ({ variables: { companyId: props.currentUser.companyId } }),
    props: props => ({
      activeEmployees: props.data.getBusinessNetwork ? props.data.getBusinessNetwork.activeEmployees : 0,
      businessConnections: props.data.getBusinessNetwork ? props.data.getBusinessNetwork.businessConnections : 0,
      firstConnections: props.data.getBusinessNetwork ? props.data.getBusinessNetwork.firstConnections : 0,
    }),
	})(Component);
};
