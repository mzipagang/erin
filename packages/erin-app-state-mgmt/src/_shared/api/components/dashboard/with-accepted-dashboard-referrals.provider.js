import { graphql } from 'react-apollo';
import { GetDashboardReferrals } from '../../graphql/queries';

export const withAcceptedDashboardReferrals = (Component) => {
  return graphql(GetDashboardReferrals, {
  	options: props => ({ variables: { companyId: props.currentUser.companyId, status: 'accepted' } }),
    props: props => ({
      currentAcceptedReferralCount: props.data.getDashboardReferrals
        ? props.data.getDashboardReferrals.currentReferralCount
        : [],
      previousAcceptedReferralCount: props.data.getDashboardReferrals
        ? props.data.getDashboardReferrals.previousReferralCount
        : [],
    }),
  })(Component);
};
