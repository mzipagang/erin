import { graphql } from 'react-apollo';
import { GetTopReferrers } from '../../graphql/queries';

export const withTopReferrers = (Component) => {
  return graphql(GetTopReferrers, {
  	options: props => ({ variables: { companyId: "5cb0bc6b-397c-41ca-bb8e-ad9d2e9c39f5" } }),
    props: props => ({
      topReferrers: props.data.getTopReferrers || [],
    }),
	})(Component);
};
