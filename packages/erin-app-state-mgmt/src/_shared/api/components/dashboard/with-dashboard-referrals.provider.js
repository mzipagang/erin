import { graphql } from 'react-apollo';
import { GetDashboardReferrals } from '../../graphql/queries';

export const withDashboardReferrals = (Component) => {
  return graphql(GetDashboardReferrals, {
  	options: props => ({ variables: { companyId: props.currentUser.companyId } }),
    props: props => ({
      currentReferralCount: props.data.getDashboardReferrals
        ? props.data.getDashboardReferrals.currentReferralCount
        : [],
      previousReferralCount: props.data.getDashboardReferrals
        ? props.data.getDashboardReferrals.previousReferralCount
        : [],
    }),
  })(Component);
};
