import { graphql } from 'react-apollo';
import { GetDashboardJobs } from '../../graphql/queries';

export const withDashboardJobs = Component => {
  return graphql(GetDashboardJobs, {
    options: props => ({
      variables: { companyId: props.currentUser.companyId },
    }),
    props: props => ({
      openPositions: props.data.getDashboardJobs
        ? props.data.getDashboardJobs.openPositions
        : [],
      inNetworkMatches: props.data.getDashboardJobs
        ? props.data.getDashboardJobs.inNetworkMatches
        : [],
      jobShares: props.data.getDashboardJobs
        ? props.data.getDashboardJobs.jobShares
        : [],
      totalJobViews: props.data.getDashboardJobs
        ? props.data.getDashboardJobs.totalJobViews
        : [],
    }),
  })(Component);
};
