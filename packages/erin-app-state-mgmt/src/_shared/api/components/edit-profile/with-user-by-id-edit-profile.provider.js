import { graphql, compose } from 'react-apollo';
import { ListDepartments } from '../../graphql/queries';
import { UpdateUser } from '../../graphql/mutations';
import { GetEmployeeById } from '../../graphql/custom/users/employee-by-id.graphql';

export const withUserByIdEditProfile = (Component, variables) => {
  return compose(
    graphql(GetEmployeeById, {
      options: {
        variables,
        fetchPolicy: 'cache-and-network',
      },
      props: props => ({
        user: props.data.getUser ? props.data.getUser : undefined,
      }),
    }),
    graphql(ListDepartments, {
      options: { variables },
      props: props => ({
        departments: props.data.listDepartments
          ? props.data.listDepartments.items
          : [],
      }),
    }),
    graphql(UpdateUser, {
      props: props => ({
        onUpdate: input =>
          props.mutate({
            variables: input,
          }),
      }),
    })
  )(Component);
};
