import { state as initialState } from './user.state';
import {
  SET_CURRENT_USER,
  ADD_SUPER_USER,
  ADD_COMPANY,
  SIGN_OUT,
  UPDATE_USER_COMPANY,
} from './user.actions';

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return handleSetCurrentUserAction(state, action);
    case ADD_SUPER_USER:
      return handleAddSuperUserAction(state, action);
    case ADD_COMPANY:
      return handleAddCompanyAction(state, action);
    case UPDATE_USER_COMPANY:
      return handleUpdateUserCompany(state, action);
    case SIGN_OUT:
      return handleSignOut(state, action);
    default:
      return state;
  }
};

const handleSetCurrentUserAction = (state, action) => {
  const { currentUser } = action.payload;
  return {
    ...state,
    currentUser: currentUser,
  };
};

const handleAddSuperUserAction = (state, { payload }) => {
  let newUser = {};
  Object.keys(payload).forEach(key => {
    newUser[key] = payload[key];
  });

  return {
    ...state,
    superUser: { ...state.superUser, ...newUser },
  };
};

const handleAddCompanyAction = (state, { payload }) => {
  let newCompany = {};
  Object.keys(payload).forEach(key => {
    newCompany[key] = payload[key];
  });

  return {
    ...state,
    company: { ...state.company, ...newCompany },
  };
};

const handleSignOut = state => {
  return {
    ...state,
    currentUser: {},
  };
};

const handleUpdateUserCompany = (state, action) => {
  const { company } = action.payload;
  const { currentUser } = state;
  const updatedUser = { ...currentUser, company: company };
  return {
    ...state,
    currentUser: updatedUser,
  };
};
