export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const ADD_SUPER_USER = 'ADD_SUPER_USER';
export const ADD_COMPANY = 'ADD_COMPANY';
export const SIGN_OUT = 'SIGN_OUT';
export const UPDATE_USER_COMPANY = 'UPDATE_USER_COMPANY';

export const createSetCurrentUserAction = currentUser => ({
  type: SET_CURRENT_USER,
  payload: {
    currentUser,
  },
});

export const addSuperUserAction = superUser => ({
  type: ADD_SUPER_USER,
  payload: {
    superUser,
  },
});

export const addCompanyAction = company => ({
  type: ADD_COMPANY,
  payload: {
    company,
  },
});

export const updateUserCompany = company => ({
  type: UPDATE_USER_COMPANY,
  payload: {
    company,
  },
});

export const signOut = token => {
  return {
    type: SIGN_OUT,
    payload: {
      token,
    },
  };
};
