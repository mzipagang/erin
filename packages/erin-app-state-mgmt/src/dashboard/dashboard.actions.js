import axios from 'axios';

export const SET_ACTIVE_JOBS = 'SET_ACTIVE_JOBS';
export const GET_BUSINESS_NETWORK_DATA = 'GET_BUSINESS_NETWORK_DATA';
export const GET_REFERRALS = 'GET_REFERRALS';
export const GET_DEPARTMENTS = 'GET_DEPARTMENTS';
export const GET_EMPLOYEES = 'GET_EMPLOYEES';
export const INVITE_EMPLOYEES = 'INVITE_EMPLOYEES';
export const INVITE_EMPLOYEES_STARTED = 'INVITE_EMPLOYEES_STARTED';
export const INVITE_EMPLOYEES_SUCCEEDED = 'INVITE_EMPLOYEES_SUCCEEDED';
export const INVITE_EMPLOYEES_FAILED = 'INVITE_EMPLOYEES_FAILED';

export const createSetActiveJobsAction = activeJobs => ({
  type: SET_ACTIVE_JOBS,
  payload: {
    activeJobs,
  },
});

export const createGetBusinessNetworkAction = businessNetwork => ({
  type: GET_BUSINESS_NETWORK_DATA,
  payload: {
    businessNetwork,
  },
});

export const createGetDepartmentsAction = departments => ({
  type: GET_DEPARTMENTS,
  payload: {
    departments,
  },
});

export const createGetReferralsAction = referrals => ({
  type: GET_REFERRALS,
  payload: {
    referrals,
  },
});

export const createGetEmployeesAction = employees => ({
  type: GET_EMPLOYEES,
  payload: {
    employees,
  },
});

export const createInviteEmployeesAction = (users, fileData, createdById) => {
  return dispatch => {
    dispatch(createInviteEmployeesStartedAction());

    const promises = [];
    if (users && users.length) {
      promises.push(
        axios({
          method: 'post',
          url: 'https://cr0ejvm2g5.execute-api.us-east-2.amazonaws.com/default/invite',
          data: {
            createdById,
            users: users,
          },
          headers: {
            'x-api-key': '3Xz3EjFP1eanepjOD5cOY1ZLyWq5NOT125zLgdPU'
          }
        }).then(
          () => dispatch(createInviteEmployeesSucceededAction()),
          error => dispatch(createInviteEmployeesFailedAction(error))
        )
      )
    }
    if (fileData) {
      promises.push(
        axios({
          method: 'post',
          url: 'https://cr0ejvm2g5.execute-api.us-east-2.amazonaws.com/default/batchInvite',
          data: fileData,
          headers: {
            'x-api-key': '3Xz3EjFP1eanepjOD5cOY1ZLyWq5NOT125zLgdPU'
          }
        }).then(
          () => dispatch(createInviteEmployeesSucceededAction()),
          error => dispatch(createInviteEmployeesFailedAction(error))
        )
      )
    }
    if ((users && users.length > 0) || fileData) {
      axios.all(promises).then(() => {console.log("invited")});
    } else {
      dispatch(createInviteEmployeesFailedAction("No inputs specified"));
    }
  };
};

const createInviteEmployeesStartedAction = () => ({
  type: INVITE_EMPLOYEES_STARTED,
});

const createInviteEmployeesSucceededAction = () => ({
  type: INVITE_EMPLOYEES_SUCCEEDED,
});

const createInviteEmployeesFailedAction = error => ({
  type: INVITE_EMPLOYEES_FAILED,
  payload: {
    error: error,
  },
});