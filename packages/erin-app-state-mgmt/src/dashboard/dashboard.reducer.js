import { state as initialState } from './dashboard.state';
import * as Actions from './dashboard.actions';

export const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.INVITE_EMPLOYEES_STARTED:
      return {
        ...state,
        loading: true,
        error: null
      };
    case Actions.INVITE_EMPLOYEES_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null
      };
    case Actions.INVITE_EMPLOYEES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    default:
      return state;
  }
};
