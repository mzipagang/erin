import store from './src/store';

import {
  employeeDetailsActions,
  editUserProfileActions,
  dashboardActions,
  manageJobsActions,
  browseJobsActions,
  userActions,
  myReferralsActions,
  notificationsActions,
  referralActions,
  settingsActions,
  navigationActions,
} from './src/actions';

const actions = {
  editUserProfileActions,
  dashboardActions,
  manageJobsActions,
  browseJobsActions,
  userActions,
  referralActions,
  employeeDetailsActions,
  myReferralsActions,
  notificationsActions,
  settingsActions,
  navigationActions,
};

export { actions };

export default store;
