const Path = require('path');

module.exports = {
  getProjectRoots: () => [
    __dirname,
    Path.join(__dirname, '../erin-app-state-mgmt'),
    Path.join(__dirname, '../erin-shared-resources'),
    Path.join(__dirname, '../../test'),
  ],
  getSourceExts: () => ['jsx', 'js'],
};
