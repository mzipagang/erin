import { StyleSheet } from 'react-native';
import { COLORS } from '../_shared/styles/colors';

export default StyleSheet.create({
  tabbar: {
    backgroundColor: COLORS.darkBlue,
  },
  dashboard: {
    marginLeft: '25%',
    marginRight: '25%',
    width: 110,
  },
  company: {
    textAlign: 'center',
    color: COLORS.lightGray,
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    letterSpacing: 1,
  },

  rightbutton: {
    height: 40,
    marginRight: 20,
    marginBottom: 5,
  },
  image: {
    width: 40,
    height: 40,
  },
  leftbutton: {
    color: COLORS.black,
    fontSize: 30,
  },
});
