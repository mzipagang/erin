import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { COLORS } from '../_shared/styles/colors';

export class TabIcon extends React.Component {
  render() {
    return (
      <View>
        <Image
          style={[
            styles.IconStyle,
            { tintColor: this.props.focused ? COLORS.white : COLORS.lightGray },
          ]}
          source={this.props.iconName}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  IconStyle: {
    width: 25,
    height: 25,
  },
});
