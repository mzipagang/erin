import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Router, Scene, Drawer } from 'react-native-router-flux';
import Aside from '../scenes/side-navigation/aside';
import Dashboard from '../scenes/dashboard/dashboard';
import Contacts from '../scenes/my-network/contacts';
import Jobs from '../scenes/jobs';
import Referrals from '../scenes/referrals';
import Alerts from '../scenes/alerts';
import { COLORS } from '../_shared/styles/colors';
import { TabIcon } from './tab-icon.component';
import styles from './router.component.style';

const Routes = () => {
  return (
    <Router>
      <Scene key="root">
        <Drawer
          key="aside"
          contentComponent={Aside}
          drawerWidth={250}
          hideNavBar
        >
          <Scene
            key="tabbar"
            tabBarStyle={styles.tabbar}
            activeTintColor={COLORS.white}
            tabs
            initial
          >
            <Scene
              title="Dashboard"
              icon={TabIcon}
              iconName={require('../_shared/assets/dashboard.png')}
              renderTitle={() => (
                <View style={styles.dashboard}>
                  <TouchableOpacity>
                    <Text style={styles.company}>BestCo</Text>
                    <Text style={styles.title}>Dashboard </Text>
                  </TouchableOpacity>
                </View>
              )}
              renderRightButton={() => (
                <View style={styles.rightbutton}>
                  <TouchableOpacity>
                    <Image
                      style={styles.image}
                      source={require('../_shared/assets/person-icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
              leftTitle="&#9776;"
              leftButtonTextStyle={styles.leftbutton}
              component={Dashboard}
            />
            <Scene
              key="jobs"
              title="Jobs"
              icon={TabIcon}
              iconName={require('../_shared/assets/id.png')}
              renderTitle={() => (
                <View style={styles.dashboard}>
                  <TouchableOpacity>
                    <Text style={styles.company}>BestCo</Text>
                    <Text style={styles.title}>Jobs </Text>
                  </TouchableOpacity>
                </View>
              )}
              renderRightButton={() => (
                <View style={styles.rightbutton}>
                  <TouchableOpacity>
                    <Image
                      style={styles.image}
                      source={require('../_shared/assets/person-icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
              leftTitle="&#9776;"
              leftButtonTextStyle={styles.leftbutton}
              component={Jobs}
            />
            <Scene
              title="Referrals"
              icon={TabIcon}
              iconName={require('../_shared/assets/icongroup.png')}
              renderTitle={() => (
                <View style={styles.dashboard}>
                  <TouchableOpacity>
                    <Text style={styles.company}>BestCo</Text>
                    <Text style={styles.title}>Referrals </Text>
                  </TouchableOpacity>
                </View>
              )}
              renderRightButton={() => (
                <View style={styles.rightbutton}>
                  <TouchableOpacity>
                    <Image
                      style={styles.image}
                      source={require('../_shared/assets/person-icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              )}
              leftTitle="&#9776;"
              leftButtonTextStyle={styles.leftbutton}
              component={Referrals}
            />
            <Scene
              tabs={false}
              title="Alerts"
              icon={TabIcon}
              iconName={require('../_shared/assets/ring.png')}
            >
              <Scene
                title="Alerts"
                icon={TabIcon}
                iconName={require('../_shared/assets/ring.png')}
                renderTitle={() => (
                  <View style={styles.dashboard}>
                    <TouchableOpacity>
                      <Text style={styles.company}>BestCo</Text>
                      <Text style={styles.title}>Alerts </Text>
                    </TouchableOpacity>
                  </View>
                )}
                renderRightButton={() => (
                  <View style={styles.rightbutton}>
                    <TouchableOpacity>
                      <Image
                        style={styles.image}
                        source={require('../_shared/assets/person-icon.png')}
                      />
                    </TouchableOpacity>
                  </View>
                )}
                leftTitle="&#9776;"
                leftButtonTextStyle={styles.leftbutton}
                component={Alerts}
              />
              <Scene
                key="contacts"
                component={Contacts}
                title="My Network"
                leftTitle="&#9776;"
                leftButtonTextStyle={styles.leftbutton}
              />
            </Scene>
          </Scene>
        </Drawer>
      </Scene>
    </Router>
  );
};
export default Routes;
