import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert } from 'react-native';
import { COLORS } from '../../styles/colors';
import AppIcon from '../app-icon.component';
import styles from './refer.component.style';

class ReferSomeone extends Component {
  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}
        >
          <View style={styles.container}>
            <TouchableOpacity
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            >
              <Text style={styles.close}>X</Text>
            </TouchableOpacity>
            <View>
              <Text>Refer Someone screen</Text>
            </View>
          </View>
        </Modal>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <AppIcon name="user" color={COLORS.white} />
          <Text style={styles.buttontext}>Refer Someone</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ReferSomeone;
