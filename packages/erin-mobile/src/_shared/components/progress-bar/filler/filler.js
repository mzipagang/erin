import React from 'react';
import { View } from 'react-native';
import { COLORS } from '../../../styles/colors';

const Filler = props => {
  const percentage = `${props.percentage}%`;
  return (
    <View
      style={{
        width: percentage,
        backgroundColor:
          percentage >= '50' ? COLORS.dashboardGreen : COLORS.red,
        height: '100%',
        borderRadius: 50,
      }}
    />
  );
};
export default Filler;
