import { StyleSheet } from 'react-native';
import { COLORS } from '../../styles/colors';

export default StyleSheet.create({
  tile: {
    width: '90%',
    marginTop: 10,
    marginLeft: 15,
    backgroundColor: COLORS.white,
    shadowColor: COLORS.lightGray,
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    padding: 15,
  },
});
