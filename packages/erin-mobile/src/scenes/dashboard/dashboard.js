import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { ReferralCard } from './tiles/referral-card/referral-card';
import { RecommendedReferralCard } from './tiles/recommended-referral/recommended';
import { EmployeeRankCard } from './tiles/employee-ranking/employee-ranking';
import { JobCard } from './tiles/job-card/jobcard';
import { ConnectCard } from './tiles/connect-earn/connect';
import { ContactsCard } from './tiles/connect-contacts/contacts';

export default class Dashboard extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          <JobCard title="OPEN JOBS" />
          <ConnectCard title="CONNECT & EARN MORE" />
          <RecommendedReferralCard title="RECOMMENDED REFERRAL" />
          <ReferralCard title="REFERRALS MADE" />
          <EmployeeRankCard title="YOUR RANKING" />
          <ContactsCard title="CONNECT CONTACTS" />
        </View>
      </ScrollView>
    );
  }
}
