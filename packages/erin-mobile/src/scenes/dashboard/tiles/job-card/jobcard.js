import React from 'react';
import { View, Text } from 'react-native';
import { borderedTile } from '../../../../_shared/components/bordered-tile/bordered-tile.component';
import styles from './jobcard.component.style';

class BaseRefCard extends React.Component {
  render() {
    const { title } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.row}>
          <View style={styles.leftcontent}>
            <Text style={styles.numopen}>4</Text>
            <Text style={styles.jobs}>Open Positions</Text>
          </View>
          <View style={styles.rightcontent}>
            <Text style={styles.numreferrals}>19</Text>
            <Text style={styles.recommended}>Referrals Recommended</Text>
          </View>
        </View>
      </View>
    );
  }
}
export const JobCard = borderedTile(BaseRefCard);
