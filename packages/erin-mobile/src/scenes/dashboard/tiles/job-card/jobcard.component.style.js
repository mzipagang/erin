import { StyleSheet } from 'react-native';
import { COLORS } from '../../../../_shared/styles/colors';

export default StyleSheet.create({
  container: {
    height: 100,
  },
  title: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  leftcontent: {
    width: '50%',
    height: 73,
    paddingTop: 3,
    alignItems: 'center',
  },
  rightcontent: {
    width: '50%',
    height: 73,
    paddingTop: 3,
    alignItems: 'center',
    backgroundColor: COLORS.dashboardLightOrange,
  },
  numopen: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  jobs: {
    color: COLORS.lightGray,
    textAlign: 'center',
  },
  numreferrals: {
    fontSize: 25,
    fontWeight: 'bold',
    color: COLORS.dashboardDarkOrange,
  },
  recommended: {
    color: COLORS.white,
    textAlign: 'center',
  },
});
