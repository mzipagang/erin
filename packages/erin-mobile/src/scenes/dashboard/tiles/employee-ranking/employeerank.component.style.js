import { StyleSheet } from 'react-native';
import { COLORS } from '../../../../_shared/styles/colors';

export default StyleSheet.create({
  container: {
    height: 100,
  },
  title: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  content: {
    width: '50%',
    height: 50,
    alignItems: 'center',
  },
  rank: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  employees: {
    color: COLORS.lightGray,
  },
  percent: {
    flexDirection: 'row',
  },
});
