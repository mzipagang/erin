import React from 'react';
import { View, Text } from 'react-native';
import { borderedTile } from '../../../../_shared/components/bordered-tile/bordered-tile.component';
import ProgressBar from '../../../../_shared/components/progress-bar/progressbar';
import AppIcon from '../../../../_shared/components/app-icon.component';
import { COLORS } from '../../../../_shared/styles/colors';
import styles from './employeerank.component.style';

class BaseRefCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { percentage: 50 };
  }
  getReferralStatus() {
    if (this.state.percentage >= 50) {
      return 'up-arrow';
    }
    if (this.state.percentage < 50) {
      return 'download-arrow';
    }
    return '';
  }
  render() {
    const { title } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.row}>
          <View style={styles.content}>
            <Text style={styles.rank}># 5</Text>
            <Text style={styles.employees}>Of All Employees</Text>
          </View>
          <View style={styles.content}>
            <View
              style={{
                backgroundColor:
                  this.state.percentage >= 50
                    ? COLORS.dashboardGreen
                    : COLORS.red,
                padding: 15,
              }}
            >
              <AppIcon
                name={this.getReferralStatus()}
                color={COLORS.black}
                size={15}
              />
            </View>
          </View>
        </View>
        <View style={styles.percent}>
          <ProgressBar percentage={this.state.percentage} />
          <Text
            style={{
              color:
                this.state.percentage >= 50
                  ? COLORS.dashboardGreen
                  : COLORS.red,
              fontWeight: 'bold',
            }}
          >
            {this.state.percentage}%
          </Text>
        </View>
      </View>
    );
  }
}
export const EmployeeRankCard = borderedTile(BaseRefCard);
