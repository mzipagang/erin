import { StyleSheet } from 'react-native';
import { COLORS } from '../../../../_shared/styles/colors';

export default StyleSheet.create({
  container: {
    height: 135,
  },
  title: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  jobtitle: {
    color: COLORS.blue,
    letterSpacing: 1,
  },
  department: {
    flexDirection: 'row',
    marginRight: 10,
    color: COLORS.darkGray,
  },
  deptext: {
    fontSize: 13,
    marginLeft: 3,
  },
  network: {
    color: COLORS.blue,
    fontSize: 11,
    marginLeft: 5,
  },
  match: {
    color: COLORS.lightGray,
    fontSize: 11,
    marginLeft: 2,
  },
  location: {
    width: 15,
    height: 15,
    tintColor: COLORS.lightGray,
  },
  tick: {
    width: 15,
    height: 15,
    tintColor: COLORS.green,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  amount: {
    alignItems: 'center',
    marginBottom: 15,
  },
  total: {
    fontSize: 25,
    fontWeight: 'bold',
    color: COLORS.green,
  },
  referral: {
    flexDirection: 'row',
    marginLeft: 10,
  },
});
