import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AppIcon from '../../../../_shared/components/app-icon.component';
import { borderedTile } from '../../../../_shared/components/bordered-tile/bordered-tile.component';
import { COLORS } from '../../../../_shared/styles/colors';
import styles from './recommended.component.style';

class BaseRefCard extends React.Component {
  render() {
    const { title } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity onPress={() => Actions.jobs()}>
          <Text style={styles.jobtitle}>Sales Director</Text>
        </TouchableOpacity>
        <View style={styles.row}>
          <View style={styles.department}>
            <AppIcon name="folder" color={COLORS.darkGray} />
            <Text style={styles.deptext}>Sales</Text>
          </View>
          <View style={styles.department}>
            <Image
              style={styles.location}
              source={require('../../../../_shared/assets/location.png')}
            />
            <Text style={styles.deptext}>New York, NY</Text>
          </View>
        </View>
        <View style={styles.amount}>
          <Text style={styles.total}>$2,500</Text>
        </View>
        <View style={styles.referral}>
          <Image
            style={styles.tick}
            source={require('../../../../_shared/assets/tick-inside-circle.png')}
          />
          <TouchableOpacity onPress={() => Actions.contacts()}>
            <Text style={styles.network}>3 People</Text>
          </TouchableOpacity>
          <Text style={styles.match}>In Your Network Match This Job!</Text>
        </View>
      </View>
    );
  }
}
export const RecommendedReferralCard = borderedTile(BaseRefCard);
