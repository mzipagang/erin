import React from 'react';
import { View, Text } from 'react-native';
import { borderedTile } from '../../../../_shared/components/bordered-tile/bordered-tile.component';
import styles from './referralcard.component.style';

class BaseRefCard extends React.Component {
  render() {
    const { title, referralAcceptedTotal } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.row}>
          <Text style={styles.total}>1{referralAcceptedTotal}</Text>
          <Text style={styles.referral}>Made This Year</Text>
        </View>
      </View>
    );
  }
}
export const ReferralCard = borderedTile(BaseRefCard);
