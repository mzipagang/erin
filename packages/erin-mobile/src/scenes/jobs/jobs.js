import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { JobCard } from './job-card/jobcard';
import { COLORS } from '../../_shared/styles/colors';

export default class Jobs extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View>
              <Text style={{ marginTop: 20, marginLeft: 15, fontSize: 15 }}>
                Open Jobs
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                marginTop: 15,
                marginRight: 22,
              }}
            >
              <TouchableOpacity>
                <Text
                  style={{
                    padding: 5,
                    backgroundColor: COLORS.red,
                    color: COLORS.white,
                    marginRight: 5,
                  }}
                >
                  Open
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text
                  style={{
                    padding: 5,
                    borderWidth: 0.5,
                    borderColor: COLORS.lightGray,
                  }}
                >
                  Closed
                </Text>
              </TouchableOpacity>
            </View>
            <View />
          </View>
          <JobCard />
        </View>
      </ScrollView>
    );
  }
}
