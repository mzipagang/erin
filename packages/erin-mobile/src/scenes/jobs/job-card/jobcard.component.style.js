import { StyleSheet } from 'react-native';
import { COLORS } from '../../../_shared/styles/colors';

export default StyleSheet.create({
  container: {
    height: 165,
  },
  title: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  jobtitle: {
    color: COLORS.blue,
    letterSpacing: 1,
  },
  department: {
    flexDirection: 'row',
    marginRight: 10,
    color: COLORS.darkGray,
  },
  deptext: {
    fontSize: 13,
    marginLeft: 3,
  },
  network: {
    color: COLORS.blue,
    fontSize: 11,
    marginLeft: 5,
  },
  match: {
    color: COLORS.lightGray,
    fontSize: 11,
    marginLeft: 2,
  },
  location: {
    width: 15,
    height: 15,
    tintColor: COLORS.lightGray,
  },
  tick: {
    width: 15,
    height: 15,
    tintColor: COLORS.green,
    marginLeft: 20,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 15,
  },
  amount: {
    marginBottom: 15,
    marginLeft: 15,
    height: 50,
    overflow: 'hidden',
  },
  total: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLORS.green,
  },
  referral: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
  },
  button: {
    flexDirection: 'row',
    backgroundColor: COLORS.blue,
    justifyContent: 'center',
    paddingLeft: 22,
    paddingRight: 22,
    paddingTop: 7,
    paddingBottom: 7,
  },
  buttontext: {
    fontSize: 11,
    color: COLORS.white,
    marginLeft: 5,
  },
});
