import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AppIcon from '../../../_shared/components/app-icon.component';
import { borderedTile } from '../../../_shared/components/bordered-tile/bordered-tile';
import ReferSomeone from '../../../_shared/components/refer-someone/refer';
import { COLORS } from '../../../_shared/styles/colors';
import styles from './jobcard.component.style';
import { Share } from 'react-native';

class BaseRefCard extends React.Component {
  onShare() {
    Share.share(
      {
        message: 'Share this Job',
        url: 'http://go.erinapp.com',
        title: 'Share This Job',
      },
      {
        // Android only:
        dialogTitle: 'Share This Job',
        // iOS only:
        excludedActivityTypes: ['com.apple.UIKit.activity.PostToTwitter'],
      }
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <View>
            <Text
              style={{
                marginLeft: 15,
                color: COLORS.blue,
                fontSize: 15,
                fontWeight: 'bold',
              }}
            >
              Sales Director
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}
          >
            <Text style={{ fontSize: 15, color: COLORS.green }}>$2,500</Text>
          </View>
          <View />
        </View>
        <View style={styles.row}>
          <View style={styles.department}>
            <AppIcon name="folder" color={COLORS.darkGray} />
            <Text style={styles.deptext}>Sales</Text>
          </View>
          <View style={styles.department}>
            <Image
              style={styles.location}
              source={require('../../../_shared/assets/location.png')}
            />
            <Text style={styles.deptext}>New York, NY</Text>
          </View>
        </View>
        <View style={styles.amount}>
          <Text
            numberOfLines={3}
            style={{ fontSize: 13, color: COLORS.lightGray }}
          >
            text text text text text text text text text text text text text
            text text text text text text text text text text text text text
            text text text text text text text text text text text text text
            text text text
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
          }}
        >
          <ReferSomeone />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.onShare();
            }}
          >
            <AppIcon name="network" color={COLORS.white} />
            <Text style={styles.buttontext}>Share & Send</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.referral}>
          <Image
            style={styles.tick}
            source={require('../../../_shared/assets/tick-inside-circle.png')}
          />
          <TouchableOpacity onPress={() => Actions.contacts()}>
            <Text style={styles.network}>3 People</Text>
          </TouchableOpacity>
          <Text style={styles.match}>In Your Network Match This Job!</Text>
        </View>
      </View>
    );
  }
}
export const JobCard = borderedTile(BaseRefCard);
