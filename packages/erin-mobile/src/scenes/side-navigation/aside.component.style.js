import { StyleSheet } from 'react-native';
import { COLORS } from '../../_shared/styles/colors';

export default StyleSheet.create({
  sidenav: {
    flex: 1,
  },
  logo: {
    height: 60,
    width: 200,
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 20,
  },
  item: {
    padding: 5,
    marginTop: 10,
    marginLeft: 25,
    fontSize: 18,
    color: COLORS.black,
  },

  navfooter: {
    marginLeft: 25,
  },

  navlink: {
    color: COLORS.blue,
    marginLeft: 25,
  },

  navheight: {
    height: 70,
  },
  list: {
    fontSize: 18,
    lineHeight: 40,
    marginLeft: 30,
  },
});
