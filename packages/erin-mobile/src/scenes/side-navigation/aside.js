import React from 'react';
import { View, Text, Image, TouchableOpacity, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './aside.component.style';

const Aside = () => {
  return (
    <View style={styles.sidenav}>
      <View style={styles.sidenav}>
        <Image
          source={require('../../_shared/assets/erinlogo.png')}
          style={styles.logo}
        />
        <View>
          <Text style={styles.list}>Profile</Text>
          <TouchableOpacity onPress={() => Actions.contacts()}>
            <Text style={styles.list}>Contacts</Text>
          </TouchableOpacity>
          <Text style={styles.list}>Support</Text>
          <Text style={styles.list}>Sign Out</Text>
        </View>
      </View>
      <View style={{ height: 70 }}>
        <Text style={styles.navfooter}>Get the full experience online at</Text>
        <Text
          style={styles.navlink}
          onPress={() => Linking.openURL('http://go.erinapp.com')}
        >
          go.erinapp.com
        </Text>
      </View>
    </View>
  );
};
export default Aside;
