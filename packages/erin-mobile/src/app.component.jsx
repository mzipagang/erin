import React, { Component } from 'react';
import Routes from './router/router';

export default class App extends Component {
  render() {
    return <Routes />;
  }
}
