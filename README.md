# erin

[![Build Status](https://travis-ci.com/ParsedLLC/erin-app.svg?token=dQmAtUifupGu6H8uRyDT&branch=master)](https://travis-ci.com/ParsedLLC/erin-app)

[DEMO site](https://suspicious-curran-2d9c27.netlify.com)

[Storybook site](https://parsedllc.github.io/erin-app)

[Invision App Designs - Web](https://projects.invisionapp.com/share/HYN667EX89P#/screens)

[Invision App Designs - Mobile](https://projects.invisionapp.com/share/N9NBOPIV2DX#/screens/312355654_Home)

[UI style guide](./docs/style-guide.pdf)

[File naming and folder structure style guide](https://github.com/ParsedLLC/parsed-engineering-wiki/blob/master/style-guides/horizontal-slicing.md#horizontal-slicing-style-guide)

## Project Setup

Run `yarn` from the command line in the project root folder.

## Running Mobile

Run `yarn mobile` in one terminal and then in another terminal run either `yarn run-android` or `yarn run-ios`.

## Yarn Workspaces

This mono repo uses [Yarn Workspaces](https://yarnpkg.com/lang/en/docs/workspaces/), therefore, adding packages is done a little differently than just `npm install some-library` or `yarn add some-library`.

The repo is organized into a `packages` folder which is in the project root. Each top level subdirecty of `packages` is a standalone project. Each project can reference each other by adding it to it's respective node_modules cache. This should be done by running the CLI command:

> `yarn workspace <destination-package> add <source-package>`.

For instance, to add the `erin-app-state-mgmt` package to `erin-web`, you would run:

> `yarn workspace erin-web add erin-app-state-mgmt@1.0.0`.

Note that for packages included in this mono repo, you must specify the version number that is found in the respective `package.json` of the package or else yarn will try to install the module from the public npm registry.

For public packages, you do not need to specify the version (which will install the latest version publicly available at that time).

Example:

> `yarn workspace erin-web add lodash`
